# Routes
# This file defines all application routes (Higher priority routes first)
# ~~~~

# MainController
GET   /info                                                                                @controllers.MainController.info()
GET   /                                                                                    @controllers.MainController.index()
GET   /javascriptRoutes                                                                    @controllers.MainController.javascriptRoutes()

# SessionHandlingController
GET   /login_redirect/:redirect                                                            @controllers.SessionHandlingController.loginRedirect(redirect: String)
GET   /login                                                                               @controllers.SessionHandlingController.login()
GET   /login/callback                                                                      @controllers.SessionHandlingController.callback(code: String)
GET   /logout                                                                              @controllers.SessionHandlingController.logout()

# GitHubController
GET   /GitHubController/:owner/:repository/pulls/:pullRequestNumber/files                  @controllers.GitHubController.getChangedFiles(owner: String, repository: String, pullRequestNumber: Integer)
GET   /GitHubController/getFileContent/*contentUrl                                         @controllers.GitHubController.getFileContent(contentUrl: String)
GET   /GitHubController/:owner/:name/diffs/:pullRequestNumber/:filename                    @controllers.GitHubController.getDiffs(owner: String, name: String, pullRequestNumber: Integer, filename: String)
GET   /GitHubController/:owner/:name/diffLines/:pullRequestNumber                          @controllers.GitHubController.getDiffLines(owner: String, name: String, pullRequestNumber: String)
GET   /GitHubController/:owner/:name/readme                                                @controllers.GitHubController.getReadme(owner: String, name: String)
GET   /GitHubController/:owner/:name/mavenproject                                          @controllers.GitHubController.isMavenProject(owner: String, name: String)
GET   /GitHubController/:owner/:name/merge/:pulls                                          @controllers.GitHubController.merge(owner: String, name: String, pulls: String)

# UserController
GET   /user/profile                                                                        @controllers.UserController.profile()
GET   /user/favorites                                                                      @controllers.UserController.favorites()
GET   /user/favorites/add/:repoOwner/:repoName                                             @controllers.UserController.addFavorite(repoOwner: String, repoName: String)
GET   /user/favorites/delete/:repoOwner/:repoName                                          @controllers.UserController.deleteFavorite(repoOwner: String, repoName: String)
GET   /users/:username                                                                     @controllers.UserController.user(username: String)

# SearchController
GET   /search/:searchTerm                                                                  @controllers.SearchController.search(searchTerm: String)

# ReposController
GET   /repos/:repoOwner/:repoName                                                          @controllers.ReposController.repo(repoOwner: String, repoName: String)
GET   /repos/:repoOwner/:repoName/branches                                                 @controllers.ReposController.branches(repoOwner: String, repoName: String)
GET   /repos/:repoOwner/:repoName/branches/:branchName                                     @controllers.ReposController.branch(repoOwner: String, repoName: String, branchName: String)
GET   /repos/:repoOwner/:repoName/pulls                                                    @controllers.ReposController.pulls(repoOwner: String, repoName: String)
GET   /repos/:repoOwner/:repoName/pulls/:pullId                                            @controllers.ReposController.pull(repoOwner: String, repoName: String, pullId: Long)
GET   /repos/:repoOwner/:repoName/update                                                   @controllers.ReposController.update(repoOwner: String, repoName: String)


# MergeAnalysisController
GET   /repos/:repoOwner/:repoName/analyses                                                 @controllers.MergeAnalysisController.analyses(repoOwner: String, repoName: String)
GET   /repos/:repoOwner/:repoName/analyses/:analysisId                                     @controllers.MergeAnalysisController.analysis(repoOwner: String, repoName: String, analysisId: Long)
GET   /repos/:repoOwner/:repoName/analyses/create/:analysisName/:pulls                     @controllers.MergeAnalysisController.create(repoOwner: String, repoName: String, analysisName: String, pulls: String)
GET   /repos/:repoOwner/:repoName/analyses/:analysisId/start                               @controllers.MergeAnalysisController.start(repoOwner: String, repoName: String, analysisId: Long)
GET   /repos/:repoOwner/:repoName/analyses/:analysisId/status                              @controllers.MergeAnalysisController.status(repoOwner: String, repoName: String, analysisId: Long)
GET   /repos/:repoOwner/:repoName/analyses/:analysisId/stop                                @controllers.MergeAnalysisController.stop(repoOwner: String, repoName: String, analysisId: Long)
GET   /repos/:repoOwner/:repoName/analyses/:analysisId/delete                      		   @controllers.MergeAnalysisController.delete(repoOwner: String, repoName: String, analysisId: Long)
GET   /repos/:repoOwner/:repoName/analyses/:analysisId/running                             @controllers.MergeAnalysisController.running(repoOwner: String, repoName: String, analysisId: Long)
GET   /repos/:repoOwner/:repoName/analyses/:analysisId/last                                @controllers.MergeAnalysisController.last(repoOwner: String, repoName: String, analysisId: Long)
GET   /repos/:repoOwner/:repoName/analyses/:analysisId/terminated                          @controllers.MergeAnalysisController.lastTerminated(repoOwner: String, repoName: String, analysisId: Long)

# MergeAnalysisExecutionController
GET   /repos/:repoOwner/:repoName/analyses/:analysisId/execs                               @controllers.MergeAnalysisExecutionController.execs(repoOwner: String, repoName: String, analysisId: Long)
GET   /repos/:repoOwner/:repoName/analyses/:analysisId/execs/:execId                       @controllers.MergeAnalysisExecutionController.exec(repoOwner: String, repoName: String, analysisId: Long, execId: Long)
GET   /repos/:repoOwner/:repoName/analyses/:analysisId/execs/:execId/result                @controllers.MergeAnalysisExecutionController.result(repoOwner: String, repoName: String, analysisId: Long, execId: Long)
GET   /repos/:repoOwner/:repoName/analyses/:analysisId/execs/:execId/result/base           @controllers.MergeAnalysisExecutionController.baseResult(repoOwner: String, repoName: String, analysisId: Long, execId: Long)
GET   /repos/:repoOwner/:repoName/analyses/:analysisId/execs/:execId/result/pull/:pullId   @controllers.MergeAnalysisExecutionController.pullResult(repoOwner: String, repoName: String, analysisId: Long, execId: Long, pullId: Long)

# ServerSentEventsController: broadcast messages
GET   /SSE/:repoOwner/:repoName                                                            controllers.ServerSentEventsController.SSE(repoOwner: String, repoName: String)
GET   /SSE/postMessage                                                                     controllers.ServerSentEventsController.postMessage()

# Assets: Map static resources from the /public folder to the /assets URL path
GET   /assets/*file                                                                        controllers.Assets.at(path="/public", file)

# TestController: the following routes are only used for testing purposes
GET   /test/auth                                                                           @controllers.TestController.auth()
GET   /test/post                                                                           @controllers.TestController.post()
