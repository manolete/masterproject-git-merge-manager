package services.platforms;

import static org.fest.assertions.Assertions.assertThat;

import org.junit.Test;

import services.platforms.ReferencedIssueParser;

public class ReferencedIssueParserTest {

  private static final String REPO_OWNER = "repoOwner";
  private static final String REPO_NAME = "repoName";
  private static final String NULL = null;

  @Test
  public void testValidCasesWithResolve() {
    assertIssueNumberFound("Resolve #23.", "23");
    assertIssueNumberFound("Resolves #2.", "2");
    assertIssueNumberFound("Resolved #111.", "111");
    assertIssueNumberFound("Resolved https://www.github.com/" + REPO_OWNER + "/" + REPO_NAME + "/23.", "23");
    assertIssueNumberFound("Resolved https://www.github.com/" + REPO_OWNER + "/" + REPO_NAME + "/111.", "111");
  }

  @Test
  public void testValidCasesWithFix() {
    assertIssueNumberFound("Fix #23.", "23");
    assertIssueNumberFound("Fixes #2.", "2");
    assertIssueNumberFound("Fixed #111.", "111");
    assertIssueNumberFound("Fix https://www.github.com/" + REPO_OWNER + "/" + REPO_NAME + "/23.", "23");
    assertIssueNumberFound("Fix https://www.github.com/" + REPO_OWNER + "/" + REPO_NAME + "/111.", "111");
  }

  @Test
  public void testValidCasesWithClose() {
    assertIssueNumberFound("Close #23.", "23");
    assertIssueNumberFound("Closes #2.", "2");
    assertIssueNumberFound("Closed #111.", "111");
    assertIssueNumberFound("Close https://www.github.com/" + REPO_OWNER + "/" + REPO_NAME + "/23.", "23");
    assertIssueNumberFound("Close https://www.github.com/" + REPO_OWNER + "/" + REPO_NAME + "/111.", "111");
  }

  @Test
  public void testValidCasesWithoutRepoInfo() {
    assertIssueNumberFoundWithoutRepoInfo("Resolve #23.", "23");
  }

  @Test
  public void testInvalidCases() {
    assertIssueNumberFound("Something #23.", NULL);
    assertIssueNumberFound("Resolves 23.", NULL);
    assertIssueNumberFound("Resolves #.", NULL);
    assertIssueNumberFound("Resolves something #23.", NULL);
  }

  @Test
  public void testNullDescription() {
    assertIssueNumberFound(NULL, NULL);
  }

  private void assertIssueNumberFound(final String description, final String expectedIssueNr) {
    assertIssueNumberFound(description, REPO_OWNER, REPO_NAME, expectedIssueNr);
  }

  private void assertIssueNumberFoundWithoutRepoInfo(final String description, final String expectedIssueNr) {
    assertIssueNumberFound(description, NULL, NULL, expectedIssueNr);
  }

  private void assertIssueNumberFound(final String description, final String repoOwner, final String repoName, final String expectedIssueNr) {
    assertThat(new ReferencedIssueParser().parse(description, repoOwner, repoName)).isEqualTo(expectedIssueNr);
  }

}
