package services.data;

import static org.fest.assertions.Assertions.assertThat;

import java.util.ArrayList;
import java.util.List;

import model.dto.BranchDto;
import model.dto.PullRequestDto;
import model.dto.RepositoryDto;
import model.dto.UserDto;
import model.factories.RepositoryFactory;
import model.factories.UserFactory;
import model.repositories.RepositoryRepository;
import model.repositories.UserRepository;

import org.junit.Test;

import services.data.ModelEntityService;
import services.platforms.GithubService;
import tests.base.AbstractFakeDatabaseTest;

import com.google.inject.AbstractModule;
import com.google.inject.Module;

public class ModelEntityServiceImplTest extends AbstractFakeDatabaseTest {

  private static final String JOHN_DOE = "john_doe";
  private static final String HELLO_WORLD = "hello_world";
  private static final String CLONE_URL = "http://api.platform.com/owner/repo";
  private static final long ONE = 1;

  @Override
  public Module getOverrideModule() {
    return new AbstractModule() {
      @Override
      protected void configure() {
        bind(GithubService.class).to(DtoPlatformServiceMock.class);
      }
    };
  }

  @Test
  public void testGetUser() {
    final ModelEntityService modelProvider = injector.getInstance(ModelEntityService.class);
    final UserRepository userRepository = injector.getInstance(UserRepository.class);

    userRepository.store(UserFactory.INSTANCE.create(ONE, JOHN_DOE));

    assertThat(modelProvider.getUser(JOHN_DOE).getUsername()).isEqualTo(JOHN_DOE);
  }

  @Test
  public void testGetUpdatedUser() {
    final ModelEntityService modelProvider = injector.getInstance(ModelEntityService.class);

    assertThat(modelProvider.getUpdatedUser(JOHN_DOE).getUsername()).isEqualTo(JOHN_DOE);
  }

  @Test
  public void testGetRepository() {
    final ModelEntityService modelProvider = injector.getInstance(ModelEntityService.class);
    final UserRepository userRepository = injector.getInstance(UserRepository.class);
    final RepositoryRepository repositoryRepository = injector.getInstance(RepositoryRepository.class);

    userRepository.store(UserFactory.INSTANCE.create(ONE, JOHN_DOE));
    repositoryRepository.store(RepositoryFactory.INSTANCE.create(ONE, userRepository.one(ONE), HELLO_WORLD, CLONE_URL));

    assertThat(modelProvider.getRepository(JOHN_DOE, HELLO_WORLD).getName()).isEqualTo(HELLO_WORLD);
  }

  @Test
  public void testGetUpdatedRepository() {
    final ModelEntityService modelProvider = injector.getInstance(ModelEntityService.class);

    assertThat(modelProvider.getRepository(JOHN_DOE, HELLO_WORLD).getName()).isEqualTo(HELLO_WORLD);
  }

  public static class DtoPlatformServiceMock implements GithubService {

    @Override
    public UserDto getUser() {
      return null;
    }

    @Override
    public UserDto getUser(final String username) {
      final UserDto userDto = new UserDto();
      userDto.id = ONE;
      userDto.username = username;
      return userDto;
    }

    @Override
    public List<RepositoryDto> searchRepositoriesByName(final String searchTerm) {
      return null;
    }

    @Override
    public RepositoryDto getRepository(final String owner, final String name) {
      final RepositoryDto repositoryDto = new RepositoryDto();
      repositoryDto.id = ONE;
      repositoryDto.owner = owner;
      repositoryDto.name = name;
      return repositoryDto;
    }

    @Override
    public List<BranchDto> getBranches(final String repositoryOwner, final String repositoryName) {
      final List<BranchDto> branchDtos = new ArrayList<BranchDto>();
      return branchDtos;
    }

    @Override
    public List<PullRequestDto> getPullRequests(final String repositoryOwner, final String repositoryName) {
      final List<PullRequestDto> pullRequestDtos = new ArrayList<PullRequestDto>();
      return pullRequestDtos;
    }

    @Override
    public PullRequestDto getPullRequest(final String repositoryOwner, final String repositoryName, final int pullRequestNumber) {
      return null;
    }

  }

}
