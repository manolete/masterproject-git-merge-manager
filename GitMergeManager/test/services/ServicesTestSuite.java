package services;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import services.data.ModelEntityServiceImplTest;
import services.platforms.ReferencedIssueParserTest;

@RunWith(Suite.class)
@SuiteClasses({//@formatter:off
  ModelEntityServiceImplTest.class,
  ReferencedIssueParserTest.class,
})//@formatter:on
public class ServicesTestSuite {}
