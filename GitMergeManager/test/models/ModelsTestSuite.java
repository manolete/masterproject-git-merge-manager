package models;

import models.repositories.impl.RepositoryRepositoryImplTest;
import models.repositories.impl.UserRepositoryImplTest;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({//@formatter:off
  UserRepositoryImplTest.class,
  RepositoryRepositoryImplTest.class,
})//@formatter:on
public class ModelsTestSuite {

}
