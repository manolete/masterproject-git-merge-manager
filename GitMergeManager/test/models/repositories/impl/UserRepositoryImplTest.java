package models.repositories.impl;

import static org.fest.assertions.Assertions.assertThat;

import javax.persistence.PersistenceException;

import model.core.User;

import org.junit.Test;

import tests.base.models.AbstractBaseRepositoryTest;

public class UserRepositoryImplTest extends AbstractBaseRepositoryTest {

  private static final String JOHN_SMITH = "john_smith";

  @Test
  public void testStore() {
    assertThat(userRepository.all()).isEmpty();
    userRepository.store(userFactory.create(ONE, JOHN_DOE));
    assertThat(userRepository.all()).hasSize(1);
  }

  @Test
  public void testDelete() {
    final User user = userFactory.create(ONE, JOHN_DOE);
    userRepository.store(user);
    assertThat(userRepository.all()).hasSize(1);
    userRepository.delete(user.getId());
    assertThat(userRepository.all()).isEmpty();
  }

  @Test
  public void testAll() {
    assertThat(userRepository.all()).isEmpty();
    userRepository.store(userFactory.create(ONE, JOHN_DOE));
    userRepository.store(userFactory.create(TWO, JOHN_SMITH));
    assertThat(userRepository.all()).hasSize(2);
  }

  @Test
  public void testOneById() {
    assertThat(userRepository.one(JOHN_DOE)).isNull();
    userRepository.store(userFactory.create(ONE, JOHN_DOE));
    assertThat(userRepository.one(ONE).getUsername()).isEqualTo(JOHN_DOE);
  }

  @Test
  public void testOneByName() {
    assertThat(userRepository.one(JOHN_DOE)).isNull();
    userRepository.store(userFactory.create(ONE, JOHN_DOE));
    assertThat(userRepository.one(JOHN_DOE).getUsername()).isEqualTo(JOHN_DOE);
  }

  /**
   * Tests the case where an exception should arise because two items with the same name are in the database.
   */
  @Test(expected = PersistenceException.class)
  public void testOneException() {
    userRepository.store(userFactory.create(ONE, JOHN_DOE));
    userRepository.store(userFactory.create(TWO, JOHN_DOE));
    userRepository.one(JOHN_DOE);
  }
}
