package models.repositories.impl;

import static org.fest.assertions.Assertions.assertThat;

import javax.persistence.PersistenceException;

import model.core.User;
import model.factories.RepositoryFactory;
import model.repositories.RepositoryRepository;
import model.repositories.impl.RepositoryRepositoryImpl;

import org.junit.Test;

import tests.base.models.AbstractBaseRepositoryTest;

public class RepositoryRepositoryImplTest extends AbstractBaseRepositoryTest {

  private static final String HELLO_WORLD = "hello_world";
  private static final String CLONE_URL = "http://api.platform.com/owner/repo";

  private final RepositoryRepository repositoryRepository = new RepositoryRepositoryImpl();
  private final RepositoryFactory repositoryFactory = RepositoryFactory.INSTANCE;

  @Test
  public void testOne() {
    assertThat(repositoryRepository.one(JOHN_DOE, HELLO_WORLD)).isNull();
    userRepository.store(userFactory.create(ONE, JOHN_DOE));
    repositoryRepository.store(repositoryFactory.create(TWO, userRepository.one(ONE), HELLO_WORLD, CLONE_URL));
    assertThat(repositoryRepository.one(JOHN_DOE, HELLO_WORLD).getName()).isEqualTo(HELLO_WORLD);
  }

  /**
   * Tests the case where an exception should arise because two repositories with the same name and owner are in the database.
   */
  @Test(expected = PersistenceException.class)
  public void testOneException() {
    userRepository.store(userFactory.create(ONE, JOHN_DOE));
    final User user = userRepository.one(ONE);
    repositoryRepository.store(repositoryFactory.create(ONE, user, HELLO_WORLD, CLONE_URL));
    repositoryRepository.store(repositoryFactory.create(TWO, user, HELLO_WORLD, CLONE_URL));
    repositoryRepository.one(JOHN_DOE, HELLO_WORLD);
  }
}
