package tests.base.controllers;

import static play.test.Helpers.start;
import static play.test.Helpers.stop;
import static play.test.Helpers.testServer;
import static play.test.Helpers.callAction;
import static play.test.Helpers.fakeRequest;
import static play.test.Helpers.contentAsString;

import java.util.Map;

import org.junit.AfterClass;
import org.junit.BeforeClass;

import com.google.common.collect.ImmutableMap;

import play.mvc.HandlerRef;
import play.mvc.Result;
import play.test.FakeRequest;
import play.test.TestServer;
import util.ApplicationConstants;

public abstract class AbstractBaseControllerTest {

  protected static final int PORT = 3333;
  private static final String FAKED_USERNAME = "mp1401";

  private static TestServer testServer;

  @BeforeClass
  public static void setup() {
    testServer = testServer(PORT);
    start(testServer);
  }

  protected String content(final Result result) {
    return contentAsString(result);
  }

  protected Result request(final HandlerRef ref) {
    final Map<String, String> empty = ImmutableMap.of();
    return request(ref, empty);
  }

  protected Result request(final HandlerRef ref, final Map<String, String> parameters) {
    return callAction(ref, createFakeRequest(parameters));
  }

  private FakeRequest createFakeRequest(final Map<String, String> parameters) {
    FakeRequest fakeRequest = fakeRequest().withSession(ApplicationConstants.SESSION_USERNAME_KEY, FAKED_USERNAME);
    if (parameters != null && !parameters.isEmpty()) {
      fakeRequest = fakeRequest.withFormUrlEncodedBody(parameters);
    }
    return fakeRequest;
  }

  @AfterClass
  public static void teardown() {
    stop(testServer);
  }
}
