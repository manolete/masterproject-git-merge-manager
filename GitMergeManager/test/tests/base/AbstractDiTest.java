package tests.base;

import global.ProductionModule;

import org.junit.Before;

import com.google.inject.Binder;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Module;
import com.google.inject.util.Modules;

public abstract class AbstractDiTest {

  protected Injector injector;

  @Before
  public final void setupInjector() {
    if (getOverrideModule() instanceof NullModule) {
      injector = Guice.createInjector(new ProductionModule());
    }
    else {
      injector = Guice.createInjector(Modules.override(new ProductionModule()).with(getOverrideModule()));
    }
  }

  /**
   * Subclasses can override this method to specify a module that will override
   * the {@link ProductionModule}.
   * 
   * @return {@link Module} to override the {@link ProductionModule}
   */
  public Module getOverrideModule() {
    return new NullModule();
  }

  public class NullModule implements Module {

    @Override
    public void configure(final Binder binder) {
      // do nothing
    }

  }

}
