package tests.base.models;

import model.factories.UserFactory;
import model.repositories.UserRepository;
import model.repositories.impl.UserRepositoryImpl;
import tests.base.AbstractFakeDatabaseTest;

public abstract class AbstractBaseRepositoryTest extends AbstractFakeDatabaseTest {

  protected static final String JOHN_DOE = "john_doe";
  protected static final long ONE = 1;
  protected static final long TWO = 2;

  protected final UserRepository userRepository = new UserRepositoryImpl();
  protected final UserFactory userFactory = UserFactory.INSTANCE;
}
