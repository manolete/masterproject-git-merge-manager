package tests.base;

import static play.test.Helpers.fakeApplication;
import static play.test.Helpers.inMemoryDatabase;
import static play.test.Helpers.start;

import org.junit.Before;

public abstract class AbstractFakeDatabaseTest extends AbstractDiTest {

  @Before
  public final void setupDatabase() {
    start(fakeApplication(inMemoryDatabase()));
  }

}
