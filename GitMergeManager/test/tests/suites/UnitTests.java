package tests.suites;

import models.ModelsTestSuite;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import services.ServicesTestSuite;
import util.UtilTestSuite;
import controllers.ControllerTestSuite;

@RunWith(Suite.class)
@SuiteClasses({//@formatter:off
  ControllerTestSuite.class,
  ModelsTestSuite.class,
  ServicesTestSuite.class,
  UtilTestSuite.class,
})//@formatter:on
public class UnitTests {}
