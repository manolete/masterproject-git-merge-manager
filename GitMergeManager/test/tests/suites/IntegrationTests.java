package tests.suites;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import tests.integration.ExampleIntegrationTest;

@RunWith(Suite.class)
@SuiteClasses({//@formatter:off
  ExampleIntegrationTest.class, // TODO remove
})//@formatter:on
public class IntegrationTests {}
