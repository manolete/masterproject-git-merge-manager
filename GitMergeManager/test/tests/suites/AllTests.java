package tests.suites;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({//@formatter:off
  UnitTests.class,
  IntegrationTests.class,
})//@formatter:on
public class AllTests {
  // empty
}
