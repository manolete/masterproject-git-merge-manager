package tests.resources;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class TestResources {

  private static final String BASE_PATH = "tests/resources/";

  public static InputStream getResource(final String fileName) {
    return ClassLoader.getSystemClassLoader().getResourceAsStream(BASE_PATH + fileName);
  }

  public static Properties getProperties(final String fileName) {
    final Properties properties = new Properties();
    try {
      properties.load(getResource(fileName));
    }
    catch (final IOException e) {
      e.printStackTrace();
    }
    return properties;
  }

}
