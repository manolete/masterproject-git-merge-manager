package tests.integration;

import static org.fest.assertions.Assertions.assertThat;

import org.junit.Test;

//TODO remove if not used anymore
public class ExampleIntegrationTest extends AbstractBaseIntegrationTest {

  @Test
  public void example() {
    browser.goTo("/test/auth");
    assertThat(browser.pageSource()).contains("access_token");
  }
}
