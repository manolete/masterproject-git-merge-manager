package tests.integration;

import static org.fest.assertions.Assertions.assertThat;
import static play.test.Helpers.start;
import static play.test.Helpers.stop;
import static play.test.Helpers.testServer;

import java.util.Properties;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import play.test.TestBrowser;
import play.test.TestServer;
import tests.resources.TestResources;

public class AbstractBaseIntegrationTest {

  protected static final String SLASH = "/";
  private static final String HTTP = "http://";
  private static final String HOST = "localhost";
  private static final int PORT = 9000;
  private static final String ROOT_URL = HTTP + HOST + ":" + PORT;

  private static final String CONFIG_FILE = "integrationtest.conf";

  private static Properties properties;
  protected static TestServer server;
  protected static TestBrowser browser;

  @BeforeClass
  public static void setup() {
    loadProperties();
    startServer();
    startBrowser();
    startSession();
  }

  private static void loadProperties() {
    properties = TestResources.getProperties(CONFIG_FILE);
  }

  private static void startServer() {
    server = testServer(PORT);
    start(server);
  }

  private static void startBrowser() {
    browser = createChrome("-incognito");
  }

  private static TestBrowser createChrome(final String... options) {
    System.setProperty("webdriver.chrome.driver", properties.getProperty("chrome-driver"));
    final ChromeOptions chromeOptions = new ChromeOptions();
    for (final String option : options) {
      chromeOptions.addArguments(option);
    }
    return new TestBrowser(new ChromeDriver(chromeOptions), ROOT_URL);
  }

  private static void startSession() {
    browser.goTo(SLASH + "login");
    browser.fill("input[name='login']").with(properties.getProperty("username"));
    browser.fill("input[name='password']").with(properties.getProperty("password"));
    browser.submit("input[name='login']");

    assertThat(browser.pageSource()).contains("GitMergeManager");
  }

  @AfterClass
  public static void teardown() {
    stopBrowser();
    stopServer();
  }

  private static void stopBrowser() {
    browser.getDriver().close();
    browser.getDriver().quit();
  }

  private static void stopServer() {
    stop(server);
  }

}
