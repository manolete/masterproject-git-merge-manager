package controllers.auth;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static play.test.Helpers.callAction;
import static play.test.Helpers.redirectLocation;

import java.util.HashMap;

import org.junit.BeforeClass;
import org.junit.Test;

import play.mvc.Http.Context;
import play.mvc.Http.Session;
import play.mvc.Result;
import controllers.auth.Authenticator;

/**
 * Test for {@link Authenticator}.
 */
public class AuthenticatorTest {

  private static final String TEST_USER_NAME = "testusername";
  private static final String LOGIN_REDIRECT_URL = "/login_redirect";

  private static Authenticator authenticator;
  private static Session session;
  private static Context contextStub;

  @BeforeClass
  public static void setup() {
    authenticator = new Authenticator();
    session = new Session(new HashMap<String, String>());
    contextStub = mock(Context.class);
    when(contextStub.session()).thenReturn(session);
  }

  @Test
  public void testGetUsername() {
    simulateLogin();
    assertThat(authenticator.getUsername(contextStub)).isEqualTo(TEST_USER_NAME);
    simulateLogout();
    assertThat(authenticator.getUsername(contextStub)).isNull();
  }

  @Test
  public void testOnUnauthorized() {
    final Result result = callAction(controllers.routes.ref.TestController.auth());
    assertThat(redirectLocation(result)).startsWith(LOGIN_REDIRECT_URL);
  }

  private void simulateLogin() {
    session.put("username", TEST_USER_NAME);
  }

  private void simulateLogout() {
    session.clear();
  }

}
