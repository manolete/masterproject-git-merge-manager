package controllers;

import static org.fest.assertions.Assertions.assertThat;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

import play.mvc.Result;
import tests.base.controllers.AbstractBaseControllerTest;

// TODO remove
public class ExampleControllerTest extends AbstractBaseControllerTest {

  private static final String NAME = "name";
  private static final String NAME_VALUE = "Anything";
  private static final String EXPECTED_RESULT = "name is: " + NAME_VALUE;

  @Test
  public void example() {
    Map<String, String> parameters = new HashMap<String, String>();
    parameters.put(NAME, NAME_VALUE);
    Result result = request(controllers.routes.ref.TestController.post(), parameters);
    assertThat(content(result)).isEqualTo(EXPECTED_RESULT);
  }
}
