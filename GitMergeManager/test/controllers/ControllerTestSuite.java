package controllers;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import controllers.auth.AuthenticatorTest;

@RunWith(Suite.class)
@SuiteClasses({//@formatter:off
  ExampleControllerTest.class, //TODO remove
  AuthenticatorTest.class,
})//@formatter:on
public class ControllerTestSuite {
  // empty
}
