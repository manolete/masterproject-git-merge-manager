package util;

import static org.fest.assertions.Assertions.assertThat;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

import util.URL;

public class URLTest {

  private static final String QUESTION_SIGN = "?";
  private static final String EQUALS_SIGN = "=";
  private static final String AND_SIGN = "&";
  private static final String BASE_URI = "http://www.example.com";
  private static final String PARAM_1_KEY = "param1";
  private static final String PARAM_1_VALUE = "foo";
  private static final String PARAM_1 = PARAM_1_KEY + EQUALS_SIGN + PARAM_1_VALUE;
  private static final String PARAM_2_KEY = "param2";
  private static final String PARAM_2_VALUE = "bar";
  private static final String PARAM_2 = PARAM_2_KEY + EQUALS_SIGN + PARAM_2_VALUE;
  private static final String PARAM_3_KEY = "param3";
  private static final String PARAM_3_VALUE = "any";
  private static final String PARAM_3 = PARAM_3_KEY + EQUALS_SIGN + PARAM_3_VALUE;

  @Test
  public void testWithoutParameter() {
    final URL url = new URL(BASE_URI);
    assertThat(url.toString()).isEqualTo(BASE_URI);
  }

  @Test
  public void testWithOneParameter() {
    final URL url = new URL(BASE_URI);
    url.addParameter(PARAM_1_KEY, PARAM_1_VALUE);
    final String expectedUrl = BASE_URI + QUESTION_SIGN + PARAM_1;
    assertThat(url.toString()).isEqualTo(expectedUrl);
  }

  @Test
  public void testWithMultipleParameters() {
    final URL url = new URL(BASE_URI);
    url.addParameter(PARAM_1_KEY, PARAM_1_VALUE);
    url.addParameter(PARAM_2_KEY, PARAM_2_VALUE);
    url.addParameter(PARAM_3_KEY, PARAM_3_VALUE);
    final String expectedUrl = BASE_URI + QUESTION_SIGN + PARAM_1 + AND_SIGN + PARAM_2 + AND_SIGN + PARAM_3;
    assertThat(url.toString()).isEqualTo(expectedUrl);

  }

  @Test
  public void testWithMultipleParametersAsMap() {
    final URL url = new URL(BASE_URI);
    final Map<String, String> parameters = new HashMap<String, String>();
    parameters.put(PARAM_1_KEY, PARAM_1_VALUE);
    parameters.put(PARAM_2_KEY, PARAM_2_VALUE);
    parameters.put(PARAM_3_KEY, PARAM_3_VALUE);
    url.addParameters(parameters);
    final String expectedUrl = BASE_URI + QUESTION_SIGN + PARAM_1 + AND_SIGN + PARAM_2 + AND_SIGN + PARAM_3;
    assertThat(url.toString()).isEqualTo(expectedUrl);
  }

}
