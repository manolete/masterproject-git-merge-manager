import com.github.play2war.plugin._

name := "GitMergeManager"

version := "1.0-SNAPSHOT"

Play2WarPlugin.play2WarSettings

Play2WarKeys.servletVersion := "3.0"

libraryDependencies ++= Seq(
  javaJdbc,
  javaEbean,
  "mysql" % "mysql-connector-java" % "5.1.18",
  cache,
  "com.google.inject" % "guice" % "4.0-beta",
  "org.mockito" % "mockito-core" % "1.9.5" % "test",
  "org.eclipse.jgit" % "org.eclipse.jgit" % "3.3.0.201403021825-r",
  "org.apache.maven.shared" % "maven-invoker" % "2.1.1",
  "org.apache.directory.studio" % "org.apache.commons.io" % "2.4"
)     

play.Project.playJavaSettings
