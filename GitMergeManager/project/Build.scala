import sbt._
import Keys._
import play.Project._

object ApplicationBuild extends Build {

  val appName = "GitMergeManager"
  val appVersion = "0.01"

  val appDependencies = Seq(
  )

  val main = play.Project(appName, appVersion, appDependencies).settings(
    //requireJs += "main.js"
    //requireJsShim += "main.js"
  )

}
