package model.factories;

import model.core.Branch;
import model.core.BranchReference;
import model.core.PullRequest;
import model.factories.impl.PullRequestFactoryImpl;

/**
 * A factory for {@link PullRequest}'s
 */
public interface PullRequestFactory {

  public static final PullRequestFactory INSTANCE = new PullRequestFactoryImpl();

  /**
   * Creates a {@link PullRequest}.
   * 
   * @param id
   *          id of the {@link PullRequest}
   * @param title
   *          title of the {@link PullRequest}
   * @param description
   *          description of the {@link PullRequest}
   * @param base
   *          base {@link Branch} of the {@link PullRequest}
   * @param head
   *          head {@link BranchReference} of the of the {@link PullRequest}
   * 
   * @param number
   *          number of the {@link PullRequest}
   * @return the created {@link PullRequest}
   */
  public PullRequest create(final long id, final String title, String description, final Branch base, final BranchReference head, final String number);

}
