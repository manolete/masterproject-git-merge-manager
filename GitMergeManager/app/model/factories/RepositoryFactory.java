package model.factories;

import model.core.Repository;
import model.core.User;
import model.factories.impl.RepositoryFactoryImpl;

/**
 * A factory for {@link Repository}'s.
 */
public interface RepositoryFactory {

  public static final RepositoryFactory INSTANCE = new RepositoryFactoryImpl();

  /**
   * Creates a {@link Repository}.
   * 
   * @param id
   *          id of the {@link Repository}
   * @param owner
   *          the {@link User} which is the owner of the {@link Repository}
   * @param name
   *          name of the {@link Repository}
   * @return the created {@link Repository}
   */
  public Repository create(final long id, final User owner, final String name, final String cloneUrl);

}
