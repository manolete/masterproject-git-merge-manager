package model.factories;

import model.core.BranchReference;
import model.factories.impl.BranchReferenceFactoryImpl;

/**
 * A factory for {@link BranchReference}'s.
 */
public interface BranchReferenceFactory {

  public static final BranchReferenceFactory INSTANCE = new BranchReferenceFactoryImpl();

  /**
   * Creates a {@link BranchReference}.
   * 
   * @param repositoryOwnerUsername
   *          name of the owner of the repository
   * @param repositoryName
   *          name of the repository
   * @param branchName
   *          name of the branch
   * @param cloneUrl
   *          the clone URL of the repository
   * @return the created {@link BranchReference}
   */
  public BranchReference create(final String repositoryOwnerUsername, final String repositoryName, final String branchName, final String cloneUrl);

}
