package model.factories;

import model.core.Label;
import model.factories.impl.LabelFactoryImpl;

/**
 * A factory for {@link Label}'s.
 */
public interface LabelFactory {

  public static final LabelFactory INSTANCE = new LabelFactoryImpl();

  /**
   * Creates a {@link Label}.
   * 
   * @param name
   *          name of the {@link Label}
   * @param url
   *          url of the {@link Label}
   * @param color
   *          color of the {@link Label}
   * @return the created {@link Label}
   */
  public Label create(final String name, final String url, final String color);

}
