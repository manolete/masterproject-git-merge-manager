package model.factories.impl;

import model.core.Branch;
import model.core.BranchReference;
import model.core.PullRequest;
import model.factories.PullRequestFactory;

/**
 * Implementation of {@link PullRequestFactory}.
 */
public class PullRequestFactoryImpl implements PullRequestFactory {

  /**
   * {@inheritDoc}
   */
  @Override
  public PullRequest create(final long id, final String title, final String description, final Branch base, final BranchReference head, final String number) {
    return new PullRequest(id, title, description, base, head, number);
  }

}
