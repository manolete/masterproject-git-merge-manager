package model.factories.impl;

import model.core.Label;
import model.factories.LabelFactory;

/**
 * Implementation of {@link LabelFactory}
 */
public class LabelFactoryImpl implements LabelFactory {

  @Override
  public Label create(final String name, final String url, final String color) {
    return new Label(name, url, color);
  }

}
