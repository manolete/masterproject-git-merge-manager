package model.factories.impl;

import model.core.User;
import model.factories.UserFactory;

/**
 * Implementation of {@link UserFactory}.
 */
public class UserFactoryImpl implements UserFactory {

  /**
   * {@inheritDoc}
   */
  @Override
  public User create(final long id, final String username) {
    return new User(id, username);
  }

}
