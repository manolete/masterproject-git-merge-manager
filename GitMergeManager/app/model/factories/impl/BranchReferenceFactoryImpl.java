package model.factories.impl;

import model.core.BranchReference;
import model.factories.BranchReferenceFactory;

/**
 * Implementation of {@link BranchReferenceFactory}.
 */
public class BranchReferenceFactoryImpl implements BranchReferenceFactory {

  /**
   * {@inheritDoc}
   */
  @Override
  public BranchReference create(final String repositoryOwnerUsername, final String repositoryName, final String branchName, final String cloneUrl) {
    return new BranchReference(repositoryOwnerUsername, repositoryName, branchName, cloneUrl);
  }

}
