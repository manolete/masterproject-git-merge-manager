package model.factories.impl;

import model.core.Favorite;
import model.core.Repository;
import model.core.User;
import model.factories.FavoriteFactory;

/**
 * Implementation of {@link FavoriteFactory}.
 */
public class FavoriteFactoryImpl implements FavoriteFactory {

  /**
   * {@inheritDoc}
   */
  @Override
  public Favorite create(final User user, final Repository repository) {
    return new Favorite(user, repository);
  }

}
