package model.factories.impl;

import model.core.PullRequest;
import model.core.results.BaseBranchBuildResult;
import model.core.results.ExecutionResult;
import model.core.results.PairwiseBuildResult;
import model.core.results.PullRequestBuildResult;
import model.core.results.delta.BuildResultDelta;
import model.factories.ExecutionResultFactory;

public class ExecutionResultFactoryImpl implements ExecutionResultFactory {

  /**
   * {@inheritDoc}
   */
  @Override
  public ExecutionResult createExecutionResult() {
    return new ExecutionResult();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public BaseBranchBuildResult createBaseBranchBuildResult(final boolean compileable) {
    return new BaseBranchBuildResult(compileable);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public PullRequestBuildResult createPullRequestBuildResult(final PullRequest pullRequest, final boolean mergeable, final boolean compileable) {
    return new PullRequestBuildResult(pullRequest, mergeable, compileable);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public PairwiseBuildResult createPairwiseBuildResult(final ExecutionResult containingExecutionResult, final PullRequest pullRequestA, final PullRequest pullRequestB, final boolean mergeable,
      final boolean compileable) {
    return new PairwiseBuildResult(containingExecutionResult, pullRequestA, pullRequestB, mergeable, compileable);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public BuildResultDelta createBuildResultDelta() {
    return new BuildResultDelta();
  }

}
