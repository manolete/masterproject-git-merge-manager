package model.factories.impl;

import model.core.Repository;
import model.core.User;
import model.factories.RepositoryFactory;

/**
 * Implementation fo {@link RepositoryFactory}.
 */
public class RepositoryFactoryImpl implements RepositoryFactory {

  /**
   * {@inheritDoc}
   */
  @Override
  public Repository create(final long id, final User owner, final String name, final String cloneUrl) {
    return new Repository(id, owner, name, cloneUrl);
  }

}
