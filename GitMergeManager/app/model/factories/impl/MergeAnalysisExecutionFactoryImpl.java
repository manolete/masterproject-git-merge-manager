package model.factories.impl;

import org.joda.time.DateTime;

import model.core.MergeAnalysisExecution;
import model.core.MergeAnalysis;
import model.factories.MergeAnalysisExecutionFactory;

/**
 * Implementation of {@link MergeAnalysisExecutionFactory}.
 */
public class MergeAnalysisExecutionFactoryImpl implements MergeAnalysisExecutionFactory {

  /**
   * {@inheritDoc}
   */
  @Override
  public MergeAnalysisExecution create(final MergeAnalysis mergeAnalysis, final DateTime starDateTime) {
    return new MergeAnalysisExecution(mergeAnalysis, starDateTime);
  }

}
