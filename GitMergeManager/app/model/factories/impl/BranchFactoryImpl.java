package model.factories.impl;

import model.core.Branch;
import model.core.Repository;
import model.factories.BranchFactory;

/**
 * Implementation of {@link BranchFactory}.
 */
public class BranchFactoryImpl implements BranchFactory {

  /**
   * {@inheritDoc}
   */
  @Override
  public Branch create(final Repository repository, final String name) {
    return new Branch(repository, name);
  }

}
