package model.factories.impl;

import model.core.MergeAnalysis;
import model.core.Repository;
import model.core.User;
import model.factories.MergeAnalysisFactory;

/**
 * Implementation of {@link MergeAnalysisFactory}.
 */
public class MergeAnalysisFactoryImpl implements MergeAnalysisFactory {

  /**
   * {@inheritDoc}
   */
  @Override
  public MergeAnalysis create(final Repository repository, final String name, final User creator) {
    return new MergeAnalysis(repository, name, creator);
  }

}
