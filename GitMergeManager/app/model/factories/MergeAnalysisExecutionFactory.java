package model.factories;

import org.joda.time.DateTime;

import model.core.MergeAnalysisExecution;
import model.core.MergeAnalysis;
import model.factories.impl.MergeAnalysisExecutionFactoryImpl;

/**
 * A factory for {@link MergeAnalysisExecution}'s
 */
public interface MergeAnalysisExecutionFactory {

  public static final MergeAnalysisExecutionFactory INSTANCE = new MergeAnalysisExecutionFactoryImpl();

  /**
   * Creates a {@link MergeAnalysisExecution}.
   * 
   * @param mergeAnalysis
   *          the {@link MergeAnalysis} that the {@link MergeAnalysisExecution} will execute
   * @return the created {@link MergeAnalysisExecution}
   */
  public MergeAnalysisExecution create(final MergeAnalysis mergeAnalysis, DateTime startDateTime);

}
