package model.factories;

import model.core.PullRequest;
import model.core.results.BaseBranchBuildResult;
import model.core.results.ExecutionResult;
import model.core.results.PairwiseBuildResult;
import model.core.results.PullRequestBuildResult;
import model.core.results.delta.BuildResultDelta;
import model.factories.impl.ExecutionResultFactoryImpl;

/**
 * A Factory for classes in the {@link ExecutionResult} aggregate.
 */
public interface ExecutionResultFactory {

  public static ExecutionResultFactory INSTANCE = new ExecutionResultFactoryImpl();

  /**
   * Creates a {@link ExecutionResult}.
   * 
   * @return the created {@link ExecutionResult}
   */
  public ExecutionResult createExecutionResult();

  /**
   * Creates a {@link BaseBranchBuildResult}.
   * 
   * @param compileable
   *          boolean indicating whether the compilation was successful
   * @return the created {@link BaseBranchBuildResult}
   */
  public BaseBranchBuildResult createBaseBranchBuildResult(final boolean compileable);

  /**
   * Creates a {@link PullRequestBuildResult}.
   * 
   * @param pullRequest
   *          the respective {@link PullRequest}
   * @param mergeable
   *          result of merge
   * @param compileable
   *          boolean indicating whether the compilation was successful
   * @return the created {@link PullRequestBuildResult}
   */
  public PullRequestBuildResult createPullRequestBuildResult(final PullRequest pullRequest, final boolean mergeable, final boolean compileable);

  /**
   * Creates a {@link PairwiseBuildResult}.
   * 
   * @param containingExecutionResult
   *          the {@link ExecutionResult} that contains the created {@link PairwiseBuildResult}
   * @param pullRequestA
   *          first {@link PullRequest}
   * @param pullRequestB
   *          second {@link PullRequest}
   * @param mergeable
   *          result of merge
   * @param compileable
   *          result of compilation
   * @return the created {@link PairwiseBuildResult}
   */
  public PairwiseBuildResult createPairwiseBuildResult(final ExecutionResult containingExecutionResult, final PullRequest pullRequestA, final PullRequest pullRequestB, final boolean mergeable,
      final boolean compileable);

  /**
   * Creates a {@link BuildResultDelta}.
   * 
   * @return the created {@link BuildResultDelta}
   */
  public BuildResultDelta createBuildResultDelta();

}
