package model.factories;

import model.core.Favorite;
import model.core.Repository;
import model.core.User;
import model.factories.impl.FavoriteFactoryImpl;

/**
 * A factory for {@link Favorite}'s.
 */
public interface FavoriteFactory {

  public static final FavoriteFactory INSTANCE = new FavoriteFactoryImpl();

  /**
   * Creates a {@link Favorite}.
   * 
   * @param user
   *          {@link User} of the favorite
   * @param repository
   *          {@link Repository} the favorite references
   * @return the created {@link Favorite}
   */
  public Favorite create(final User user, final Repository repository);

}
