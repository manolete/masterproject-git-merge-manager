package model.factories;

import model.core.Branch;
import model.core.Repository;
import model.factories.impl.BranchFactoryImpl;

/**
 * A factory for {@link Branch}'s
 */
public interface BranchFactory {

  public static final BranchFactory INSTANCE = new BranchFactoryImpl();

  /**
   * Creates a {@link Branch}.
   * 
   * @param repository
   *          {@link Repository} the branch belongs to
   * @param name
   *          name of the {@link Branch}
   * @return the created {@link Branch}
   */
  public Branch create(final Repository repository, final String name);

}
