package model.factories;

import model.core.User;
import model.factories.impl.UserFactoryImpl;

/**
 * A factory for {@link User}'s
 */
public interface UserFactory {

  public static final UserFactory INSTANCE = new UserFactoryImpl();

  /**
   * Creates a {@link User}.
   * 
   * @param id
   *          id of the {@link User}
   * @param username
   *          username of the {@link User}
   * @return the created {@link User}
   */
  public User create(final long id, final String username);

}
