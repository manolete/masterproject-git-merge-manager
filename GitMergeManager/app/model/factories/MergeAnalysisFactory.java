package model.factories;

import model.core.MergeAnalysis;
import model.core.Repository;
import model.core.User;
import model.factories.impl.MergeAnalysisFactoryImpl;

/**
 * A factory for {@link MergeAnalysis}'s
 */
public interface MergeAnalysisFactory {

  public static final MergeAnalysisFactory INSTANCE = new MergeAnalysisFactoryImpl();

  /**
   * Creates a {@link MergeAnalysis}.
   * 
   * @param repository
   *          the {@link Repository} for which the {@link MergeAnalysis} is
   *          defined
   * @param creator
   *          the {@link User} who creates the {@link MergeAnalysis}
   * @return the created {@link MergeAnalysis}
   */
  public MergeAnalysis create(final Repository repository, final String name, final User creator);

}
