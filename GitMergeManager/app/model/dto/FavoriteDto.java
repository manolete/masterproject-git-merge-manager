package model.dto;

import java.sql.Timestamp;

import model.dto.base.AbstractIdentifiableDto;

/**
 * Data transfer object for a favorite.
 */
public class FavoriteDto extends AbstractIdentifiableDto {

  public String repository;

  public Timestamp lastAccessed;

  public FavoriteDto() {
    // do nothing
  }

}
