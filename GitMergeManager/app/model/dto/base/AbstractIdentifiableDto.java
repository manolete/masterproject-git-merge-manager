package model.dto.base;

/**
 * Base class for data transfer objects that have an id.
 */
public abstract class AbstractIdentifiableDto {

  public Long id;

}
