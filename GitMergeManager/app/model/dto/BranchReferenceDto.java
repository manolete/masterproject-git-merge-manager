package model.dto;

/**
 * Data transfer object for a branch reference.
 */
public class BranchReferenceDto {

  private static final String UNKNOWN = "<unknown>";

  public String repositoryOwnerUsername;

  public String repositoryName;

  public String branchName;

  public String cloneUrl;

  public static BranchReferenceDto unknown() {
    final BranchReferenceDto branchReferenceDto = new BranchReferenceDto();
    branchReferenceDto.repositoryOwnerUsername = UNKNOWN;
    branchReferenceDto.repositoryName = UNKNOWN;
    branchReferenceDto.branchName = UNKNOWN;
    branchReferenceDto.cloneUrl = UNKNOWN;
    return branchReferenceDto;
  }

}
