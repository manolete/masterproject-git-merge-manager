package model.dto;

import model.dto.base.AbstractIdentifiableDto;

import org.joda.time.DateTime;
import org.joda.time.Duration;

/**
 * Data transfer object for a merge analysis execution.
 */
public class MergeAnalysisExecutionDto extends AbstractIdentifiableDto {

  public DateTime startDateTime;
  public DateTime endDateTime;
  public Duration duration;

  public boolean finished;
  public boolean aborted;
  public boolean terminated;

  public String mergeAnalysis;

  public MergeAnalysisExecutionDto() {
    // do nothing
  }

}
