package model.dto;

import java.util.ArrayList;
import java.util.List;

import model.dto.base.AbstractIdentifiableDto;

/**
 * Data transfer object for a pull request.
 */
public class PullRequestDto extends AbstractIdentifiableDto {

  public String title;

  public String url;

  public String description;

  public String base;

  public BranchReferenceDto head;

  public String number;

  public boolean isMergeable;

  public String webUri;

  public List<LabelDto> labels;

  public String referencedIssueNumber;

  public List<LabelDto> referencedIssueLabels;

  public PullRequestDto() {
    this.labels = new ArrayList<LabelDto>();
    this.referencedIssueLabels = new ArrayList<LabelDto>();
  }

}
