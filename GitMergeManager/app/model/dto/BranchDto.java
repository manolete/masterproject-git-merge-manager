package model.dto;

import java.util.ArrayList;
import java.util.List;

import model.dto.base.AbstractIdentifiableDto;

/**
 * Data transfer object for a branch.
 */
public class BranchDto extends AbstractIdentifiableDto {

  public String repositoryOwner;

  public String repository;

  public String name;

  public String latestCommitSha;

  public List<String> pullRequests;

  public BranchDto() {
    pullRequests = new ArrayList<String>();
  }

}
