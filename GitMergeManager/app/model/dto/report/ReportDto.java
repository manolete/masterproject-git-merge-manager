package model.dto.report;

import java.util.List;

public class ReportDto {

  public List<TestCaseDto> testCases;

  public List<CheckstyleViolationDto> checkstyleViolations;

  public List<PmdViolationDto> pmdViolations;

  public ReportDto(final List<TestCaseDto> testCases, final List<CheckstyleViolationDto> checkstyleViolations, final List<PmdViolationDto> pmdViolations) {
    this.testCases = testCases;
    this.checkstyleViolations = checkstyleViolations;
    this.pmdViolations = pmdViolations;
  }

}
