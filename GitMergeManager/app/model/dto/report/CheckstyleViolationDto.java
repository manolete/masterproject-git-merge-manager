package model.dto.report;

import services.maven.plugins.results.CheckstyleViolation.Severity;

public class CheckstyleViolationDto {

  public String filename;

  public int line;
  public int column;

  public Severity severity;

  public String message;

  public String source;

  public CheckstyleViolationDto(final String filename, final int line, final int column, final Severity severity, final String message, final String source) {
    this.filename = filename;
    this.line = line;
    this.column = column;
    this.severity = severity;
    this.message = message;
    this.source = source;
  }

}
