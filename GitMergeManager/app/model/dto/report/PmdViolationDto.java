package model.dto.report;

public class PmdViolationDto {

  public String file;

  public int beginLine;
  public int endLine;
  public int beginColumn;
  public int endColumn;

  public String rule;
  public String ruleSet;

  public String packageName;
  public String className;
  public String methodName;
  public String externalInfoUrl;

  public int priority;

  public String content;

  public PmdViolationDto(final String file, final int beginLine, final int endLine, final int beginColumn, final int endColumn, final String rule, final String ruleSet, final String packageName, final String className, final String methodName, final String externalInfoUrl, final int priority, final String content) {
    this.file = file;
    this.beginLine = beginLine;
    this.endLine = endLine;
    this.beginColumn = beginColumn;
    this.endColumn = endColumn;
    this.rule = rule;
    this.ruleSet = ruleSet;
    this.packageName = packageName;
    this.className = className;
    this.methodName = methodName;
    this.externalInfoUrl = externalInfoUrl;
    this.priority = priority;
    this.content = content;
  }

}
