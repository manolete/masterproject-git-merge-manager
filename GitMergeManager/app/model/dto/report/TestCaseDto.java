package model.dto.report;

public class TestCaseDto {

  public String className;

  public String methodName;

  public double time;

  public boolean isSuccessful;

  public TestCaseFailureDto failure;

  public TestCaseDto(final String className, final String methodName, final double time) {
    this(className, methodName, time, true);
  }

  public TestCaseDto(final String className, final String methodName, final double time, final TestCaseFailureDto failure) {
    this(className, methodName, time, false);
    this.failure = failure;
  }

  private TestCaseDto(final String className, final String methodName, final double time, final boolean isSuccessful) {
    this.className = className;
    this.methodName = methodName;
    this.time = time;
    this.isSuccessful = isSuccessful;
  }

  public static class TestCaseFailureDto {

    public String type;
    public String message;

    public TestCaseFailureDto(final String type, final String message) {
      this.type = type;
      this.message = message;
    }

  }

}
