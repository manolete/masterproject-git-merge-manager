package model.dto;

import java.util.ArrayList;
import java.util.List;

import model.dto.base.AbstractIdentifiableDto;

/**
 * Data transfer object for a merge analysis.
 */
public class MergeAnalysisDto extends AbstractIdentifiableDto {

  public String repository;

  public String name;

  public String creator;

  public List<String> pullRequests;

  public List<Long> mergeAnalysisExecutions;

  public MergeAnalysisDto() {
    pullRequests = new ArrayList<String>();
    mergeAnalysisExecutions = new ArrayList<Long>();
  }

}
