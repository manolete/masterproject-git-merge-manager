package model.dto.factory;

import model.core.Branch;
import model.core.BranchReference;
import model.core.Favorite;
import model.core.Label;
import model.core.MergeAnalysisExecution;
import model.core.MergeAnalysis;
import model.core.PullRequest;
import model.core.Repository;
import model.core.User;
import model.dto.BranchDto;
import model.dto.BranchReferenceDto;
import model.dto.FavoriteDto;
import model.dto.LabelDto;
import model.dto.MergeAnalysisDto;
import model.dto.MergeAnalysisExecutionDto;
import model.dto.PullRequestDto;
import model.dto.RepositoryDto;
import model.dto.UserDto;

/**
 * A factory for data transfer objects.
 */
public interface DtoFactory {

  /**
   * Creates a {@link BranchDto}.
   * 
   * @param branch
   *          the {@link Branch} that the {@link BranchDto} represents
   * @return the created {@link BranchDto}
   */
  public BranchDto createBranchDto(Branch branch);
  
  /**
   * Creates a {@link BranchReferenceDto}.
   * 
   * @param branchReference
   *          the {@link BranchReference} that the {@link BranchReferenceDto} represents
   * @return the created {@link BranchReferenceDto}
   */
  public BranchReferenceDto createBranchReferenceDto(BranchReference branchReference);

  /**
   * Creates a {@link FavoriteDto}.
   * 
   * @param the
   *          the {@link Favorite} that the {@link FavoriteDto} represents
   * @return the created {@link FavoriteDto}
   */
  public FavoriteDto createFavoriteDto(Favorite favorite);

  /**
   * Creates a {@link MergeAnalysisExecutionDto}.
   * 
   * @param mergeAnalysisExecution
   *          the {@link MergeAnalysisExecution} that the {@link MergeAnalysisExecutionDto} represents
   * @return the created {@link MergeAnalysisExecutionDto}
   */
  public MergeAnalysisExecutionDto createMergeAnalysisExecutionDto(MergeAnalysisExecution mergeAnalysisExecution);

  /**
   * Creates a {@link MergeAnalysisDto}.
   * 
   * @param mergeAnalysis
   *          the {@link MergeAnalysis} that the {@link MergeAnalysisDto} represents
   * @return the created {@link MergeAnalysisDto}
   */
  public MergeAnalysisDto createMergeAnalysisDto(MergeAnalysis mergeAnalysis);

  /**
   * Creates a {@link PullRequestDto}.
   * 
   * @param pullRequest
   *          the {@link PullRequest} that the {@link PullRequestDto} represents
   * @return the created {@link PullRequestDto}
   */
  public PullRequestDto createPullRequestDto(PullRequest pullRequest);

  /**
   * Creates a {@link LabelDto}.
   * 
   * @param label
   *          the {@link Label} that the {@link LabelDto} represents
   * @return the created {@link LabelDto}
   */
  public LabelDto createLabelDto(Label label);

  /**
   * Creates a {@link RepositoryDto}.
   * 
   * @param repository
   *          the {@link Repository} that the {@link RepositoryDto} represents
   * @return the created {@link RepositoryDto}
   */
  public RepositoryDto createRepositoryDto(Repository repository);

  /**
   * Creates a {@link UserDto}.
   * 
   * @param the
   *          {@link User} that a {@link UserDto} represents
   * @return the created {@link UserDto}
   */
  public UserDto createUserDto(User user);

}
