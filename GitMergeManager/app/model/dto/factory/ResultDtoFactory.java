package model.dto.factory;

import model.core.results.BaseBranchBuildResult;
import model.core.results.CheckstyleBuildResultItem;
import model.core.results.ExecutionResult;
import model.core.results.PairwiseBuildResult;
import model.core.results.PmdBuildResultItem;
import model.core.results.PullRequestBuildResult;
import model.core.results.TestBuildResultItem;
import model.core.results.delta.BuildResultDelta;
import model.dto.results.BaseBranchBuildResultDto;
import model.dto.results.BuildResultDeltaDto;
import model.dto.results.CheckstyleBuildResultItemDto;
import model.dto.results.ExecutionResultDto;
import model.dto.results.PairwiseBuildResultDto;
import model.dto.results.PmdBuildResultItemDto;
import model.dto.results.PullRequestBuildResultDto;
import model.dto.results.PullRequestBuildResultSummaryDto;
import model.dto.results.TestBuildResultItemDto;

public interface ResultDtoFactory {

  /**
   * Creates a {@link BaseBranchBuildResultDto}.
   * 
   * @param baseBranchBuildResult
   *          the {@link BaseBranchBuildResult} that the {@link BaseBranchBuildResultDto} represents
   * @return the created {@link BaseBranchBuildResultDto}
   */
  public BaseBranchBuildResultDto createBaseBranchBuildResultDto(BaseBranchBuildResult baseBranchBuildResult);

  /**
   * Creates a {@link PullRequestBuildResultDto}.
   * 
   * @param pullRequestBuildResult
   *          the {@link PullRequestBuildResult} that the {@link PullRequestBuildResultDto} represents
   * @return the created {@link PullRequestBuildResultDto}
   */
  public PullRequestBuildResultDto createPullRequestBuildResultDto(PullRequestBuildResult pullRequestBuildResult);

  /**
   * Creates a {@link PullRequestBuildResultSummaryDto}.
   * 
   * @param pullRequestBuildResult
   *          the {@link PullRequestBuildResult} that the {@link PullRequestBuildResultSummaryDto} represents
   * @return the created {@link PullRequestBuildResultSummaryDto}
   */
  public PullRequestBuildResultSummaryDto createPullRequestBuildResultSummaryDto(PullRequestBuildResult pullRequestBuildResult);

  /**
   * Creates a {@link PairwiseBuildResultDto}.
   * 
   * @param pairwiseBuildResult
   *          the {@link PairwiseBuildResult} that the {@link PairwiseBuildResultDto} represents
   * @return the created {@link PairwiseBuildResultDto}
   */
  public PairwiseBuildResultDto createPairwiseBuildResultDto(PairwiseBuildResult pairwiseBuildResult);

  /**
   * Creates a {@link TestBuildResultItemDto}.
   * 
   * @param testBuildResultItem
   *          the {@link TestBuildResultItem} that the {@link TestBuildResultItemDto} represents
   * @return the created {@link TestBuildResultItemDto}
   */
  public TestBuildResultItemDto createTestBuildResultItemDto(TestBuildResultItem testBuildResultItem);

  /**
   * Creates a {@link CheckstyleBuildResultItemDto}.
   * 
   * @param checkstyleBuildResultItem
   *          the {@link CheckstyleBuildResultItem} that the {@link CheckstyleBuildResultItemDto} represents
   * @return the created {@link CheckstyleBuildResultItemDto}
   */
  public CheckstyleBuildResultItemDto createCheckstyleBuildResultItemDto(CheckstyleBuildResultItem checkstyleBuildResultItem);

  /**
   * Creates a {@link PmdBuildResultItemDto}.
   * 
   * @param pmdBuildResultItem
   *          the {@link PmdBuildResultItem} that the {@link PmdBuildResultItemDto} represents
   * @return the created {@link PmdBuildResultItemDto}
   */
  public PmdBuildResultItemDto createPmdBuildResultItemDto(PmdBuildResultItem pmdBuildResultItem);

  /**
   * Creates a {@link ExecutionResultDto}.
   * 
   * @param executionResult
   *          the {@link ExecutionResult} that the {@link ExecutionResultDto} represents
   * @return the created {@link ExecutionResultDto}
   */
  public ExecutionResultDto createExecutionResultDto(ExecutionResult executionResult);

  /**
   * Creates a {@link BuildResultDeltaDto}.
   * 
   * @param buildResultDelta
   *          the {@link BuildResultDelta} that the {@link BuildResultDeltaDto} represents
   * @return the created {@link BuildResultDeltaDto}
   */
  public BuildResultDeltaDto createBuildResultDeltaDto(BuildResultDelta buildResultDelta);

}
