package model.dto.factory;

import model.core.Branch;
import model.core.BranchReference;
import model.core.Favorite;
import model.core.Label;
import model.core.MergeAnalysisExecution;
import model.core.MergeAnalysis;
import model.core.PullRequest;
import model.core.Repository;
import model.core.User;
import model.dto.BranchDto;
import model.dto.BranchReferenceDto;
import model.dto.FavoriteDto;
import model.dto.LabelDto;
import model.dto.MergeAnalysisDto;
import model.dto.MergeAnalysisExecutionDto;
import model.dto.PullRequestDto;
import model.dto.RepositoryDto;
import model.dto.UserDto;

/**
 * Implementation of {@link DtoFactory}.
 */
public class DtoFactoryImpl implements DtoFactory {

  /**
   * {@inheritDoc}
   */
  @Override
  public BranchDto createBranchDto(final Branch branch) {
    final BranchDto branchDto = new BranchDto();
    branchDto.id = branch.getId();
    branchDto.repositoryOwner = branch.getRepository().getOwner().getUsername();
    branchDto.repository = branch.getRepository().getName();
    branchDto.name = branch.getName();
    branchDto.latestCommitSha = branch.getLatestCommitSha();
    for (final PullRequest pullRequest : branch.getPullRequests()) {
      branchDto.pullRequests.add(pullRequest.getTitle());
    }
    return branchDto;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public BranchReferenceDto createBranchReferenceDto(final BranchReference branchReference) {
    final BranchReferenceDto branchReferenceDto = new BranchReferenceDto();
    branchReferenceDto.repositoryOwnerUsername = branchReference.getRepositoryOwnerUsername();
    branchReferenceDto.repositoryName = branchReference.getRepositoryName();
    branchReferenceDto.branchName = branchReference.getBranchName();
    branchReferenceDto.cloneUrl = branchReference.getCloneUrl();
    return branchReferenceDto;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public FavoriteDto createFavoriteDto(final Favorite favorite) {
    final FavoriteDto favoriteDto = new FavoriteDto();
    favoriteDto.id = favorite.getId();
    favoriteDto.repository = favorite.getRepository().getName();
    favoriteDto.lastAccessed = favorite.getLastAccessed();
    return favoriteDto;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public MergeAnalysisExecutionDto createMergeAnalysisExecutionDto(final MergeAnalysisExecution mergeAnalysisExecution) {
    final MergeAnalysisExecutionDto mergeAnalysisExecutionDto = new MergeAnalysisExecutionDto();
    mergeAnalysisExecutionDto.id = mergeAnalysisExecution.getId();
    mergeAnalysisExecutionDto.startDateTime = mergeAnalysisExecution.getStartDateTime();
    mergeAnalysisExecutionDto.endDateTime = mergeAnalysisExecution.getEndDateTime();
    mergeAnalysisExecutionDto.duration = mergeAnalysisExecution.getDuration();
    mergeAnalysisExecutionDto.finished = mergeAnalysisExecution.finished();
    mergeAnalysisExecutionDto.aborted = mergeAnalysisExecution.aborted();
    mergeAnalysisExecutionDto.terminated = mergeAnalysisExecution.terminated();
    mergeAnalysisExecutionDto.mergeAnalysis = mergeAnalysisExecution.getMergeAnalysis().getName();
    return mergeAnalysisExecutionDto;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public MergeAnalysisDto createMergeAnalysisDto(final MergeAnalysis analysisExecution) {
    final MergeAnalysisDto analysisExecutionDto = new MergeAnalysisDto();
    analysisExecutionDto.id = analysisExecution.getId();
    analysisExecutionDto.repository = analysisExecution.getRepository().getName();
    analysisExecutionDto.name = analysisExecution.getName();
    analysisExecutionDto.creator = analysisExecution.getCreator().getUsername();
    for (final PullRequest pullRequest : analysisExecution.getPullRequests()) {
      analysisExecutionDto.pullRequests.add(pullRequest.getNumber());
    }
    for (final MergeAnalysisExecution mergeAnalysisExecution : analysisExecution.getMergeAnalysisExecutions()) {
      analysisExecutionDto.mergeAnalysisExecutions.add(mergeAnalysisExecution.getId());
    }
    return analysisExecutionDto;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public PullRequestDto createPullRequestDto(final PullRequest pullRequest) {
    final PullRequestDto pullRequestDto = new PullRequestDto();
    pullRequestDto.id = pullRequest.getId();
    pullRequestDto.title = pullRequest.getTitle();
    pullRequestDto.description = pullRequest.getDescription();
    pullRequestDto.base = pullRequest.getBase().getName();
    pullRequestDto.head = createBranchReferenceDto(pullRequest.getHead());
    pullRequestDto.number = pullRequest.getNumber();
    pullRequestDto.isMergeable = pullRequest.isMergeable();
    pullRequestDto.url = pullRequest.getUrl();
    pullRequestDto.referencedIssueNumber = pullRequest.getReferencedIssueNumber();
    for (final Label label : pullRequest.getLabels()) {
      pullRequestDto.labels.add(createLabelDto(label));
    }
    for (final Label label : pullRequest.getReferencedIssueLabels()) {
      pullRequestDto.referencedIssueLabels.add(createLabelDto(label));
    }
    return pullRequestDto;
  }

  @Override
  public LabelDto createLabelDto(final Label label) {
    final LabelDto labelDto = new LabelDto();
    labelDto.name = label.getName();
    labelDto.url = label.getUrl();
    labelDto.color = label.getColor();
    return labelDto;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public RepositoryDto createRepositoryDto(final Repository repository) {
    final RepositoryDto repositoryDto = new RepositoryDto();
    repositoryDto.id = repository.getId();
    repositoryDto.name = repository.getName();
    repositoryDto.owner = repository.getOwner().getUsername();
    repositoryDto.cloneUrl = repository.getCloneUrl();
    for (final Branch branch : repository.getBranches()) {
      repositoryDto.branches.add(branch.getName());
    }
    for (final PullRequest pullRequest : repository.getPullRequests()) {
      repositoryDto.pullRequests.add(pullRequest.getTitle());
    }
    for (final MergeAnalysis mergeAnalysis : repository.getMergeAnalysis()) {
      repositoryDto.mergeAnalyses.add(mergeAnalysis.getName());
    }
    return repositoryDto;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public UserDto createUserDto(final User user) {
    final UserDto userDto = new UserDto();
    userDto.id = user.getId();
    userDto.username = user.getUsername();
    userDto.url = user.getUrl();
    userDto.avatarUrl = user.getAvatarUrl();
    return userDto;
  }

}
