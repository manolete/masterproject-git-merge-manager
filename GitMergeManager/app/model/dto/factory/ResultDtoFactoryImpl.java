package model.dto.factory;

import model.core.PullRequest;
import model.core.results.AbstractBuildResult;
import model.core.results.BaseBranchBuildResult;
import model.core.results.CheckstyleBuildResultItem;
import model.core.results.ExecutionResult;
import model.core.results.PairwiseBuildResult;
import model.core.results.PmdBuildResultItem;
import model.core.results.PullRequestBuildResult;
import model.core.results.TestBuildResultItem;
import model.core.results.TestBuildResultItem.Failure;
import model.core.results.delta.BuildResultDelta;
import model.dto.results.AbstractBuildResultDto;
import model.dto.results.BaseBranchBuildResultDto;
import model.dto.results.BuildResultDeltaDto;
import model.dto.results.CheckstyleBuildResultItemDto;
import model.dto.results.ExecutionResultDto;
import model.dto.results.PairwiseBuildResultDto;
import model.dto.results.PmdBuildResultItemDto;
import model.dto.results.PullRequestBuildResultDto;
import model.dto.results.PullRequestBuildResultSummaryDto;
import model.dto.results.TestBuildResultItemDto;
import model.dto.results.TestBuildResultItemDto.FailureDto;

import com.google.inject.Inject;

public class ResultDtoFactoryImpl implements ResultDtoFactory {

  @Inject
  private DtoFactory dtoFactory;

  /**
   * {@inheritDoc}
   */
  @Override
  public BaseBranchBuildResultDto createBaseBranchBuildResultDto(final BaseBranchBuildResult baseBranchBuildResult) {
    final BaseBranchBuildResultDto baseBranchBuildResultDto = new BaseBranchBuildResultDto();
    fillAbstractBuildResultDto(baseBranchBuildResultDto, baseBranchBuildResult);
    return baseBranchBuildResultDto;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public PullRequestBuildResultDto createPullRequestBuildResultDto(final PullRequestBuildResult pullRequestBuildResult) {
    final PullRequestBuildResultDto pullRequestBuildResultDto = new PullRequestBuildResultDto();
    fillAbstractBuildResultDto(pullRequestBuildResultDto, pullRequestBuildResult);
    pullRequestBuildResultDto.pullRequest = dtoFactory.createPullRequestDto(pullRequestBuildResult.getPullRequest());
    pullRequestBuildResultDto.mergeable = pullRequestBuildResult.isMergeable();
    pullRequestBuildResultDto.delta = createBuildResultDeltaDto(pullRequestBuildResult.getBuildResultDelta());
    for (final PullRequest pullRequest : pullRequestBuildResult.mergeConflicts()) {
      pullRequestBuildResultDto.mergeConflicts.add(dtoFactory.createPullRequestDto(pullRequest));
    }
    for (final PullRequest pullRequest : pullRequestBuildResult.compilationConflicts()) {
      pullRequestBuildResultDto.compilationConflicts.add(dtoFactory.createPullRequestDto(pullRequest));
    }
    return pullRequestBuildResultDto;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public PullRequestBuildResultSummaryDto createPullRequestBuildResultSummaryDto(final PullRequestBuildResult pullRequestBuildResult) {
    final PullRequestBuildResultSummaryDto pullRequestBuildResultSummaryDto = new PullRequestBuildResultSummaryDto();
    pullRequestBuildResultSummaryDto.pullRequest = dtoFactory.createPullRequestDto(pullRequestBuildResult.getPullRequest());
    pullRequestBuildResultSummaryDto.mergeable = pullRequestBuildResult.isMergeable();
    pullRequestBuildResultSummaryDto.compileable = pullRequestBuildResult.isCompileable();
    pullRequestBuildResultSummaryDto.delta = createBuildResultDeltaDto(pullRequestBuildResult.getBuildResultDelta());
    for (final PullRequest pullRequest : pullRequestBuildResult.mergeConflicts()) {
      pullRequestBuildResultSummaryDto.mergeConflicts.add(dtoFactory.createPullRequestDto(pullRequest));
    }
    for (final PullRequest pullRequest : pullRequestBuildResult.compilationConflicts()) {
      pullRequestBuildResultSummaryDto.compilationConflicts.add(dtoFactory.createPullRequestDto(pullRequest));
    }
    return pullRequestBuildResultSummaryDto;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public PairwiseBuildResultDto createPairwiseBuildResultDto(final PairwiseBuildResult pairwiseBuildResult) {
    final PairwiseBuildResultDto pairwiseBuildResultDto = new PairwiseBuildResultDto();
    pairwiseBuildResultDto.a = dtoFactory.createPullRequestDto(pairwiseBuildResult.getPullRequestA());
    pairwiseBuildResultDto.b = dtoFactory.createPullRequestDto(pairwiseBuildResult.getPullRequestB());
    pairwiseBuildResultDto.mergeable = pairwiseBuildResult.isMergeable();
    pairwiseBuildResultDto.compileable = pairwiseBuildResult.isCompileable();
    pairwiseBuildResultDto.conflict = pairwiseBuildResult.hasConflict();
    return pairwiseBuildResultDto;
  }

  private void fillAbstractBuildResultDto(final AbstractBuildResultDto abstractBuildResultDto, final AbstractBuildResult abstractBuildResult) {
    abstractBuildResultDto.compileable = abstractBuildResult.isCompileable();
    for (final TestBuildResultItem testCase : abstractBuildResult.getTestCases()) {
      abstractBuildResultDto.testCases.add(createTestBuildResultItemDto(testCase));
    }
    for (final CheckstyleBuildResultItem checkstyleViolation : abstractBuildResult.getCheckstyleViolations()) {
      abstractBuildResultDto.checkstyleViolations.add(createCheckstyleBuildResultItemDto(checkstyleViolation));
    }
    for (final PmdBuildResultItem pmdViolation : abstractBuildResult.getPmdViolations()) {
      abstractBuildResultDto.pmdViolations.add(createPmdBuildResultItemDto(pmdViolation));
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public TestBuildResultItemDto createTestBuildResultItemDto(final TestBuildResultItem testBuildResultItem) {
    final TestBuildResultItemDto testBuildResultItemDto = new TestBuildResultItemDto();
    testBuildResultItemDto.className = testBuildResultItem.getClassName();
    testBuildResultItemDto.methodName = testBuildResultItem.getMethodName();
    testBuildResultItemDto.time = testBuildResultItem.getTime();
    final Failure testFailure = testBuildResultItem.getFailure();
    testBuildResultItemDto.success = (testFailure == null);
    if (testFailure != null) {
      testBuildResultItemDto.failure = new FailureDto(testBuildResultItem.getFailure().getType(), testBuildResultItem.getFailure().getMessage());
    }
    testBuildResultItemDto.className = testBuildResultItem.getClassName();
    return testBuildResultItemDto;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public CheckstyleBuildResultItemDto createCheckstyleBuildResultItemDto(final CheckstyleBuildResultItem checkstyleBuildResultItem) {
    final CheckstyleBuildResultItemDto checkstyleBuildResultItemDto = new CheckstyleBuildResultItemDto();
    checkstyleBuildResultItemDto.filename = checkstyleBuildResultItem.getFilename();
    checkstyleBuildResultItemDto.line = checkstyleBuildResultItem.getLine();
    checkstyleBuildResultItemDto.column = checkstyleBuildResultItem.getColumn();
    checkstyleBuildResultItemDto.severity = checkstyleBuildResultItem.getSeverity();
    checkstyleBuildResultItemDto.message = checkstyleBuildResultItem.getMessage();
    checkstyleBuildResultItemDto.source = checkstyleBuildResultItem.getSource();
    return checkstyleBuildResultItemDto;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public PmdBuildResultItemDto createPmdBuildResultItemDto(final PmdBuildResultItem pmdBuildResultItem) {
    final PmdBuildResultItemDto pmdBuildResultItemDto = new PmdBuildResultItemDto();
    pmdBuildResultItemDto.file = pmdBuildResultItem.getFile();
    pmdBuildResultItemDto.beginLine = pmdBuildResultItem.getBeginLine();
    pmdBuildResultItemDto.endLine = pmdBuildResultItem.getEndLine();
    pmdBuildResultItemDto.beginColumn = pmdBuildResultItem.getBeginColumn();
    pmdBuildResultItemDto.endColumn = pmdBuildResultItem.getEndColumn();
    pmdBuildResultItemDto.rule = pmdBuildResultItem.getRule();
    pmdBuildResultItemDto.ruleSet = pmdBuildResultItem.getRuleSet();
    pmdBuildResultItemDto.packageName = pmdBuildResultItem.getPackageName();
    pmdBuildResultItemDto.className = pmdBuildResultItem.getClassName();
    pmdBuildResultItemDto.methodName = pmdBuildResultItem.getMethodName();
    pmdBuildResultItemDto.externalInfoUrl = pmdBuildResultItem.getExternalInfoUrl();
    pmdBuildResultItemDto.priority = pmdBuildResultItem.getPriority();
    pmdBuildResultItemDto.content = pmdBuildResultItem.getContent();
    return pmdBuildResultItemDto;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public ExecutionResultDto createExecutionResultDto(final ExecutionResult executionResult) {
    final ExecutionResultDto executionResultDto = new ExecutionResultDto();
    for (final PullRequestBuildResult pullRequestBuildResult : executionResult.getPullRequestResults()) {
      final PullRequestBuildResultSummaryDto pullRequestBuildResultSummaryDto = createPullRequestBuildResultSummaryDto(pullRequestBuildResult);
      executionResultDto.pullRequestBuildResults.add(pullRequestBuildResultSummaryDto);
    }
    for (final PairwiseBuildResult pairwiseBuildResult : executionResult.getPairwiseResults()) {
      executionResultDto.pairwiseBuildResults.add(createPairwiseBuildResultDto(pairwiseBuildResult));
    }
    return executionResultDto;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public BuildResultDeltaDto createBuildResultDeltaDto(final BuildResultDelta buildResultDelta) {
    final BuildResultDeltaDto buildResultDeltaDto = new BuildResultDeltaDto();

    for (final TestBuildResultItem testBuildResultItem : buildResultDelta.getNewSuccessfulTests()) {
      buildResultDeltaDto.newSuccessfulTests.add(createTestBuildResultItemDto(testBuildResultItem));
    }
    buildResultDeltaDto.numbers.newSuccessfulTests = buildResultDelta.getNewSuccessfulTests().size();

    for (final TestBuildResultItem testBuildResultItem : buildResultDelta.getNewFailingTests()) {
      buildResultDeltaDto.newFailingTests.add(createTestBuildResultItemDto(testBuildResultItem));
    }
    buildResultDeltaDto.numbers.newFailingTests = buildResultDelta.getNewFailingTests().size();

    for (final TestBuildResultItem testBuildResultItem : buildResultDelta.getAddedSuccessfulTests()) {
      buildResultDeltaDto.addedSuccessfulTests.add(createTestBuildResultItemDto(testBuildResultItem));
    }
    buildResultDeltaDto.numbers.addedSuccessfulTests = buildResultDelta.getAddedSuccessfulTests().size();

    for (final TestBuildResultItem testBuildResultItem : buildResultDelta.getAddedFailingTests()) {
      buildResultDeltaDto.addedFailingTests.add(createTestBuildResultItemDto(testBuildResultItem));
    }
    buildResultDeltaDto.numbers.addedFailingTests = buildResultDelta.getAddedFailingTests().size();

    for (final TestBuildResultItem testBuildResultItem : buildResultDelta.getRemovedSuccessfulTests()) {
      buildResultDeltaDto.removedSuccessfulTests.add(createTestBuildResultItemDto(testBuildResultItem));
    }
    buildResultDeltaDto.numbers.removedSuccessfulTests = buildResultDelta.getRemovedSuccessfulTests().size();

    for (final TestBuildResultItem testBuildResultItem : buildResultDelta.getRemovedFailingTests()) {
      buildResultDeltaDto.removedFailingTests.add(createTestBuildResultItemDto(testBuildResultItem));
    }
    buildResultDeltaDto.numbers.removedFailingTests = buildResultDelta.getRemovedFailingTests().size();

    for (final CheckstyleBuildResultItem checkstyleBuildResultItem : buildResultDelta.getRemovedCheckstyleViolations()) {
      buildResultDeltaDto.removedCheckstyleViolations.add(createCheckstyleBuildResultItemDto(checkstyleBuildResultItem));
    }
    buildResultDeltaDto.numbers.removedCheckstyleViolations = buildResultDelta.getRemovedCheckstyleViolations().size();

    for (final CheckstyleBuildResultItem checkstyleBuildResultItem : buildResultDelta.getAddedCheckstyleViolations()) {
      buildResultDeltaDto.addedCheckstyleViolations.add(createCheckstyleBuildResultItemDto(checkstyleBuildResultItem));
    }
    buildResultDeltaDto.numbers.addedCheckstyleViolations = buildResultDelta.getAddedCheckstyleViolations().size();

    for (final PmdBuildResultItem pmdBuildResultItem : buildResultDelta.getRemovedPmdViolations()) {
      buildResultDeltaDto.removedPmdViolations.add(createPmdBuildResultItemDto(pmdBuildResultItem));
    }
    buildResultDeltaDto.numbers.removedPmdViolations = buildResultDelta.getRemovedPmdViolations().size();

    for (final PmdBuildResultItem pmdBuildResultItem : buildResultDelta.getAddedPmdViolations()) {
      buildResultDeltaDto.addedPmdViolations.add(createPmdBuildResultItemDto(pmdBuildResultItem));
    }
    buildResultDeltaDto.numbers.addedPmdViolations = buildResultDelta.getAddedPmdViolations().size();

    return buildResultDeltaDto;
  }

}
