package model.dto;

import model.dto.base.AbstractIdentifiableDto;

/**
 * Data transfer object for a user.
 */
public class UserDto extends AbstractIdentifiableDto {

  public String username;

  public String url;

  public String avatarUrl;

  public UserDto() {
    // do nothing
  }

}
