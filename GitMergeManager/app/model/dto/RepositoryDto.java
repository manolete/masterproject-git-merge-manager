package model.dto;

import java.util.ArrayList;
import java.util.List;

import model.dto.base.AbstractIdentifiableDto;

/**
 * Data transfer object for a pull request.
 */
public class RepositoryDto extends AbstractIdentifiableDto {

  public String name;

  public String owner;

  public List<String> branches;

  public List<String> pullRequests;

  public List<String> mergeAnalyses;

  public String cloneUrl;

  public RepositoryDto() {
    branches = new ArrayList<String>();
    pullRequests = new ArrayList<String>();
    mergeAnalyses = new ArrayList<String>();
  }

}
