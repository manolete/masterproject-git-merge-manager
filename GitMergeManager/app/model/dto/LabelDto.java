package model.dto;

import model.dto.base.AbstractIdentifiableDto;

public class LabelDto extends AbstractIdentifiableDto {

  public String name;

  public String url;

  public String color;

}
