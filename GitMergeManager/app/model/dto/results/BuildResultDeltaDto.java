package model.dto.results;

import java.util.List;

import com.google.common.collect.Lists;

public class BuildResultDeltaDto {

  /**
   * All test cases that are successful in the pull request, but not in the base branch.
   */
  public final List<TestBuildResultItemDto> newSuccessfulTests;

  /**
   * All test cases that fail when applying the pull request, but are successful on the base branch.
   */
  public final List<TestBuildResultItemDto> newFailingTests;

  /**
   * All successful test cases that were added in the pull request.
   */
  public final List<TestBuildResultItemDto> addedSuccessfulTests;

  /**
   * All failing test cases that were added in the pull request.
   */
  public final List<TestBuildResultItemDto> addedFailingTests;

  /**
   * All tests removed in the pull request that were successful on the base branch.
   */
  public final List<TestBuildResultItemDto> removedSuccessfulTests;

  /**
   * All tests removed in the pull request that were failing on the base branch.
   */
  public final List<TestBuildResultItemDto> removedFailingTests;

  /**
   * All checkstyle violations that are resolved when applying the pull request.
   */
  public final List<CheckstyleBuildResultItemDto> removedCheckstyleViolations;

  /**
   * All checkstyle violations that are introduced when applying the pull request.
   */
  public final List<CheckstyleBuildResultItemDto> addedCheckstyleViolations;

  /**
   * All PMD violations that are resolved when applying the pull request.
   */
  public final List<PmdBuildResultItemDto> removedPmdViolations;

  /**
   * All PMD violations that are introduced when applying the pull request.
   */
  public final List<PmdBuildResultItemDto> addedPmdViolations;

  public final NumbersDto numbers;

  public BuildResultDeltaDto() {
    this.newSuccessfulTests = Lists.newArrayList();
    this.newFailingTests = Lists.newArrayList();
    this.addedSuccessfulTests = Lists.newArrayList();
    this.addedFailingTests = Lists.newArrayList();
    this.removedSuccessfulTests = Lists.newArrayList();
    this.removedFailingTests = Lists.newArrayList();
    this.removedCheckstyleViolations = Lists.newArrayList();
    this.addedCheckstyleViolations = Lists.newArrayList();
    this.removedPmdViolations = Lists.newArrayList();
    this.addedPmdViolations = Lists.newArrayList();
    this.numbers = new NumbersDto();
  }

  /**
   * Stores the sizes of the lists with the respective name.
   */
  public static class NumbersDto {
    public int newSuccessfulTests;
    public int newFailingTests;
    public int addedSuccessfulTests;
    public int addedFailingTests;
    public int removedSuccessfulTests;
    public int removedFailingTests;

    public int removedCheckstyleViolations;
    public int addedCheckstyleViolations;

    public int removedPmdViolations;
    public int addedPmdViolations;
  }

}
