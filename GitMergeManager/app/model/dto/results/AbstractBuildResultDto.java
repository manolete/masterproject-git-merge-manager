package model.dto.results;

import java.util.List;

import com.google.common.collect.Lists;

public abstract class AbstractBuildResultDto {

  public boolean compileable;

  public List<TestBuildResultItemDto> testCases;

  public List<CheckstyleBuildResultItemDto> checkstyleViolations;

  public List<PmdBuildResultItemDto> pmdViolations;

  public AbstractBuildResultDto() {
    this.testCases = Lists.newArrayList();
    this.checkstyleViolations = Lists.newArrayList();
    this.pmdViolations = Lists.newArrayList();
  }

}
