package model.dto.results;

import java.util.List;

import com.google.common.collect.Lists;

public class ExecutionResultDto {

  public List<PullRequestBuildResultSummaryDto> pullRequestBuildResults;

  public List<PairwiseBuildResultDto> pairwiseBuildResults;

  public ExecutionResultDto() {
    this.pullRequestBuildResults = Lists.newArrayList();
    this.pairwiseBuildResults = Lists.newArrayList();
  }

}
