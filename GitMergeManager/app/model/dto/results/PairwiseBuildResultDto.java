package model.dto.results;

import model.dto.PullRequestDto;

public class PairwiseBuildResultDto {

  public PullRequestDto a;
  public PullRequestDto b;

  /**
   * If conflict is true, this means the pair is either not mergeable or not compileable.
   */
  public boolean conflict;

  public boolean mergeable;
  public boolean compileable;
}
