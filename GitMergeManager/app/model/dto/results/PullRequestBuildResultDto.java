package model.dto.results;

import java.util.List;

import com.google.common.collect.Lists;

import model.dto.PullRequestDto;

public class PullRequestBuildResultDto extends AbstractBuildResultDto {

  public PullRequestDto pullRequest;

  public boolean mergeable;

  public BuildResultDeltaDto delta;

  public List<PullRequestDto> mergeConflicts;
  public List<PullRequestDto> compilationConflicts;

  public PullRequestBuildResultDto() {
    this.mergeConflicts = Lists.newArrayList();
    this.compilationConflicts = Lists.newArrayList();
  }

}
