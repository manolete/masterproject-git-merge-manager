package model.dto.results;

public class CheckstyleBuildResultItemDto {

  public String filename;
  public int line;
  public int column;
  public String severity;
  public String message;
  public String source;

}
