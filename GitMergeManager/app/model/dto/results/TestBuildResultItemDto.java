package model.dto.results;

public class TestBuildResultItemDto {

  public String className;
  public String methodName;
  public double time;
  public boolean success;
  public FailureDto failure;

  public static class FailureDto {
    public String failureType;
    public String failureMessage;

    public FailureDto(final String failureType, final String failureMessage) {
      this.failureType = failureType;
      this.failureMessage = failureMessage;
    }
  }

}
