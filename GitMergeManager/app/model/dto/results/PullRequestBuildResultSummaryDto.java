package model.dto.results;

import java.util.List;

import com.google.common.collect.Lists;

import model.dto.PullRequestDto;

public class PullRequestBuildResultSummaryDto {

  public PullRequestDto pullRequest;

  public boolean mergeable;

  public boolean compileable;

  public BuildResultDeltaDto delta;

  public List<PullRequestDto> mergeConflicts;
  public List<PullRequestDto> compilationConflicts;

  public PullRequestBuildResultSummaryDto() {
    this.mergeConflicts = Lists.newArrayList();
    this.compilationConflicts = Lists.newArrayList();
  }

}
