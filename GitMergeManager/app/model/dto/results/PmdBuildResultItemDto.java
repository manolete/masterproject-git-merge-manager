package model.dto.results;

public class PmdBuildResultItemDto {

  public String file;
  public int beginLine;
  public int endLine;
  public int beginColumn;
  public int endColumn;
  public String rule;
  public String ruleSet;
  public String packageName;
  public String className;
  public String methodName;
  public String externalInfoUrl;
  public int priority;
  public String content;

}
