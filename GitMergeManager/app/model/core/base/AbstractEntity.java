package model.core.base;

import javax.persistence.MappedSuperclass;

import play.db.ebean.Model;

/**
 * Abstract base implementation of {@link Entity}.
 */
@MappedSuperclass
public abstract class AbstractEntity extends Model implements Entity {

  private static final long serialVersionUID = 1L;
}