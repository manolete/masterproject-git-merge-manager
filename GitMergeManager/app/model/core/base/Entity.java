package model.core.base;

/**
 * Base interface for all entities in the model.
 */
public interface Entity {

  public Long getId();

}
