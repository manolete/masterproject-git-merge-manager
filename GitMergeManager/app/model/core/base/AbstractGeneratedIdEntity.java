package model.core.base;

import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

/**
 * Abstract implementation of an entity that gets a generated id when being
 * constructed.
 */
@MappedSuperclass
public abstract class AbstractGeneratedIdEntity extends AbstractEntity implements Entity {

  private static final long serialVersionUID = 1L;

  @Id
  protected Long id;

  /**
   * Returns the generated id.
   */
  @Override
  public Long getId() {
    return id;
  }

}