package model.core.base;

/**
 * Interface for elements that provide a URL to a website displaying a representation of the respective model entity.
 */
public interface WebAccessibleEntity {

  public String getUrl();

}