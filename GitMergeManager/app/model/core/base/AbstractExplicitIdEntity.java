package model.core.base;

import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

/**
 * Abstract implementation of an entity that gets an explicit id when being
 * constructed.
 */
@MappedSuperclass
public abstract class AbstractExplicitIdEntity extends AbstractEntity implements Entity {

  private static final long serialVersionUID = 1L;

  @Id
  private final Long id;

  public AbstractExplicitIdEntity(final Long id) {
    this.id = id;
  }

  /**
   * Returns the explicit id.
   */
  @Override
  public Long getId() {
    return id;
  }

}