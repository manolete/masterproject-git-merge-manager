package model.core;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import model.core.base.AbstractGeneratedIdEntity;
import model.factories.PullRequestFactory;
import play.data.validation.Constraints.Required;

import com.google.common.collect.ImmutableList;

/**
 * Representation of a branch of a {@link Repository}.
 */
@Entity
@Table(name = "branches")
public class Branch extends AbstractGeneratedIdEntity {

  private static final long serialVersionUID = 1L;

  @Required
  @ManyToOne
  private Repository repository;

  @Required
  private String name;

  private String latestCommitSha;

  @OneToMany(mappedBy = "base")
  private List<PullRequest> basePullRequests;

  public Branch(final Repository repository, final String name) {
    this.repository = repository;
    this.name = name;
  }

  public Repository getRepository() {
    return repository;
  }

  public String getName() {
    return name;
  }

  public String getLatestCommitSha() {
    return latestCommitSha;
  }

  public void setLatestCommitSha(final String latestCommitSha) {
    this.latestCommitSha = latestCommitSha;
  }

  /**
   * Returns the {@link List} of {@link PullRequest}'s having this {@link Branch} as base.
   * 
   * @return {@link List} of {@link PullRequest}'s having this {@link Branch} as base
   */
  public List<PullRequest> getPullRequests() {
    return ImmutableList.copyOf(basePullRequests);
  }

  public PullRequest addPullRequest(final Long id, final String title, final String description, final BranchReference head, final String pullRequestNr) {
    final PullRequest pullRequest = PullRequestFactory.INSTANCE.create(id, title, description, this, head, pullRequestNr);
    pullRequest.save();
    return pullRequest;
  }

  public boolean removePullRequest(final PullRequest pullRequest) {
    // TODO B1: pull requests that are used in a merge analysis cannot be removed (that's why the list is checked to be empty. At the moment if a pull
    // request is used in a analysis , nothing is done. Discussion is required what has to be done in this case.
    if (pullRequest.getBase().equals(this) && pullRequest.getMergeAnalyses().isEmpty()) {
      pullRequest.delete();
      return true;
    }
    return false;
  }

  @Override
  public void save() {
    for (final PullRequest pullRequest : getPullRequests()) {
      pullRequest.save();
    }
    super.save();
  }

  @Override
  public void delete() {
    for (final PullRequest pullRequest : getPullRequests()) {
      pullRequest.delete();
    }
    super.delete();
  }
}