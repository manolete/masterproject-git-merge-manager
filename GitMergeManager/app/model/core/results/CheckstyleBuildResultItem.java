package model.core.results;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Lob;

/**
 * Checkstyle violation.
 */
@Entity
@DiscriminatorValue("checkstyle")
public class CheckstyleBuildResultItem extends AbstractBuildResultItem {

  private static final long serialVersionUID = 1L;

  private String filename;
  private int lineNr;
  private int columnNr;
  private String severity;
  @Lob
  private String message;
  private String source;

  public CheckstyleBuildResultItem(final String filename, final int lineNr, final int columnNr, final String severity, final String message, final String source) {
    this.filename = filename;
    this.lineNr = lineNr;
    this.columnNr = columnNr;
    this.severity = severity;
    this.message = message;
    this.source = source;
  }

  public String getFilename() {
    return filename;
  }

  public int getLine() {
    return lineNr;
  }

  public int getColumn() {
    return columnNr;
  }

  public String getSeverity() {
    return severity;
  }

  public String getMessage() {
    return message;
  }

  public String getSource() {
    return source;
  }

}
