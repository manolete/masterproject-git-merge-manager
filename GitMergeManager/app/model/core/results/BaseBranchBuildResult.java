package model.core.results;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * Result of cloning and building the base branch.
 */
@Entity
@DiscriminatorValue("base_branch")
public class BaseBranchBuildResult extends AbstractBuildResult {

  private static final long serialVersionUID = 1L;

  public BaseBranchBuildResult(final boolean compileable) {
    super(compileable);
  }

}
