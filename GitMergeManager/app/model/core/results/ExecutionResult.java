package model.core.results;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import model.core.MergeAnalysisExecution;
import model.core.PullRequest;
import model.core.base.AbstractGeneratedIdEntity;
import model.factories.ExecutionResultFactory;
import play.data.validation.Constraints.Required;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;

/**
 * This entity wraps all the results of a {@link MergeAnalysisExecution}.
 */
@Entity
@Table(name = "results_executions")
public class ExecutionResult extends AbstractGeneratedIdEntity {

  private static final long serialVersionUID = 1L;

  @Required
  @OneToOne
  private BaseBranchBuildResult baseBranchResult;

  @OneToMany(mappedBy = "containingExecutionResult")
  private List<PullRequestBuildResult> pullRequestResults;

  @OneToMany(mappedBy = "containingExecutionResult")
  private List<PairwiseBuildResult> pairwiseResults;

  public BaseBranchBuildResult getBaseBranchResult() {
    return baseBranchResult;
  }

  public void setBaseBranchResult(final BaseBranchBuildResult baseBranchResult) {
    this.baseBranchResult = baseBranchResult;
    save();
  }

  public List<PullRequestBuildResult> getPullRequestResults() {
    return ImmutableList.copyOf(pullRequestResults);
  }

  public PullRequestBuildResult getPullRequestBuildResult(final Long pullRequestNr) {
    for (final PullRequestBuildResult pullRequestBuildResult : getPullRequestResults()) {
      if (pullRequestBuildResult.getPullRequest().getNumber().equals(pullRequestNr.toString())) {
        return pullRequestBuildResult;
      }
    }
    return null;
  }

  public PullRequestBuildResult addPullRequestResult(final PullRequestBuildResult pullRequestBuildResult) {
    pullRequestBuildResult.setContainingExecutionResult(this);
    pullRequestBuildResult.save();
    save();
    return pullRequestBuildResult;
  }

  public List<PullRequest> getCompileablePullRequests() {
    final List<PullRequest> compileablePullRequests = Lists.newArrayList();
    for (final PullRequestBuildResult pullRequestBuildResult : getPullRequestResults()) {
      if (pullRequestBuildResult.isCompileable()) {
        compileablePullRequests.add(pullRequestBuildResult.getPullRequest());
      }
    }
    return compileablePullRequests;
  }

  public List<PairwiseBuildResult> getPairwiseResults() {
    return ImmutableList.copyOf(pairwiseResults);
  }

  public PairwiseBuildResult addPairwiseResult(final PullRequest a, final PullRequest b, final boolean mergeable, final boolean compileable) {
    final PairwiseBuildResult pairwiseBuildResult = ExecutionResultFactory.INSTANCE.createPairwiseBuildResult(this, a, b, mergeable, compileable);
    pairwiseBuildResult.save();
    return pairwiseBuildResult;
  }

  public List<PairwiseBuildResult> getMergeConflicts() {
    final List<PairwiseBuildResult> pairwiseBuildResults = Lists.newArrayList();
    for (final PairwiseBuildResult pairwiseBuildResult : getPairwiseResults()) {
      if (!pairwiseBuildResult.isMergeable()) {
        pairwiseBuildResults.add(pairwiseBuildResult);
      }
    }
    return pairwiseBuildResults;
  }

  public List<PairwiseBuildResult> getCompilationConflicts() {
    final List<PairwiseBuildResult> pairwiseBuildResults = Lists.newArrayList();
    for (final PairwiseBuildResult pairwiseBuildResult : getPairwiseResults()) {
      if (!pairwiseBuildResult.isCompileable()) {
        pairwiseBuildResults.add(pairwiseBuildResult);
      }
    }
    return pairwiseBuildResults;
  }

  public List<PullRequest> mergeConflictsOf(final PullRequest pullRequest) {
    final List<PullRequest> conflictingPullRequests = Lists.newArrayList();
    for (final PairwiseBuildResult pairwiseBuildResult : getMergeConflicts()) {
      if (pullRequest.equals(pairwiseBuildResult.getPullRequestA())) {
        conflictingPullRequests.add(pairwiseBuildResult.getPullRequestB());
      }
      else if (pullRequest.equals(pairwiseBuildResult.getPullRequestB())) {
        conflictingPullRequests.add(pairwiseBuildResult.getPullRequestA());
      }
    }
    return conflictingPullRequests;
  }

  public List<PullRequest> compilationConflictsOf(final PullRequest pullRequest) {
    final List<PullRequest> conflictingPullRequests = Lists.newArrayList();
    for (final PairwiseBuildResult pairwiseBuildResult : getCompilationConflicts()) {
      if (pullRequest.equals(pairwiseBuildResult.getPullRequestA())) {
        conflictingPullRequests.add(pairwiseBuildResult.getPullRequestB());
      }
      else if (pullRequest.equals(pairwiseBuildResult.getPullRequestB())) {
        conflictingPullRequests.add(pairwiseBuildResult.getPullRequestA());
      }
    }
    return conflictingPullRequests;
  }

  @Override
  public void delete() {
    baseBranchResult.delete();
    for (final PullRequestBuildResult pullRequestBuildResult : pullRequestResults) {
      pullRequestBuildResult.delete();
    }
    for (final PairwiseBuildResult pairwiseBuildResult : pairwiseResults) {
      pairwiseBuildResult.delete();
    }
    super.delete();
  }

}