package model.core.results;

import java.util.List;

import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import model.core.base.AbstractGeneratedIdEntity;
import play.data.validation.Constraints.Required;

import com.google.common.collect.ImmutableList;

/**
 * Abstract implementation of a build result.
 */
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "type", discriminatorType = DiscriminatorType.STRING)
@Table(name = "results_builds")
public abstract class AbstractBuildResult extends AbstractGeneratedIdEntity {

  private static final long serialVersionUID = 1L;

  @Required
  private boolean compileable;

  @OneToMany(mappedBy = "containingBuildResult")
  private List<TestBuildResultItem> testCases;

  @OneToMany(mappedBy = "containingBuildResult")
  private List<CheckstyleBuildResultItem> checkstyleViolations;

  @OneToMany(mappedBy = "containingBuildResult")
  private List<PmdBuildResultItem> pmdViolations;

  public AbstractBuildResult(final boolean compileable) {
    this.compileable = compileable;
  }

  public boolean isCompileable() {
    return compileable;
  }

  public List<TestBuildResultItem> getTestCases() {
    return ImmutableList.copyOf(testCases);
  }

  public TestBuildResultItem addTestCase(final TestBuildResultItem testBuildResultItem) {
    testBuildResultItem.setContainingBuildResult(this);
    testBuildResultItem.save();
    return testBuildResultItem;
  }

  public List<CheckstyleBuildResultItem> getCheckstyleViolations() {
    return ImmutableList.copyOf(checkstyleViolations);
  }

  public CheckstyleBuildResultItem addCheckstyleViolation(final CheckstyleBuildResultItem checkstyleBuildResultItem) {
    checkstyleBuildResultItem.setContainingBuildResult(this);
    checkstyleBuildResultItem.save();
    return checkstyleBuildResultItem;
  }

  public List<PmdBuildResultItem> getPmdViolations() {
    return ImmutableList.copyOf(pmdViolations);
  }

  public PmdBuildResultItem addPmdViolation(final PmdBuildResultItem pmdBuildResultItem) {
    pmdBuildResultItem.setContainingBuildResult(this);
    pmdBuildResultItem.save();
    return pmdBuildResultItem;
  }

  @Override
  public void delete() {
    for (final TestBuildResultItem testBuildResultItem : testCases) {
      testBuildResultItem.delete();
    }
    for (final CheckstyleBuildResultItem checkstyleBuildResultItem : checkstyleViolations) {
      checkstyleBuildResultItem.delete();
    }
    for (final PmdBuildResultItem pmdBuildResultItem : pmdViolations) {
      pmdBuildResultItem.delete();
    }
    super.delete();
  }

}
