package model.core.results.delta;

import java.util.List;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;

import model.core.results.BaseBranchBuildResult;
import model.core.results.CheckstyleBuildResultItem;
import model.core.results.PmdBuildResultItem;
import model.core.results.PullRequestBuildResult;
import model.core.results.TestBuildResultItem;

/**
 * This class contains the actual difference between a {@link BaseBranchBuildResult} and a {@link PullRequestBuildResult} of the same execution.
 */
public class BuildResultDelta {

  private List<TestBuildResultItem> newSuccessfulTests;
  private List<TestBuildResultItem> newFailingTests;
  private List<TestBuildResultItem> addedSuccessfulTests;
  private List<TestBuildResultItem> addedFailingTests;
  private List<TestBuildResultItem> removedSuccessfulTests;
  private List<TestBuildResultItem> removedFailingTests;

  private List<CheckstyleBuildResultItem> removedCheckstyleViolations;
  private List<CheckstyleBuildResultItem> addedCheckstyleViolations;

  private List<PmdBuildResultItem> removedPmdViolations;
  private List<PmdBuildResultItem> addedPmdViolations;

  public BuildResultDelta() {
    this.newSuccessfulTests = Lists.newArrayList();
    this.newFailingTests = Lists.newArrayList();
    this.addedSuccessfulTests = Lists.newArrayList();
    this.addedFailingTests = Lists.newArrayList();
    this.removedSuccessfulTests = Lists.newArrayList();
    this.removedFailingTests = Lists.newArrayList();
    this.removedCheckstyleViolations = Lists.newArrayList();
    this.addedCheckstyleViolations = Lists.newArrayList();
    this.removedPmdViolations = Lists.newArrayList();
    this.addedPmdViolations = Lists.newArrayList();
  }

  /**
   * Returns all test cases that are successful when applying the pull request, but fail on the base branch.
   * 
   * @return all test cases that are successful in the pull request, but not in the base branch
   */
  public List<TestBuildResultItem> getNewSuccessfulTests() {
    return ImmutableList.copyOf(newSuccessfulTests);
  }

  public void addNewSuccessfulTest(final TestBuildResultItem testBuildResultItem) {
    this.newSuccessfulTests.add(testBuildResultItem);
  }

  /**
   * Returns all test cases that fail when applying the pull request, but are successful on the base branch.
   * 
   * @return all test cases that fail when applying the pull request, but are successful on the base branch
   */
  public List<TestBuildResultItem> getNewFailingTests() {
    return ImmutableList.copyOf(newFailingTests);
  }

  public void addNewFailingTest(final TestBuildResultItem testBuildResultItem) {
    this.newFailingTests.add(testBuildResultItem);
  }

  /**
   * Returns all successful test cases that were added in the pull request.
   * 
   * @return all successful test cases that were added in the pull request
   */
  public List<TestBuildResultItem> getAddedSuccessfulTests() {
    return ImmutableList.copyOf(addedSuccessfulTests);
  }

  public void addAddedSuccessfulTest(final TestBuildResultItem testBuildResultItem) {
    addedSuccessfulTests.add(testBuildResultItem);
  }

  /**
   * Returns all failing test cases that were added in the pull request.
   * 
   * @return all failing test cases that were added in the pull request
   */
  public List<TestBuildResultItem> getAddedFailingTests() {
    return ImmutableList.copyOf(addedFailingTests);
  }

  public void addAddedFailingTest(final TestBuildResultItem testBuildResultItem) {
    this.addedFailingTests.add(testBuildResultItem);
  }

  /**
   * Returns all tests removed in the pull request that were successful on the base branch.
   * 
   * @return all tests removed in the pull request that were successful on the base branch
   */
  public List<TestBuildResultItem> getRemovedSuccessfulTests() {
    return ImmutableList.copyOf(removedSuccessfulTests);
  }

  public void addRemovedSuccessfulTest(final TestBuildResultItem testBuildResultItem) {
    this.removedSuccessfulTests.add(testBuildResultItem);
  }

  /**
   * Returns all tests removed in the pull request that were failing on the base branch.
   * 
   * @return all tests removed in the pull request that were failing on the base branch
   */
  public List<TestBuildResultItem> getRemovedFailingTests() {
    return ImmutableList.copyOf(removedFailingTests);
  }

  public void addRemovedFailingTest(final TestBuildResultItem testBuildResultItem) {
    this.removedFailingTests.add(testBuildResultItem);
  }

  /**
   * Returns all checkstyle violations that are resolved when applying the pull request.
   * 
   * @return all checkstyle violations that are resolved when applying the pull request
   */
  public List<CheckstyleBuildResultItem> getRemovedCheckstyleViolations() {
    return removedCheckstyleViolations;
  }

  public void addRemovedCheckstyleViolation(final CheckstyleBuildResultItem checkstyleBuildResultItem) {
    this.removedCheckstyleViolations.add(checkstyleBuildResultItem);
  }

  /**
   * Returns all checkstyle violations that are introduced when applying the pull request.
   * 
   * @return all checkstyle violations that are introduced when applying the pull request
   */
  public List<CheckstyleBuildResultItem> getAddedCheckstyleViolations() {
    return addedCheckstyleViolations;
  }

  public void addAddedCheckstyleViolation(final CheckstyleBuildResultItem checkstyleBuildResultItem) {
    this.addedCheckstyleViolations.add(checkstyleBuildResultItem);
  }

  /**
   * Returns all PMD violations that are resolved when applying the pull request.
   * 
   * @return all PMD violations that are resolved when applying the pull request
   */
  public List<PmdBuildResultItem> getRemovedPmdViolations() {
    return removedPmdViolations;
  }

  public void addRemovedPmdViolation(final PmdBuildResultItem pmdBuildResultItem) {
    this.removedPmdViolations.add(pmdBuildResultItem);
  }

  /**
   * Returns all PMD violations that are introduced when applying the pull request.
   * 
   * @return all PMD violations that are introduced when applying the pull request
   */
  public List<PmdBuildResultItem> getAddedPmdViolations() {
    return addedPmdViolations;
  }

  public void addAddedPmdViolation(final PmdBuildResultItem pmdBuildResultItem) {
    this.addedPmdViolations.add(pmdBuildResultItem);
  }

}
