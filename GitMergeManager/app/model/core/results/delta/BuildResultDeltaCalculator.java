package model.core.results.delta;

import model.core.results.AbstractBuildResultItem;
import model.core.results.BaseBranchBuildResult;
import model.core.results.PullRequestBuildResult;

/**
 * This class is responsible to calculate the delta between a {@link BaseBranchBuildResult} and a {@link PullRequestBuildResult}. The calculated
 * result is stored in a {@link BuildResultDelta} object.
 */
public interface BuildResultDeltaCalculator {

  /**
   * Calculates the delta (i.e. the difference) between a {@link BaseBranchBuildResult} and a {@link PullRequestBuildResult} regarding the different
   * {@link AbstractBuildResultItem}'s.
   * 
   * @param baseBranchBuildResult
   *          the {@link BaseBranchBuildResult}
   * @param pullRequestBuildResult
   *          the {@link PullRequestBuildResult}
   * @return a {@link BuildResultDelta} object containing the calculated delta
   */
  public BuildResultDelta delta(final BaseBranchBuildResult baseBranchBuildResult, final PullRequestBuildResult pullRequestBuildResult);

}
