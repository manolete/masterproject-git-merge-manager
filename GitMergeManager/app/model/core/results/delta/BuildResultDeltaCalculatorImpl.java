package model.core.results.delta;

import static model.core.results.delta.BuildResultItemComparator.areSame;

import java.util.Iterator;
import java.util.List;

import model.core.results.BaseBranchBuildResult;
import model.core.results.CheckstyleBuildResultItem;
import model.core.results.PmdBuildResultItem;
import model.core.results.PullRequestBuildResult;
import model.core.results.TestBuildResultItem;
import model.factories.ExecutionResultFactory;

import com.google.common.collect.Lists;

/**
 * Implementation of {@link BuildResultDeltaCalculator}.
 */
public class BuildResultDeltaCalculatorImpl implements BuildResultDeltaCalculator {

  private BuildResultDeltaCalculatorImpl() {}

  /**
   * {@inheritDoc}
   */
  @Override
  public BuildResultDelta delta(final BaseBranchBuildResult baseBranchBuildResult, final PullRequestBuildResult pullRequestBuildResult) {
    final BuildResultDelta buildResultDelta = ExecutionResultFactory.INSTANCE.createBuildResultDelta();
    addTestCaseDeltas(buildResultDelta, baseBranchBuildResult, pullRequestBuildResult);
    addCheckstyleViolations(buildResultDelta, baseBranchBuildResult, pullRequestBuildResult);
    addPmdViolations(buildResultDelta, baseBranchBuildResult, pullRequestBuildResult);
    return buildResultDelta;
  }

  private void addTestCaseDeltas(final BuildResultDelta buildResultDelta, final BaseBranchBuildResult baseBranchBuildResult, final PullRequestBuildResult pullRequestBuildResult) {
    final List<TestBuildResultItem> baseBranchTestItems = Lists.newArrayList(baseBranchBuildResult.getTestCases());
    for (final TestBuildResultItem pullRequestTestItem : pullRequestBuildResult.getTestCases()) {
      boolean isAdded = true;
      final Iterator<TestBuildResultItem> baseBranchTestItemsIt = baseBranchTestItems.iterator();
      while (baseBranchTestItemsIt.hasNext()) {
        final TestBuildResultItem baseBranchTestItem = baseBranchTestItemsIt.next();
        if (areSame(pullRequestTestItem, baseBranchTestItem)) {
          if (pullRequestTestItem.isSuccessful() && !baseBranchTestItem.isSuccessful()) {
            buildResultDelta.addNewSuccessfulTest(pullRequestTestItem);
          }
          else if (!pullRequestTestItem.isSuccessful() && baseBranchTestItem.isSuccessful()) {
            buildResultDelta.addNewFailingTest(pullRequestTestItem);
          }
          baseBranchTestItemsIt.remove();
          isAdded = false;
          break;
        }
      }
      if (isAdded) {
        if (pullRequestTestItem.isSuccessful()) {
          buildResultDelta.addAddedSuccessfulTest(pullRequestTestItem);
        }
        else {
          buildResultDelta.addAddedFailingTest(pullRequestTestItem);
        }
      }
    }
    for (final TestBuildResultItem testBuildResultItem : baseBranchTestItems) {
      if (testBuildResultItem.isSuccessful()) {
        buildResultDelta.addRemovedSuccessfulTest(testBuildResultItem);
      }
      else {
        buildResultDelta.addRemovedFailingTest(testBuildResultItem);
      }
    }
  }

  private void addCheckstyleViolations(final BuildResultDelta buildResultDelta, final BaseBranchBuildResult baseBranchBuildResult, final PullRequestBuildResult pullRequestBuildResult) {
    final List<CheckstyleBuildResultItem> baseBranchCheckstyleItems = Lists.newArrayList(baseBranchBuildResult.getCheckstyleViolations());
    for (final CheckstyleBuildResultItem pullRequestCheckstyleItem : pullRequestBuildResult.getCheckstyleViolations()) {
      boolean isAdded = true;
      final Iterator<CheckstyleBuildResultItem> baseBranchCheckstyleItemsIt = baseBranchCheckstyleItems.iterator();
      while (baseBranchCheckstyleItemsIt.hasNext()) {
        final CheckstyleBuildResultItem baseBranchCheckstyleItem = baseBranchCheckstyleItemsIt.next();
        if (areSame(pullRequestCheckstyleItem, baseBranchCheckstyleItem)) {
          baseBranchCheckstyleItemsIt.remove();
          isAdded = false;
          break;
        }
      }
      if (isAdded) {
        buildResultDelta.addAddedCheckstyleViolation(pullRequestCheckstyleItem);
      }
    }
    for (final CheckstyleBuildResultItem checkstyleBuildResultItem : baseBranchCheckstyleItems) {
      buildResultDelta.addRemovedCheckstyleViolation(checkstyleBuildResultItem);
    }
  }

  private void addPmdViolations(final BuildResultDelta buildResultDelta, final BaseBranchBuildResult baseBranchBuildResult, final PullRequestBuildResult pullRequestBuildResult) {
    final List<PmdBuildResultItem> baseBranchPmdItems = Lists.newArrayList(baseBranchBuildResult.getPmdViolations());
    for (final PmdBuildResultItem pullRequestPmdItem : pullRequestBuildResult.getPmdViolations()) {
      boolean isAdded = true;
      final Iterator<PmdBuildResultItem> baseBranchPmdItemsIt = baseBranchPmdItems.iterator();
      while (baseBranchPmdItemsIt.hasNext()) {
        final PmdBuildResultItem baseBranchPmdItem = baseBranchPmdItemsIt.next();
        if (areSame(pullRequestPmdItem, baseBranchPmdItem)) {
          baseBranchPmdItemsIt.remove();
          isAdded = false;
          break;
        }
      }
      if (isAdded) {
        buildResultDelta.addAddedPmdViolation(pullRequestPmdItem);
      }
    }
    for (final PmdBuildResultItem pmdBuildResultItem : baseBranchPmdItems) {
      buildResultDelta.addRemovedPmdViolation(pmdBuildResultItem);
    }
  }

  public static BuildResultDeltaCalculatorImpl create() {
    return new BuildResultDeltaCalculatorImpl();
  }

}
