package model.core.results.delta;

import play.db.ebean.Model;
import model.core.results.AbstractBuildResultItem;
import model.core.results.CheckstyleBuildResultItem;
import model.core.results.PmdBuildResultItem;
import model.core.results.TestBuildResultItem;

/**
 * Provides methods to compare {@link AbstractBuildResultItem}'s. This is required to compute the {@link BuildResultDelta}.
 * 
 * Using the methods here avoids overriding the equals-method of the {@link Model} class in the subclasses of {@link AbstractBuildResultItem}'s. The
 * equals method in {@link Model} compares entities by their id's and this makes perfectly sense in some cases. The comparison done here in contrast
 * do not check for 'real' equality but only take relevant attributes for the delta calculation.
 */
public class BuildResultItemComparator {

  /**
   * Compares two {@link TestBuildResultItem}'s
   * 
   * @param a
   *          {@link TestBuildResultItem}
   * @param b
   *          {@link TestBuildResultItem}
   * @return true if the relevant attributes of the two {@link TestBuildResultItem}'s are the same, false otherwise
   */
  public static boolean areSame(final TestBuildResultItem a, final TestBuildResultItem b) {
    return a.getClassName().equals(b.getClassName()) && a.getMethodName().equals(b.getMethodName());
  }

  /**
   * Compares two {@link CheckstyleBuildResultItem}'s.
   * 
   * @param a
   *          {@link CheckstyleBuildResultItem}
   * @param b
   *          {@link CheckstyleBuildResultItem}
   * @return true if the relevant attributes of the two {@link CheckstyleBuildResultItem}'s are the same, false otherwise
   */
  public static boolean areSame(final CheckstyleBuildResultItem a, final CheckstyleBuildResultItem b) {
    return a.getFilename().equals(b.getFilename()) && a.getLine() == b.getLine() && a.getColumn() == b.getColumn() && a.getSeverity().equals(b.getSeverity()) && a.getMessage().equals(b.getMessage())
        && a.getSource().equals(b.getSource());
  }

  /**
   * Compares two {@link PmdBuildResultItem}'s.
   * 
   * @param a
   *          {@link PmdBuildResultItem}
   * @param b
   *          {@link PmdBuildResultItem}
   * @return true if the relevant attributes of the two {@link PmdBuildResultItem}'s are the same, false otherwise
   */
  public static boolean areSame(final PmdBuildResultItem a, final PmdBuildResultItem b) {
    return a.getFile().equals(b.getFile()) && a.getBeginLine() == b.getBeginLine() && a.getEndLine() == b.getEndLine() && a.getBeginColumn() == b.getBeginColumn()
        && a.getEndColumn() == b.getEndColumn() && a.getRule().equals(b.getRule()) && a.getRuleSet().equals(b.getRuleSet()) && a.getPackageName().equals(b.getPackageName())
        && a.getClassName().equals(b.getClassName()) && a.getMethodName().equals(b.getMethodName());
  }

}
