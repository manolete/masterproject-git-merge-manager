package model.core.results;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import model.core.PullRequest;
import model.core.base.AbstractGeneratedIdEntity;
import play.data.validation.Constraints.Required;

/**
 * Result of merging and building two pull requests.
 */
@Entity
@Table(name = "results_pairwise")
public class PairwiseBuildResult extends AbstractGeneratedIdEntity {

  private static final long serialVersionUID = 1L;

  @ManyToOne
  private ExecutionResult containingExecutionResult;

  @Required
  @ManyToOne
  private PullRequest pullRequestA;

  @Required
  @ManyToOne
  private PullRequest pullRequestB;

  @Required
  private boolean mergeable;

  @Required
  private boolean compileable;

  public PairwiseBuildResult(final ExecutionResult containingExecutionResult, final PullRequest pullRequestA, final PullRequest pullRequestB, final boolean mergeable, final boolean compileable) {
    this.containingExecutionResult = containingExecutionResult;
    this.pullRequestA = pullRequestA;
    this.pullRequestB = pullRequestB;
    this.mergeable = mergeable;
    this.compileable = compileable;
  }

  public PullRequest getPullRequestA() {
    return pullRequestA;
  }

  public PullRequest getPullRequestB() {
    return pullRequestB;
  }

  public boolean hasConflict() {
    return !isMergeable() || !isCompileable();
  }

  public boolean isMergeable() {
    return mergeable;
  }

  public boolean isCompileable() {
    return compileable;
  }

}
