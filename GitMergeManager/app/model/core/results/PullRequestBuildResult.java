package model.core.results;

import java.util.List;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

import model.core.PullRequest;
import model.core.results.delta.BuildResultDelta;
import model.core.results.delta.BuildResultDeltaCalculatorImpl;
import model.factories.ExecutionResultFactory;
import play.data.validation.Constraints.Required;

/**
 * Result of merging and building a pull request.
 */
@Entity
@DiscriminatorValue("pull_request")
public class PullRequestBuildResult extends AbstractBuildResult {

  private static final long serialVersionUID = 1L;

  @ManyToOne
  private ExecutionResult containingExecutionResult;

  @Required
  @OneToOne
  private PullRequest pullRequest;

  @Required
  private boolean mergeable;

  @Transient
  private BuildResultDelta cachedBuildResultDelta;

  public PullRequestBuildResult(final PullRequest pullRequest, final boolean mergeable, final boolean compileable) {
    super(compileable);
    this.pullRequest = pullRequest;
    this.mergeable = mergeable;
  }

  public ExecutionResult getContainingExecutionResult() {
    return containingExecutionResult;
  }

  public void setContainingExecutionResult(final ExecutionResult containingExecutionResult) {
    this.containingExecutionResult = containingExecutionResult;
  }

  public PullRequest getPullRequest() {
    return pullRequest;
  }

  public boolean isMergeable() {
    return mergeable;
  }

  public BuildResultDelta getBuildResultDelta() {
    if (cachedBuildResultDelta == null) {
      if (isCompileable()) {
        cachedBuildResultDelta = BuildResultDeltaCalculatorImpl.create().delta(getContainingExecutionResult().getBaseBranchResult(), this);
      }
      else {
        cachedBuildResultDelta = ExecutionResultFactory.INSTANCE.createBuildResultDelta();
      }
    }
    return cachedBuildResultDelta;
  }

  public List<PullRequest> mergeConflicts() {
    return getContainingExecutionResult().mergeConflictsOf(getPullRequest());
  }

  public List<PullRequest> compilationConflicts() {
    return getContainingExecutionResult().compilationConflictsOf(getPullRequest());
  }
}
