package model.core.results;

import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import model.core.base.AbstractGeneratedIdEntity;

/**
 * Base class for result items (e.g. test cases) fetched from maven execution.
 */
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "type", discriminatorType = DiscriminatorType.STRING)
@Table(name = "results_builds_items")
public abstract class AbstractBuildResultItem extends AbstractGeneratedIdEntity {

  private static final long serialVersionUID = 1L;

  @ManyToOne
  private AbstractBuildResult containingBuildResult;

  public AbstractBuildResult getContainingBuildResult() {
    return containingBuildResult;
  }

  public void setContainingBuildResult(final AbstractBuildResult containingBuildResult) {
    this.containingBuildResult = containingBuildResult;
  }

}
