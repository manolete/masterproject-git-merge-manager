package model.core.results;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Lob;

/**
 * Result of an executed test case.
 */
@Entity
@DiscriminatorValue("test")
public class TestBuildResultItem extends AbstractBuildResultItem {

  private static final long serialVersionUID = 1L;

  private String className;
  private String methodName;
  private double time;
  @Embedded
  private Failure failure;

  public TestBuildResultItem(final String className, final String methodName, final double time, final Failure failure) {
    this.time = time;
    this.className = className;
    this.methodName = methodName;
    this.failure = failure;
  }

  public String getClassName() {
    return className;
  }

  public String getMethodName() {
    return methodName;
  }

  public double getTime() {
    return time;
  }

  public boolean isSuccessful() {
    return failure == null;
  }

  public Failure getFailure() {
    return failure;
  }

  @Embeddable
  public static class Failure {
    private final String failureType;
    @Lob
    private final String failureMessage;

    public Failure(final String failureType, final String failureMessage) {
      this.failureType = failureType;
      this.failureMessage = failureMessage;
    }

    public String getType() {
      return failureType;
    }

    public String getMessage() {
      return failureMessage;
    }
  }

}
