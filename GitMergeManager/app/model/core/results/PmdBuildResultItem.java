package model.core.results;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Lob;

/**
 * PMD violation.
 */
@Entity
@DiscriminatorValue("pmd")
public class PmdBuildResultItem extends AbstractBuildResultItem {

  private static final long serialVersionUID = 1L;

  private String file;
  private int beginLine;
  private int endLine;
  private int beginColumn;
  private int endColumn;
  private String rule;
  private String ruleSet;
  private String packageName;
  private String className;
  private String methodName;
  private String externalInfoUrl;
  private int priority;
  @Lob
  private String content;

  public PmdBuildResultItem(final String file, final int beginLine, final int endLine, final int beginColumn, final int endColumn, final String rule, final String ruleSet, final String packageName,
      final String className, final String methodName, final String externalInfoUrl, final int priority, final String content) {
    this.file = file;
    this.beginLine = beginLine;
    this.endLine = endLine;
    this.beginColumn = beginColumn;
    this.endColumn = endColumn;
    this.rule = rule;
    this.ruleSet = ruleSet;
    this.packageName = packageName;
    this.className = className;
    this.methodName = methodName;
    this.externalInfoUrl = externalInfoUrl;
    this.priority = priority;
    this.content = content;
  }

  public String getFile() {
    return file;
  }

  public int getBeginLine() {
    return beginLine;
  }

  public int getEndLine() {
    return endLine;
  }

  public int getBeginColumn() {
    return beginColumn;
  }

  public int getEndColumn() {
    return endColumn;
  }

  public String getRule() {
    return rule;
  }

  public String getRuleSet() {
    return ruleSet;
  }

  public String getPackageName() {
    return packageName;
  }

  public String getClassName() {
    return className;
  }

  public String getMethodName() {
    return methodName;
  }

  public String getExternalInfoUrl() {
    return externalInfoUrl;
  }

  public int getPriority() {
    return priority;
  }

  public String getContent() {
    return content;
  }

}
