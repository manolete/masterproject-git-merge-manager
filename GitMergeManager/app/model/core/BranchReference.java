package model.core;

import javax.persistence.Embeddable;

@Embeddable
public class BranchReference {

  private String repositoryOwnerUsername;

  private String repositoryName;

  private String branchName;

  private String cloneUrl;

  public BranchReference(final String repositoryOwnerUsername, final String repositoryName, final String branchName, final String cloneUrl) {
    this.repositoryOwnerUsername = repositoryOwnerUsername;
    this.repositoryName = repositoryName;
    this.branchName = branchName;
    this.cloneUrl = cloneUrl;
  }

  public String getRepositoryOwnerUsername() {
    return repositoryOwnerUsername;
  }

  public String getRepositoryName() {
    return repositoryName;
  }

  public String getBranchName() {
    return branchName;
  }

  public String getCloneUrl() {
    return cloneUrl;
  }
}
