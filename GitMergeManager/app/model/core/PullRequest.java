package model.core;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.PersistenceException;
import javax.persistence.Table;
import javax.persistence.Transient;

import model.core.base.AbstractExplicitIdEntity;
import model.core.base.WebAccessibleEntity;
import model.repositories.impl.AbstractBaseRepositoryImpl;
//import model.core.util.ResolvedIssueParser;
import play.data.validation.Constraints.Required;
import util.StringUtil;

import com.google.common.collect.ImmutableList;

/**
 * Representation of a pull request from one {@link Branch} to another. The two {@link Branch}'s can either be located on the same {@link Repository}
 * or on different ones.
 */
@Entity
@Table(name = "pull_requests")
public class PullRequest extends AbstractExplicitIdEntity implements WebAccessibleEntity {

  private static final long serialVersionUID = 1L;

  @Transient
  private static final LabelRepository labelRepository = new LabelRepository();

  @Required
  private String title;

  private String url;

  @Lob
  private String description;

  @Required
  @ManyToOne
  private Branch base;

  @Embedded
  private BranchReference head;

  @Required
  private String number;

  private boolean isMergeable;

  private String referencedIssueNumber;

  @ManyToMany
  @JoinTable(name = "labels_pull_requests")
  private List<Label> labels = new ArrayList<Label>();

  @ManyToMany
  @JoinTable(name = "labels_issues")
  private List<Label> referencedIssueLabels = new ArrayList<Label>();

  @ManyToMany(mappedBy = "pullRequests")
  private List<MergeAnalysis> mergeAnalyses;

  public PullRequest(final Long id, final String title, final String description, final Branch base, final BranchReference head, final String number) {
    super(id);
    setTitle(title);
    setDescription(description);
    this.base = base;
    this.head = head;
    this.number = number;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(final String title) {
    this.title = title;
  }

  @Override
  public String getUrl() {
    return url;
  }

  public void setUrl(final String url) {
    this.url = url;
  }

  public String getDescription() {
    if (this.description != null) {
      return description;
    }
    else {
      return StringUtil.EMPTY;
    }
  }

  public void setDescription(final String description) {
    this.description = description;
  }

  /**
   * The head branch is what should be applied. It is only stored as a {@link BranchReference}, this prohibits circular dependencies between
   * repositories.
   */
  public BranchReference getHead() {
    return head;
  }

  /**
   * the base {@link Branch} is the one on which changes should be applied
   */
  public Branch getBase() {
    return base;
  }

  public String getNumber() {
    return number;
  }

  public boolean isMergeable() {
    return isMergeable;
  }

  public void setIsMergeable(final boolean isMergeable) {
    this.isMergeable = isMergeable;
  }

  public Label addLabel(final Label label) {
    final Label existingLabel = labelRepository.one(label.getUrl());
    final Label newLabel = existingLabel != null ? existingLabel : label;
    labelRepository.store(newLabel);
    labels.add(newLabel);
    newLabel.save();
    save();
    return newLabel;
  }

  public void removeLabel(final Label label) {
    labels.remove(label);
    save();
  }

  public List<Label> getLabels() {
    return ImmutableList.copyOf(labels);
  }

  public void clearLabels() {
    labels.clear();
    save();
  }

  /**
   * The description of a {@link PullRequest} can contain a reference to an issue that is resolved. This can be done using one of the following
   * notations: {close, closes, closed, fix, fixes, fixed, resolve, resolves, resolved} followed by the issue number prefixed with a hash tag.
   * 
   * @return issue number of the issue resolved by this {@link PullRequest}
   */
  public String getReferencedIssueNumber() {
    return referencedIssueNumber;
  }

  public Label addReferencedIssueLabel(final Label label) {
    final Label existingLabel = labelRepository.one(label.getUrl());
    final Label newLabel = existingLabel != null ? existingLabel : label;
    labelRepository.store(newLabel);
    referencedIssueLabels.add(newLabel);
    newLabel.save();
    save();
    return newLabel;
  }

  public void removeReferencedIssueLabel(final Label label) {
    referencedIssueLabels.remove(label);
    save();
  }

  public List<Label> getReferencedIssueLabels() {
    return ImmutableList.copyOf(referencedIssueLabels);
  }

  public void clearReferencedIssueLabels() {
    referencedIssueLabels.clear();
    save();
  }

  public void setReferencedIssueNumber(final String referencedIssueNumber) {
    this.referencedIssueNumber = referencedIssueNumber;
  }

  public List<MergeAnalysis> getMergeAnalyses() {
    return ImmutableList.copyOf(mergeAnalyses);
  }

  private static class LabelRepository extends AbstractBaseRepositoryImpl<Label> {
    private final Finder<Long, Label> finder = new Finder<Long, Label>(Long.class, Label.class);

    @Override
    protected Finder<Long, Label> finder() {
      return finder;
    }

    public Label one(final String url) {
      try {
        final Label label = finder().query().where().eq("url", url).findUnique();
        return label;
      }
      catch (final PersistenceException e) {
        return null;
      }
    }
  }
}
