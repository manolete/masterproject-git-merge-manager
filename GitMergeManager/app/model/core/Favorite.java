package model.core;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import model.core.base.AbstractGeneratedIdEntity;
import play.data.validation.Constraints.Required;

/**
 * Stores the time a user lastly accessed a {@link Repository}.
 */
@Entity
@Table(name = "favorites")
public class Favorite extends AbstractGeneratedIdEntity {

  private static final long serialVersionUID = 1L;

  @ManyToOne
  private Repository repository;

  @ManyToOne
  private User user;

  @Required
  private Timestamp lastAccessed;

  public Favorite(final User user, final Repository repository) {
    this.user = user;
    this.repository = repository;
  }

  public User getUser() {
    return user;
  }

  public Repository getRepository() {
    return repository;
  }

  public Timestamp getLastAccessed() {
    return lastAccessed;
  }

  public void setLastAccessed(final Timestamp lastAccessed) {
    this.lastAccessed = lastAccessed;
  }

}