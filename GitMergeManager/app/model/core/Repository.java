package model.core;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import model.core.base.AbstractExplicitIdEntity;
import model.factories.BranchFactory;
import model.factories.MergeAnalysisFactory;
import play.data.validation.Constraints.Required;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableListMultimap;
import com.google.common.collect.ListMultimap;

/**
 * Representation of a git repository.
 */
@Entity
@Table(name = "repositories")
public class Repository extends AbstractExplicitIdEntity {

  private static final long serialVersionUID = 1L;

  private static final String SLASH = "/";

  @ManyToOne
  private User owner;

  @Required
  private String name;

  private String cloneUrl;

  @OneToMany(mappedBy = "repository")
  private List<Branch> branches;

  @OneToMany(mappedBy = "repository")
  private List<MergeAnalysis> mergeAnalysis;

  public Repository(final Long id, final User owner, final String name, final String cloneUrl) {
    super(id);
    setName(name);
    this.owner = owner;
    this.cloneUrl = cloneUrl;
  }

  public User getOwner() {
    return owner;
  }

  public String getName() {
    return name;
  }

  public String getQualifiedName() {
    return getOwner().getUsername() + SLASH + getName();
  }

  public void setName(final String name) {
    this.name = name;
  }

  public String getCloneUrl() {
    return cloneUrl;
  }

  public List<Branch> getBranches() {
    return ImmutableList.copyOf(branches);
  }

  public Branch getBranch(final String name) {
    for (final Branch branch : getBranches()) {
      if (branch.getName().equals(name)) {
        return branch;
      }
    }
    return null;
  }

  public Branch addBranch(final String name) {
    final Branch branch = BranchFactory.INSTANCE.create(this, name);
    branch.save();
    return branch;
  }

  public boolean removeBranch(final Branch branch) {
    if (branch.getRepository().equals(this)) {
      branch.delete();
      return true;
    }
    return false;
  }

  /**
   * Returns the {@link List} of {@link PullRequest}'s having a {@link Branch} of this {@link Repository} as base.
   * 
   * @return {@link List} of {@link PullRequest}'s having a {@link Branch} of this {@link Repository} as base
   */
  public List<PullRequest> getPullRequests() {
    final List<PullRequest> pullRequests = new ArrayList<PullRequest>();
    for (final Branch branch : getBranches()) {
      for (final PullRequest pullRequest : branch.getPullRequests()) {
        pullRequests.add(pullRequest);
      }
    }
    return pullRequests;
  }

  public PullRequest getPullRequest(final String number) {
    for (final PullRequest pullRequest : getPullRequests()) {
      if (pullRequest.getNumber().equals(number)) {
        return pullRequest;
      }
    }
    return null;
  }

  /**
   * Returns the {@link PullRequest}'s sorted by the Issue they reference.
   * 
   * @return {@link ListMultimap} containing Lists of pull requests per issue number
   */
  public ListMultimap<String, PullRequest> getPullRequestsPerIssue() {
    final ListMultimap<String, PullRequest> pullRequestsPerIssue = ArrayListMultimap.create();
    for (final PullRequest pullRequest : getPullRequests()) {
      pullRequestsPerIssue.put(pullRequest.getReferencedIssueNumber(), pullRequest);
    }
    return ImmutableListMultimap.copyOf(pullRequestsPerIssue);
  }

  public List<MergeAnalysis> getMergeAnalysis() {
    return ImmutableList.copyOf(mergeAnalysis);
  }

  public MergeAnalysis getMergeAnalysis(final Long id) {
    for (final MergeAnalysis mergeAnalysis : getMergeAnalysis()) {
      if (mergeAnalysis.getId().equals(id)) {
        return mergeAnalysis;
      }
    }
    return null;
  }

  public MergeAnalysis addMergeAnalysis(final String name, final User creator) {
    final MergeAnalysis mergeAnalysis = MergeAnalysisFactory.INSTANCE.create(this, name, creator);
    mergeAnalysis.save();
    return mergeAnalysis;
  }

  public boolean removeMergeAnalysis(final MergeAnalysis mergeAnalysis) {
    if (mergeAnalysis.getRepository().equals(this)) {
      mergeAnalysis.delete();
      return true;
    }
    return false;
  }

  public boolean isEmpty() {
    return getBranches().size() == 0;
  }

  /**
   * Checks whether a {@link User} has the permission to merge a pull request on this {@link Repository}.
   * 
   * @param user
   * @return
   */
  boolean hasPermissionToMergePullRequest(final User user) {
    if (this.owner.getId() == user.getId())
      return true;
    else
      return false;
  }

  @Override
  public void save() {
    getOwner().save();
    for (final Branch branch : getBranches()) {
      branch.save();
    }
    for (final MergeAnalysis mergeAnalysis : getMergeAnalysis()) {
      mergeAnalysis.save();
    }
    super.save();
  }

  @Override
  public void delete() {
    for (final Branch branch : getBranches()) {
      branch.delete();
    }
    for (final MergeAnalysis mergeAnalysis : getMergeAnalysis()) {
      mergeAnalysis.delete();
    }
    super.delete();
  }
}
