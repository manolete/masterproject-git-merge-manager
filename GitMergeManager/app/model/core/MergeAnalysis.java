package model.core;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import model.core.base.AbstractGeneratedIdEntity;
import model.factories.MergeAnalysisExecutionFactory;

import org.joda.time.DateTime;

import play.data.validation.Constraints.Required;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;

/**
 * Specifies the properties of an analysis that is done on a specific {@link Repository}.
 */
@Entity
@Table(name = "merge_analyses")
public class MergeAnalysis extends AbstractGeneratedIdEntity {

  private static final long serialVersionUID = 1L;

  @Required
  @ManyToOne
  private Repository repository;

  @Required
  private String name;

  @Required
  @ManyToOne
  private User creator;

  @ManyToMany
  private List<PullRequest> pullRequests = new ArrayList<PullRequest>();

  @OneToMany(mappedBy = "mergeAnalysis")
  private List<MergeAnalysisExecution> mergeAnalysisExecutions;

  public MergeAnalysis(final Repository repository, final String name, final User creator) {
    setName(name);
    this.repository = repository;
    this.creator = creator;
  }

  public Repository getRepository() {
    return repository;
  }

  public String getName() {
    return name;
  }

  public void setName(final String name) {
    this.name = name;
  }

  public User getCreator() {
    return creator;
  }

  public Branch getBaseBranch() {
    if (!getPullRequests().isEmpty()) {
      return getPullRequests().get(0).getBase();
    }
    return null;
  }

  public List<PullRequest> getPullRequests() {
    return ImmutableList.copyOf(pullRequests);
  }

  /**
   * Adds a {@link PullRequest} to this {@link MergeAnalysis}. A {@link PullRequest} can only be added if has the same base branch as the already
   * added {@link PullRequest}s. If this is not the case, the {@link PullRequest} is not added and the method returns false.
   * 
   * @param pullRequest
   *          {@link PullRequest}
   * @return true if the {@link PullRequest} has been added, false otherwise
   */
  public boolean addPullRequest(final PullRequest pullRequest) {
    final Branch baseBranch = getBaseBranch();
    if (baseBranch == null || pullRequest.getBase().equals(baseBranch)) {
      pullRequests.add(pullRequest);
      pullRequest.save();
      save();
      return true;
    }
    return false;
  }

  public List<MergeAnalysisExecution> getMergeAnalysisExecutions() {
    final List<MergeAnalysisExecution> sortedExecutions = Lists.newArrayList(mergeAnalysisExecutions);
    Collections.sort(sortedExecutions, MergeAnalysisExecution.Comparators.START_TIME_DESC);
    return ImmutableList.copyOf(sortedExecutions);
  }

  public MergeAnalysisExecution getMergeAnalysisExecution(final long id) {
    for (final MergeAnalysisExecution mergeAnalysisExecution : getMergeAnalysisExecutions()) {
      if (mergeAnalysisExecution.getId().equals(id)) {
        return mergeAnalysisExecution;
      }
    }
    return null;
  }

  /**
   * Creates a {@link MergeAnalysisExecution} and returns it. This operation is only possible if no other {@link MergeAnalysisExecution} of this
   * {@link MergeAnalysis} is currently running.
   * 
   * @param dateTime
   * @return the created {@link MergeAnalysisExecution} or null, if no {@link MergeAnalysisExecution} could be created, because another one is still
   *         running
   */
  public MergeAnalysisExecution start(final DateTime dateTime) {
    final MergeAnalysisExecution mergeAnalysisExecution = MergeAnalysisExecutionFactory.INSTANCE.create(this, dateTime);
    mergeAnalysisExecution.save();
    return mergeAnalysisExecution;
  }

  public MergeAnalysisExecution getRunningExecution() {
    if (getMergeAnalysisExecutions().size() > 0) {
      final MergeAnalysisExecution last = getMergeAnalysisExecutions().get(0);
      if (!last.terminated()) {
        return last;
      }
    }
    return null;
  }

  public MergeAnalysisExecution getLastTerminatedExecution() {
    if (getMergeAnalysisExecutions().size() > 0) {
      final MergeAnalysisExecution last = getMergeAnalysisExecutions().get(0);
      if (last.terminated()) {
        return last;
      }
      else if (getMergeAnalysisExecutions().size() > 1) {
        return getMergeAnalysisExecutions().get(1);
      }
    }
    return null;
  }

  public MergeAnalysisExecution getLastExecution() {
    if (getMergeAnalysisExecutions().size() > 0) {
      return getMergeAnalysisExecutions().get(0);
    }
    return null;
  }

  @Override
  public void save() {
    for (final MergeAnalysisExecution mergeAnalysisExecution : getMergeAnalysisExecutions()) {
      mergeAnalysisExecution.save();
    }
    super.save();
  }

  @Override
  public void delete() {
    for (final MergeAnalysisExecution mergeAnalysisExecution : getMergeAnalysisExecutions()) {
      mergeAnalysisExecution.delete();
    }
    super.delete();
  }
}
