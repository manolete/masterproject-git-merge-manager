package model.core;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import model.core.base.AbstractExplicitIdEntity;
import model.core.base.WebAccessibleEntity;
import model.factories.FavoriteFactory;
import play.data.validation.Constraints.Required;

import com.google.common.collect.ImmutableList;

/**
 * Representation of a git user.
 */
@Entity
@Table(name = "users")
public class User extends AbstractExplicitIdEntity implements WebAccessibleEntity {

  private static final long serialVersionUID = 1L;

  @Required
  private String username;

  private String url;

  private String avatarUrl;

  @OneToMany(mappedBy = "owner")
  private List<Repository> repositories;

  @OneToMany(mappedBy = "user")
  private List<Favorite> favorites;

  public User(final Long id, final String username) {
    super(id);
    setUsername(username);
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(final String username) {
    this.username = username;
  }

  public String getAvatarUrl() {
    return avatarUrl;
  }

  public void setAvatarUrl(final String avatarUrl) {
    this.avatarUrl = avatarUrl;
  }

  @Override
  public String getUrl() {
    return url;
  }

  public void setUrl(final String url) {
    this.url = url;
  }

  public List<Repository> getRepositories() {
    return repositories;
  }

  public List<Favorite> getFavorites() {
    return ImmutableList.copyOf(favorites);
  }

  public Favorite addFavorite(final Repository repository) {
    final Favorite existingFavorite = getFavorite(repository);
    if (existingFavorite == null) {
      final Favorite favorite = FavoriteFactory.INSTANCE.create(this, repository);
      favorite.save();
      return favorite;
    }
    return null;
  }

  private Favorite getFavorite(final Repository repository) {
    for (final Favorite favorite : getFavorites()) {
      if (favorite.getRepository().equals(repository)) {
        return favorite;
      }
    }
    return null;
  }

  public boolean removeFavorite(final Repository repository) {
    final Favorite favorite = getFavorite(repository);
    if (favorite != null) {
      favorite.delete();
      save();
      return true;
    }
    return false;
  }

  @Override
  public void save() {
    for (final Favorite favorite : getFavorites()) {
      favorite.save();
    }
    super.save();
  }

  @Override
  public void delete() {
    for (final Favorite favorite : getFavorites()) {
      favorite.delete();
    }
    super.delete();
  }
}