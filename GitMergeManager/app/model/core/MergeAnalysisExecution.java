package model.core;

import java.util.Comparator;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import model.core.base.AbstractGeneratedIdEntity;
import model.core.results.ExecutionResult;

import org.joda.time.DateTime;
import org.joda.time.Duration;

import play.data.validation.Constraints.Required;

/**
 * Concrete execution of a {@link MergeAnalysis}.
 */
@Entity
@Table(name = "merge_analysis_executions")
public class MergeAnalysisExecution extends AbstractGeneratedIdEntity implements Comparable<MergeAnalysisExecution> {

  private static final long serialVersionUID = 1L;

  @Required
  @ManyToOne
  private MergeAnalysis mergeAnalysis;

  private DateTime startDateTime;

  private DateTime endDateTime;

  private boolean finished;

  @OneToOne
  private ExecutionResult result;

  public MergeAnalysisExecution(final MergeAnalysis mergeAnalysis, final DateTime startDateTime) {
    this.mergeAnalysis = mergeAnalysis;
    this.startDateTime = startDateTime;
    this.finished = false;
  }

  public MergeAnalysis getMergeAnalysis() {
    return mergeAnalysis;
  }

  public DateTime getStartDateTime() {
    return startDateTime;
  }

  public DateTime getEndDateTime() {
    return endDateTime;
  }

  public Duration getDuration() {
    return new Duration(getStartDateTime(), getEndDateTime());
  }

  public void setEndDateTime(final DateTime endDateTime) {
    this.endDateTime = endDateTime;
  }

  public void setFinished() {
    this.finished = true;
  }

  public boolean finished() {
    return finished;
  }

  public boolean aborted() {
    return !finished && getEndDateTime() != null;
  }

  public boolean terminated() {
    return finished() || aborted();
  }

  public ExecutionResult getResult() {
    return result;
  }

  public void setResult(final ExecutionResult result) {
    result.save();
    this.result = result;
    save();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public int compareTo(final MergeAnalysisExecution o) {
    return Comparators.START_TIME_DESC.compare(this, o);
  }

  /**
   * Provides different {@link Comparator}'s to compare {@link MergeAnalysisExecution}'s.
   */
  public static class Comparators {

    /**
     * {@link Comparator} for {@link MergeAnalysisExecution}'s comparing according to {@link MergeAnalysisExecution#startDateTime}.
     */
    public static Comparator<MergeAnalysisExecution> START_TIME_DESC = new Comparator<MergeAnalysisExecution>() {

      private static final int BEFORE = 1;
      private static final int EQUAL = 0;
      private static final int AFTER = -1;

      @Override
      public int compare(final MergeAnalysisExecution o1, final MergeAnalysisExecution o2) {
        if (o1.getStartDateTime().isBefore(o2.getStartDateTime())) {
          return BEFORE;
        }
        else if (o1.getStartDateTime().isAfter(o2.getStartDateTime())) {
          return AFTER;
        }
        return EQUAL;
      }
    };
  }

  @Override
  public void delete() {
    result.delete();
    super.delete();
  }

}
