package model.core;

import javax.persistence.Entity;
import javax.persistence.Table;

import model.core.base.AbstractGeneratedIdEntity;

@Entity
@Table(name = "labels")
public class Label extends AbstractGeneratedIdEntity {

  private static final long serialVersionUID = 1L;

  private String name;

  private String url;

  private String color;

  public Label(final String name, final String url, final String color) {
    this.name = name;
    this.url = url;
    this.color = color;
  }

  public String getName() {
    return name;
  }

  public String getUrl() {
    return url;
  }

  public String getColor() {
    return color;
  }

}
