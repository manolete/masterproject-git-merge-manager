package model.repositories;

import javax.persistence.PersistenceException;

import model.core.Repository;

/**
 * A repository for {@link Repository}'s.
 */
public interface RepositoryRepository extends BaseRepository<Repository> {

  /**
   * Gets a {@link Repository} by the name of its owner and its name. If more
   * than one repository is found with the given owner and name a
   * {@link PersistenceException} is thrown.
   * 
   * @param owner
   *          name of the owner
   * @param name
   *          name of the {@link Repository}
   * @return {@link Repository} with the given owner and name or null, if no
   *         respective {@link Repository} has bee found
   */
  public Repository one(String owner, String name);

}
