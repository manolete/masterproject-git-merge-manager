package model.repositories;

import model.core.MergeAnalysis;

public interface MergeAnalysisRepository extends BaseRepository<MergeAnalysis> {

}
