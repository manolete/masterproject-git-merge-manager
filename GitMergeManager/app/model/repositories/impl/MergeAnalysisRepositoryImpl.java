package model.repositories.impl;

import model.core.MergeAnalysis;
import model.repositories.MergeAnalysisRepository;
import play.db.ebean.Model.Finder;

public class MergeAnalysisRepositoryImpl extends AbstractBaseRepositoryImpl<MergeAnalysis> implements MergeAnalysisRepository {

  private final Finder<Long, MergeAnalysis> finder = new Finder<Long, MergeAnalysis>(Long.class, MergeAnalysis.class);

  @Override
  protected Finder<Long, MergeAnalysis> finder() {
    return finder;
  }

}
