package model.repositories.impl;

import javax.persistence.PersistenceException;

import model.core.Repository;
import model.repositories.RepositoryRepository;
import play.db.ebean.Model.Finder;

import com.avaje.ebean.Ebean;

/**
 * {@link Ebean} specific implementation of {@link RepositoryRepository}.
 */
public class RepositoryRepositoryImpl extends AbstractBaseRepositoryImpl<Repository> implements RepositoryRepository {

  private final Finder<Long, Repository> finder = new Finder<Long, Repository>(Long.class, Repository.class);

  /**
   * {@inheritDoc}
   */
  @Override
  protected Finder<Long, Repository> finder() {
    return finder;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Repository one(final String owner, final String name) {
    Repository result = null;
    for (final Repository repository : all()) {
      if (repository.getOwner().getUsername().equals(owner) && repository.getName().equals(name)) {
        if (result == null) {
          result = repository;
        }
        else {
          throw new PersistenceException();
        }
      }
    }
    return result;
  }
}
