package model.repositories.impl;

import javax.persistence.PersistenceException;

import model.core.User;
import model.repositories.UserRepository;
import play.db.ebean.Model.Finder;

import com.avaje.ebean.Ebean;

/**
 * {@link Ebean} specific implementation of {@link UserRepository}.
 */
public class UserRepositoryImpl extends AbstractBaseRepositoryImpl<User> implements UserRepository {

  private final Finder<Long, User> finder = new Finder<Long, User>(Long.class, User.class);

  /**
   * {@inheritDoc}
   */
  @Override
  protected Finder<Long, User> finder() {
    return finder;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public User one(final String username) {
    User result = null;
    for (final User user : all()) {
      if (user.getUsername().equals(username)) {
        if (result == null) {
          result = user;
        }
        else {
          throw new PersistenceException();
        }
      }
    }
    return result;
  }
}
