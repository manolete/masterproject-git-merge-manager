package model.repositories;

import javax.persistence.PersistenceException;

import model.core.User;

/**
 * A repository for {@link User}'s.
 */
public interface UserRepository extends BaseRepository<User> {

  /**
   * Gets a user by its username. If more than one user is found with the given
   * username a {@link PersistenceException} is thrown.
   * 
   * @param username
   *          username
   * @return {@link User} with the given username or null, if no {@link User}
   *         with the given name was found
   */
  public User one(String username);

}