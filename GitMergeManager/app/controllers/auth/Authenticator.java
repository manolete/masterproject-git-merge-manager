package controllers.auth;

import controllers.routes;
import play.mvc.Http.Context;
import play.mvc.Result;
import play.mvc.Security;
import util.ApplicationConstants;

/**
 * Handles authentication.
 */
public class Authenticator extends Security.Authenticator {

  /**
   * {@inheritDoc}
   */
  @Override
  public String getUsername(final Context ctx) {
    return ctx.session().get(ApplicationConstants.SESSION_USERNAME_KEY);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Result onUnauthorized(final Context ctx) {
    return redirect(routes.SessionHandlingController.loginRedirect(ctx.request().uri()));
  }
}
