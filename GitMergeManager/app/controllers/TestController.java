package controllers;

import global.SessionHandler;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security.Authenticated;
import util.ApplicationConstants;
import controllers.auth.Authenticator;

public class TestController extends Controller {

  @Authenticated(Authenticator.class)
  public Result auth() {
    final String access_token = session(ApplicationConstants.SESSION_ACCESS_TOKEN_KEY);
    return ok("access_token: " + access_token + "\nuser: " + SessionHandler.username());
  }

  @Authenticated(Authenticator.class)
  public Result post() {
    final String name = Form.form().bindFromRequest().get("name");
    return ok("name is: " + name);
  }
}
