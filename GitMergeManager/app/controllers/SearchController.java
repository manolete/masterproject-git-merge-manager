package controllers;

import java.util.List;

import model.dto.RepositoryDto;
import play.mvc.BodyParser;
import play.mvc.BodyParser.Json;
import play.mvc.Result;
import play.mvc.Security.Authenticated;
import services.platforms.GithubService;

import com.google.inject.Inject;

import controllers.auth.Authenticator;
import controllers.base.AbstractBaseController;

@Authenticated(Authenticator.class)
public class SearchController extends AbstractBaseController {

  @Inject
  private GithubService githubService;

  @BodyParser.Of(Json.class)
  public Result search(final String searchTerm) {
    final List<RepositoryDto> repositoryDtos = githubService.searchRepositoriesByName(searchTerm);
    return ok(jsonUtil().createFormattedJsonString(repositoryDtos));
  }
}
