package controllers;

import java.util.ArrayList;
import java.util.List;

import model.core.Branch;
import model.core.MergeAnalysisExecution;
import model.core.PullRequest;
import model.core.Repository;
import model.dto.BranchDto;
import model.dto.MergeAnalysisExecutionDto;
import model.dto.PullRequestDto;
import model.dto.RepositoryDto;
import play.mvc.BodyParser;
import play.mvc.BodyParser.Json;
import play.mvc.Result;
import play.mvc.Security.Authenticated;
import controllers.auth.Authenticator;
import controllers.base.AbstractBaseController;

@Authenticated(Authenticator.class)
public class ReposController extends AbstractBaseController {

  @BodyParser.Of(Json.class)
  public Result repo(final String repoOwner, final String repoName) {
    final Repository repository = modelEntityService().getRepository(repoOwner, repoName);
    final RepositoryDto repositoryDto = dtoFactory().createRepositoryDto(repository);
    return ok(jsonUtil().createFormattedJsonString(repositoryDto));
  }

  @BodyParser.Of(Json.class)
  public Result branches(final String repoOwner, final String repoName) {
    final List<Branch> branches = modelEntityService().getRepository(repoOwner, repoName).getBranches();
    final List<BranchDto> branchDtos = new ArrayList<BranchDto>();
    for (final Branch branch : branches) {
      branchDtos.add(dtoFactory().createBranchDto(branch));
    }
    return ok(jsonUtil().createFormattedJsonString(branchDtos));
  }

  @BodyParser.Of(Json.class)
  public Result branch(final String repoOwner, final String repoName, final String branchName) {
    final Branch branch = modelEntityService().getRepository(repoOwner, repoName).getBranch(branchName);
    final BranchDto branchDto = dtoFactory().createBranchDto(branch);
    return ok(jsonUtil().createFormattedJsonString(branchDto));
  }

  @BodyParser.Of(Json.class)
  public Result pulls(final String repoOwner, final String repoName) {
    final List<PullRequest> pullRequests = modelEntityService().getRepository(repoOwner, repoName).getPullRequests();
    final List<PullRequestDto> pullRequestDtos = new ArrayList<PullRequestDto>();
    for (final PullRequest pullRequest : pullRequests) {
      pullRequestDtos.add(dtoFactory().createPullRequestDto(pullRequest));
    }
    return ok(jsonUtil().createFormattedJsonString(pullRequestDtos));
  }

  @Authenticated(Authenticator.class)
  @BodyParser.Of(Json.class)
  public Result pull(final String repoOwner, final String repoName, final Long pullId) {
    final PullRequest pullRequest = modelEntityService().getRepository(repoOwner, repoName).getPullRequest(pullId.toString());
    final PullRequestDto pullRequestDto = dtoFactory().createPullRequestDto(pullRequest);
    return ok(jsonUtil().createFormattedJsonString(pullRequestDto));
  }

  @BodyParser.Of(Json.class)
  public Result clone(final String repoOwner, final String repoName, final Long planId, final Long execId) {
    final MergeAnalysisExecution mergeAnalysisExecution = modelEntityService().getRepository(repoOwner, repoName).getMergeAnalysis(planId).getMergeAnalysisExecution(execId);
    final MergeAnalysisExecutionDto mergeAnalysisExecutionDto = dtoFactory().createMergeAnalysisExecutionDto(mergeAnalysisExecution);
    return ok(jsonUtil().createFormattedJsonString(mergeAnalysisExecutionDto));
  }

  @BodyParser.Of(Json.class)
  public Result update(final String repoOwner, final String repoName) {
    final Repository repository = modelEntityService().getUpdatedRepository(repoOwner, repoName);
    final RepositoryDto repositoryDto = dtoFactory().createRepositoryDto(repository);
    return ok(jsonUtil().createFormattedJsonString(repositoryDto));
  }
}
