package controllers;

import global.SessionHandler;
import play.Routes;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.index;
import views.html.info;

public class MainController extends Controller {

  public Result index() {
    return SessionHandler.isAuthenticated() ? indexAuthenticated() : indexNotAuthenticated();
  }

  public Result javascriptRoutes() {
    response().setContentType("text/javascript");
    return ok(Routes.javascriptRouter("jsRoutes",//@formatter:off
      controllers.routes.javascript.SearchController.search(),
      controllers.routes.javascript.UserController.addFavorite(),
      controllers.routes.javascript.UserController.favorites(),
      controllers.routes.javascript.UserController.deleteFavorite(),
      controllers.routes.javascript.GitHubController.getChangedFiles(),
      controllers.routes.javascript.GitHubController.getFileContent(),
      controllers.routes.javascript.GitHubController.getDiffs(),
      controllers.routes.javascript.GitHubController.getDiffLines(),
      controllers.routes.javascript.GitHubController.getReadme(),
      controllers.routes.javascript.GitHubController.isMavenProject(),
      controllers.routes.javascript.GitHubController.merge(),
      controllers.routes.javascript.ReposController.update(),
      controllers.routes.javascript.ReposController.pulls(),
      controllers.routes.javascript.ServerSentEventsController.SSE(),
      controllers.routes.javascript.MergeAnalysisController.analyses(),
      controllers.routes.javascript.MergeAnalysisController.analysis(),
      controllers.routes.javascript.MergeAnalysisController.create(),
      controllers.routes.javascript.MergeAnalysisController.start(),
      controllers.routes.javascript.MergeAnalysisController.stop(),
      controllers.routes.javascript.MergeAnalysisController.delete(),
      controllers.routes.javascript.MergeAnalysisController.status(),
      controllers.routes.javascript.MergeAnalysisExecutionController.pullResult(),
      controllers.routes.javascript.MergeAnalysisExecutionController.baseResult(),
      controllers.routes.javascript.MergeAnalysisExecutionController.result()
    ));//@formatter:on
  }

  private Result indexAuthenticated() {
    return ok(index.render());
  }

  private Result indexNotAuthenticated() {
    return redirect(routes.MainController.info());
  }

  public Result info() {
    return ok(info.render());
  }
}
