package controllers;

import model.core.PullRequest;
import infrastructure.platforms.GithubApiAdapter;
import play.mvc.BodyParser;
import play.mvc.BodyParser.Json;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security.Authenticated;
import services.events.Event;
import services.events.EventService;

import com.fasterxml.jackson.databind.JsonNode;
import com.google.common.base.Splitter;
import com.google.inject.Inject;

import controllers.auth.Authenticator;
import controllers.base.AbstractBaseController;

@Authenticated(Authenticator.class)
public class GitHubController extends AbstractBaseController {

  @Inject
  private GithubApiAdapter githubApiAdapter;

  @Inject
  private EventService eventService;
  
  @BodyParser.Of(Json.class)
  public Result getChangedFiles(final String owner, final String repository, final int pullRequestNumber) {
    return ok(githubApiAdapter.getChangedFiles(owner, repository, pullRequestNumber));
  }

  public Result getFileContent(final String path) {
    return ok(githubApiAdapter.getFileContent(path));
  }

  public Result getDiffs(final String owner, final String repository, final int pullRequestNumber, final String filename) {
    return ok(githubApiAdapter.getDiffs(owner, repository, pullRequestNumber, filename));
  }

  public Result getDiffLines(final String owner, final String repository, final String pullRequestNumber) {
    return ok(githubApiAdapter.getDiffLines(owner, repository, pullRequestNumber));
  }

  public Result getReadme(final String owner, final String repository) {
    return ok(githubApiAdapter.getReadme(owner, repository));
  }
  
  public Result isMavenProject(final String owner, final String repository) {
	return ok(githubApiAdapter.isMavenProject(owner, repository));
  }
  
  public Result merge(final String owner, final String repository, final String pullRequestNumbers) {
	  Iterable<String> pullRequestIds = Splitter.on(',').trimResults().split(pullRequestNumbers);
	  JsonNode mergeResult = null;
	  for(String pullRequestId : pullRequestIds)
	  {
		  final PullRequest pullRequest = modelEntityService().getRepository(owner, repository).getPullRequest(pullRequestId);
		  mergeResult = githubApiAdapter.merge(owner, repository, pullRequest);
	  }
	  
	  String msg = "The pull requests have been merged. Please sync the project";
	  eventService.sendEvent(msg, repository, owner, "", Event.EventType.alert, "success");
	  
	  return ok(mergeResult);
  }
}
