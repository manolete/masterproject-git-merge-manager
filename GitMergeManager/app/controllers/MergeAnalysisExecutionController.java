package controllers;

import java.util.ArrayList;
import java.util.List;

import com.google.inject.Inject;

import model.core.MergeAnalysis;
import model.core.MergeAnalysisExecution;
import model.core.results.ExecutionResult;
import model.core.results.PullRequestBuildResult;
import model.dto.MergeAnalysisExecutionDto;
import model.dto.factory.ResultDtoFactory;
import model.dto.results.BaseBranchBuildResultDto;
import model.dto.results.ExecutionResultDto;
import model.dto.results.PullRequestBuildResultDto;
import play.mvc.BodyParser;
import play.mvc.Result;
import play.mvc.BodyParser.Json;
import play.mvc.Security.Authenticated;
import controllers.auth.Authenticator;
import controllers.base.AbstractBaseController;

@Authenticated(Authenticator.class)
public class MergeAnalysisExecutionController extends AbstractBaseController {

  @Inject
  private ResultDtoFactory resultDtoFactory;

  @BodyParser.Of(Json.class)
  public Result execs(final String repoOwner, final String repoName, final Long analysisId) {
    final List<MergeAnalysisExecution> mergeAnalysisExecutions = getMergeAnalysis(repoOwner, repoName, analysisId).getMergeAnalysisExecutions();
    final List<MergeAnalysisExecutionDto> mergeAnalysisExecutionDtos = new ArrayList<MergeAnalysisExecutionDto>();
    for (final MergeAnalysisExecution mergeAnalysisExecution : mergeAnalysisExecutions) {
      mergeAnalysisExecutionDtos.add(dtoFactory().createMergeAnalysisExecutionDto(mergeAnalysisExecution));
    }
    return ok(jsonUtil().createFormattedJsonString(mergeAnalysisExecutionDtos));
  }

  @BodyParser.Of(Json.class)
  public Result exec(final String repoOwner, final String repoName, final Long analysisId, final Long execId) {
    final MergeAnalysisExecution mergeAnalysisExecution = getMergeAnalysisExecution(repoOwner, repoName, analysisId, execId);
    if (mergeAnalysisExecution != null) {
      final MergeAnalysisExecutionDto mergeAnalysisExecutionDto = dtoFactory().createMergeAnalysisExecutionDto(mergeAnalysisExecution);
      return ok(jsonUtil().createFormattedJsonString(mergeAnalysisExecutionDto));
    }
    return badRequest(error());
  }

  public Result result(final String repoOwner, final String repoName, final Long analysisId, final Long execId) {
    final MergeAnalysisExecution mergeAnalysisExecution = getMergeAnalysisExecution(repoOwner, repoName, analysisId, execId);
    if (mergeAnalysisExecution != null) {
      final ExecutionResult executionResult = mergeAnalysisExecution.getResult();
      if (executionResult != null) {
        final ExecutionResultDto executionResultDto = resultDtoFactory.createExecutionResultDto(executionResult);
        return ok(jsonUtil().createFormattedJsonString(executionResultDto));
      }
    }
    return badRequest(error());
  }

  public Result baseResult(final String repoOwner, final String repoName, final Long analysisId, final Long execId) {
    final MergeAnalysisExecution mergeAnalysisExecution = getMergeAnalysisExecution(repoOwner, repoName, analysisId, execId);
    if (mergeAnalysisExecution != null) {
      final BaseBranchBuildResultDto baseBranchBuildResultDto = resultDtoFactory.createBaseBranchBuildResultDto(mergeAnalysisExecution.getResult().getBaseBranchResult());
      return ok(jsonUtil().createFormattedJsonString(baseBranchBuildResultDto));
    }
    return badRequest(error());
  }

  public Result pullResult(final String repoOwner, final String repoName, final Long analysisId, final Long execId, final Long pullId) {
    final MergeAnalysisExecution mergeAnalysisExecution = getMergeAnalysisExecution(repoOwner, repoName, analysisId, execId);
    if (mergeAnalysisExecution != null) {
      final PullRequestBuildResult pullRequestBuildResult = mergeAnalysisExecution.getResult().getPullRequestBuildResult(pullId);
      if (pullRequestBuildResult != null) {
        final PullRequestBuildResultDto pullRequestBuildResultDto = resultDtoFactory.createPullRequestBuildResultDto(pullRequestBuildResult);
        return ok(jsonUtil().createFormattedJsonString(pullRequestBuildResultDto));
      }
    }
    return badRequest(error());
  }

  private MergeAnalysisExecution getMergeAnalysisExecution(final String repoOwner, final String repoName, final Long analysisId, final Long execId) {
    return modelEntityService().getRepository(repoOwner, repoName).getMergeAnalysis(analysisId).getMergeAnalysisExecution(execId);
  }

  private MergeAnalysis getMergeAnalysis(final String repoOwner, final String repoName, final Long analysisId) {
    return modelEntityService().getRepository(repoOwner, repoName).getMergeAnalysis(analysisId);
  }

}
