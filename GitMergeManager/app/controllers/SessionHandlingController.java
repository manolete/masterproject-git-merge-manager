package controllers;

import model.dto.UserDto;
import global.ConfigLoader;
import global.SessionHandler;
import play.libs.F.Function;
import play.libs.F.Promise;
import play.libs.WS;
import play.libs.WS.Response;
import play.mvc.Controller;
import play.mvc.Result;
import services.platforms.GithubService;
import util.ApplicationConstants;
import util.URL;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.inject.Inject;

/**
 * Provides methods for login and logout.
 */
public class SessionHandlingController extends Controller {

  private static final long DEFAULT_ASYNC_TIMEOUT = 5000;

  @Inject
  private GithubService dtoPlatformService;
  @Inject
  private ConfigLoader conf;

  public Result loginRedirect(final String redirect) {
    flash(ApplicationConstants.FLASH_REDIRECT_AFTER_LOGIN_KEY, redirect);
    return redirect(routes.SessionHandlingController.login());
  }

  /**
   * Handles the login of a user. Redirects to the Github OAuth URL.
   * 
   * @return {@link Result}
   */
  public Result login() {
    if (SessionHandler.isAuthenticated()) {
      return redirect(routes.MainController.index());
    }
    final URL url = new URL(conf.string("oauth.github.base_uri"));
    url.addParameter("scope", conf.string("oauth.github.scope"));
    url.addParameter("client_id", conf.string("oauth.github.client_id"));
    url.addParameter("redirect_uri", conf.string("oauth.github.redirect_uri"));
    final String redirectAfterLogin = flash(ApplicationConstants.FLASH_REDIRECT_AFTER_LOGIN_KEY);
    if (redirectAfterLogin != null) {
      flash(ApplicationConstants.FLASH_REDIRECT_AFTER_LOGIN_KEY, redirectAfterLogin);
    }
    return redirect(url.toString());
  }

  /**
   * Handles the callback of the Github OAuth authentication. Requests the
   * access_token.
   * 
   * @param code
   *          temporary code required to get the access_token
   * @return {@link Result}
   */
  public Result callback(final String code) {
    final ObjectNode parameters = JsonNodeFactory.instance.objectNode();
    parameters.put("code", code);
    parameters.put("client_id", conf.string("oauth.github.client_id"));
    parameters.put("client_secret", conf.string("oauth.github.client_secret"));
    final Promise<Result> resultPromise = WS.url(conf.string("oauth.github.access_token_uri")).setHeader("Accept", "application/json").post(parameters).map(new Function<WS.Response, Result>() {

      @Override
      public Result apply(final Response response) throws Throwable {
        final JsonNode data = response.asJson();
        session(ApplicationConstants.SESSION_ACCESS_TOKEN_KEY, data.findValue("access_token").asText());
        final String redirectAfterLogin = flash(ApplicationConstants.FLASH_REDIRECT_AFTER_LOGIN_KEY);
        if (redirectAfterLogin != null) {
          return redirect(redirectAfterLogin);
        }
        return redirect(routes.MainController.index());
      }
    });
    final Result result = resultPromise.get(DEFAULT_ASYNC_TIMEOUT);
    final UserDto user = dtoPlatformService.getUser();
    session(ApplicationConstants.SESSION_USERNAME_KEY, user.username);
    return result;
  }

  /**
   * Handles the logout of a user. Clears the session content.
   * 
   * @return {@link Result}
   */
  public Result logout() {
    if (SessionHandler.isAuthenticated()) {
      session().clear();
    }
    return redirect(routes.MainController.info());
  }
}
