package controllers.base;

import model.dto.factory.DtoFactory;

import com.fasterxml.jackson.databind.JsonNode;
import com.google.inject.Inject;

import play.mvc.Controller;
import services.data.ModelEntityService;
import util.JsonUtil;

/**
 * Base functionality for controller classes that provide access to model entities.
 */
public abstract class AbstractBaseController extends Controller {

  private static final String ID = "id";
  private static final String INVALID_REQUEST = "Invalid Request";

  @Inject
  private ModelEntityService modelEntityService;

  @Inject
  private DtoFactory dtoFactory;

  private JsonUtil jsonUtil;

  protected ModelEntityService modelEntityService() {
    return modelEntityService;
  }

  protected DtoFactory dtoFactory() {
    return dtoFactory;
  }

  protected JsonUtil jsonUtil() {
    if (jsonUtil == null) {
      jsonUtil = new JsonUtil();
    }
    return jsonUtil;
  }

  /**
   * Creates a {@link JsonNode} containing only a numeric id.
   * 
   * @param id
   *          id to be serialised
   * @return {@link JsonNode} containing the id
   */
  public String id(final Long id) {
    return jsonUtil().createJsonNodeAsString(ID, id.toString());
  }

  /**
   * Creates a {@link JsonNode} containing a message.
   * 
   * @param type
   *          {@link MessageType}
   * @param message
   *          text-messages
   * @return {@link JsonNode} containing the message
   */
  public String message(final MessageType type, final String message) {
    return jsonUtil().createJsonNodeAsString(type.toString(), message);
  }

  public String info(final String message) {
    return message(MessageType.INFO, message);
  }

  public String warning(final String message) {
    return message(MessageType.WARNING, message);
  }

  public String error(final String message) {
    return message(MessageType.ERROR, message);
  }

  public String error() {
    return message(MessageType.ERROR, INVALID_REQUEST);
  }

  public static enum MessageType {
    INFO("INFO"),
    WARNING("WARNING"),
    ERROR("ERROR");

    private final String text;

    MessageType(final String text) {
      this.text = text;
    }

    @Override
    public String toString() {
      return text;
    }
  }

}
