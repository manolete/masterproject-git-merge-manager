package controllers;

import global.SessionHandler;

import java.util.ArrayList;
import java.util.List;

import model.core.Favorite;
import model.core.Repository;
import model.core.User;
import model.dto.RepositoryDto;
import model.dto.UserDto;
import play.mvc.BodyParser;
import play.mvc.BodyParser.Json;
import play.mvc.Result;
import play.mvc.Security.Authenticated;
import controllers.auth.Authenticator;
import controllers.base.AbstractBaseController;

@Authenticated(Authenticator.class)
public class UserController extends AbstractBaseController {

  private static final String FAVORITE_ALREADY_EXISTS_MESSAGE = "The user already has set a favorite to this repository";
  private static final String REMOVED_FAVORITE_MESSAGE = "Successfully removed favorite.";
  private static final String FAVORITE_NOT_FOUND_MESSAGE = "Favorite not found";

  @BodyParser.Of(Json.class)
  public Result profile() {
    final User user = getAuthenticatedUser();
    final UserDto userDto = dtoFactory().createUserDto(user);
    return ok(jsonUtil().createFormattedJsonString(userDto));
  }

  @BodyParser.Of(Json.class)
  public Result favorites() {
    final List<RepositoryDto> repositoryDtos = new ArrayList<RepositoryDto>();
    for (final Favorite favorite : getAuthenticatedUser().getFavorites()) {
      repositoryDtos.add(dtoFactory().createRepositoryDto(favorite.getRepository()));
    }
    return ok(jsonUtil().createFormattedJsonString(repositoryDtos));
  }

  public Result addFavorite(final String repoOwner, final String repoName) {
    final User user = getAuthenticatedUser();
    final Favorite favorite = user.addFavorite(getRepository(repoOwner, repoName));
    return favorite != null ? ok(id(favorite.getId())) : ok(info(FAVORITE_ALREADY_EXISTS_MESSAGE));
  }

  public Result deleteFavorite(final String repoOwner, final String repoName) {
    final boolean success = getAuthenticatedUser().removeFavorite(getRepository(repoOwner, repoName));
    return success ? ok(info(REMOVED_FAVORITE_MESSAGE)) : ok(warning(FAVORITE_NOT_FOUND_MESSAGE));
  }

  @BodyParser.Of(Json.class)
  public Result user(final String username) {
    final User user = modelEntityService().getUser(username);
    final UserDto userDto = dtoFactory().createUserDto(user);
    return ok(jsonUtil().createFormattedJsonString(userDto));
  }

  private User getAuthenticatedUser() {
    return modelEntityService().getUser(SessionHandler.username());
  }

  private Repository getRepository(final String repoOwner, final String repoName) {
    return modelEntityService().getRepository(repoOwner, repoName);
  }
}
