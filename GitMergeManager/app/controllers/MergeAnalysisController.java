package controllers;

import global.SessionHandler;

import java.util.ArrayList;
import java.util.List;

import model.core.MergeAnalysis;
import model.core.MergeAnalysisExecution;
import model.core.Repository;
import model.core.User;
import model.dto.MergeAnalysisDto;
import model.dto.MergeAnalysisExecutionDto;
import play.libs.Json;
import play.mvc.BodyParser;
import play.mvc.Result;
import play.mvc.Security.Authenticated;
import scala.collection.Iterator;
import services.analysis.MergeAnalysisService;
import services.events.Event;
import services.events.EventService;
import services.events.EventStorage;
import util.JsonUtil;

import com.fasterxml.jackson.databind.JsonNode;
import com.google.common.base.Splitter;
import com.google.inject.Inject;

import controllers.auth.Authenticator;
import controllers.base.AbstractBaseController;

@Authenticated(Authenticator.class)
public class MergeAnalysisController extends AbstractBaseController {

  private static final String START_SUCCESS_MESSAGE = "MergeAnalysisExecution has been started.";
  private static final String START_FAILURE_MESSAGE = "Could not start MergeAnalysisExecution. Maybe there is already an execution running for this MergeAnalysis.";
  private static final String NO_BASE_BRANCH_FAILURE_MESSAGE = "MergeAnalyses has no base branch";
  private static final String STOP_SUCCESS_MESSAGE = "MergeAnalysisExecution has been triggered to stop.";
  private static final String STOP_FAILURE_MESSAGE = "Could not stop MergeAnalysisExecution";

  @Inject
  private MergeAnalysisService mergeAnalysisService;

  @Inject
  private EventService eventService;

  @BodyParser.Of(play.mvc.BodyParser.Json.class)
  public Result analyses(final String repoOwner, final String repoName) {
    final List<MergeAnalysis> mergeAnalyses = modelEntityService().getRepository(repoOwner, repoName).getMergeAnalysis();
    final List<MergeAnalysisDto> mergeAnalysisDtos = new ArrayList<MergeAnalysisDto>();
    for (final MergeAnalysis mergeAnalysis : mergeAnalyses) {
      mergeAnalysisDtos.add(dtoFactory().createMergeAnalysisDto(mergeAnalysis));
    }
    return ok(jsonUtil().createFormattedJsonString(mergeAnalysisDtos));
  }

  @BodyParser.Of(play.mvc.BodyParser.Json.class)
  public Result analysis(final String repoOwner, final String repoName, final Long analysisId) {
    final MergeAnalysis mergeAnalysis = getMergeAnalysis(repoOwner, repoName, analysisId);
    if (mergeAnalysis != null) {
      final MergeAnalysisDto mergeAnalysisDto = dtoFactory().createMergeAnalysisDto(mergeAnalysis);
      return ok(jsonUtil().createFormattedJsonString(mergeAnalysisDto));
    }
    return badRequest();
  }

  public Result delete(final String repoOwner, final String repoName, final Long analysisId) {
    final MergeAnalysis mergeAnalysis = getMergeAnalysis(repoOwner, repoName, analysisId);
    if (mergeAnalysis != null) {
      mergeAnalysis.delete();
      eventService.sendAnalysisUpdateEvent(mergeAnalysis);
      return ok(info("Successfully deleted analyses '" + analysisId + "'"));
    }
    return badRequest();

  }

  public Result create(final String repoOwner, final String repoName, final String analysisName, final String pulls) {
    final Repository repository = modelEntityService().getRepository(repoOwner, repoName);
    final User user = modelEntityService().getUser(SessionHandler.username());
    final Iterable<String> pullRequestNumbers = Splitter.on(',').trimResults().split(pulls);
    final MergeAnalysis mergeAnalysis = mergeAnalysisService.createMergeAnalysis(repository, user, analysisName, pullRequestNumbers);
    eventService.sendAnalysisUpdateEvent(mergeAnalysis);
    return ok(jsonUtil().createFormattedJsonString(dtoFactory().createMergeAnalysisDto(mergeAnalysis)));
  }

  public Result start(final String repoOwner, final String repoName, final long analysisId) {
    final MergeAnalysis mergeAnalysis = getMergeAnalysis(repoOwner, repoName, analysisId);
    if (mergeAnalysis != null) {
      if (mergeAnalysis.getBaseBranch() == null) {
        return ok(error(NO_BASE_BRANCH_FAILURE_MESSAGE));
      }
      EventStorage.clean(mergeAnalysis.getRepository());
      final boolean success = mergeAnalysisService.start(mergeAnalysis);
      return success ? ok(info(START_SUCCESS_MESSAGE)) : ok(warning(START_FAILURE_MESSAGE));
    }
    return badRequest();
  }

  public Result stop(final String repoOwner, final String repoName, final long analysisId) {
    final MergeAnalysis mergeAnalysis = getMergeAnalysis(repoOwner, repoName, analysisId);
    if (mergeAnalysis != null) {
      final boolean success = mergeAnalysisService.stop(mergeAnalysis);
      return success ? ok(info(STOP_SUCCESS_MESSAGE)) : ok(warning(STOP_FAILURE_MESSAGE));
    }
    return badRequest();
  }

  public Result status(final String repoOwner, final String repoName, final long analysisId) {
    if (EventStorage.buildEvents != null && EventStorage.buildEvents.containsKey(repoOwner + repoName)) {
      final Iterator<Event> events = EventStorage.buildEvents.get(repoOwner + repoName).iterator();

      final ArrayList<JsonNode> eventNodes = new ArrayList<JsonNode>();
      while (events.hasNext()) {
        eventNodes.add(Json.toJson(events.next()));
      }

      return ok(new JsonUtil().createFormattedJsonString(eventNodes));
    }
    else {
      return ok();
    }
  }

  public Result running(final String repoOwner, final String repoName, final long analysisId) {
    final MergeAnalysis mergeAnalysis = getMergeAnalysis(repoOwner, repoName, analysisId);
    if (mergeAnalysis != null) {
      final MergeAnalysisExecution mergeAnalysisExecution = mergeAnalysis.getRunningExecution();
      if (mergeAnalysisExecution != null) {
        final MergeAnalysisExecutionDto mergeAnalysisExecutionDto = dtoFactory().createMergeAnalysisExecutionDto(mergeAnalysisExecution);
        return ok(jsonUtil().createFormattedJsonString(mergeAnalysisExecutionDto));
      }
      else {
        return ok(info("There is currently no running execution for this analysis."));
      }
    }
    return badRequest();
  }

  public Result last(final String repoOwner, final String repoName, final long analysisId) {
    final MergeAnalysis mergeAnalysis = getMergeAnalysis(repoOwner, repoName, analysisId);
    if (mergeAnalysis != null) {
      final MergeAnalysisExecution mergeAnalysisExecution = mergeAnalysis.getLastExecution();
      if (mergeAnalysisExecution != null) {
        final MergeAnalysisExecutionDto mergeAnalysisExecutionDto = dtoFactory().createMergeAnalysisExecutionDto(mergeAnalysisExecution);
        return ok(jsonUtil().createFormattedJsonString(mergeAnalysisExecutionDto));
      }
      else {
        return ok(info("There is not yet an execution for this analysis."));
      }
    }
    return badRequest();
  }

  public Result lastTerminated(final String repoOwner, final String repoName, final long analysisId) {
    final MergeAnalysis mergeAnalysis = getMergeAnalysis(repoOwner, repoName, analysisId);
    if (mergeAnalysis != null) {
      final MergeAnalysisExecution mergeAnalysisExecution = mergeAnalysis.getLastTerminatedExecution();
      if (mergeAnalysisExecution != null) {
        final MergeAnalysisExecutionDto mergeAnalysisExecutionDto = dtoFactory().createMergeAnalysisExecutionDto(mergeAnalysisExecution);
        return ok(jsonUtil().createFormattedJsonString(mergeAnalysisExecutionDto));
      }
      else {
        return ok(info("There is not yet a terminated execution for this analysis."));
      }
    }
    return badRequest();
  }

  private MergeAnalysis getMergeAnalysis(final String repoOwner, final String repoName, final long analysisId) {
    final Repository repository = modelEntityService().getRepository(repoOwner, repoName);
    if (repository != null) {
      final MergeAnalysis mergeAnalysis = repository.getMergeAnalysis(analysisId);
      if (mergeAnalysis != null) {
        return mergeAnalysis;
      }
    }
    return null;
  }
}
