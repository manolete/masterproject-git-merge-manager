package controllers

import play.api._
import play.api.mvc._
import play.api.libs.EventSource
import play.api.libs.json.Json
import play.api.libs.iteratee.Enumerator
import play.api.libs.iteratee.Concurrent
import scala.io.Source
import play.api.libs.json.JsValue
import play.api.libs.iteratee.Enumeratee
import play.api.libs.concurrent.Execution.Implicits._
import play.api.libs.EventSource.EventNameExtractor
import services.events.EventBroadcaster
import services.events.Event

object ServerSentEventsController extends Controller {

  implicit val eventNameExtractor: EventNameExtractor[JsValue] = EventNameExtractor[JsValue](eventName = (event) => event.\("event").asOpt[String])

  def SSE(owner: String, repository: String) = Action {
    implicit req =>
      {
        println(req.remoteAddress + " - SSE connected")
        Ok.feed(EventBroadcaster.out
          &> Concurrent.buffer(1000)
          &> filter(owner, repository)
          &> EventSource()).as("text/event-stream")
      }
  }

  def filter(owner: String, repository: String) =
    Enumeratee.filter[JsValue] { json: JsValue =>
      (json \ "repository").as[String] == repository
      (json \ "owner").as[String] == owner
    }

  // for testing
  var event = new Event();
  event.eventInfo = "success";
  event.event = Event.EventType.buildEvent.toString();
  event.msg = "I am a beautiful notification";
  event.owner = "junit-team";
  event.repository = "junit";

  def postMessage = Action { implicit req => EventBroadcaster.pushEvent(event); Ok }
}

