package infrastructure.filesystem;

import global.Loggers;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;

public class FileSystemOperator {

  /**
   * Copies a directory. If the destination folder already exists, it is deleted.
   * 
   * @param sourcePath
   *          path of the directory to be copied
   * @param destinationPath
   *          path to which the directory is copied
   * 
   * @throws IOException
   */
  public void copy(final String sourcePath, final String destinationPath) {
    final File destination = new File(destinationPath);
    if (destination.exists()) {
      clean(destination);
    }
    try {
      final File source = new File(sourcePath);
      FileUtils.copyDirectory(source, destination);
      Loggers.FS.info("Copied from '" + source.getAbsolutePath() + "' to '" + destination.getAbsolutePath() + "'");
    }
    catch (final IOException e) {
      e.printStackTrace();
    }
  }

  /**
   * Deletes file or directory containing files
   * 
   * @param file
   *          {@link File}
   */
  public void cleanAndDelete(final File file) {
    if (file.exists()) {
      clean(file);
      file.delete();
      Loggers.FS.info("Cleaned and deleted '" + file.getAbsolutePath() + "'");
    }
  }

  /**
   * Cleans a directory.
   * 
   * @param file
   *          {@link File}
   */
  public void clean(final File file) {
    if (file.isDirectory()) {
      try {
        FileUtils.cleanDirectory(file);
        Loggers.FS.info("Cleaned '" + file.getAbsolutePath() + "'");
      }
      catch (final IOException e) {}
    }
  }

  public static FileSystemOperator create() {
    return new FileSystemOperator();
  }
}
