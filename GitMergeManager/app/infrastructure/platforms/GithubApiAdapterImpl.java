package infrastructure.platforms;

import global.ConfigLoader;
import global.SessionHandler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import model.core.PullRequest;
import play.libs.WS;
import play.libs.WS.Response;
import play.libs.WS.WSRequestHolder;
import play.mvc.Http.Session;
import util.DiffParser;

import com.fasterxml.jackson.databind.JsonNode;
import com.google.inject.Inject;

public class GithubApiAdapterImpl implements GithubApiAdapter {

  private static final String ISSUES = "issues";
  private static final String USER = "user";
  private static final String DIFF = ".diff";
  private static final String FILES = "files";
  private static final String USERS = "users";
  private static final String PULLS = "pulls";
  private static final String BRANCHES = "branches";
  private static final String REPOS = "repos";
  private static final String HTTPS = "https://";
  private static final String HTTP = "http://";
  private static final String SLASH = "/";
  private static final String MERGES = "merges";
  private static final long DEFAULT_ASYNC_TIMEOUT = 5000;
  private static final int HTTP_STATUS_OK = 200;
  private static final String DIFF_URI = "https://github.com";

  @Inject
  private ConfigLoader conf;

  /**
   * {@inheritDoc}
   */
  @Override
  public JsonNode getUser() {
    return apiGetCallReturningJson(USER);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public JsonNode getUser(final String username) {
    return apiGetCallReturningJson(USERS + SLASH + username);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public JsonNode searchRepositoriesByName(final String searchTerm) {
    final Map<String, String> parameters = new HashMap<String, String>();
    parameters.put("sort", "stars");
    parameters.put("q", searchTerm);
    return apiGetCallReturningJson("search/repositories", parameters);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public JsonNode getRepository(final String repositoryOwner, final String repositoryName) {
    return apiGetCallReturningJson(REPOS + SLASH + repositoryOwner + SLASH + repositoryName);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public JsonNode getBranches(final String repositoryOwner, final String repositoryName) {
    return apiGetCallReturningJson(REPOS + SLASH + repositoryOwner + SLASH + repositoryName + SLASH + BRANCHES);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public JsonNode getPullRequests(final String repositoryOwner, final String repositoryName) {
    return apiGetCallReturningJson(REPOS + SLASH + repositoryOwner + SLASH + repositoryName + SLASH + PULLS);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public JsonNode getReadme(final String repositoryOwner, final String repositoryName) {
    return apiGetCallReturningJson(REPOS + SLASH + repositoryOwner + SLASH + repositoryName + SLASH + "readme");
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public JsonNode getPullRequest(final String repositoryOwner, final String repositoryName, final int pullRequestNumber) {
    return apiGetCallReturningJson(REPOS + SLASH + repositoryOwner + SLASH + repositoryName + SLASH + PULLS + SLASH + pullRequestNumber);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public JsonNode getIssue(final String repositoryOwner, final String repositoryName, final String issueNumber) {
    return apiGetCallReturningJson(REPOS + SLASH + repositoryOwner + SLASH + repositoryName + SLASH + ISSUES + SLASH + issueNumber);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public JsonNode getChangedFiles(final String repositoryOwner, final String repositoryName, final int pullRequestNumber) {
    return apiGetCallReturningJson(REPOS + SLASH + repositoryOwner + SLASH + repositoryName + SLASH + PULLS + SLASH + pullRequestNumber + SLASH + FILES);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String getFileContent(final String contentUrl) {
    return apiGetCallReturningString(contentUrl);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public JsonNode isMavenProject(final String repositoryOwner, final String repositoryName) {
    return apiGetCallReturningJson(REPOS + SLASH + repositoryOwner + SLASH + repositoryName + SLASH + "contents/");
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public JsonNode getDiffs(final String repositoryOwner, final String repositoryName, final int pullRequestNumber, final String filename) {
    final String uri = DIFF_URI + SLASH + repositoryOwner + SLASH + repositoryName + SLASH + "pull" + SLASH + pullRequestNumber + DIFF;
    final JsonNode diffs = DiffParser.parseDiff(apiGetDiffs(uri), filename);
    return diffs;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public JsonNode getDiffLines(final String repositoryOwner, final String repositoryName, final String pullRequestNumber) {
    final JsonNode out = apiGetCallReturningJson(REPOS + SLASH + repositoryOwner + SLASH + repositoryName + SLASH + PULLS + SLASH + pullRequestNumber + SLASH + FILES);
    final String uri = DIFF_URI + SLASH + repositoryOwner + SLASH + repositoryName + SLASH + "pull" + SLASH + pullRequestNumber + DIFF;
    final ArrayList<String> filenames = new ArrayList<String>();
    JsonNode temp;
    for (int i = 0; i < out.size(); ++i) {
      temp = out.get(i);
      temp = temp.get("filename");
      filenames.add(temp.textValue());
    }
    return DiffParser.parseDiffLines(filenames, apiGetDiffs(uri));
  }

  /**
   * Performs a GET request to the Github API.
   * 
   * @param session
   *          the current {@link Session}
   * @param uri
   *          either an URI fragment that is appended to the base Github API URI or an absolute URI
   * @return response of the GET request as {@link String}
   */
  private String apiGetCallReturningString(final String uri) {
    return apiGetCallReturningString(uri, null);
  }

  /**
   * Performs a GET request to the Github API.
   * 
   * @param session
   *          the current {@link Session}
   * @param uri
   *          either an URI fragment that is appended to the base Github API URI or an absolute URI
   * @param parameters
   *          {@link List} of parameters added to the GET call
   * @return response of the GET request as {@link String}
   */
  private String apiGetCallReturningString(final String uri, final Map<String, String> parameters) {
    return apiGetCall(uri, parameters).getBody();
  }

  /**
   * Performs a GET request to the Github API.
   * 
   * @param session
   *          the current {@link Session}
   * @param uri
   *          either an URI fragment that is appended to the base Github API URI or an absolute URI
   * @return response of the GET request as {@link JsonNode}
   */
  private JsonNode apiGetCallReturningJson(final String uri) {
    return apiGetCallReturningJson(uri, null);
  }

  /**
   * Performs a GET request to the Github API.
   * 
   * @param session
   *          the current {@link Session}
   * @param uri
   *          either an URI fragment that is appended to the base Github API URI or an absolute URI
   * @param parameters
   *          {@link List} of parameters added to the GET call
   * @return response of the GET request as {@link JsonNode}
   */
  private JsonNode apiGetCallReturningJson(final String uri, final Map<String, String> parameters) {
    return apiGetCall(uri, parameters).asJson();
  }

  /**
   * Performs a GET request to the Github API.
   * 
   * @param session
   *          the current {@link Session}
   * @param uri
   *          either an URI fragment that will be appended to the base Github API URI or an absolute URI
   * @return {@link Response} of the GET request
   */
  private Response apiGetCall(final String uri, final Map<String, String> parameters) {
    final String absoluteUri = getAbsoluteUri(uri);
    WSRequestHolder requestHolder = WS.url(absoluteUri).setQueryParameter("access_token", SessionHandler.authenticationToken());
    if (parameters != null) {
      for (final Entry<String, String> parameter : parameters.entrySet()) {
        requestHolder = requestHolder.setQueryParameter(parameter.getKey(), parameter.getValue());
      }
    }
    final Response response = requestHolder.get().get(DEFAULT_ASYNC_TIMEOUT);
    errorHandling(response);
    return response;
  }

  private Response apiPostCall(final String uri, final Map<String, String> parameters) {
    final String absoluteUri = getAbsoluteUri(uri);
    WSRequestHolder requestHolder = WS.url(absoluteUri).setQueryParameter("access_token", SessionHandler.authenticationToken());
    if (parameters != null) {
      for (final Entry<String, String> parameter : parameters.entrySet()) {
        requestHolder = requestHolder.setQueryParameter(parameter.getKey(), parameter.getValue());
      }
    }
    final Response response = requestHolder.post("").get(DEFAULT_ASYNC_TIMEOUT);
    errorHandling(response);
    return response;
  }

  private String getAbsoluteUri(final String uriFragment) {
    return isAbsoluteUri(uriFragment) ? uriFragment : conf.string("api.github.base_uri") + SLASH + uriFragment;
  }

  private boolean isAbsoluteUri(final String uri) {
    return uri.startsWith(HTTP) || uri.startsWith(HTTPS);
  }

  /**
   * Throws an {@link Exception} if the HTTP status of the given {@link Response} is not ok.
   * 
   * @param response
   *          {@link Response}
   * @throws GithubApiException
   * @throws Throwable
   */
  private void errorHandling(final Response response) throws GithubApiException {
    if (response.getStatus() != HTTP_STATUS_OK) {
      final JsonNode errorMessage = response.asJson().findValue("message");
      if (errorMessage != null) {
        throw new GithubApiException(errorMessage.asText());
      }
      throw new GithubApiException();
    }
  }

  private String apiGetDiffs(final String uri) {
    final WSRequestHolder requestHolder = WS.url(uri);
    final Response response = requestHolder.get().get(DEFAULT_ASYNC_TIMEOUT);
    errorHandling(response);
    return response.getBody();
  }

  @Override
  public JsonNode merge(final String repositoryOwner, final String repositoryName, final PullRequest pullRequest) {

    final Map<String, String> map = new HashMap<String, String>();
    map.put("commit_message", "GitMergeManager_" + pullRequest.getNumber());
    map.put("base", pullRequest.getBase().getName());
    map.put("head", pullRequest.getHead().getBranchName());
    System.out.println(REPOS + SLASH + repositoryOwner + SLASH + repositoryName + SLASH + MERGES);
    final Response response = apiPostCall(REPOS + SLASH + repositoryOwner + SLASH + repositoryName + SLASH + MERGES, map);

    return response.asJson();
  }

}
