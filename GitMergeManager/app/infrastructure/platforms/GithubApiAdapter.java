package infrastructure.platforms;

import model.core.PullRequest;

import com.fasterxml.jackson.databind.JsonNode;

/**
 * Provides methods to fetch data from a VCS platform (e.g. Github).
 */
public interface GithubApiAdapter {

  /**
   * Fetch data about the currently authenticated user.
   * 
   * @return {@link JsonNode} containing the users data
   */
  public JsonNode getUser();

  /**
   * Fetch data about the currently authenticated user.
   * 
   * @return {@link JsonNode} containing the users data
   */
  public JsonNode getUser(String username);

  /**
   * Search on a project by name.
   * 
   * @param searchTerm
   *          search term
   * @return list of projects that correspond to the given search term
   */
  public JsonNode searchRepositoriesByName(String searchTerm);

  /**
   * Fetch data about a repository.
   * 
   * @param repositoryOwner
   *          the name of the owner of the repository
   * @param repositoryName
   *          the name of the repository
   * @return {@link JsonNode} containing the data of the repository
   */
  public JsonNode getRepository(String repositoryOwner, String repositoryName);

  /**
   * Fetch all branches of a repository.
   * 
   * @param repositoryOwner
   *          name of the owner of the repository
   * @param repositoryName
   *          name of the repository
   * @return all branches of the repository
   */
  public JsonNode getBranches(String repositoryOwner, String repositoryName);

  /**
   * Fetch all pull requests of a repository.
   * 
   * @param repositoryOwner
   *          name of the owner of the repository
   * @param repositoryName
   *          name of the repository
   * @return all pull requests of the repository
   */
  public JsonNode getPullRequests(String repositoryOwner, String repositoryName);

  /**
   * Fetch a pull request.
   * 
   * @param repositoryOwner
   *          name of the owner of the repository
   * @param repositoryName
   *          name of the repository
   * @param pullRequestNumber
   *          number of the pull request
   * @return pull request with the given number
   */
  public JsonNode getPullRequest(String repositoryOwner, String repositoryName, int pullRequestNumber);

  /**
   * Fetch an issue.
   * 
   * @param repositoryOwner
   *          name of the owner of the repository
   * @param repositoryName
   *          name of the repository
   * @param pullRequestNumber
   *          number of the issue
   * @return issue with the given number
   */
  public JsonNode getIssue(String repositoryOwner, String repositoryName, String issueNumber);

  /**
   * Fetch all changed files of a pull request.
   * 
   * @param repositoryOwner
   *          name of the owner of the repository of the pull request
   * @param repositoryName
   *          name of the repository of the pull request
   * @param pullRequestNumber
   *          number of the pull request
   * @return all changed files of the pull request
   */
  public JsonNode getChangedFiles(String repositoryOwner, String repositoryName, int pullRequestNumber);

  /**
   * Get content of a file.
   * 
   * @param contentUrl
   *          url of the file
   * @return file content as {@link String}
   */
  public String getFileContent(String contentUrl);

  /**
   * Returns the diffs of a pull request.
   * 
   * @param repositoryOwner
   *          name of the owner of the repository
   * @param repositoryName
   *          name of the repository
   * @param pullRequestNumber
   *          number of the pull request
   * @param filename
   *          name of the file
   * @return the diffs of the pull request
   */
  public JsonNode getDiffs(String repositoryOwner, String repositoryName, int pullRequestNumber, String filename);

  /**
   * Get {@link JsonNode} with diffs per PullRequest to create heat map.
   * 
   * @param repositoryOwner
   *          name of the owner of the repository
   * @param repositoryName
   *          name of the repository
   * @param pullRequestNumber
   *          number of the pull request
   * @return diffs per PullRequest to create heat map
   */
  public JsonNode getDiffLines(String repositoryOwner, String repositoryName, String pullRequestNumber);

  /**
   * Get JsonNode with Readme information.
   * 
   * @param repositoryOwner
   *          name of the owner of the repository
   * @param repositoryName
   *          name of the repository
   * @return JsonNode with Readme information
   */
  public JsonNode getReadme(String repositoryOwner, String repositoryName);

  /**
   * Check wether a project is a maven project or not
   * 
   * @param repositoryOwner
   *          name of the owner of the repository
   * @param repositoryName
   *          name of the repository
   * @return bool
   */
  public JsonNode isMavenProject(String repositoryOwner, String repositoryName);
  
  /**
   * Merges the given pull requests in a comma separated string
   * 
   * @param repositoryOwner
   *          name of the owner of the repository
   * @param repositoryName
   *          name of the repository
   * @return response message
   */
  public JsonNode merge(String repositoryOwner, String repositoryName, final PullRequest pullRequest);

}
