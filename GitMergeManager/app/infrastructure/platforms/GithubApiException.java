package infrastructure.platforms;

/**
 * {@link Exception} that is thrown when the data returned from a VCS platform
 * contains an error message.
 */
public class GithubApiException extends RuntimeException {

  private static final long serialVersionUID = 1L;

  public GithubApiException() {
    super();
  }

  public GithubApiException(final String message) {
    super(message);
  }

}
