package infrastructure.git;

import org.eclipse.jgit.lib.NullProgressMonitor;
import org.eclipse.jgit.lib.ProgressMonitor;

/**
 * Provides methods to perform JGit operations on file system.
 * 
 */
public interface GitCommandExecutor {

  /**
   * Clones all branches of a specified repository from remote to local path. The {@link NullProgressMonitor} is used.
   * 
   * @param remoteUrl
   *          URL of the remote repository
   * @param localPath
   *          path of the local repository
   * @param branchName
   *          the branch that will be checked out
   * 
   * @return true if clone finished successfully, false otherwise
   */
  public boolean clone(final String remoteUrl, final String localPath, final String branchName);

  /**
   * Clones all branches of a specified repository from remote to local path.
   * 
   * @param remoteUrl
   *          URL of the remote repository
   * @param localPath
   *          path of the local repository
   * @param branchName
   *          the branch that will be checked out
   * @param progressMonitor
   *          {@link ProgressMonitor} to be used
   * 
   * @return true if clone finished successfully, false otherwise
   */
  public boolean clone(final String remoteUrl, final String localPath, final String branchName, final ProgressMonitor progressMonitor);

  /**
   * Clones a single branch from remote to local path.
   * 
   * @param remoteUrl
   *          URL of the remote repository
   * @param localPath
   *          path of the local repository
   * @param branchName
   *          name of the branch to be cloned
   * 
   * @return true if clone finished successfully, false otherwise
   */
  public boolean cloneSingleBranch(final String remoteUrl, final String localPath, final String branchName);

  /**
   * Clones a single branch from remote to local path.
   * 
   * @param remoteUrl
   *          URL of the remote repository
   * @param localPath
   *          path of the local repository
   * @param branchName
   *          name of the branch to be cloned
   * @param progressMonitor
   *          {@link ProgressMonitor} to be used
   * 
   * @return true if clone finished successfully, false otherwise
   */
  public boolean cloneSingleBranch(final String remoteUrl, final String localPath, final String branchName, final ProgressMonitor progressMonitor);

  /**
   * Fetches and merges the given branch on the provided repository.
   * 
   * @param baseRepoPath
   *          path to base repository
   * @param baseBranch
   *          branch on which the merge should be applied
   * @param headRepoPath
   *          path to the head repository
   * @param headBranch
   *          branch that should be merged
   * 
   * @return {@link GitMergeResult}
   */
  public GitMergeResult merge(final String baseRepoPath, final String baseBranch, final String headRepoPath, final String headBranch, final ProgressMonitor progressMonitor);

}