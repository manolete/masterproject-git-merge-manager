package infrastructure.git;

import org.eclipse.jgit.lib.RepositoryState;

public class GitMergeResult {

  private RepositoryState repositoryState;

  private String mergeCommitMessage;

  GitMergeResult(final RepositoryState repositoryState, final String mergeCommitMessage) {
    this.repositoryState = repositoryState;
    this.mergeCommitMessage = mergeCommitMessage;
  }

  public RepositoryState getRepositoryState() {
    return repositoryState;
  }

  public String getMergeCommitMessage() {
    return mergeCommitMessage;
  }

  public boolean mergeable() {
    return repositoryState.equals(RepositoryState.SAFE) || repositoryState.equals(RepositoryState.MERGING_RESOLVED);
  }

}
