package infrastructure.git;

import static util.StringUtil.SLASH;
import global.Loggers;
import infrastructure.filesystem.FileSystemOperator;

import java.io.File;
import java.io.IOException;
import java.util.Collections;

import org.eclipse.jgit.api.CheckoutCommand;
import org.eclipse.jgit.api.CloneCommand;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.lib.NullProgressMonitor;
import org.eclipse.jgit.lib.ProgressMonitor;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.lib.StoredConfig;
import org.eclipse.jgit.storage.file.FileRepositoryBuilder;

public class GitCommandExecutorImpl implements GitCommandExecutor {

  private static final String URL = "url";
  private static final String ORIGIN = "origin";
  private static final String REMOTE = "remote";
  private static final String GIT_FOLDER = ".git";
  private static final String REFS_HEADS = "refs" + SLASH + "heads" + SLASH;

  private Repository getRepoFromFilepath(final String localUrl) {
    final FileRepositoryBuilder builder = new FileRepositoryBuilder();
    try {
      final Repository repo = builder.setGitDir(new File(localUrl + SLASH + GIT_FOLDER)).readEnvironment().findGitDir().build();
      return repo;
    }
    catch (final IOException e) {
      e.printStackTrace();
    }
    return null;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean clone(final String remoteUrl, final String localPath, final String branchName) {
    return clone(remoteUrl, localPath, branchName, NullProgressMonitor.INSTANCE);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean clone(final String remoteUrl, final String localPath, final String branchName, final ProgressMonitor progressMonitor) {
    return clone(localPath, createAllBranchesCloneCommand(remoteUrl, localPath, branchName, progressMonitor));
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean cloneSingleBranch(final String remoteUrl, final String localPath, final String branchName) {
    return cloneSingleBranch(remoteUrl, localPath, branchName, NullProgressMonitor.INSTANCE);
  }

  @Override
  public boolean cloneSingleBranch(final String remoteUrl, final String localPath, final String branchName, final ProgressMonitor progressMonitor) {
    return clone(localPath, createSingleBranchCloneCommand(remoteUrl, localPath, branchName, progressMonitor));
  }

  private boolean clone(final String localPath, final CloneCommand cloneCommand) {
    final File directory = new File(localPath);
    FileSystemOperator.create().cleanAndDelete(directory);
    try {
      Loggers.FS.info("Clone in folder '" + localPath + "'");
      final Git git = cloneCommand.call();
      git.close();
    }
    catch (final GitAPIException e) {
      e.printStackTrace();
      return false;
    }
    return true;
  }

  private CloneCommand createAllBranchesCloneCommand(final String remoteUrl, final String localPath, final String branchName, final ProgressMonitor progressMonitor) {
    final CloneCommand cloneCommand = createBaseCloneCommand(remoteUrl, localPath, branchName, progressMonitor);
    cloneCommand.setCloneAllBranches(true);
    return cloneCommand;
  }

  private CloneCommand createSingleBranchCloneCommand(final String remoteUrl, final String localPath, final String branchName, final ProgressMonitor progressMonitor) {
    final CloneCommand cloneCommand = createBaseCloneCommand(remoteUrl, localPath, branchName, progressMonitor);
    cloneCommand.setBranchesToClone(Collections.singletonList(REFS_HEADS + branchName));
    return cloneCommand;
  }

  private CloneCommand createBaseCloneCommand(final String remoteUrl, final String localPath, final String branchName, final ProgressMonitor progressMonitor) {
    final CloneCommand command = Git.cloneRepository();
    command.setDirectory(new File(localPath));
    command.setURI(remoteUrl);
    command.setBranch(branchName);
    command.setProgressMonitor(progressMonitor);
    return command;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public GitMergeResult merge(final String baseRepoPath, final String baseBranch, final String headRepoPath, final String headBranch, final ProgressMonitor progressMonitor) {
    final Repository repository = getRepoFromFilepath(baseRepoPath);
    final Git git = new Git(repository);
    final StoredConfig config = git.getRepository().getConfig();
    config.setString(REMOTE, ORIGIN, URL, headRepoPath);

    try {
      final CheckoutCommand checkoutCommand = git.checkout().setName(baseBranch).setStartPoint(ORIGIN + SLASH + baseBranch);
      if (!repository.getFullBranch().startsWith(REFS_HEADS)) {
        checkoutCommand.setCreateBranch(true);
      }
      checkoutCommand.call();
      Loggers.FS.info("Merge in folder '" + baseRepoPath + "'");
      git.pull().setRemoteBranchName(headBranch).call();
      final GitMergeResult gitMergeResult = new GitMergeResult(git.getRepository().getRepositoryState(), git.getRepository().readMergeCommitMsg());
      git.close();
      return gitMergeResult;
    }
    catch (final GitAPIException | IOException e) {
      git.close();
      throw new RuntimeException(e.getMessage());
    }
  }
}
