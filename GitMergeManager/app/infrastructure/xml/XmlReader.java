package infrastructure.xml;

import java.io.File;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;

public class XmlReader {

  private static final String EMPTY = "";

  public static XmlReader create() {
    return new XmlReader();
  }

  private XmlReader() {}

  public Document getDocument(final String pathToXmlFile) {
    return getDocument(new File(pathToXmlFile));
  }

  public Document getDocument(final File xmlFile) {
    final DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
    try {
      final DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
      final Document document = documentBuilder.parse(xmlFile);
      document.normalize();
      return document;
    }
    catch (final Exception e) {
      return null;
    }
  }

  public List<Element> getElementsByTagName(final Document document, final String tagName) {
    return nodeListToElementList(document.getElementsByTagName(tagName));
  }

  public List<Element> getElementsByTagName(final Element element, final String tagName) {
    return nodeListToElementList(element.getElementsByTagName(tagName));
  }

  private List<Element> nodeListToElementList(final NodeList nodeList) {
    final List<Element> elements = Lists.newArrayList();
    for (int i = 0; i < nodeList.getLength(); i++) {
      final Node node = nodeList.item(i);
      if (node instanceof Element) {
        elements.add(((Element) node));
      }
    }
    return elements;
  }

  public Element getElementByTagName(final Document document, final String tagName) {
    return nodeListToElement(document.getElementsByTagName(tagName));
  }

  public Element getElementByTagName(final Element element, final String tagName) {
    return nodeListToElement(element.getElementsByTagName(tagName));
  }

  private Element nodeListToElement(final NodeList nodeList) {
    if (nodeList.getLength() != 0 && nodeList.item(0) instanceof Element) {
      return (Element) nodeList.item(0);
    }
    return null;
  }

  public String getTextContent(final Element element) {
    return element != null ? element.getTextContent() : EMPTY;
  }

  public String getElementTextContentByTagName(final Element element, final String tagName) {
    return getTextContent(getElementByTagName(element, tagName));
  }

  public String getAttributeValue(final Element element, final String attributeKey) {
    return element.getAttribute(attributeKey);
  }

  public List<Element> getChildElements(final Element element) {
    final List<Element> elements = Lists.newArrayList();
    final NodeList childNodes = element.getChildNodes();
    for (int i = 0; i < childNodes.getLength(); i++) {
      final Node node = childNodes.item(i);
      if (node instanceof Element) {
        elements.add((Element) node);
      }
    }
    return ImmutableList.copyOf(elements);
  }

  public Element getChildElementByTagName(final Element parentElement, final String tagName) {
    final NodeList childNodes = parentElement.getChildNodes();
    for (int i = 0; i < childNodes.getLength(); i++) {
      final Node node = childNodes.item(i);
      if (node instanceof Element) {
        final Element element = (Element) node;
        if (element.getTagName().equals(tagName)) {
          return element;
        }
      }
    }
    return null;
  }
}
