package infrastructure.async.interruptible;

import infrastructure.async.tasks.AbstractBaseAsyncTask;

/**
 * Implementation of {@link InterruptionObserver} that checks if an {@link Interruptible} should be interrupted due to a parent
 * {@link AbstractBaseAsyncTask} that should be cancelled.
 */
public class TaskInterruptionObserver implements InterruptionObserver {

  private final AbstractBaseAsyncTask<?, ?> task;

  public TaskInterruptionObserver(final AbstractBaseAsyncTask<?, ?> task) {
    this.task = task;
  }

  /**
   * Checks whether the {@link AbstractBaseAsyncTask} should be cancelled. If yes, true is returned to tell the {@link Interruptible} that it should
   * interrupt.
   */
  @Override
  public boolean shouldInterrupt() {
    return task.hasCancelRequest();
  }

}
