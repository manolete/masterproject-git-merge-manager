package infrastructure.async.interruptible;

/**
 * Default implementation of {@link InterruptionObserver}.
 */
public class DefaultInterruptionObserver implements InterruptionObserver {

  public static InterruptionObserver INSTANCE = new DefaultInterruptionObserver();

  /**
   * Avoid instantiation from outside.
   */
  private DefaultInterruptionObserver() {}

  /**
   * Always returns false.
   * 
   * {@inheritDoc}
   * 
   */
  @Override
  public boolean shouldInterrupt() {
    return false;
  }

}
