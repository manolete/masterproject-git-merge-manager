package infrastructure.async.interruptible;

/**
 * Wraps the actual result of the execution of an {@link Interruptible} and additionally holds the information if the {@link Interruptible} has been
 * finished or interrupted.
 * 
 * @param <Result>
 *          the actual result of the {@link Interruptible}
 */
public class InterruptibleResult<Result> {

  private final Result result;
  private final boolean interrupted;

  private InterruptibleResult(final Result result, final boolean interrupted) {
    this.result = result;
    this.interrupted = interrupted;
  }

  /**
   * Checks whether the {@link Interruptible} has been interrupted.
   * 
   * @return true if the {@link Interruptible} has been interrupted, false if it has been finished
   */
  public boolean interrupted() {
    return interrupted;
  }

  /**
   * Checks whether the {@link Interruptible} has been finished.
   * 
   * @return true if the {@link Interruptible} has been finished, false if it has been interrupted
   */
  public boolean finished() {
    return !interrupted();
  }

  /**
   * Returns the actual result of the {@link Interruptible}.
   * 
   * @return the actual result of the {@link Interruptible}
   */
  public Result get() {
    return result;
  }

  /**
   * Creates a {@link InterruptibleResult} for an {@link Interruptible} that has been interrupted.
   * 
   * @param result
   *          the actual result of the {@link Interruptible}
   * @return {@link InterruptibleResult} of the {@link Interruptible} that has been interrupted
   */
  public static <Result> InterruptibleResult<Result> interrupt(final Result result) {
    return new InterruptibleResult<Result>(result, true);
  }

  /**
   * Creates a {@link InterruptibleResult} for an {@link Interruptible} that has been finished.
   * 
   * @param result
   *          the actual result of the {@link Interruptible}
   * @return {@link InterruptibleResult} of the {@link Interruptible} that has been finished
   */
  public static <Result> InterruptibleResult<Result> finish(final Result result) {
    return new InterruptibleResult<Result>(result, false);
  }
}
