package infrastructure.async.interruptible;

/**
 * This is an interface for classes that provide a method that is interruptible at some points. The {@link #execute(Object)} method returns an
 * {@link InterruptibleResult}. This wraps the actual result and additionally holds the information if the method has been finished or interrupted.
 * 
 * @param <Parameter>
 *          an arbitrary parameter that can be passed to the {@link #execute(Object)}-method
 * @param <Result>
 *          the result returned after finishing or interrupting the {@link #execute(Object)}-method
 */
public interface Interruptible<Parameter, Result> {

  /**
   * Execution that can be interrupted at any point of time. The interruption has to be done in the subclass itself.
   * 
   * @param parameter
   *          parameter that can be used for execution
   * @return {@link InterruptibleResult} wrapping the actual result and holding the information if the method has been finished or interrupted
   */
  public InterruptibleResult<Result> execute(final Parameter parameter);

  /**
   * Sets the {@link InterruptionObserver}. The {@link InterruptionObserver} takes over the responsibility of checking if the process should
   * interrupt.
   * 
   * @param interruptionObserver
   *          {@link InterruptionObserver}
   */
  public void setInterruptionObserver(final InterruptionObserver interruptionObserver);

}
