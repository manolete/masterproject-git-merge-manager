package infrastructure.async.interruptible;

/**
 * Base implementation of {@link Interruptible}.
 * 
 * The {@link InterruptionObserver} is triggered by calling the {@link #shouldInterrupt()}-method. Subclasses have to do this at any point in the
 * {@link #execute(Object)}-method if they want to check whether they should interrupt the execution. If yes, they furthermore have to call
 * {@link #interrupt(Object)} themselves.
 * 
 */
public abstract class AbstractInterruptible<Parameter, Result> implements Interruptible<Parameter, Result> {

  private InterruptionObserver interruptionObserver;

  public AbstractInterruptible() {
    this.interruptionObserver = DefaultInterruptionObserver.INSTANCE;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void setInterruptionObserver(final InterruptionObserver interruptionObserver) {
    this.interruptionObserver = interruptionObserver;
  }

  /**
   * Checks whether the currently running {@link #execute(Object)}-method should be interrupted.
   * 
   * @return true if the work unit should be interrupted, false otherwise
   */
  protected boolean shouldInterrupt() {
    return interruptionObserver.shouldInterrupt();
  }

  /**
   * Should be called to interrupt the {@link #execute(Object)}-method.
   * 
   * @param result
   *          result that should be returned
   * @return {@link InterruptibleResult} stating that the execution has been interrupted
   */
  protected final InterruptibleResult<Result> interrupt(final Result result) {
    return InterruptibleResult.interrupt(result);
  }

  /**
   * Should be called to finish the {@link #execute(Object)}-method.
   * 
   * @param result
   *          result that should be returned
   * @return {@link InterruptibleResult} stating that the execution has been finished
   */
  protected final InterruptibleResult<Result> finish(final Result result) {
    return InterruptibleResult.finish(result);
  }

}
