package infrastructure.async.interruptible;

/**
 * An {@link InterruptionObserver} is responsible to check whether an {@link Interruptible} should be interrupted.
 */
public interface InterruptionObserver {

  /**
   * Is called by an {@link InterruptionObserver} and checks whether it should be interrupted.
   * 
   * @return true if the {@link InterruptionObserver} should be interrupted, false otherwise
   */
  public boolean shouldInterrupt();

}
