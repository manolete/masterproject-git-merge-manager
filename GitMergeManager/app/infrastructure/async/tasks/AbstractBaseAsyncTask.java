package infrastructure.async.tasks;

import play.Logger;

/**
 * This class can be used to execute some work asynchronously in a new {@link Thread}. It furthermore provides methods to cancel a running task.
 * 
 * @param <Parameter>
 *          parameter to be passed to the {@link AbstractBaseAsyncTask}
 * @param <Result>
 *          result that is passed to the {@link #onSuccess(Object)} as well as the {@link #onCancelled(Object)} method
 */
public abstract class AbstractBaseAsyncTask<Parameter, Result> implements AsyncTask<Parameter, Result> {

  private static final String FINISHED = "Finished";
  private static final String CANCELLED = "Cancelled";
  private static final String STARTED = "Started";

  private volatile State state;

  public AbstractBaseAsyncTask() {
    state = State.READY;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public final boolean start(final Parameter parameter) {
    if (isReady()) {
      info(STARTED);
      state = State.RUNNING;
      startThread(parameter);
      return true;
    }
    return false;
  }

  private void startThread(final Parameter parameter) {
    new Thread(new Runnable() {
      @Override
      public void run() {
        execute(parameter);
      }
    }).start();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void sendCancelRequest() {
    if (isReady()) {
      state = State.CANCELLED;
    }
    else if (isRunning()) {
      state = State.REQUESTING_CANCEL;
    }
  }

  /**
   * Cancels the execution. Has to be called by the task itself (i.e. in {@link #doWork(Object)}.
   * 
   * @param result
   *          the partial result that should be passed to {@link #onCancelled(Object)}
   * @return {@link AsyncResult}
   */
  protected AsyncResult<Result> cancel(final Result result) {
    final AsyncResult<Result> asyncResult = AsyncResult.cancel(result);
    return asyncResult;
  }

  /**
   * Finishes the execution.
   * 
   * @param result
   *          the result of the execution
   * @return {@link AsyncResult}
   */
  protected AsyncResult<Result> finish(final Result result) {
    final AsyncResult<Result> asyncResult = AsyncResult.finish(result);
    return asyncResult;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean isReady() {
    return state.equals(State.READY);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean isRunning() {
    return state.equals(State.RUNNING);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean hasCancelRequest() {
    return state.equals(State.REQUESTING_CANCEL);
  }

  private void execute(final Parameter parameter) {
    final AsyncResult<Result> result = doWork(parameter);
    if (result.cancelled() && state.equals(State.REQUESTING_CANCEL)) {
      state = State.CANCELLED;
      warning(CANCELLED);
      onCancelled(result.get());
    }
    else {
      state = State.FINISHED;
      info(FINISHED);
      onSuccess(result.get());
    }
  }

  /**
   * Contains the work that is actually executed by the task.
   * 
   * @param parameter
   *          parameter that can be used for execution
   * @return
   */
  protected abstract AsyncResult<Result> doWork(final Parameter parameter);

  /**
   * Is called after successfully finishing {@link #doWork(Object)}.
   * 
   * @param result
   *          the result of {@link #doWork(Object)}
   */
  protected abstract void onSuccess(final Result result);

  /**
   * Is called if the task is cancelled during execution.
   * 
   * @param result
   *          the partial result of the cancelled {@link #doWork(Object)}
   */
  protected abstract void onCancelled(final Result result);

  private static enum State {
    READY,
    RUNNING,
    REQUESTING_CANCEL,
    CANCELLED,
    FINISHED;
  }

  private void info(final String action) {
    Logger.info(message(action));
  }

  private void warning(final String action) {
    Logger.warn(message(action));
  }

  private String message(final String action) {
    return action + " async task '" + getClass().getSimpleName() + "'";
  }

}
