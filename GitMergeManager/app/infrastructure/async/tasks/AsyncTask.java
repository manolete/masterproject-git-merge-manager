package infrastructure.async.tasks;

/**
 * Interface for asynchronous executable tasks.
 */
public interface AsyncTask<Parameter, Result> {

  /**
   * Starts the execution of the {@link AsyncTask}. The {@link AsyncTask} can only be started if it has not been started before.
   * 
   * @param parameter
   *          parameter that is passed for execution
   * @return true if the task has been started, false otherwise
   */
  public boolean start(final Parameter parameter);

  /**
   * Sends a cancel request to the {@link AsyncTask}. This tells the task that it should stop as soon as possible. The task is then responsible itself to
   * cancel at a reasonable point of execution.
   */
  public void sendCancelRequest();

  /**
   * Checks whether the {@link AsyncTask} is ready to be executed.
   * 
   * @return true if the {@link AsyncTask} is ready to be executed, false otherwise
   */
  public boolean isReady();

  /**
   * Checks whether the {@link AsyncTask} is currently running.
   * 
   * @return true if the {@link AsyncTask} is running, false otherwise
   */
  public boolean isRunning();

  /**
   * Checks whether the {@link AsyncTask} became a request to cancel (but did not yet cancel).
   * 
   * @return true if the {@link AsyncTask} should be cancel, false otherwise
   */
  public boolean hasCancelRequest();

}
