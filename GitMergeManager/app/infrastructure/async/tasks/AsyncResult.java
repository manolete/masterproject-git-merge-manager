package infrastructure.async.tasks;

/**
 * Wraps the actual result of the execution of an {@link AsyncTask} and additionally holds the information if the {@link AsyncTask} has been finished
 * or cancelled.
 * 
 * @param <Result>
 *          the actual result of the {@link AsyncTask}
 */
public class AsyncResult<Result> {

  private final Result result;
  private final boolean cancelled;

  private AsyncResult(final Result result, final boolean cancelled) {
    this.result = result;
    this.cancelled = cancelled;
  }

  /**
   * Checks whether the {@link AsyncTask} has been cancelled.
   * 
   * @return true if the {@link AsyncTask} has been cancelled, false if it has been finished
   */
  public boolean cancelled() {
    return cancelled;
  }

  /**
   * Checks whether the {@link AsyncTask} has been finished.
   * 
   * @return true if the {@link AsyncTask} has been finished, false if it has been cancelled
   */
  public boolean finished() {
    return !cancelled();
  }

  /**
   * Returns the actual result of the {@link AsyncTask}.
   * 
   * @return the actual result of the {@link AsyncTask}
   */
  public Result get() {
    return result;
  }

  /**
   * Creates a {@link AsyncResult} for an {@link AsyncTask} that has been cancelled.
   * 
   * @param result
   *          the actual result of the {@link AsyncTask}
   * @return {@link AsyncResult} of the {@link AsyncTask} that has been interrupted
   */
  public static <Result> AsyncResult<Result> cancel(final Result result) {
    return new AsyncResult<Result>(result, true);
  }

  /**
   * Creates a {@link AsyncResult} for an {@link AsyncTask} that has been finished.
   * 
   * @param result
   *          the actual result of the {@link AsyncTask}
   * @return {@link AsyncResult} of the {@link AsyncTask} that has been finished
   */
  public static <Result> AsyncResult<Result> finish(final Result result) {
    return new AsyncResult<Result>(result, false);
  }
}
