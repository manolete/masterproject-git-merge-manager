package infrastructure.async.tasks;

/**
 * Allows tasks to be registered with a key.
 */
public interface AsyncTaskRegistry {

  /**
   * Registers a task with a given key. The task can only be registered if the key does not yet exist.
   * 
   * @param key
   *          key
   * @param task
   *          {@link AsyncTask}
   * @return true if the {@link AsyncTask} could be registered, false otherwise
   */
  public boolean register(String key, AsyncTask<?, ?> task);

  /**
   * Gets the {@link AsyncTask} that is registered with this key or null, if no {@link AsyncTask} is registered with this key.
   * 
   * @param key
   *          key
   * @return {@link AsyncTask} that is registered with this key or null, if no {@link AsyncTask} is registered with this key
   */
  public AsyncTask<?, ?> get(String key);

  /**
   * Removes an {@link AsyncTask} that is registered with the given key from the {@link AsyncTaskRegistry}.
   * 
   * @param key
   *          key for the {@link AsyncTask} to be removed
   * @return the {@link AsyncTask} that is removed or null, if no task is registered for the given key
   */
  public AsyncTask<?, ?> remove(String key);

}
