package infrastructure.async.tasks;

import java.util.HashMap;
import java.util.Map;

/**
 * Implementation of {@link AsyncTaskRegistry}.
 */
public class AsyncTaskRegistryImpl implements AsyncTaskRegistry {

  private Map<String, AsyncTask<?, ?>> registry;

  public AsyncTaskRegistryImpl() {
    registry = new HashMap<String, AsyncTask<?, ?>>();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean register(final String key, final AsyncTask<?, ?> task) {
    if (!registry.containsKey(key)) {
      registry.put(key, task);
      return true;
    };
    return false;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public AsyncTask<?, ?> get(final String key) {
    return registry.get(key);
  }

  @Override
  public AsyncTask<?, ?> remove(final String key) {
    return registry.remove(key);
  }

}
