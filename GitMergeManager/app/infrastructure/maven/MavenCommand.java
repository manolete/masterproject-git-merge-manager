package infrastructure.maven;

/**
 * Represents command that can be executed with maven. This is either a {@link MavenLifecyclePhase} or {@link MavenGoal}
 */
public interface MavenCommand {

  /**
   * Returns the textual representation of the {@link MavenCommand}.
   * 
   * @return textual representation of the {@link MavenCommand}
   */
  public String getText();

}
