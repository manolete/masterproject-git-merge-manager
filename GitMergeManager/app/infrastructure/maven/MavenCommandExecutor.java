package infrastructure.maven;

import java.io.File;

/**
 * Provides methods to execute {@link MavenCommand}'s.
 */
public interface MavenCommandExecutor {

  /**
   * Executes a specified phase of a maven lifecycle
   * 
   * @param projectRoot
   *          the root directory of the maven project
   * @param lifecyclePhase
   *          the lifecycle phase to be executed
   * @return {@link MavenCommandExecutionOutput}
   */
  public MavenCommandExecutionOutput execute(final File projectRoot, final MavenLifecyclePhase lifecyclePhase);

  /**
   * Executes a specified phase of a maven lifecycle
   * 
   * @param projectRoot
   *          the root directory of the maven project
   * @param progressMonitor
   *          a {@link MavenProgressMonitor}
   * @param lifecyclePhase
   *          the lifecycle phase to be executed
   * @return {@link MavenCommandExecutionOutput}
   */
  public MavenCommandExecutionOutput execute(final File projectRoot, MavenProgressMonitor progressMonitor, final MavenLifecyclePhase lifecyclePhase);

  /**
   * Executes a list of maven goals
   * 
   * @param projectRoot
   *          the root directory of the maven project
   * @param goals
   *          {@link MavenGoal}'s to be executed
   * @return {@link MavenCommandExecutionOutput}
   */
  public MavenCommandExecutionOutput execute(final File projectRoot, final MavenGoal... goals);

  /**
   * Executes a list of maven goals
   * 
   * @param projectRoot
   *          the root directory of the maven project
   * @param progressMonitor
   *          a {@link MavenProgressMonitor}
   * @param goals
   *          {@link MavenGoal}'s to be executed
   * @return {@link MavenCommandExecutionOutput}
   */
  public MavenCommandExecutionOutput execute(final File projectRoot, MavenProgressMonitor progressMonitor, final MavenGoal... goals);

}
