package infrastructure.maven;

public class NullMavenProgressMonitor implements MavenProgressMonitor {

  public static MavenProgressMonitor INSTANCE = new NullMavenProgressMonitor();

  private NullMavenProgressMonitor() {}

  @Override
  public void update(final String line) {
    // do nothing
  }

}
