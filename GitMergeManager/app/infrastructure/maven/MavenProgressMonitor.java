package infrastructure.maven;

/**
 * Interface for a progress monitor for maven execution.
 */
public interface MavenProgressMonitor {

  /**
   * Execute an arbitrary action.
   * 
   * @param line
   *          line sent to standard output
   */
  public void update(String line);

}
