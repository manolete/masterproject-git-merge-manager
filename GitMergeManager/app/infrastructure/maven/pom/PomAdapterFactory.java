package infrastructure.maven.pom;

import java.io.File;

/**
 * Factory for {@link PomAdapter}.
 */
public interface PomAdapterFactory {

  public static final PomAdapterFactory INSTANCE = new PomAdapterFactoryImpl();

  /**
   * Creates a {@link PomAdapter}.
   * 
   * @param projectRoot
   *          the root path of the maven project
   * @return created {@link PomAdapter}
   */
  public PomAdapter create(final File projectRoot);
}
