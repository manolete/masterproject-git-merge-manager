package infrastructure.maven.pom;

import java.io.File;

/**
 * Implementation of {@link PomAdapterFactory}.
 */
public class PomAdapterFactoryImpl implements PomAdapterFactory {

  /**
   * {@inheritDoc}
   */
  @Override
  public PomAdapter create(final File projectRoot) {
    return new PomAdapterImpl(projectRoot);
  }

}
