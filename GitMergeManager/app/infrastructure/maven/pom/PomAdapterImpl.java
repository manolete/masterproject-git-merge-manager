package infrastructure.maven.pom;

import infrastructure.maven.MavenPluginType;
import infrastructure.xml.XmlReader;

import java.io.File;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;

import util.StringUtil;

/**
 * Implementation of {@link PomAdapter}.
 */
public class PomAdapterImpl implements PomAdapter {

  private static final String POM_XML = "pom.xml";
  private static final String GOAL = "goal";
  private static final String PHASE = "phase";
  private static final String CONFIGURATION = "configuration";
  private static final String EXECUTION = "execution";
  private static final String BUILD = "build";
  private static final String PLUGIN = "plugin";
  private static final String GROUP_ID = "groupId";
  private static final String ARTIFACT_ID = "artifactId";
  private static final String VERSION = "version";
  private static final String DIRECTORY = "directory";
  private static final String TARGET = "target";

  private final File projectRoot;
  private final File pomFile;
  private Document pomXmlFile;
  private final XmlReader xmlReader;

  public PomAdapterImpl(final File projectRoot) {
    this.projectRoot = projectRoot;
    this.pomFile = new File(projectRoot, POM_XML);
    this.xmlReader = XmlReader.create();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public List<PomPluginConfiguration> getPluginConfigurations() {
    final List<PomPluginConfiguration> pluginConfigurations = Lists.newArrayList();
    final Element buildElement = xmlReader.getElementByTagName(getPomAsXml(), BUILD);
    if (buildElement != null) {
      final List<Element> pluginElements = xmlReader.getElementsByTagName(buildElement, PLUGIN);
      for (final Element element : pluginElements) {
        pluginConfigurations.add(createPluginConfigurationFromXml(element));
      }
    }
    return ImmutableList.copyOf(pluginConfigurations);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public PomPluginConfiguration getPluginConfiguration(final MavenPluginType plugin) {
    for (final PomPluginConfiguration pluginConfiguration : getPluginConfigurations()) {
      if (pluginConfiguration.isOfType(plugin)) {
        return pluginConfiguration;
      }
    }
    return null;
  }

  private PomPluginConfiguration createPluginConfigurationFromXml(final Element pluginElement) {
    // create plugin configuration
    final Element groupIdElement = xmlReader.getElementByTagName(pluginElement, GROUP_ID);
    final String groupId = groupIdElement != null ? groupIdElement.getTextContent() : MavenPluginType.DEFAULT_GROUP_ID;
    final String artifactId = xmlReader.getElementByTagName(pluginElement, ARTIFACT_ID).getTextContent();
    final Element versionElement = xmlReader.getElementByTagName(pluginElement, VERSION);
    final PomPluginConfiguration pluginConfiguration = new PomPluginConfiguration(groupId, artifactId);
    if (versionElement != null) {
      pluginConfiguration.setVersion(versionElement.getTextContent());
    }

    // add executions
    final List<Element> executionElements = xmlReader.getElementsByTagName(pluginElement, EXECUTION);
    for (final Element executionElement : executionElements) {
      pluginConfiguration.addExecution(createExecutionFromXml(executionElement));
    }

    // add configurations
    final Element configurationElement = xmlReader.getElementByTagName(pluginElement, CONFIGURATION);
    if (configurationElement != null && configurationElement.hasChildNodes()) {
      for (final Element element : xmlReader.getChildElements(configurationElement)) {
        pluginConfiguration.addConfiguration(element.getNodeName(), element.getTextContent());
      }
    }

    return pluginConfiguration;
  }

  private PomPluginExecutionConfiguration createExecutionFromXml(final Element executionElement) {
    final String phase = xmlReader.getElementTextContentByTagName(executionElement, PHASE);
    final PomPluginExecutionConfiguration executionConfiguration = new PomPluginExecutionConfiguration(phase);
    final List<Element> goalElements = xmlReader.getElementsByTagName(executionElement, GOAL);
    for (final Element goalElement : goalElements) {
      executionConfiguration.addGoal(goalElement.getTextContent());
    }
    return executionConfiguration;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public List<PomPluginConfiguration> getDefaultLifecyclePluginConfigurations() {
    final List<PomPluginConfiguration> pluginConfigurations = Lists.newArrayList();
    for (final PomPluginConfiguration pluginConfiguration : getPluginConfigurations()) {
      if (pluginConfiguration.isConfiguredForDefaultLifecycle()) {
        pluginConfigurations.add(pluginConfiguration);
      }
    }
    return pluginConfigurations;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public PomPluginConfiguration getDefaultLifecyclePluginConfiguration(final MavenPluginType plugin) {
    for (final PomPluginConfiguration pluginConfiguration : getDefaultLifecyclePluginConfigurations()) {
      if (pluginConfiguration.isOfType(plugin)) {
        return pluginConfiguration;
      }
    }
    return null;
  }

  private Document getPomAsXml() {
    if (pomXmlFile == null) {
      pomXmlFile = xmlReader.getDocument(pomFile);
    }
    return pomXmlFile;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean isBuildDirInsideProjectRoot() {
    return isBuildDirAbsolute();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public File getAbsoluteBuildDir() {
    final File buildDir = getBuildDir();
    return buildDir.isAbsolute() ? buildDir : new File(projectRoot, buildDir.getPath());
  }

  private boolean isBuildDirAbsolute() {
    return getBuildDir().isAbsolute();
  }

  private File getBuildDir() {
    return hasBuildDirConfig() ? getConfiguredBuildDir() : getRelativeDefaultBuildDir();
  }

  private boolean hasBuildDirConfig() {
    return getBuildDirConfig() != null;
  }

  private String getBuildDirConfig() {
    final Element buildElement = xmlReader.getElementByTagName(getPomAsXml(), BUILD);
    if (buildElement != null) {
      final Element buildDirElement = xmlReader.getChildElementByTagName(buildElement, DIRECTORY);
      if (buildDirElement != null) {
        return xmlReader.getTextContent(buildDirElement);
      }
    }
    return null;
  }

  private File getConfiguredBuildDir() {
    String configuredBuildDir = getBuildDirConfig();
    if (configuredBuildDir != null && configuredBuildDir.startsWith(PROJECT_BASEDIR)) {
      configuredBuildDir = removeDirVariable(configuredBuildDir, PROJECT_BASEDIR);
    }
    return new File(configuredBuildDir);
  }

  private File getRelativeDefaultBuildDir() {
    return new File(TARGET);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public File getAbsolutePath(final String configuredPath) {
    if (configuredPath != null) {
      if (configuredPath.startsWith(PomAdapter.PROJECT_BASEDIR)) {
        return new File(projectRoot, removeDirVariable(configuredPath, PomAdapter.PROJECT_BASEDIR));
      }
      else if (configuredPath.startsWith(PomAdapter.BUILD_DIR)) {
        return new File(projectRoot, new File(getBuildDir(), removeDirVariable(configuredPath, PomAdapter.BUILD_DIR)).getPath());
      }
      else {
        final File path = new File(configuredPath);
        return path.isAbsolute() ? path : new File(projectRoot, configuredPath);
      }
    }
    return null;
  }

  /**
   * Removes a directory-variable from the beginning of a path.
   * 
   * Example: path = ${project.basedir}/folder => result = folder
   * 
   * @param path
   *          the path as {@link String}
   * @param variable
   *          the variable to be removed
   * @return path without the removed variable
   */
  private String removeDirVariable(final String path, final String variable) {
    return StringUtils.strip(StringUtils.removeStart(path, variable), StringUtil.SLASH + StringUtil.BACKSLASH);
  }

}
