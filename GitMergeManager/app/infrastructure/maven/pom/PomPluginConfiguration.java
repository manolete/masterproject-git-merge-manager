package infrastructure.maven.pom;

import infrastructure.maven.MavenPluginType;

import java.util.List;
import java.util.Map;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

/**
 * Wraps the configurations of a maven plugin in the build-section of the pom-file.
 */
public class PomPluginConfiguration {

  private final String groupId;
  private final String artifactId;
  private String version;

  private final List<PomPluginExecutionConfiguration> executions;

  private final Map<String, String> configurations;

  public PomPluginConfiguration(final String groupId, final String artifactId) {
    this.groupId = groupId;
    this.artifactId = artifactId;
    this.executions = Lists.newArrayList();
    this.configurations = Maps.newHashMap();
  }

  public String getGroupId() {
    return groupId;
  }

  public String getArtifactId() {
    return artifactId;
  }

  public void setVersion(final String version) {
    this.version = version;
  }

  public String getVersion() {
    return version;
  }

  public boolean isOfType(final MavenPluginType plugin) {
    return getGroupId().equals(plugin.getGroupId()) && getArtifactId().equals(plugin.getArtifactid());
  }

  public List<PomPluginExecutionConfiguration> getExecutions() {
    return ImmutableList.copyOf(executions);
  }

  public void addExecution(final PomPluginExecutionConfiguration execution) {
    executions.add(execution);
  }

  public boolean isConfiguredForDefaultLifecycle() {
    for (final PomPluginExecutionConfiguration execution : executions) {
      if (execution.isConfiguredForDefaultLifecycle()) {
        return true;
      }
    }
    return false;
  }

  public Map<String, String> getConfigurations() {
    return ImmutableMap.copyOf(configurations);
  }

  public String getConfiguration(final String key) {
    return configurations.get(key);
  }

  public boolean existsConfiguration(final String key) {
    return configurations.containsKey(key);
  }

  public void addConfiguration(final String key, final String value) {
    configurations.put(key, value);
  }

  @Override
  public String toString() {
    String executionsAsString = "executions: {";
    for (int i = 0; i < getExecutions().size(); i++) {
      executionsAsString += "[" + executions.toString() + "]";
      if ((i + 1) != getExecutions().size()) {
        executionsAsString += ", ";
      }
    }
    executionsAsString += "}";

    String configurationsAsString = "configurations: {";
    int i = 0;
    for (final Map.Entry<String, String> entry : getConfigurations().entrySet()) {
      configurationsAsString += entry.getKey() + ": " + entry.getValue();
      i++;
      if (i != getConfigurations().size()) {
        configurationsAsString += ", ";
      }
    }
    configurationsAsString += "}";

    return "[" + "<" + groupId + ", " + artifactId + ", " + version + ">" + ", " + executionsAsString + ", " + configurationsAsString + "]";
  }
}
