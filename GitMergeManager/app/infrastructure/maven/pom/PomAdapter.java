package infrastructure.maven.pom;

import infrastructure.maven.MavenPluginType;

import java.io.File;
import java.util.List;

/**
 * Provides methods to access elements of a maven POM file.
 */
public interface PomAdapter {

  public static final String PROJECT_BASEDIR = "${project.basedir}";
  public static final String BUILD_DIR = "${project.build.directory}";

  /**
   * Returns a {@link List} of all {@link PomPluginConfiguration}'s that are listed in the 'build' section
   * 
   * @return {@link List} of all {@link PomPluginConfiguration}'s that are listed in the 'build' section
   */
  public List<PomPluginConfiguration> getPluginConfigurations();

  /**
   * Checks if there exists a configuration for a given {@link MavenPluginType} and returns it.
   * 
   * @param plugin
   *          {@link MavenPluginType}
   * @return configuration for the given {@link MavenPluginType} or null, if no appropriate configuration exists
   */
  public PomPluginConfiguration getPluginConfiguration(final MavenPluginType plugin);

  /**
   * Returns a {@link List} of all plugins that are configured to be executed in the default lifecycle. (plugins that are listed in the POM section
   * 'build' are only executed if they are linked with a lifecycle phase)
   * 
   * @return {@link List} of all plugins that are configured to be executed in the default lifecycle
   */
  public List<PomPluginConfiguration> getDefaultLifecyclePluginConfigurations();

  /**
   * Checks if there exists a configuration for a given {@link MavenPluginType} and returns it.
   * 
   * @param plugin
   *          {@link MavenPluginType}
   * @return configuration for the given {@link MavenPluginType} or null, if no appropriate configuration exists
   */
  public PomPluginConfiguration getDefaultLifecyclePluginConfiguration(final MavenPluginType plugin);

  /**
   * Checks whether the build directory (.i.e. ${project.build.directory}) is inside the project root directory.
   * 
   * @return true if the build directory is inside the project root directory, false otherwise
   */
  public boolean isBuildDirInsideProjectRoot();

  /**
   * Returns the absolute path to the build directory.
   * 
   * @return absolute path to the build directory.
   */
  public File getAbsoluteBuildDir();

  /**
   * Parses a path that is given as {@link String}, replaces possible directory variables and returns the correct absolute path.
   * 
   * @param configuredPath
   *          the path as {@link String}
   * @return absolute path
   */
  public File getAbsolutePath(final String configuredPath);

}
