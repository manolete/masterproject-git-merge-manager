package infrastructure.maven.pom;

import java.util.List;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;

/**
 * Wraps the execution configurations of a maven plugin in the build-section of the pom-file.
 */
public class PomPluginExecutionConfiguration {

  private final String phase;
  private final List<String> goals;

  public PomPluginExecutionConfiguration(final String phase) {
    this.phase = phase;
    this.goals = Lists.newArrayList();
  }

  public String getPhase() {
    return phase;
  }

  public List<String> getGoals() {
    return ImmutableList.copyOf(goals);
  }

  public void addGoal(final String goal) {
    goals.add(goal);
  }

  /**
   * Checks whether this {@link PomPluginExecutionConfiguration} is configured to be executed in the maven default lifecycle (i.e. at least one goal
   * is specified).
   * 
   * @return true if this {@link PomPluginExecutionConfiguration} is configured to be executed in the maven default lifecycle, false otherwise
   */
  public boolean isConfiguredForDefaultLifecycle() {
    return goals.size() > 0;
  }
}
