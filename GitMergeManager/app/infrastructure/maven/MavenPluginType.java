package infrastructure.maven;

/**
 * Represents the type of a maven plugin.
 */
public enum MavenPluginType {

  SUREFIRE(MavenPluginType.DEFAULT_GROUP_ID, "maven-surefire-plugin", "running tests"),
  CHECKSTYLE(MavenPluginType.DEFAULT_GROUP_ID, "maven-checkstyle-plugin", "running SCA with checkstyle"),
  PMD(MavenPluginType.DEFAULT_GROUP_ID, "maven-pmd-plugin", "running SCA with PMD");

  public static final String DEFAULT_GROUP_ID = "org.apache.maven.plugins";

  private final String groupId;
  private final String artifactid;
  private final String description;

  MavenPluginType(final String groupId, final String artifactId, final String description) {
    this.groupId = groupId;
    this.artifactid = artifactId;
    this.description = description;
  }

  public String getGroupId() {
    return groupId;
  }

  public String getArtifactid() {
    return artifactid;
  }

  public String getDescription() {
    return description;
  }
}
