package infrastructure.maven;

/**
 * Represents a maven goal.
 */
public enum MavenGoal implements MavenCommand {

  PMD("pmd:pmd"),
  CHECKSTYLE("checkstyle:checkstyle");

  private String text;

  private MavenGoal(final String text) {
    this.text = text;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String getText() {
    return text;
  }
}
