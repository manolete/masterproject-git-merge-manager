package infrastructure.maven;

import java.io.File;
import java.util.Collections;
import java.util.List;

import org.apache.maven.shared.invoker.DefaultInvocationRequest;
import org.apache.maven.shared.invoker.DefaultInvoker;
import org.apache.maven.shared.invoker.InvocationOutputHandler;
import org.apache.maven.shared.invoker.InvocationRequest;
import org.apache.maven.shared.invoker.Invoker;
import org.apache.maven.shared.invoker.MavenInvocationException;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;

/**
 * Implementation of {@link MavenCommandExecutor}.
 */
public class MavenCommandExecutorImpl implements MavenCommandExecutor {

  private static final String POM_XML = "pom.xml";

  private final Invoker invoker;

  private MavenCommandExecutorImpl(final Invoker invoker) {
    this.invoker = invoker;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public MavenCommandExecutionOutput execute(final File projectRoot, final MavenLifecyclePhase lifecyclePhase) {
    return execute(projectRoot, NullMavenProgressMonitor.INSTANCE, lifecyclePhase);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public MavenCommandExecutionOutput execute(final File projectRoot, final MavenProgressMonitor progressMonitor, final MavenLifecyclePhase lifecyclePhase) {
    return execute(projectRoot, progressMonitor, Collections.singletonList(lifecyclePhase.getText()));
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public MavenCommandExecutionOutput execute(final File projectRoot, final MavenGoal... goals) {
    return execute(projectRoot, NullMavenProgressMonitor.INSTANCE, goals);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public MavenCommandExecutionOutput execute(final File projectRoot, final MavenProgressMonitor progressMonitor, final MavenGoal... goals) {
    final List<String> commands = Lists.newArrayList();
    for (final MavenGoal goal : goals) {
      commands.add(goal.getText());
    }
    return execute(projectRoot, progressMonitor, commands);
  }

  private MavenCommandExecutionOutput execute(final File projectRoot, final MavenProgressMonitor progressMonitor, final List<String> commands) {
    final List<String> output = Lists.newArrayList();

    final InvocationRequest invocationRequest = new DefaultInvocationRequest();
    invocationRequest.setPomFile(new File(projectRoot, POM_XML));
    invocationRequest.setGoals(commands);
    invocationRequest.setOutputHandler(new InvocationOutputHandler() {
      @Override
      public void consumeLine(final String line) {
        progressMonitor.update(line);
        output.add(line);
      }
    });

    try {
      final int exitCode = invoker.execute(invocationRequest).getExitCode();
      return MavenCommandExecutionOutput.create(exitCode, ImmutableList.copyOf(output));
    }
    catch (final MavenInvocationException e) {
      return MavenCommandExecutionOutput.createNullOutput();
    }
  }

  public static MavenCommandExecutor create(final String mavenHome) {
    final Invoker invoker = new DefaultInvoker();
    invoker.setMavenHome(new File(mavenHome));
    return new MavenCommandExecutorImpl(invoker);
  }
}
