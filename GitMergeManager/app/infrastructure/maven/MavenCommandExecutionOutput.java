package infrastructure.maven;

import java.util.List;

import util.StringUtil;

import com.google.common.collect.ImmutableList;

/**
 * This class wraps the output of a {@link MavenCommand} execution.
 */
public class MavenCommandExecutionOutput {

  private static final String COMPILATION_ERROR = "COMPILATION ERROR";

  private final int exitCode;
  private final ImmutableList<String> consoleOutput;

  private MavenCommandExecutionOutput(final int exitCode, final ImmutableList<String> consoleOutput) {
    super();
    this.exitCode = exitCode;
    this.consoleOutput = consoleOutput;
  }

  /**
   * Returns a {@link List} containing the lines of the console output.
   * 
   * @return {@link List} containing the lines of the console output
   */
  public List<String> getConsoleOutput() {
    return consoleOutput;
  }

  /**
   * Returns the console output as text.
   * 
   * @return console output as text
   */
  public String getConsoleOutputAsText() {
    return StringUtil.merge(consoleOutput, StringUtil.NEWLINE);
  }

  /**
   * Returns the exit code of the {@link MavenCommand} execution.
   * 
   * @return exit code of the {@link MavenCommand} execution
   */
  public int getExitCode() {
    return exitCode;
  }

  /**
   * Checks if the compilation was okay.
   * 
   * An exit code of zero implies that there are no compilation errors. An exit code of non-zero can also arise due to test failures or for example
   * PMD violations. Therefore the output has to be parsed for possible build failures in this case.
   * 
   * @return
   */
  public boolean compilationOk() {
    return exitCode == 0 || !hasCompilationError();
  }

  private boolean hasCompilationError() {
    for (final String line : consoleOutput) {
      if (line.contains(COMPILATION_ERROR)) {
        return true;
      }
    }
    return false;
  }

  public static MavenCommandExecutionOutput create(final int exitCode, final ImmutableList<String> consoleOutput) {
    return new MavenCommandExecutionOutput(exitCode, consoleOutput);
  }

  public static MavenCommandExecutionOutput createNullOutput() {
    final ImmutableList<String> emptyList = ImmutableList.of();
    return create(-1, emptyList);
  }

}
