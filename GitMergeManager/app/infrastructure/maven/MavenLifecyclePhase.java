package infrastructure.maven;

/**
 * Represents a maven lifecycle phase.
 */
public enum MavenLifecyclePhase implements MavenCommand {

  CLEAN("clean"),
  COMPILE("copmile"),
  TEST("test"),
  INTEGRATION_TEST("integration-test"),
  VERIFY("verify");

  private String text;

  private MavenLifecyclePhase(final String text) {
    this.text = text;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String getText() {
    return text;
  }

}
