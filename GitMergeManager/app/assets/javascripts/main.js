require.config({
   baseUrl: "assets/javascripts",
   
   paths : {
           angular : [ 'https://ajax.googleapis.com/ajax/libs/angularjs/1.2.9/angular.min', 'angular.min' ],
           angularroute : [ 'https://code.angularjs.org/1.2.9/angular-route.min', 'angular-route.min' ],
           underscore: ['https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.5.2/underscore-min', 'underscore-min'],
           uibootstrap : 'ui-bootstrap-tpls-0.10.0.min',
           d3: [ 'https://cdnjs.cloudflare.com/ajax/libs/d3/3.4.9/d3.min', 'd3.min' ],
           angularAMD: 'angularAMD',
           ngload: 'ngload',
           
           HomeCtrl: 'controllers/HomeCtrl',
           PullRequestCtrl: 'controllers/PullRequestCtrl',
           PullRequestSizeFileCtrl: 'controllers/PullRequestSizeFileCtrl',
           RepositoryCtrl: 'controllers/RepositoryCtrl',
           PullRequestOverlappingCtrl: 'controllers/PullRequestOverlappingCtrl',
           MergingPlanCtrl: 'controllers/MergingPlanCtrl',
           StatusCtrl: 'controllers/StatusCtrl',
           ResultCtrl: 'controllers/ResultCtrl',
           FirstChoiceCtrl: 'controllers/FirstChoiceCtrl',
           FinalChoiceCtrl: 'controllers/FinalChoiceCtrl',
           FilteringCtrl: 'controllers/FilteringCtrl',
           AnalysisCtrl: 'controllers/AnalysisCtrl'
   },
   shim: {  
       'angularAMD': ['angular'],
       'angularroute': ['angular'],
       
       'underscore': {
 	      exports: '_'
 	  	},       
      'bootstrap':{
                  deps:['jquery']
      },
      'uibootstrap': { deps: ['angular']}
   },
   
   // kick start application
   deps: ['init']
});
