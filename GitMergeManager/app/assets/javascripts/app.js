define(['angularAMD', 'angularroute', 'uibootstrap'], function (angularAMD) {
	
  var pullRequestApp = angular.module('PullRequestApp', [
    'ngRoute',
    'ui.bootstrap'
  ]);

  var routePrefix = '/:owner/:repository';
  pullRequestApp.config(['$routeProvider',
      function($routeProvider) {
      $routeProvider.
      when(routePrefix +'/PullRequests', angularAMD.route({
      	  templateUrl: 'assets/partials/pullRequests.html',
          controller: 'FirstChoiceCtrl'
      })).
      when(routePrefix + '/PullRequests/:pullRequestNumber', angularAMD.route({
          templateUrl: 'assets/partials/pullRequest.html',
          controller: 'PullRequestCtrl',
	  reloadOnSearch: false
      })).
      when(routePrefix + '/Repository', angularAMD.route({
        templateUrl: 'assets/partials/repository.html',
        controller: 'RepositoryCtrl'
      })).
      when('/', angularAMD.route({
          templateUrl: 'assets/partials/home.html',
          controller: 'HomeCtrl'
      })).
      when('/Repositories', angularAMD.route({
          templateUrl: 'assets/partials/allFavorites.html',
          controller: 'RepositoryCtrl'
      })).
      when(routePrefix + '/Visualization/PullRequestSizeFile', angularAMD.route({
    	  templateUrl: 'assets/partials/pullRequestSizeFile.html',
    	  controller: 'PullRequestSizeFileCtrl'
      })).
      when(routePrefix + '/Visualization/PullRequestOverlapping', angularAMD.route({
    	  templateUrl: 'assets/partials/pullRequestOverlapping.html',
    	  controller: 'PullRequestOverlappingCtrl'
      })).
      when(routePrefix + '/Status/:analysisId', angularAMD.route({
    	  templateUrl: 'assets/partials/status.html',
    	  controller: 'StatusCtrl'
      })).
      when(routePrefix + '/Results/:analysisId', angularAMD.route({
    	  templateUrl: 'assets/partials/results.html',
    	  controller: 'ResultCtrl'
      })).
      when(routePrefix + '/Analyses', angularAMD.route({
    	  templateUrl: 'assets/partials/analyses.html',
    	  controller: 'AnalysisCtrl'
      })).
      when(routePrefix + '/FinalChoice', angularAMD.route({
    	  templateUrl: 'assets/partials/finalChoice.html',
    	  controller: 'FinalChoiceCtrl'
      })).
      otherwise({
        redirectTo: '/'
      });
  }]);
  
  pullRequestApp.run(function($rootScope) {	        
      $rootScope.$on("$routeChangeStart", function(event, next, current) {
    	  $rootScope.owner = next.params.owner;
	      $rootScope.repository = next.params.repository;
	      $rootScope.currentTab;
	      $rootScope.searching = false;
	  });
  });
  
  return pullRequestApp;
});
