var $jq = jQuery.noConflict(); 

"use strict";
define(['app'], function(app){
    app.service('FilteringService', function($http, $rootScope, $window, $routeParams) {
    	
    	this.PullRequests = [];
    	this.currentFilteringStage = 1;
    	
    	this.ChangedFiles = {};
    	this.Diffs = {};
    	this.DiffLines = {};
    	 
    	this.deleteLoadedData = function() {
         	this.PullRequests = [];
        	this.currentFilteringStage = 1;
        	
        	this.ChangedFiles = {};
        	this.Diffs = {};
        	this.DiffLines = {};
     	 };
    	
    	this.getPullRequests = function() {
    		
    		if(this.PullRequests > 0 && this.PullRequests[0].repository != $rootScope.repository)
			{
				this.deleteLoadedData();
			}
    		
    		if(Object.keys(this.PullRequests).length == 0 && $rootScope.owner != null) {
	    		var url = jsRoutes.controllers.ReposController.pulls($rootScope.owner, $rootScope.repository).url;
	    		this.PullRequests = $http.get(url).then(function(response) { return response.data; });
    		}

     		return this.PullRequests;
    	};
    	
    	this.updateRepository = function() {
    		var url = jsRoutes.controllers.ReposController.update($rootScope.owner, $rootScope.repository).url;
    		$http.get(url).then(function(response) { return response.data; });
    	};
    	
    	this.getChangedLines = function(pullRequestNumber) {
    		if(this.DiffLines.hasOwnProperty(pullRequestNumber) == false) {
	    		var url = jsRoutes.controllers.GitHubController.getDiffLines($rootScope.owner, $rootScope.repository, pullRequestNumber).url;
	    		this.DiffLines[pullRequestNumber] = $http.get(url).success(function(response) { return response.data; });
    		}
    		
    		return this.DiffLines[pullRequestNumber];
    	};
    	
    	this.getChangedFiles = function(serverUpdate, requestNumber) {  
    		// only load the files when not loaded yet
    		if(this.ChangedFiles.hasOwnProperty(requestNumber) == false) {
	    		var url = jsRoutes.controllers.GitHubController.getChangedFiles(
	            $rootScope.owner,
	            $rootScope.repository,
	            requestNumber).url;
	
	            this.ChangedFiles[requestNumber] = $http.get(url).success(function(response) { return response.data; });
    		}
            
            return this.ChangedFiles[requestNumber];
    	};
    	
    	this.getDiffs = function(requestNumber, filename)  {
    		if(this.Diffs.hasOwnProperty(requestNumber) == false) {
    			this.Diffs[requestNumber] = {};
    		};
    		
    		if(this.Diffs[requestNumber].hasOwnProperty(filename) == false) {
    	    		var url = jsRoutes.controllers.GitHubController.getDiffs($rootScope.owner, $rootScope.repository, requestNumber, filename).url;
    	    		this.Diffs[requestNumber][filename] = $http.get(url).success(function(response) {return response.data; });
    		}    		
    		
    		return this.Diffs[requestNumber][filename];
    	};  	
 	  	
 	  	this.getChosenPullRequests = function() {
 	  		return this.ChosenPullRequests;
 	  	};
 	  	
 	  	this.saveChosenPullRequests = function(data) {
 	  		this.ChosenPullRequests = data;
 	  	};
 	  	
 	  	this.setCurrentFilteringStage = function(stage) {
 	  		this.currentFilteringStage = stage;
 	  	};
 	  	
 	  	this.getCurrentFilteringStage = function() {
 	  		return this.currentFilteringStage;
 	  	};
 	  	
 	  	this.goForward = function() {
 	  		this.currentFilteringStage = this.currentFilteringStage + 1;
 	  		this.goToFilteringStage();
 	  	};
 	  	
 	  	this.goToFilteringStage = function() {
 	  		if(this.currentFilteringStage > 4 || this.currentFilteringStage < 1) {
 	  			$window.location = '#/' + $routeParams.owner + '/' + $routeParams.repository + '/PullRequests'; 
 	  		} else {
 	  			switch (this.currentFilteringStage) {
	 	  			case 1: $window.location = '#/' + $routeParams.owner + '/' + $routeParams.repository + '/PullRequests'; break;
	 	  			case 2: $window.location = '#/' + $routeParams.owner + '/' + $routeParams.repository + '/Visualization/PullRequestSizeFile'; break;
	 	  			case 3: $window.location = '#/' + $routeParams.owner + '/' + $routeParams.repository + '/Visualization/PullRequestOverlapping'; break;
	 	  			case 4: $window.location = '#/' + $routeParams.owner + '/' + $routeParams.repository + '/FinalChoice'; break;
 	  			}
 	  		}
 	  		
 	  	};
 	  	
    	this.build = function(pullRequests, analysisName)
    	{
    		var pullRequestIds = _.map(pullRequests, function(d) { return d.number; }).join();
    		var createAnalysis = jsRoutes.controllers.MergeAnalysisController.create(
    				$rootScope.owner, $rootScope.repository, analysisName, pullRequestIds);
    		    		
    		$http.get(createAnalysis.url).then(function(data) {
    			analysis = data.data;
    			var startAnalysis = jsRoutes.controllers.MergeAnalysisController.start(
        				$rootScope.owner, $rootScope.repository, analysis.id);
    			// TODO: update the screen if something goes wrong with starting the analysis
    			$http.get(startAnalysis.url);
    			
        		$window.location = '#/' + $rootScope.owner + '/' + $rootScope.repository + '/Status/' + analysis.id;    			
    		});
    	};
    	
    	// controller methods for filteringService so that we don't have to define them for each controller
    	// unfortunately common inheritance methods don't work since we use ngLoader
    	this.registerMethods = function($scope, FilteringService)
    	{    		
    		$scope.fileSizeHasBeenCalculated = false;
    		$scope.diffSizeHasBeenCalculated = false;
    		
    		$scope.Bases = [];
    		
    		$scope.idProperty =  {text: "Id", valueFunc: function(d){ return d.id; }};
    		$scope.titleProperty = {text: "Title", valueFunc: function(d) { return d.title; }};
    		$scope.ownerProperty = {text: "Owner", valueFunc: function(d) { return d.head.repositoryOwnerUsername; }};
    		$scope.labelProperty = {text: "Label", valueFunc: function(d) { return d.labels[0]; }};
    		$scope.branchProperty = {text: "Branch", valueFunc: function(d) { return d.base; }};
    		$scope.choiceProperty = {text: "Selected", valueFunc: function(d) { return d.isChosen; }};
        	$scope.PullRequestProperties = [$scope.idProperty,$scope.titleProperty, $scope.ownerProperty, $scope.labelProperty, $scope.choiceProperty, $scope.branchProperty];

        	var selectedProperty = {text: "On", valueFunc: function(d) { return d.isChosen == true; }};
        	$scope.FilterOptions = [
        	                        selectedProperty,
        	                        {text: "Off", valueFunc: function(d) { return d.isChosen == false; }},
        	                        {text: "Unknown", valueFunc: function(d) { return d.isChosen == null; }},
        	                        {text: "All", valueFunc: function(d) { return true; }}
        	];
        	
    		$scope.loadAvailablePullRequests = function(callbackFunctions) {    				
    			$scope.UpdateList(callbackFunctions);
    		};
    		
    		$scope.UpdateList = function(callbackFunctions)
    		{
    			FilteringService.getPullRequests().then(function(data) {
    				$scope.PullRequests = data;    				
    				
    				if($scope.sortBy != null)
        			{
        				$scope.PullRequests = _.sortBy($scope.PullRequests, function(d) { return $scope.sortBy.valueFunc(d); });
        			};
        			if($scope.filterBy != null)
        			{
    	    			$scope.PullRequests = _.filter($scope.PullRequests, function(d) { return $scope.filterBy.valueFunc(d); });
        			};
        			
    				_.forEach(callbackFunctions, function(d) { d(data); });
    			});
    		};
    		
    		$scope.goForward = function() {
    			FilteringService.goForward();
    		};
    		
    		$scope.goBack = function() {
    			$window.history.back();
    		};
    				
    		$scope.activatePullRequest = function(pullRequest) {
    			if(pullRequest.isChosen == true)
    			{
    				pullRequest.isChosen = null;
    				$scope.deleteBase(pullRequest);
    			}
    			else
    			{
    				pullRequest.isChosen = true;
    				$scope.Bases.push(pullRequest.number);
    	 			$scope.currentBase = pullRequest.base;
    			}
    		};
    		
    		$scope.deactivatePullRequest = function(pullRequest) {
    			if(pullRequest.isChosen == false)
				{
    				pullRequest.isChosen = null;
				}
    			else
    			{
    				pullRequest.isChosen = false;
    				$scope.deleteBase(pullRequest);
    			}
    		};

		$scope.SortPullRequests = function(property) {
			this.sortBy = property;
			$scope.PullRequests = _.sortBy($scope.PullRequests, function(d) 				{ return $scope.sortBy.valueFunc(d); });
		}
    		
    		$scope.deleteBase = function(pullRequest) {
				for(var i = $scope.Bases.length - 1; i >= 0; i--) {
	 				if($scope.Bases[i] == pullRequest.number) {
	 					$scope.Bases.splice(i, 1);
	 				}
	 			}
	 			if($scope.Bases.length == 0) {
	 				$scope.currentBase = "";
	 			}
    		};
    		
     		$scope.isEnabled = function(pullRequest) {
     			if(pullRequest.base == $scope.currentBase || $scope.currentBase == "" || $scope.currentBase == null) {
     				return true;
     			} else {
     				return false;
     			}
     		};
    		
    		$scope.getpullRequestUrl = function(pullRequest) {
    			//FilteringService.saveChosenPullRequests($scope.ChosenPullRequests);
    			//FilteringService.setCurrentFilteringStage("1");
    			return '#/' + $rootScope.owner + '/' + $rootScope.repository + '/PullRequests/' + pullRequest.number;
    		};
    	};
     });
});
