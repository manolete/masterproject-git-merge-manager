var $jq = jQuery.noConflict(); 

"use strict";
define(['app', 'underscore'], function(app){
    app.service('MergingService', function($http, $rootScope, UserService, $window) {
    	this.PullRequests = {};
    	this.ChosenPullRequests = {};
    	//this.Options = [];
    	//this.Results = {};
    	
    	this.initialPullRequestLoad = function() {
     		return PullRequests = UserService.getPullRequests().then(function(response) { return response; });
        };
    	
    	this.returnPullRequests = function() {
    		alert("PullRequests " + this.PullRequests.length);
    		return this.PullRequests;
    	};
    	
    	this.getChosenPullRequests = function() {
    		return this.ChosenPullRequests;
    	};
    	
		
		this.updatePullRequests = function(pullis) {
			this.PullRequests = pullis;
		};
		
		this.updateChosenPullRequests = function(pullis) {
			this.ChosenPullRequests = pullis;
		};
		
		this.buildParameters = function(chosenPullRequests, options) {
			var parameters = {};
			parameters['numberOfRequests'] = chosenPullRequests.length;
			parameters['owner'] = $rootScope.owner;
			parameters['repository'] = $rootScope.repository;
			for(var i = 0; i < chosenPullRequests.length; ++i) {
				var name = "PullRequest" + i;
				parameters[name] = chosenPullRequests[i].number;
			}
			for(var j = 0; j < options.length; ++j) {
				parameters[options[j].option] = options[j].value;
			}
			return parameters;
		};
		
		this.saveNewMergingPlan = function(chosenPullRequests, options, planName) {
			var parameters = this.buildParameters(chosenPullRequests, options);
			var url = jsRoutes.controllers.MergeAnalysisController.save($rootScope.owner, $rootScope.repository, planName).url;			
			$http.post(url, parameters).success(function(response) {
			  });
			
		};
		
    	this.getMergingPlan = function(id) {
    		var url = jsRoutes.controllers.MergeAnalysisController.analyses($rootScope.owner, $rootScope.repository, id).url;
    		MergingPlan = $http.get(url);
    		return MergingPlan;
    	};
    	
    	this.getResults = function(mergeAnalysisExecutionId) {
    		var url = jsRoutes.controllers.ReportController.getExampleReport().url;
    		return $http.get(url).then(function(data) { return data; }); 
    	};
    	
    	this.executeMerge = function(chosenPullRequests) {
    		var pullRequestIds = _.map(chosenPullRequests, function(d) { return d.pullRequest.number; }).join(',');
    		console.log(pullRequestIds);
    		var url = jsRoutes.controllers.GitHubController.merge($rootScope.owner, $rootScope.repository, pullRequestIds).url;
    		console.log(url);
    		response = $http.get(url).then(function(data) { return data; });
    		return response;
    	};
     });
});
