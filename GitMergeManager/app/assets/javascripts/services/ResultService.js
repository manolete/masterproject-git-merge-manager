var $jq = jQuery.noConflict(); 

"use strict";
define(['app'], function(app){
    app.service('ResultService', function($http, $rootScope, $window, $routeParams) {
    	
    	
    	this.getDetailsAnalysis = function(id) {
    		var url = jsRoutes.controllers.MergeAnalysisController.analysis($rootScope.owner, $rootScope.repository, id).url;
    		return $http.get(url).then(function(response) { return response.data });
    	}
    	
    	this.getPullRequestResults = function(id, exec, pullRequest) {
    		var url = jsRoutes.controllers.MergeAnalysisExecutionController.pullResult($rootScope.owner, $rootScope.repository, id, exec, pullRequest).url;
    		return $http.get(url).then(function(response) { return response.data });
    	}
    	
    	this.getBase = function(id, exec) {
    		var url = jsRoutes.controllers.MergeAnalysisExecutionController.baseResult($rootScope.owner, $rootScope.repository, id, exec).url;
    		return $http.get(url).then(function(response) { return response.data});
    	}
    	
    	this.getResults = function(id, exec) {
    		var url = jsRoutes.controllers.MergeAnalysisExecutionController.result($rootScope.owner, $rootScope.repository, id, exec).url;
    		return $http.get(url).then(function(response) { return response.data });
    	}
    	
    	this.rebuilt = function(analysisId) {
    		var createAnalysis = jsRoutes.controllers.MergeAnalysisController.start(
    				$rootScope.owner, $rootScope.repository, analysisId).url;
    		$http.get(createAnalysis);
       		$window.location = '#/' + $rootScope.owner + '/' + $rootScope.repository + '/Status/' + analysisId;   
    	}
     });
});
