var $jq = jQuery.noConflict(); 

"use strict";
define(['app', 'underscore'], function(app, _){
    app.service('SSEService', function($routeParams, $http, $rootScope) {
 
    	eventListeners = [];
    	sse = null;
    	
    	// watch for sse becoming set and then register event listeners
    	// this is because when controllers get called, the repository might not be set
    	$rootScope.$watch("repository",function(){
    		if($rootScope.repository != null && $rootScope.repository != '')
    		{
    		      var sseRoute = 
    		    	  jsRoutes.controllers.ServerSentEventsController.SSE($rootScope.owner, $rootScope.repository);
    		      sse = new EventSource(sseRoute.url, { withCredentials: true });
    			
    		      sse.onmessage = function(m)
    		      {
    		    	  console.log(m);
    		      };
    		      
    		      sse.onerror = function(e)
    		      {
    		    	  console.log(e);
    		      };
    		      
	    		_.forEach(eventListeners, function(e) {
	    			sse.addEventListener(e.type, e.handler, e.bool);
	    		});
    		}
    		else
			{
    			sse = null;
			}
    	});
    	
    	this.addEventListener = function(type, handler, bool)
    	{
    		newEventListener = {type: type, handler: handler, bool: bool};
    		eventListeners.push(newEventListener);
    		// if sse is already running we can add the event listener directly since watch is not triggered
    		if(sse != null)
			{
    			sse.addEventListener(newEventListener.type, 
    					newEventListener.handler, newEventListener.bool);
			}
    	};
    	
    	this.getBuildStatus = function()
    	{
    		var url = jsRoutes.controllers.MergeAnalysisController.status($rootScope.owner, $rootScope.repository, $routeParams.analysisId).url;
    		buildStatus = $http.get(url);
    		return buildStatus;
    	};
    });
});
