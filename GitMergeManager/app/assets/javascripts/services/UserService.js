var $jq = jQuery.noConflict(); 

"use strict";
define(['app'], function(app){
    app.service('UserService', function($http, $rootScope) {
    	    	
    	this.loadFavorites = function() {
    		var url = jsRoutes.controllers.UserController.favorites().url;
			return $http.get(url).success(function(response) { return 			response.data; });
    	};

    	this.updateSearchResults = function(SearchTerm) {
    	        var url = jsRoutes.controllers.SearchController.search(SearchTerm).url;
    			var SearchResults = $http.get(url).success(function(response) { return response.data;});
    			return SearchResults;
    	};

    	this.getTypeaheadSearchResults = function(SearchTerm) {
    		var url = jsRoutes.controllers.SearchController.search(SearchTerm).url;
            return $http.get(url, {
              }).then(function(results){
                repos = [];
                angular.forEach(results.data, function(item){
                	repos.push(item);
                });
                $rootScope.searching = false;
                return repos;
              });
    	};
    	
    	this.addFavorite = function(repoOwner, repoName) {
    		$http.get(jsRoutes.controllers.UserController.addFavorite(repoOwner, repoName).url);
    	};
    
    	this.getAnalyses = function() {
    		var url = jsRoutes.controllers.MergeAnalysisController.analyses($rootScope.owner, $rootScope.repository).url;
			return $http.get(url).success(function(response) { return response.data; });
    	};
    	
    	this.syncRepository = function() {
    		var url = jsRoutes.controllers.ReposController.update($rootScope.owner, $rootScope.repository).url;
			return $http.get(url).success(function(response) { return response.data; });
    	};
    	
    	this.deleteAnalysis = function(analysis)
    	{
    		var url = jsRoutes.controllers.MergeAnalysisController.deleteAnalysis($rootScope.owner, $rootScope.repository, analysis.id).url;
    		return $http.get(url).success(function(response) { return response.data; });
    	};
    	
    	this.getCode = function(serverUpdate, rawUrl) {
    		if(serverUpdate) {
    		    var url = jsRoutes.controllers.GitHubController.getFileContent(rawUrl).url;
    			var code = $http.get(url)
				.success(function(response) { return response.data; });
    		}
    		return code;
    	};
    	
    	this.deleteFavoriteFromList = function(repository) {
    		$http.get(jsRoutes.controllers.UserController.deleteFavorite(repository.owner, repository.name).url);
    	};
    	
    	this.deleteFavorite = function() {
    		$http.get(jsRoutes.controllers.UserController.deleteFavorite($rootScope.owner, $rootScope.repository).url);
    	};
    	
    	this.getReadme = function() {
    		var url = jsRoutes.controllers.GitHubController.getReadme($rootScope.owner, $rootScope.repository).url;
    		return $http.get(url).then(function(response) { 
    			return response.data; });	
    	};
    	    	
    	this.checkMavenProject = function() {
    		var url = jsRoutes.controllers.GitHubController.isMavenProject($rootScope.owner, $rootScope.repository).url;
    		return $http.get(url).then(function(response) {
    			var flag = false;
    			for(var i = 0; i < response.data.length; ++i) {
    				if(response.data[i].name == "pom.xml") {
    					flag = true;
    				}
    			}
    			return flag;
    		});
    	};
     });
});
