var $jq = jQuery.noConflict();

"use strict";
define(['app', 'd3', 'underscore'], function(app, d3, _){
	app.register.directive('d3Bars', function() {
      return {
        restrict: 'EA',
        scope: {
          data: "=",
          label: "@",
          onClick: "&",
          getSize: "&",
          filter: "=",
          open: "&"
        },
        link: function(scope, iElement, iAttrs) {
          
        	var svg = d3.select(iElement[0])
              .append("svg")
              .attr("width", "100%");
            
          // on window resize, re-render d3 canvas
          window.onresize = function() {
            return scope.$apply();
          };
          
          scope.$watch(function(){
              return angular.element(window)[0].innerWidth;
            }, function(){
              return scope.render(scope.data);
            }
          );

          // watch for data changes and re-render
          scope.$watch('data', function(newVals, oldVals) {
            return scope.render(newVals);
          }, true);

          // define render function
          scope.render = function(data){
        	
        	if(data == null)
    		{
        		return;
    		}
        	
            // remove all previous items before render
            svg.selectAll("*").remove();

            // setup variables
            var width, height, max;
            width = d3.select(iElement[0])[0][0].offsetWidth - 20 - 60;
              // 20 is for margins and can be changed
            height = data.length * 35 + 30;
              // 35 = 30(bar height) + 5(margin between bars)
            
            var getSize = function(pullRequest, filter)
            {
            	if(filter.name == 'Files' && pullRequest.changed_files != null)
				{
					return pullRequest.changed_files.length;
				}
	    		else if(pullRequest.changed_lines != null)
				{
					return pullRequest.changed_lines.length;
				}
            	return 0.1;
            };
            
            max = Math.max.apply(null, _.map(data, function(pullRequest){ return getSize(pullRequest, scope.filter); }));
            // set the height based on the calculations above
            svg.attr('height', height);
            
            //create the rectangles for the bar chart
            svg.selectAll("rect")
              .data(data)
              .enter()
                .append("rect")
                .on("click", function(d, i){ return scope.open({item: d});})
                .on("mouseover", function() { d3.select(this).style("fill", "#FFA500"); })
                .on("mouseout", function() { d3.select(this).style("fill", "#0000FF"); })
		.attr("style", "cursor: pointer;")
                .attr("height", 30) // height of each bar
                .attr("width", 0) // initial width of 0 for transition
                .attr("x", 10) // half of the 20 side margin specified above
                .attr("fill", "#0000FF")
                .attr("transform", "translate(50," + 30 +")")
                .attr("y", function(d, i){
                  return i * 35;
                }) // height + margin between bars
                .transition()
                  .duration(400) // time of duration
                  .attr("width", function(d){
                    return Math.abs(getSize(d, scope.filter)/(max/width));
                  }); // width based on scale
            
	    svg.selectAll("text")
              .data(data)
              .enter()
                .append("text")
                .on("click", function(d, i){ return scope.onClick({item: d});})
		.on("mouseover", function() { d3.select(this).style("fill", "#0000FF"); })
                .on("mouseout", function() { d3.select(this).style("fill", "#000"); })
		.attr("style", "cursor: pointer;")
                .attr("fill", "#000")
                .attr("y", function(d, i){return i * 35 + 22;})
                .attr("transform", "translate(0," + 30 +")")
                .attr("x", 15)
                .text(function(d){ return d.number;})
		.append("title")
   		.text(function(d) { return d.title; });
               
            	var axisScale = d3.scale.linear()
                        .domain([0,max])
                        .range([0,width]);

				var xAxis = d3.svg.axis()
						.orient("top")
                  		.scale(axisScale);

				var xAxisGroup = svg.append("g")
					.attr("class", "axis")
					.attr("x", 10)
					.attr("transform", "translate(60," + 30 +")")
					.call(xAxis);
          	};
          }
      };
    });
});
