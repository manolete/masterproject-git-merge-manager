var $jq = jQuery.noConflict();

"use strict";
define(['app', 'd3', 'underscore'], function(app, d3, _){
	app.register.directive('d3Heatmap', function() {
      return {
        restrict: 'EA',
        scope: {
          data: "=",
          label: "@",
          onYClick: "&onYClick",
          onClick: "&onClick",
          filter: "=filter"
        },
        link: function(scope, iElement, iAttrs) {
          
        	var svg = d3.select(iElement[0])
              .append("svg")
              .attr("width", "100%");
              
          // on window resize, re-render d3 canvas
          window.onresize = function() {
            return scope.$apply();
          };
          
          scope.$watch(function(){
              return angular.element(window)[0].innerWidth;
            }, function(){
              return scope.render(scope.data);
            }
          );

          // watch for data changes and re-render
          scope.$watch('data', function(newVals, oldVals) {
            return scope.render(newVals);
          }, true);

          // define render function
          scope.render = function(data){
        	  
        	if(data == null)
      		{
          		return;
      		}
        	  
        	  svg.selectAll("*").remove();
        	  
        	    var schakels = new Array();

        	    _.forEach(data, function(pullRequest) { // row
        	    	
        	        var schakel_id = pullRequest.number;
        	        schakels.push(schakel_id);
        	    
        	    });

        	    var width, height;
                w = d3.select(iElement[0])[0][0].offsetWidth - 20;
                  // 20 is for margins and can be changed
                h = schakels.length * 15 + 30;
                  // 35 = 30(bar height) + 5(margin between bars)
                
                var padding = 40;

        	    var xscale = d3.scale.ordinal()
        	                         .domain(schakels)
        	                         .rangeBands([0, w]);
        	    
        	    var xAxis = d3.svg.axis()
        	                .scale(xscale)
        	                .orient("top");

        	   var yscale = d3.scale.ordinal()
        	                         .domain(schakels)
        	                         .rangeBands([0, h]);

        	    var yAxis = d3.svg.axis()
        	                      .scale(yscale)
        	                      .orient('left');

        	    var zscale = d3.scale.linear()
        	                         .domain([0,1])
        	                         .range(['white', 'red']);

        	      var cellw = w/schakels.length;
        	      var cellh = h/schakels.length;

                  if(data != null)
                  {
        	      var row = svg.selectAll('.row')
        	                   .data(data)
        	                   .enter()
        	                   .append('svg:g')
        	                   .attr('class', 'row');

        	      var col = row.selectAll('.cell')
        	                   .data(function(d) { return scope.filter.name == "Files" ? d.file_conflicts : d.diff_conflicts; })
        	                   .enter()
        	                   .append('rect')
        	                   .on("click", function(d){console.log(d); return scope.onClick({item: d});})
        	                   .attr('class', 'cell')
        	                   .attr('x', function(d, i){
        	                      return xscale(d.x);
        	                    })
        	                   .attr('y', function(d, i){
        	                     return yscale(d.y);
        	                    })
        	                   .attr("transform", "translate(" + padding + ","+ padding +")")
        	                   .attr('width', cellw)
				   .attr("style", "cursor: pointer;")
        	                   .attr('height', cellh)
        	                   .attr('conflict',function(d) { return d.conflict;})
        	                   .attr('x_issue',function(d) { return d.x;})
        	                   .attr('y_issue',function(d) { return d.y;})
        	                   .attr('fill', function(d){
        	                    	return (d.x == d.y) ? "#000000" : zscale(d.conflict);
        	                   });
        	        
        	      	svg.append("g")
        	          .attr("id", "xaxis")
        	          .attr("class", "axis")
        	          .attr("transform", "translate(" + padding + "," + padding + ")")
        	          .call(xAxis);

        	        svg.append("g")
        	            .attr("id", "yaxis")
        	            .attr("class", "axis")
        	            .attr("transform", "translate(" + padding + "," + padding + ")")
        	            .call(yAxis);
        	        
        	        svg.select('#yaxis')
        	        .selectAll('.tick')
        	        .on('click', function(d){ return scope.onYClick({item: d});})
			.on("mouseover", function() { d3.select(this).style("fill", "#0000FF"); })
                	.on("mouseout", function() { d3.select(this).style("fill", "#000"); })
			.attr("style", "cursor: pointer;")

			svg.select('#xaxis')
        	        .selectAll('.tick')
        	        .on('click', function(d){ return scope.onYClick({item: d});})
			.on("mouseover", function() { d3.select(this).style("fill", "#0000FF"); })
                	.on("mouseout", function() { d3.select(this).style("fill", "#000"); })
			.attr("style", "cursor: pointer;")
                  };
          };
        }
      };
    });
});
