define(['angularAMD', 
        'app', 
        // load controllers which are present always
        'controllers/NavbarCtrl', 
        'controllers/AlertCtrl',
        'services/UserService',
        'services/MergingService',
        'services/FilteringService',
        'services/ResultService'
        ], function (angularAMD, app) {
    'use strict';
     angularAMD.bootstrap(app);
});
