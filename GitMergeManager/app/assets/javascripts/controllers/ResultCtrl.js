var $jq = jQuery.noConflict();

"use strict";
define(['app',  'underscore', 'ngload!services/MergingService'], function(app){
    app.register.controller('ResultCtrl', ['$scope', 'MergingService', 'ResultService', 'FilteringService'
                                        , '$routeParams', '$modal', '$rootScope', '$window',
                                        function ($scope, MergingService, ResultService, FilteringService, $routeParams, $modal, $rootScope, $window) {
    	
    	$scope.AnalyzedPullRequests = [];
    	$scope.PullRequests = [];
    	$scope.DetailsAnalysis = [];
    	$scope.currentExecution;
    	$scope.exec = [];
    	$scope.Executions = [];
    	$scope.Base = [];
		$scope.AllActivated = false;
		$scope.Conflicts = [];
		$scope.loaded = false;
		$scope.noConflicts = true;
    	
		$scope.MergeablePullRequests = [];
		$scope.ChosenPullRequests = [];
		$scope.PullRequestBoxes = [];
		
		$scope.init = function() {
			ResultService.getDetailsAnalysis($routeParams.analysisId).then(function(response) {
				$scope.DetailsAnalysis = response;
				for(var i = 0; i < response.mergeAnalysisExecutions.length; ++i) {
					$scope.Executions.push({"number": i + 1, "id": response.mergeAnalysisExecutions[i] });
				}
				$scope.currentExecution = $scope.Executions[$scope.Executions.length-1].id;
				$scope.exec = $scope.Executions[$scope.Executions.length-1];
				
				$scope.loadInformation(response);
			});
		};
		
		$scope.loadInformation = function(response) {
			ResultService.getBase($routeParams.analysisId, $scope.currentExecution).then(function(base) {
				$scope.Base = base;
			});
			
			ResultService.getResults($routeParams.analysisId, $scope.currentExecution).then(function(response) {
				for(var i = 0; i < response.pullRequestBuildResults.length; ++i) {
					var pulli = response.pullRequestBuildResults[i];
					$scope.AnalyzedPullRequests.push(pulli);
					if($scope.isMergeable(pulli)) {
						$scope.MergeablePullRequests.push(pulli);
						$scope.PullRequestBoxes.push({
							id: pulli.pullRequest.number,
							value: false
						});
					} else {
						$scope.PullRequestBoxes.push({
							id: pulli.pullRequest.number,
							value: false
						});
					}
				}
				
				for(var j = 0; j < response.pairwiseBuildResults.length; ++j ) {
					if(response.pairwiseBuildResults[j].conflict) {
						var temp = response.pairwiseBuildResults[j]
						$scope.Conflicts.push({
							id: temp.a.number,
							conflict: temp.b.number
						});
						$scope.Conflicts.push({
							id: temp.b.number,
							conflict: temp.a.number
						});
					}
				}
				$scope.loaded = true;
				if($scope.Conflicts.length > 0) {
					$scope.noConflicts = false;
				}
			});
			
		};
		
		$scope.changeExecution = function() {
			ResultService.getDetailsAnalysis($routeParams.analysisId).then(function(response) {
		    	$scope.AnalyzedPullRequests = [];
		    	$scope.Base = [];
				$scope.AllActivated = false;
		    	
				$scope.MergeablePullRequests = [];
				$scope.ChosenPullRequests = [];
				$scope.PullRequestBoxes = [];
				$scope.AnalyzedPullRequests = [];
				$scope.currentExecution = $scope.exec.id;
				$scope.loadInformation(response);
				
			});
		}
		
		$scope.openCheckstyleDetails = function(number) {
	       	 modalInstance = $modal.open({
	    		 templateUrl: 'assets/partials/resultModals/checkstyleDetails.html',
	    		 controller: ModalInstanceCtrl,
	    		 windowClass: 'testDetails-modal',
	    		 resolve: {
         		        results: function() {
         		          return $scope.AnalyzedPullRequests;
         		        },
         		        id: function() {
         		        	return number;
         		        },
         		        base: function() {
         		        	return $scope.Base;
         		        }
	    		 }
	       	 });
		};
		
		$scope.openPmdDetails = function(number) {
	       	 modalInstance = $modal.open({
	    		 templateUrl: 'assets/partials/resultModals/pmdDetails.html',
	    		 controller: ModalInstanceCtrl,
	    		 windowClass: 'testDetails-modal',
	    		 resolve: {
        		        results: function() {
        		          return $scope.AnalyzedPullRequests;
        		        },
        		        id: function() {
        		        	return number;
        		        },
        		        base: function() {
        		        	return $scope.Base;
        		        }
	    		 }
	       	 });
		};
		
		$scope.openTestDetails = function(number) {
	       	 modalInstance = $modal.open({
	    		 templateUrl: 'assets/partials/resultModals/testDetails.html',
	    		 controller: ModalInstanceCtrl,
	    		 windowClass: 'testDetails-modal',
	    		 resolve: {
        		        results: function() {
        		          return $scope.AnalyzedPullRequests;
        		        },
        		        id: function() {
        		        	return number;
        		        },
        		        base: function() {
        		        	return $scope.Base;
        		        }
	    		 }
	       	 });
		};
		
		$scope.show = function(number) {
			if(number != "0") {
				return true;
			} else {
				return false;
			}
		};
		
		$scope.anyChanges = function(result, id) {
			if(id == "1") {
				if(result.delta.numbers.newSuccessfulTests != "0" || 
						result.delta.numbers.newFailingTests != "0" ||	
						result.delta.numbers.addedSuccessfulTests != "0" ||
						result.delta.numbers.addedFailingTests != "0" ||
						result.delta.numbers.removedSuccessfulTests != "0" ||
						result.delta.numbers.removedFailingTests != "0") {
					return false;
				}
					return true;
			} else if (id == "2") {
				if(result.delta.numbers.removedPmdViolations != "0" ||
						result.delta.numbers.addedPmdViolations != "0") {
					return false;
				} else {
					return true;
				}
			}  else if (id == "3") {
				if(result.delta.numbers.removedCheckstyleViolations != "0" ||
						result.delta.numbers.addedCheckstyleViolations != "0") {
					return false;
				} else {
					return true;
				}
			}
		};
		
		
		$scope.isMergeable = function(element) {
			var flag = true;
			if(element.mergeable && element.compileable) {
				flag = true;
			} else {
				flag = false;
			}
			for(var i = 0; i < $scope.Conflicts.length; ++i) {
				if($scope.Conflicts[i].id == element.pullRequest.number) {
					for(var j = 0; j < $scope.PullRequestBoxes.length; ++j) {
						if($scope.PullRequestBoxes[j].id == $scope.Conflicts[i].conflict && $scope.PullRequestBoxes[j].value == "true") {
							flag  = false;
						}
					}
				}
			}
			return flag;
		};
		
		$scope.activateAll = function() {
				$scope.ChosenPullRequests = [];
				if(!$scope.AllActivated) {
					for(var i = 0; i < $scope.MergeablePullRequests.length; ++i ) {
						var element = document.getElementById($scope.MergeablePullRequests[i].pullRequest.number);
						element.checked = true;
						$scope.setPullRequestBoxesValue($scope.MergeablePullRequests[i].pullRequest.number, "true");
					}
					$scope.AllActivated = !$scope.AllActivated;
				} else {
					for(var i = 0; i < $scope.MergeablePullRequests.length; ++i ) {
						var element = document.getElementById($scope.MergeablePullRequests[i].pullRequest.number);
						element.checked = false;
						$scope.setPullRequestBoxesValue($scope.MergeablePullRequests[i].pullRequest.number, "false");
					}
					$scope.AllActivated = !$scope.AllActivated;
				}
		};
		
		$scope.setPullRequestBoxesValue = function(id, value) {
			for(var i = 0; i < $scope.PullRequestBoxes.length; ++i) {
				if($scope.PullRequestBoxes[i].id == id) {
					$scope.PullRequestBoxes[i].value = value;
				}
			}
		};
		
		
		$scope.executeMerging = function() {
			$scope.ChosenPullRequests = [];
			for(var i = 0; i < $scope.PullRequestBoxes.length; ++i) {
				if($scope.PullRequestBoxes[i].value == "true") {
					var element = _.find($scope.AnalyzedPullRequests, function(element) {
						return element.pullRequest.number == $scope.PullRequestBoxes[i].id; });
					$scope.ChosenPullRequests.push(element);
				}
			}
	
			if($scope.ChosenPullRequests.length < 1) {
				alert("No Pull Requests chosen.");
			} else {
				MergingService.executeMerge($scope.ChosenPullRequests)
				.then(function(d) {
					$window.location = "#/" + $rootScope.owner + "/" + $rootScope.repository;
				});
			}
		};
		
		$scope.checkPullRequest = function(id) {
			for(var j = 0; j < $scope.PullRequestBoxes.length; ++j) {
				if($scope.PullRequestBoxes[j].id == id) {
					if($scope.PullRequestBoxes[j].value == "true") {
						$scope.PullRequestBoxes[j].value = "false";
					} else {
						$scope.PullRequestBoxes[j].value = "true"; 
					}
				}
			}
		};
		
		$scope.getMergeConflictsLength = function(mergeConflicts) {
			return true;
			if(mergeConflicts.length > 0) {
				return false;
			} else {
				return true;
			}
		};
		
		$scope.buildAgain = function() {
			ResultService.rebuilt($scope.DetailsAnalysis.id);
		};
		
		$scope.existConflictingPairs = function(id) {
			var flag = false;
			for(var i = 0; i < $scope.Conflicts.length; ++i) {
				if($scope.Conflicts[i].id == id) {
					flag = true;
				} 
			}
			return flag;
		};
		
		$scope.giveConflicts = function(id) {
			var conflictString = "";
			for(var i = 0; i < $scope.Conflicts.length; ++i) {
				if($scope.Conflicts[i].id == id) {
					conflictString += $scope.Conflicts[i].conflict + ", ";
				};
			}
			conflictString = conflictString.substr(0, conflictString.length - 2);
			return conflictString;
		}
    }]);
    
    var ModalInstanceCtrl = function ($scope, $modalInstance, results, id, base) {
    	$scope.Id = id;
	    $scope.Results = results;
	    $scope.PullRequest;
	    $scope.Base = base;
	    $scope.ToggleBoolean = [];
	    $scope.sort = "class";
	    
	    $scope.init = function() {
	    	for(var i = 0; i < $scope.Results.length; ++i) {
	    		if($scope.Results[i].pullRequest.number == $scope.Id) {
	    			$scope.PullRequest = $scope.Results[i];
	    		}
	    	}
	    	for(var i = 1; i < 8; ++i) {
	    		$scope.ToggleBoolean[i] = false;
	    	}
	    };
	    
	    $scope.changeAcc = function(id) {
	    	$scope.ToggleBoolean[id] = !$scope.ToggleBoolean[id];
	    };
	    
	    $scope.getToggleBoolean = function(id) {
	    	return $scope.ToggleBoolean[id];
	    };
	    
	    $scope.hasElements = function(array) {
	    	if(array != null && array.length > 0) {
	    		return true;
	    	} else {
	    		return false;
	    	}
	    };
	    
	    $scope.getClassName = function(path) {
	    	var position = path.lastIndexOf("/");
	    	return path.substr(position+1, path.length);
	    };
	    
	    $scope.sortBy = function(value) {
	    	alert(value);
	    	$scope.sort = value;
	    };
	    
	    $scope.close = function() {
	    	$modalInstance.close();
	    };

    };
});