var $jq = jQuery.noConflict();

"use strict";
define(['app', 'underscore', 'ngload!services/UserService', 'ngload!services/SSEService'], function(app, _){
    app.register.controller('StatusCtrl', ['$scope', 'UserService', 'SSEService', '$window', function ($scope, UserService, sseService, $window) {
    	    	
    	$scope.tasks = [];
    	
    	$scope.handleEvent = function(event)
    	{
    		console.log(event);
    		if(event.type  + "" == 'startTask')
			{
    			var task = {name: event.msg, isFinished: false, subTasks: [], hasRunningSubTasks: false };
    			$scope.tasks.push(task);
			}
			else if(event.type  + ""  == 'endTask')
			{
    			var task = _.findWhere($scope.tasks, { isFinished: false});
			if(task != null) {    			
				task.isFinished = true;
	    			console.log(task.isFinished);
			}	
			}
			
			var task = _.findWhere($scope.tasks, { isFinished: false});
			if(task != null)
			{
    			if(event.type + ""  == 'startSubTask')
	    		{
	    			var subTask = {name: event.eventInfo, progress: 0, isFinished: false, totalItems: event.msg }
	    			task.hasRunningSubTasks = true;
	    			task.subTasks.push(subTask);
	    		}
	    		else if(event.type + ""  == 'statusUpdate')
	    		{
	    			var task = _.findWhere($scope.tasks, { isFinished: false});
	    			var subTask = _.findWhere(task.subTasks, { name: event.eventInfo });
	    			subTask.progress = event.msg;
	    		}
	    		else if(event.type + ""  == 'endSubTask')
				{
	    			var task = _.findWhere($scope.tasks, { isFinished: false});
	    			var subTask = _.findWhere(task.subTasks, { name: event.eventInfo });
	    			task.hasRunningSubTasks = false;
	    			subTask.progress = event.msg;
				}
			}
    	};
    	
    	$scope.handleEventWithApply = function(d)
    	{
    		var event;
    		event = $jq.parseJSON(d.data);
    		
    		$scope.$apply(function(){
    			$scope.handleEvent(event);
    		});
    	};

	$scope.goToExecution = function(event) {
		analysis = $jq.parseJSON(event.data);
		$window.location = "#/" + analysis.owner + "/" + analysis.repository + "/Results/" + parseInt(analysis.msg);
	}
    	
    	$scope.init = function() {
    		sseService.getBuildStatus().then(function(data) {
    			_.forEach(data.data, function(d) { $scope.handleEvent(d); });
            	
		sseService.addEventListener('buildEvent', $scope.handleEventWithApply, false);
			});

		sseService.addEventListener('analysisFinished', function(d) { 				$scope.goToExecution(d); }, false);			
    	};
     }]);
});
