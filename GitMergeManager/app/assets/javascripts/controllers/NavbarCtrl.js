

define(['app'], function(app){
	'use strict';
    var $jq = jQuery.noConflict();
    app.controller('NavbarCtrl', ['$scope', '$rootScope', 'UserService', 'FilteringService', 'SSEService', '$window', '$http', function ($scope, $rootScope, UserService, FilteringService, sseService, $window, $http) {
        
    	$scope.notifications = [];
    	$scope.searching = false;

    	$scope.setTab = function(tab) {
    		$rootScope.currentTab = tab;
    	};
    	
    	$scope.setRepositoryTab = function() {
    		$rootScope.currentTab = 'Repository';
    		UserService.loadFavorites().then(function(repositories) {
           		$scope.Projects = repositories.data;
           	});
    	};

    	$scope.isCurrentTab = function(tab) {
    		if($rootScope.currentTab == tab) {
    			return true;
    		} else {
    			return false;
    		}
    	};
    	
        $scope.searchRepository = function() {
    		UserService.updateSearchResults($scope.SearchTerm).then(function(data) {
    			$scope.SearchResults = data;
    			$scope.searchActivated = true;
    		});
    	};
        
        $scope.formatLabel = function(model) {console.log(model);};
        
        $scope.addFavorite = function(item) {
        	UserService.addFavorite(item.owner, item.name);
        };      
        
	  // Typeahead autocomplete
	  $scope.getRepos = function(val) {
		  $scope.searching = true;
		  var repos = UserService.getTypeaheadSearchResults(val);
		  return repos;
	  };
          
      $scope.onSelect = function ($item, $model, $label) {
    	  $scope.addFavorite($item);
    	  $scope.searching = false;
    	  $window.location.href = '#/' + $item.owner + '/' + $item.name + '/Repository';
      };
      
      $scope.addNotification = function(event) {
          $scope.$apply(function(){
       	   		$scope.notifications.push($jq.parseJSON(event.data));
          });
       };
       
       $scope.updateAnalysis = function() {
    	   return UserService.getAnalyses().then(function(response) {
    		   $scope.analyses = response.data;
    	   });
       };
       
       // necessary since the route is updated after controller initialisation
       $scope.$watch(function() {
    	   return $rootScope.repository;
    	 }, function() {
    		 if($rootScope.repository != null && $rootScope.owner != null)
    			 {
    			 	$scope.updateAnalysis(); 
    			 };
    	 }, true);
       
       $scope.init = function() {
			$rootScope.currentTab = "Home";
		   					
	       	sseService.addEventListener('notification', $scope.addNotification, false);
	       	
	       	sseService.addEventListener('analysisUpdate', $scope.updateAnalysis, false);
	       	
       };  
       
       $scope.choseProject = function(project) {
   		$rootScope.repository = project.name;
   		$rootScope.owner = project.owner;
   		FilteringService.deleteLoadedData();
   		$window.location = '#/' + $rootScope.owner + '/' + $rootScope.repository + '/Repository';
   	};
      
     }]);
});
