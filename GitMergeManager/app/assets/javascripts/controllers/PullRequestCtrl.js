var $jq = jQuery.noConflict();

"use strict";
define(['app', 'underscore','ngload!services/UserService', 'ngload!services/FilteringService'], function(app){
    app.register.controller( 'PullRequestCtrl', ['$scope', 'UserService', 'FilteringService', '$routeParams', '$location', '$anchorScroll', '$modal', '$window', '$rootScope',
                                                 function ($scope, UserService, FilteringService, $routeParams, $location, $anchorScroll, $modal, $window, $rootScope) {
    	$scope.Diffs = [];
    	$scope.Changes = [];
    	$scope.PullRequests = [];
    	$scope.PullRequestNumber = $routeParams.pullRequestNumber;
    	$scope.currentPullRequest;
	$scope.CodeTab = false;
	$scope.DiffsTab = false;
	$scope.Information = false;
	$scope.IsLoading = false;
    	
    	$scope.initializeCtrl = function() {
		$scope.IsLoading = true;
    		$scope.loadPullRequest();
    		$scope.getPullRequest();
		$scope.switchTab($location.search()['tab']);
    	}
    	
    	$scope.getPullRequest = function() {
			FilteringService.getPullRequests().then(function(data) {
				$scope.PullRequests = data;
				for(var i = 0; i < $scope.PullRequests.length; ++i) {
					if($scope.PullRequests[i].number == $routeParams.pullRequestNumber) {
						$scope.currentPullRequest = $scope.PullRequests[i];
					}
				}
			});
			
    	};

	$scope.switchTab = function(tab) {
		$location.search('tab', tab)
		if(tab == 'Information') { $scope.Information = true; }
		if(tab == 'Code') { $scope.CodeTab = true; }
		if(tab == 'Diffs') { $scope.DiffsTab = true; }
	};
			
    	$scope.getDiff = function(filename) {
    		FilteringService.getDiffs($routeParams.pullRequestNumber, filename).then(function(data) {
    			$scope.Diffs[filename] =  _.sortBy(data.data, function(obj) {return obj.id});
			$anchorScroll();
    		})
    	};
    	
    	$scope.getChangedLines = function() {
    		FilteringService.getChangedLines($routeParams.pullRequestNumber).then(function(data) {
    			$scope.ChangedLines = data.data;
    		});
    	};
    	
    	$scope.loadPullRequest = function() {
    		FilteringService.getChangedFiles(true, $routeParams.pullRequestNumber).then(function(data) {
                $scope.ChangedFiles = data.data;
		$scope.IsLoading = false;
           });
        };
    
         $scope.getSpecificChangedFiles = function(currentRequest) {
        	 return $scope.ChangedFiles[currentRequest];
         };
         
         $scope.anchorScroll = function(id) {
        	 $location.hash(id);
        	 $anchorScroll();
         };     
         
         $scope.goToFilteringList = function() {
        	 FilteringService.goToFilteringStage();        	 
         };
                  
         $scope.openCode = function(rawUrl) {
        	 UserService.getCode(true, rawUrl).then(function(data) {
            	 modalInstance = $modal.open({
            		 templateUrl: 'assets/partials/codeTemplate.html',
            		 controller: ModalInstanceCtrl,
            		 resolve: {
            		        data: function () {
            		          return data;
            		        }
            		 }
            	 });
        	 });
         };
    }]);
    
    var ModalInstanceCtrl = function ($scope, $modalInstance, data) {
    	    $scope.Code = data.data;
    };
});
