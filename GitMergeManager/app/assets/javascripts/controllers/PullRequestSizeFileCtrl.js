var $jq = jQuery.noConflict();

"use strict";
define(['app', 'underscore', 'directives/d3Bars', 'ngload!services/FilteringService'], function(app, _){
    app.register.controller('PullRequestSizeFileCtrl', ['$scope', 'FilteringService', '$http', '$window', '$rootScope', '$modal', 
                                                        function ($scope, FilteringService, $http, $window, $rootScope, $modal) {
    	        
        $scope.filters = [{name: 'Files'},{name: 'Diff Lines'}];
        $scope.filter = $scope.filters[0];
        
    	$scope.stacked = [
    	                  {value: 25, type:'', name:'Preselection'},
    	                  {value: 25, type:'warning', name:'Size'}
    	                  ];
        
        $scope.changeFilter = function() {
	    	FilteringService.registerMethods($scope, FilteringService);
        	$scope.filterBy = $scope.FilterOptions[0];
        	$scope.sortBy = $scope.PullRequestProperties[0];
        	FilteringService.setCurrentFilteringStage(2);
        	$scope.loadAvailablePullRequests([$scope.init_files, $scope.init_diffs]);
        };
        
        $scope.init_files = function(pullRequests) {
        	$scope.fileSizeHasBeenCalculated = false;
	        	_.forEach(pullRequests, function(pullRequest, index){
	        		FilteringService.getChangedFiles(true, pullRequest.number).then(function(data) {
	        			pullRequest.changed_files = data.data;
	        			if(index == pullRequests.length - 1)
        				{
	        	        	$scope.fileSizeHasBeenCalculated = true;
        				}
	               });
	        });
        };
        
    	$scope.init_diffs = function(pullRequests) {
        	$scope.diffSizeHasBeenCalculated = false;
	        	_.forEach(pullRequests, function(pullRequest, index){
	        		FilteringService.getChangedLines(pullRequest.number).then(function(data) {
	        			pullRequest.changed_lines = data.data;
	        			if(index == pullRequests.length - 1)
        				{
	        	        	$scope.diffSizeHasBeenCalculated = true;
        				}
	               });
	        	});
        };
        
        $scope.d3OnClick = function(item){
        	console.log(item);
        	FilteringService.setCurrentFilteringStage("2");
        	$window.location = '#/' + $rootScope.owner + '/' + $rootScope.repository + '/PullRequests/' + item.number;
        };
        
        $scope.open = function (item) {
        	console.log(item);
            var modalInstance = $modal.open({
              templateUrl: 'assets/partials/visualizationModal.html',
              controller: ModalInstanceCtrl,
              resolve: {
                item: function () {
                  return item;
                },
                conflict: function () {
                	return { files: _.map(item.changed_files, function(d) { return d.filename; }) };
                },
                FilteringService: function() {
                	return FilteringService;
                }
              }
            });

            modalInstance.result.then(function (selectedItem) {
              $scope.selected = selectedItem;
            }, function () {
              console.log('Modal dismissed at: ' + new Date());
            });
          };        
      }]);
    
    var ModalInstanceCtrl = function ($scope, $modalInstance, item, conflict, FilteringService) {

    	  $scope.item = item;
    	  $scope.conflict = conflict;
    	  
	      FilteringService.registerMethods($scope, FilteringService);

    	  $scope.cancel = function () {
    	    $modalInstance.dismiss('cancel');
    	  };
    	};
});
