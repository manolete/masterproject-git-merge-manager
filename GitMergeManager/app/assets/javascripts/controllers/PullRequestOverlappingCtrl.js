var $jq = jQuery.noConflict();

"use strict";
define(['app', 'underscore', 'directives/d3Heatmap', 'ngload!services/FilteringService'], function(app, _){
    app.register.controller( 'PullRequestOverlappingCtrl', ['$scope', '$rootScope', 'FilteringService', '$http', '$window', '$modal', '$q',
                                                            function ($scope, $rootScope, FilteringService, $http, $window, $modal, $q) {
    	    	
        $scope.filters = [{name: 'Files'},{name: 'Diff'}];
        $scope.filter = $scope.filters[0];
        
    	$scope.stacked = [
    	                  {value: 25, type:'', name:'Preselection'},
    	                  {value: 25, type:'warning', name:'Size'},
    	                  {value: 25, type:'danger', name:'Conflicts'}
    	                  ];
        
        $scope.init = function() {
        	FilteringService.registerMethods($scope, FilteringService);
        	FilteringService.setCurrentFilteringStage(3);
        	$scope.filterBy = $scope.FilterOptions[0];
        	$scope.sortBy = $scope.PullRequestProperties[0];
        	$scope.loadAvailablePullRequests([$scope.init_files, $scope.init_diffs]);
        };
        
        $scope.init_files = function(pullRequests) {
	        	_.forEach(pullRequests, function(pullRequest_x, x){	  
	        		pullRequest_x.file_conflicts = [];
	        		_.forEach(pullRequests, function(pullRequest_y, y){
	            		
				$q.all([
	            		FilteringService.getChangedFiles(true, pullRequest_x.number),
				FilteringService.getChangedFiles(true, pullRequest_y.number)])
				.then(function(data) {
	            		changedFiles_x = _.map(data[0].data, function(val){ return val.filename; }); 					changedFiles_y = _.map(data[1].data, function(val){ return val.filename; });
	            				                			        			
                    		var intersection = _.intersection(changedFiles_x, changedFiles_y);
                    		pullRequest_x.file_conflicts.push({x: pullRequest_x.number, y: pullRequest_y.number, 
                    			conflict: intersection.length / changedFiles_y.length, files: intersection });
	                    		
	                    		if(x == pullRequests.length - 1)
	            				{
	    	        	        		$scope.fileConflictsHaveBeenCalculated = true;
	            				}
	                		});
	            		});
	            	});
	          };
        
        
        $scope.init_diffs = function(pullRequests) {
        	$scope.diffConflictsHaveBeenCalculated = false;
	        	_.forEach(pullRequests, function(pullRequest_x, x){
	        		pullRequest_x.diff_conflicts = [];
	        		_.forEach(pullRequests, function(pullRequest_y, y){
	            		
				$q.all([
	            		FilteringService.getChangedLines(pullRequest_x.number),
				FilteringService.getChangedLines(pullRequest_y.number)])
				.then(function(data) {
	            			
				changedLines_x = _.map(data[0].data, function(val) { return val.file + " " + val.line; });
                		changedLines_x = _.uniq(changedLines_x);
	            			
	                	changedLines_y = _.map(data[1].data, function(val){ return val.file + " " + val.line; });
	                	changedLines_y = _.uniq(changedLines_y);
	                			
                    		var intersection = _.intersection(changedLines_x, changedLines_y);
                    		
                    		var conflicts = {x: pullRequest_x.number, y: pullRequest_y.number, 
	                    			conflict: intersection.length != 0, files: intersection };
                    		
                    		pullRequest_x.diff_conflicts.push(conflicts);
	                    		
                    		if(x == pullRequests.length - 1) {
        	        		$scope.diffConflictsHaveBeenCalculated = true;
    				};
		           });
	        	});
	        });
        };
        
        $scope.goToPullRequest = function(item)
        {
        	FilteringService.setCurrentFilteringStage("3");
        	$window.location = '#/' + $rootScope.owner + '/' + $rootScope.repository + '/PullRequests/' + item;
        };
        
        $scope.open = function (item) {
      	  	
        	pullRequest = _.find($scope.PullRequests, function(d) { return d.number == item.y; });
            console.log(pullRequest);
      	  	var modalInstance = $modal.open({
              templateUrl: 'assets/partials/visualizationModal.html',
              controller: ModalInstanceCtrl,
              resolve: {
                item: function () {
                  return pullRequest;
                },
                conflict: function () {
                	if($scope.filter == $scope.filters[0]) {
                	return item;
                	}
                	else {
                		var groups = _.groupBy(item.files, function(d) { return d.split(' ')[0]; });
                		var lines = _.map(groups, function(group) {
                			var first = _.first(group).split(' ');
                			var last = _.last(group).split(' ');
                			return first[0] + " (line " + first[1] + " - " + last[1] + ")";
                		});
                		return { x: item.x, y: item.y, files: lines };
                	}
                },
                FilteringService: function() {
                	return FilteringService;
                }
              }
            });

            modalInstance.result.then(function (selectedItem) {
            }, function () {
              console.log('Modal dismissed at: ' + new Date());
            });
          };        
    }]);
    
    var ModalInstanceCtrl = function ($scope, $modalInstance, item, conflict, FilteringService) {

	  FilteringService.registerMethods($scope, FilteringService);

  	  $scope.item = item; 
  	  $scope.conflict = conflict;
  	  console.log(conflict);
	  
  	  $scope.cancel = function () {
  	    $modalInstance.dismiss('cancel');
  	  };
  	};
});
