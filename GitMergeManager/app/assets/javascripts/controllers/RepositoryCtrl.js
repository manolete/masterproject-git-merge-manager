var $jq = jQuery.noConflict();

"use strict";
define(['app', 'ngload!services/UserService', 'ngload!services/FilteringService'], function(app){
    app.register.controller( 'RepositoryCtrl', ['$scope', '$rootScope', '$routeParams', 'UserService', 'FilteringService', '$window', function ($scope, $rootScope, $routeParams, UserService, FilteringService, $window) {
    	
    	$scope.PullRequests = [];
    	$scope.Favorites = [];
    	$scope.readme = [];
    	$scope.Html;
    	$scope.content;
    	$scope.active = false;
    	$scope.Style;
    	$scope.Info;
    	$scope.loaded = false;
    	
    	$scope.initRepository = function() {
    		FilteringService.updateRepository();
    		$scope.getAllFavorites();
    		UserService.getReadme().then(function(data) {
    			$scope.readme = data;
    			$scope.content = atob(data.content);
    		});
    		$scope.checkMavenProject();
    	};
    	
    	$scope.isFavorite = function() {
			for(var i = 0; i < $scope.Favorites.length; ++i) {
    			if(($scope.Favorites[i].owner == $rootScope.owner) && ($scope.Favorites[i].name == $rootScope.repository)) {
    				$scope.active = true;
    			}
    		}
    	};
    	
    	$scope.pushStar = function() {
    		if(!$scope.active) {
    			$scope.addFavorite();
    			$scope.active = !$scope.active;
    			$scope.setFavoriteStyle();
    		} else {
    			$scope.deleteFavorite();
    			$scope.active = !$scope.active;
    			$scope.setFavoriteStyle();
    		}
    	};
    	
    	$scope.addFavorite = function()
    	{
    		UserService.addFavorite($routeParams.owner,
    				$routeParams.repository);
    	};
    	
    	$scope.deleteFavorite = function() {
    		UserService.deleteFavorite();
    	};
    	
    	$scope.syncRepository = function() {
    		$scope.loaded = false;
    		UserService.syncRepository().then(function(data) {
    			$scope.loaded = true;
    		});
    	};
    	
    	$scope.deleteFavoriteFromList = function(repository) {
    		UserService.deleteFavoriteFromList(repository);
    		for(var i = 0; i < $scope.Favorites.length; ++i) {
    			if($scope.Favorites[i].name == repository.name) {
    				$scope.Favorites.splice(i, 1);
    			}
    		}
    	};
    	
    	$scope.getAllFavorites = function() {
    		UserService.loadFavorites().then(function(data) {
    			$scope.Favorites = data.data;
    			$scope.isFavorite();
    			$scope.setFavoriteStyle();
    		});
    	};
    	
    	$scope.choseProject = function(name, owner) {
    		$rootScope.repository = name;
    		$rootScope.owner = owner;
    		$window.location = '#/' + $rootScope.owner + '/' + $rootScope.repository + '/Repository';
    	};
    	
    	$scope.startAnalysis = function() {
    		$rootScope.currentTab = "MergeManager";
    		$window.location = '#/' + $rootScope.owner + '/' + $rootScope.repository + '/PullRequests';
    	};
    	
    	$scope.setFavoriteStyle = function() {
    		if($scope.active) {
    			$scope.Style = {"color" : "#E6E600" };
    			$scope.Info = "Remove from Favorites";
    		} else {
    			$scope.Style =  {};
    			$scope.Info = "Add to Favorites";
    		}
    	};
    	
    	$scope.checkMavenProject = function() {
    		UserService.checkMavenProject().then(function(answer) {
    			$scope.mavenProject = answer;
    			$scope.loaded = true;
    		});
    	};
    	
     }]);
});