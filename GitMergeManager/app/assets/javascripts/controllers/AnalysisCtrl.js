var $jq = jQuery.noConflict();

"use strict";
define(['app', 'ngload!services/UserService', 'ngload!services/FilteringService'], function(app){
    app.register.controller( 'AnalysisCtrl', ['$scope', '$rootScope', '$routeParams', 'UserService', function ($scope, $rootScope, $routeParams, UserService) {
    	
    	$scope.analyses = [];
    	
    	$scope.init = function() 
    	{
    		UserService.getAnalyses().then(function(data) { $scope.analyses = data.data; });
    	};
    	
    	$scope.deleteAnalysis = function(analysis)
    	{
    		UserService.deleteAnalysis(analysis);
    	};
    	
     }]);
});