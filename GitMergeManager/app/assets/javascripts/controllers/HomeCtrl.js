var $jq = jQuery.noConflict();

"use strict";
define(['app', 'ngload!services/UserService'], function(app){
    app.register.controller('HomeCtrl', ['$scope', '$window', function ($scope, $window) {
    		$scope.showTextTrackWarning = $window.TextTrack == null;
     }]);
});
