var $jq = jQuery.noConflict();

"use strict";
define(['app', 'services/SSEService'], function(app){
    app.controller('AlertCtrl', ['$scope', '$rootScope', 'SSEService', function ($scope, $rootScope, sseService) {   	
    	$scope.alerts = [];
    	
        $scope.addAlert = function(event) {
           $scope.$apply(function(){
        	   $scope.alerts.push($jq.parseJSON(event.data));
           });
        };

        $scope.closeAlert = function(index) {
          $scope.alerts.splice(index, 1);
        };
        
        $scope.init = function()
        {
        	sseService.addEventListener('alert', $scope.addAlert, false);
        };
     }]);
});
