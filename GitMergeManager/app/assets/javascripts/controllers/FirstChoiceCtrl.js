var $jq = jQuery.noConflict();

"use strict";
define(['app', 'underscore', 'ngload!services/FilteringService'], function(app){
	
    app.register.controller('FirstChoiceCtrl', ['$scope', 'FilteringService'
                                        , '$routeParams', '$modal', '$rootScope', '$window',
                                        function ($scope, FilteringService, $routeParams, $modal, $rootScope, $window) {
    	
    	$scope.stacked = [{value: 25, type:'', name:'Preselection'}];
    	
    	$scope.currentBase = "";
    	$scope.Labels = [];
    	$scope.currentLabel = "All Labels";
    	
		$scope.init = function() {
	    	FilteringService.registerMethods($scope, FilteringService);
			FilteringService.setCurrentFilteringStage(1);
        		$scope.filterBy = $scope.FilterOptions[3];
        		$scope.sortBy = $scope.PullRequestProperties[0];
			$scope.loadAvailablePullRequests();
		};	
 		
 		$scope.getStyle = function(pullRequest) {
 			if($scope.isEnabled(pullRequest)) {
 				return {};
 			} else {
 				return {"opacity" : "0.5" };
 			}
 		};
 		
		$scope.isShown = function(pullRequest) {
			var flag = false;
			var labels = pullRequest.labels;
			if(labels.length == 0 && $scope.currentLabel == "All Labels") {
				flag = true;
			}
			for(var i = 0; i < labels.length; ++i) {
				if(labels[i].name == $scope.currentLabel || $scope.currentLabel == "All Labels") {
					flag = true;
				}
			}
			return flag;
		};
		
		$scope.initLabels = function() {
			var tempList = [];
			if($scope.Labels.length == 0) {
				for(var i = 0; i < $scope.PullRequests.length; ++i) {
					for(var j = 0; j < $scope.PullRequests[i].labels.length; ++j) {
						tempList.push($scope.PullRequests[i].labels[j]);
					}
				}
			}
			$scope.removeDuplicates(tempList);
		};
					
		$scope.setLabel = function(label) {
			if(label == 0) {
				$scope.currentLabel = "All Labels";
			} else {
				$scope.currentLabel = label;
			}
		};
		
		$scope.removeDuplicates = function(tempList) {
			for(var i = 0; i < tempList.length; ++i) {
				var flag = true;
				for(var j = 0; j < $scope.Labels.length; ++j) {
					if(tempList[i].name == $scope.Labels[j].name) {
						flag = false;
					}
				}
				if(flag) {
					$scope.Labels.push(tempList[i]);
				}
			}
		};
		
		$scope.labelBackgroundColor = function(label) {
			var color = "#" + label.color
			return {"background-color" : color };
		};
    }]);
});
