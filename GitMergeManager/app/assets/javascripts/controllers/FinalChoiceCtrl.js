var $jq = jQuery.noConflict();

"use strict";
define(['app',  'underscore', 'ngload!services/FilteringService'], function(app){
    app.register.controller('FinalChoiceCtrl', ['$scope', 'FilteringService'
                                        , '$routeParams', '$modal', '$rootScope', '$window',
                                        function ($scope, FilteringService, $routeParams, $modal, $rootScope, $window) {
    	
    	$scope.stacked = [
    	                  {value: 25, type:'', name:'Preselection'},
    	                  {value: 25, type:'warning', name:'Size'},
    	                  {value: 25, type:'danger', name:'Conflicts'},
    	                  {value: 25, type:'success', name:'Build'}
    	                  ];
		
		$scope.initializeFilter = function() {
	    	FilteringService.registerMethods($scope, FilteringService);
			$scope.filterBy = $scope.FilterOptions[0];
			$scope.sortBy = $scope.PullRequestProperties[0];
			FilteringService.setCurrentFilteringStage(4);
			$scope.loadAvailablePullRequests();
		};
	
		var today = new Date();
		$scope.analysisName = 'Analysis ' + today.getDate() + '.' +  today.getMonth() + '.' + today.getFullYear();
		
		$scope.build = function() {
			FilteringService.build($scope.PullRequests, $scope.analysisName);
		};
    }]); 
});
