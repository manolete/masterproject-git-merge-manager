package services.analysis;

import services.maven.MavenVerifyResult;
import model.core.PullRequest;
import model.core.results.ExecutionResult;

/**
 * Provides methods to store results of the execution into an {@link ExecutionResult}.
 */
public interface ExecutionResultService {

  /**
   * Stores the content of a {@link MavenVerifyResult} for the base branch into a given {@link ExecutionResult}.
   * 
   * @param executionResult
   *          the {@link ExecutionResult} that should store the results
   * @param mavenVerifyResult
   *          the {@link MavenVerifyResult}
   */
  public void setBaseBranchResult(ExecutionResult executionResult, MavenVerifyResult mavenVerifyResult);

  /**
   * Stores the content of a {@link MavenVerifyResult} of a {@link PullRequest} into a given {@link ExecutionResult}.
   * 
   * @param executionResult
   *          the {@link ExecutionResult} that should store the results
   * @param pullRequest
   *          the {@link PullRequest} for which the {@link MavenVerifyResult} has been computed
   * @param mergeable
   *          boolean that indicates whether the {@link PullRequest} could be successfully merged
   * @param mavenVerifyResult
   *          the {@link MavenVerifyResult}
   */
  public void addPullRequestResult(ExecutionResult executionResult, PullRequest pullRequest, boolean mergeable, MavenVerifyResult mavenVerifyResult);

}
