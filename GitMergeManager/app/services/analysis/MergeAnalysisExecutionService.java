package services.analysis;

import infrastructure.async.interruptible.Interruptible;
import infrastructure.async.interruptible.InterruptibleResult;
import model.core.MergeAnalysisExecution;

public interface MergeAnalysisExecutionService extends Interruptible<MergeAnalysisExecution, Void> {

  /**
   * Executes a {@link MergeAnalysisExecution}
   * 
   * @param mergeAnalysisExecution
   *          {@link MergeAnalysisExecution}
   */
  @Override
  public InterruptibleResult<Void> execute(final MergeAnalysisExecution mergeAnalysisExecution);

}
