package services.analysis;

import model.core.MergeAnalysis;
import model.core.MergeAnalysisExecution;
import model.core.Repository;
import model.core.User;

public interface MergeAnalysisService {

  /**
   * Creates a {@link MergeAnalysis}.
   * 
   * @param repository
   *          {@link Repository}
   * @param user
   *          {@link User}
   * @param name
   *          name of the {@link MergeAnalysis}
   * @param pullRequestIds
   *          id's of the pull requests to be added
   * @return created {@link MergeAnalysis}
   */
  public MergeAnalysis createMergeAnalysis(final Repository repository, final User user, final String name, final Iterable<String> pullRequestIds);

  /**
   * Starts the execution of a {@link MergeAnalysis}.
   * 
   * @param mergeAnalysis
   *          {@link MergeAnalysis}
   * @return true if the {@link MergeAnalysis} has been successfully started, false otherwise (i.e. another {@link MergeAnalysisExecution} is already
   *         running for the given {@link MergeAnalysis})
   */
  public boolean start(final MergeAnalysis mergeAnalysis);

  /**
   * Stops a running {@link MergeAnalysisExecution}.
   * 
   * @param mergeAnalysis
   *          {@link MergeAnalysis} for which the running {@link MergeAnalysisExecution} shall be stopped
   * @return true if the {@link MergeAnalysisExecution} has been stopped, false otherwise
   */
  public boolean stop(final MergeAnalysis mergeAnalysis);

}
