package services.analysis.tasks;

import infrastructure.async.interruptible.InterruptibleResult;
import infrastructure.async.tasks.AbstractBaseAsyncTask;
import infrastructure.async.tasks.AsyncResult;
import infrastructure.async.tasks.AsyncTaskRegistry;
import model.core.MergeAnalysisExecution;

import org.joda.time.DateTime;

import services.analysis.MergeAnalysisExecutionService;

public final class MergeAnalyseExecutionTask extends AbstractBaseAsyncTask<MergeAnalysisExecution, MergeAnalysisExecution> {

  private final AsyncTaskRegistry taskRegistry;
  private final MergeAnalysisExecutionService mergeAnalysisExecutionService;

  public MergeAnalyseExecutionTask(final MergeAnalysisExecutionService mergeAnalysisExecutionService, final AsyncTaskRegistry taskRegistry) {
    this.mergeAnalysisExecutionService = mergeAnalysisExecutionService;
    this.taskRegistry = taskRegistry;
  }

  @Override
  protected AsyncResult<MergeAnalysisExecution> doWork(final MergeAnalysisExecution mergeAnalysisExecution) {
    final InterruptibleResult<Void> result = mergeAnalysisExecutionService.execute(mergeAnalysisExecution);
    if (result.interrupted()) {
      return cancel(mergeAnalysisExecution);
    }
    return finish(mergeAnalysisExecution);
  }

  @Override
  protected void onSuccess(final MergeAnalysisExecution mergeAnalysisExecution) {
    mergeAnalysisExecution.setFinished();
    setEndDateTime(mergeAnalysisExecution);
    taskRegistry.remove(mergeAnalysisExecution.getId().toString());
  }

  @Override
  protected void onCancelled(final MergeAnalysisExecution mergeAnalysisExecution) {
    setEndDateTime(mergeAnalysisExecution);
  }

  private void setEndDateTime(final MergeAnalysisExecution mergeAnalysisExecution) {
    mergeAnalysisExecution.setEndDateTime(DateTime.now());
    mergeAnalysisExecution.save();
  }

}
