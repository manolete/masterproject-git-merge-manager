package services.analysis;

import infrastructure.async.interruptible.TaskInterruptionObserver;
import infrastructure.async.tasks.AsyncTask;
import infrastructure.async.tasks.AsyncTaskRegistry;
import model.core.MergeAnalysis;
import model.core.MergeAnalysisExecution;
import model.core.PullRequest;
import model.core.Repository;
import model.core.User;

import org.joda.time.DateTime;

import services.analysis.tasks.MergeAnalyseExecutionTask;
import services.events.Event.EventType;
import services.events.EventService;

import com.google.inject.Inject;

public class MergeAnalysisServiceImpl implements MergeAnalysisService {

  @Inject
  private MergeAnalysisExecutionService mergeAnalysisExecutionService;

  @Inject
  private AsyncTaskRegistry taskRegistry;

  @Inject
  private EventService eventService;

  @Override
  public MergeAnalysis createMergeAnalysis(final Repository repository, final User user, final String name, final Iterable<String> pullRequestIds) {
    final MergeAnalysis mergeAnalysis = repository.addMergeAnalysis(name, user);
    for (final String pullRequestId : pullRequestIds) {
      final PullRequest pullRequest = repository.getPullRequest(pullRequestId);
      if (pullRequest != null) {
        mergeAnalysis.addPullRequest(pullRequest);
      }
    }
    mergeAnalysis.save();

    eventService.sendEvent(mergeAnalysis, "Added merge analysis: " + mergeAnalysis.getName(), "success", EventType.notification, "success");

    return mergeAnalysis;
  }

  @Override
  public boolean start(final MergeAnalysis mergeAnalysis) {
    final MergeAnalysisExecution mergeAnalysisExecution = mergeAnalysis.start(DateTime.now());
    if (mergeAnalysisExecution != null && taskRegistry.get(mergeAnalysis.getId().toString()) == null) {
      startAsyncExecution(mergeAnalysisExecution);
      return true;
    }
    return false;
  }

  private void startAsyncExecution(final MergeAnalysisExecution mergeAnalysisExecution) {
    final MergeAnalyseExecutionTask task = new MergeAnalyseExecutionTask(mergeAnalysisExecutionService, taskRegistry);
    mergeAnalysisExecutionService.setInterruptionObserver(new TaskInterruptionObserver(task));
    taskRegistry.register(mergeAnalysisExecution.getMergeAnalysis().getId().toString(), task);
    task.start(mergeAnalysisExecution);
  }

  @Override
  public boolean stop(final MergeAnalysis mergeAnalysis) {
    if (mergeAnalysis != null) {
      final AsyncTask<?, ?> task = taskRegistry.remove(mergeAnalysis.getId().toString());
      if (task != null) {
        task.sendCancelRequest();
        return true;
      }
    }
    return false;
  }
}
