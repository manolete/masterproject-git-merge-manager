package services.analysis;

import global.ConfigLoader;
import infrastructure.async.interruptible.AbstractInterruptible;
import infrastructure.async.interruptible.InterruptibleResult;
import infrastructure.maven.MavenCommandExecutorImpl;

import java.io.File;
import java.util.List;

import model.core.MergeAnalysis;
import model.core.MergeAnalysisExecution;
import model.core.PullRequest;
import model.core.Repository;
import model.core.results.ExecutionResult;
import model.factories.ExecutionResultFactory;
import play.Logger;
import services.events.Event.EventType;
import services.events.EventService;
import services.git.GitService;
import services.maven.MavenCompilationResult;
import services.maven.MavenProgressMonitorImpl;
import services.maven.MavenService;
import services.maven.MavenServiceFactory;
import services.maven.MavenVerifyResult;
import util.StringUtil;

import com.google.inject.Inject;

public class MergeAnalysisExecutionServiceImpl extends AbstractInterruptible<MergeAnalysisExecution, Void> implements MergeAnalysisExecutionService {

  @Inject
  private GitService gitService;

  @Inject
  private ConfigLoader conf;

  @Inject
  private MavenServiceFactory mavenServiceFactory;

  @Inject
  private ExecutionResultService executionResultService;

  @Inject
  private EventService eventService;

  private MavenService mavenService;

  @Override
  public InterruptibleResult<Void> execute(final MergeAnalysisExecution mergeAnalysisExecution) {
    // PREPARE //
    final ExecutionResult executionResult = ExecutionResultFactory.INSTANCE.createExecutionResult();
    mergeAnalysisExecution.setResult(executionResult);
    final MergeAnalysis mergeAnalysis = mergeAnalysisExecution.getMergeAnalysis();
    final String message = "Started execution for analysis: " + mergeAnalysis.getName();
    Logger.info(message);
    eventService.sendEvent(mergeAnalysisExecution, message, "success", EventType.notification, "success");

    // CLEANUP DIRECTORY
    gitService.cleanupRepositoryFolder(mergeAnalysis);

    // BASE BRANCH //
    sendStartBuildEvent(mergeAnalysis, "Start cloning base branch");
    final boolean cloneSuccess = cloneBaseBranch(mergeAnalysis);
    sendEndBuildEvent(mergeAnalysis, "Finished cloning base branch");
    logResult("Cloning", cloneSuccess);

    if (shouldInterrupt()) {
      return interrupt();
    }

    if (cloneSuccess) {
      sendStartBuildEvent(mergeAnalysis, "Start building base branch");
      final MavenVerifyResult mavenVerifyResult = verify(mergeAnalysis);
      executionResultService.setBaseBranchResult(executionResult, mavenVerifyResult);
      sendEndBuildEvent(mergeAnalysis, "Finished building base branch");
      logResult("Compilation", mavenVerifyResult.isCompilationOk());
    }
    else {
      Logger.error("Could not clone base branch of '" + mergeAnalysis.getRepository().getQualifiedName() + "'");
    }

    if (shouldInterrupt()) {
      return interrupt();
    }

    // PULL REQUESTS //
    for (final PullRequest pullRequest : mergeAnalysis.getPullRequests()) {
      sendStartBuildEvent(mergeAnalysis, "Start merging pull request " + pullRequest.getNumber());
      final boolean mergeable = mergeSinglePullRequest(mergeAnalysis, pullRequest);
      sendEndBuildEvent(mergeAnalysis, "Finished merging pull request " + pullRequest.getNumber());
      logResult("Merging", mergeable);

      if (shouldInterrupt()) {
        return interrupt();
      }
      MavenVerifyResult mavenVerifyResult = null;
      if (mergeable) {
        sendStartBuildEvent(mergeAnalysis, "Start building pull request " + pullRequest.getNumber());
        mavenVerifyResult = verify(mergeAnalysis);
        sendEndBuildEvent(mergeAnalysis, "Finished building pull request " + pullRequest.getNumber());
        logResult("Compilation", mavenVerifyResult.isCompilationOk());
      }
      else {
        Logger.info("Could not merge pull request '" + pullRequest.getNumber() + "' of '" + mergeAnalysis.getRepository().getQualifiedName() + "'");
      }
      executionResultService.addPullRequestResult(executionResult, pullRequest, mergeable, mavenVerifyResult);
    }
    executionResult.refresh();

    // MERGE PAIRWISE //
    final List<PullRequest> compileablePullRequests = executionResult.getCompileablePullRequests();
    for (int i = 0; i < compileablePullRequests.size(); i++) {
      final PullRequest a = compileablePullRequests.get(i);
      for (int j = i + 1; j < compileablePullRequests.size(); j++) {
        final PullRequest b = compileablePullRequests.get(j);
        sendStartBuildEvent(mergeAnalysis, "Start merging pair <" + a.getNumber() + ", " + b.getNumber() + ">");
        final boolean mergeable = mergePairwise(mergeAnalysis, a, b);
        sendEndBuildEvent(mergeAnalysis, "Finished merging pair <" + a.getNumber() + ", " + b.getNumber() + ">");
        logResult("Merging", mergeable);

        if (shouldInterrupt()) {
          return interrupt();
        }

        boolean compileable = false;
        if (mergeable) {
          sendStartBuildEvent(mergeAnalysis, "Start compiling pair <" + a.getNumber() + ", " + b.getNumber() + ">");
          final MavenCompilationResult mavenCompilationResult = compile(mergeAnalysis);
          sendEndBuildEvent(mergeAnalysis, "Finished compiling pair <" + a.getNumber() + ", " + b.getNumber() + ">");
          compileable = mavenCompilationResult.isCompilationOk();
        }
        logResult("Compilation", compileable);
        executionResult.addPairwiseResult(a, b, mergeable, compileable);

        if (shouldInterrupt()) {
          return interrupt();
        }
      }

      eventService.sendAnalysisFinishedEvent(mergeAnalysis);
    }

    // FINISH //
    Logger.info("Successfully finished analysis");
    return InterruptibleResult.finish(null);
  }

  private boolean cloneBaseBranch(final MergeAnalysis mergeAnalysis) {
    return gitService.cloneBaseRepository(mergeAnalysis);
  }

  private boolean mergeSinglePullRequest(final MergeAnalysis mergeAnalysis, final PullRequest pullRequest) {
    gitService.resetWorkingFolder(mergeAnalysis);
    return gitService.mergePullRequest(mergeAnalysis, pullRequest);
  }

  private boolean mergePairwise(final MergeAnalysis mergeAnalysis, final PullRequest a, final PullRequest b) {
    gitService.resetWorkingFolder(mergeAnalysis);
    final boolean mergeable = gitService.mergePullRequest(mergeAnalysis, a);
    return mergeable && gitService.mergePullRequest(mergeAnalysis, b);
  }

  private MavenVerifyResult verify(final MergeAnalysis mergeAnalysis) {
    final File projectRootPath = gitService.getWorkingDirectory(mergeAnalysis);
    final Repository repository = mergeAnalysis.getRepository();
    return mavenService().executeMavenVerify(projectRootPath, MavenProgressMonitorImpl.create(eventService, repository.getOwner().getUsername(), repository.getName()));
  }

  private MavenCompilationResult compile(final MergeAnalysis mergeAnalysis) {
    final File projectRootPath = gitService.getWorkingDirectory(mergeAnalysis);
    final Repository repository = mergeAnalysis.getRepository();
    return mavenService().executeMavenCompilation(projectRootPath, MavenProgressMonitorImpl.create(eventService, repository.getOwner().getUsername(), repository.getName()));
  }

  public MavenService mavenService() {
    if (mavenService == null) {
      mavenService = mavenServiceFactory.create(MavenCommandExecutorImpl.create(conf.string("maven.home")));
    }
    return mavenService;
  }

  private void sendStartBuildEvent(final MergeAnalysis mergeAnalysis, final String message) {
    sendBuildEvent(mergeAnalysis, message, "startTask");
  }

  private void sendEndBuildEvent(final MergeAnalysis mergeAnalysis, final String message) {
    sendBuildEvent(mergeAnalysis, message, "endTask");
  }

  private void sendBuildEvent(final MergeAnalysis mergeAnalysis, final String message, final String type) {
    Logger.info(message);
    eventService.sendEvent(mergeAnalysis, message, "", EventType.buildEvent, type);
  }

  private void logResult(final String message, final boolean result) {
    final String resultText = result ? "success" : "failure";
    if (result) {
      Logger.info(message + ": " + resultText);
    }
    else {
      Logger.warn(message + ": " + resultText);
    }
    Logger.info(StringUtil.EMPTY);
  }

  private InterruptibleResult<Void> interrupt() {
    Logger.warn("ANALYSIS INTERRUPTED");
    return InterruptibleResult.interrupt(null);
  }
}
