package services.analysis;

import model.core.results.CheckstyleBuildResultItem;
import model.core.results.PmdBuildResultItem;
import model.core.results.TestBuildResultItem;
import services.maven.plugins.results.CheckstyleViolation;
import services.maven.plugins.results.PmdViolation;
import services.maven.plugins.results.TestCase;

/**
 * Implementation of {@link ExecutionResultTranslator}.
 */
public class ExecutionResultTranslatorImpl implements ExecutionResultTranslator {

  /**
   * {@inheritDoc}
   */
  @Override
  public TestBuildResultItem create(final TestCase testCase) {
    final TestBuildResultItem.Failure failure = testCase.isSuccessful() ? null : new TestBuildResultItem.Failure(testCase.getFailure().getType(), testCase.getFailure().getMessage());
    return new TestBuildResultItem(testCase.getClassName(), testCase.getMethodName(), testCase.getTime(), failure);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public CheckstyleBuildResultItem create(final CheckstyleViolation checkstyleViolation) {
    return new CheckstyleBuildResultItem(checkstyleViolation.getFilename(), checkstyleViolation.getLine(), checkstyleViolation.getColumn(), checkstyleViolation.getSeverity().toString(),
        checkstyleViolation.getMessage(), checkstyleViolation.getSource());
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public PmdBuildResultItem create(final PmdViolation pmdViolation) {
    return new PmdBuildResultItem(pmdViolation.getFile(), pmdViolation.getBeginLine(), pmdViolation.getEndLine(), pmdViolation.getBeginColumn(), pmdViolation.getEndColumn(), pmdViolation.getRule(),
        pmdViolation.getRuleSet(), pmdViolation.getPackageName(), pmdViolation.getClassName(), pmdViolation.getMethodName(), pmdViolation.getExternalInfoUrl(), pmdViolation.getPriority(),
        pmdViolation.getContent());
  }

}
