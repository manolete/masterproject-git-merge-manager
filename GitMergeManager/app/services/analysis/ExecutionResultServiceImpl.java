package services.analysis;

import infrastructure.maven.MavenPluginType;
import model.core.PullRequest;
import model.core.results.AbstractBuildResult;
import model.core.results.BaseBranchBuildResult;
import model.core.results.ExecutionResult;
import model.core.results.PullRequestBuildResult;
import model.factories.ExecutionResultFactory;
import services.maven.MavenVerifyResult;
import services.maven.plugins.results.CheckstyleViolation;
import services.maven.plugins.results.MavenCheckstyleReport;
import services.maven.plugins.results.MavenPmdReport;
import services.maven.plugins.results.MavenTestingReport;
import services.maven.plugins.results.PmdViolation;
import services.maven.plugins.results.TestCase;

import com.google.inject.Inject;

/**
 * Implementation of {@link ExecutionResultService}.
 */
public class ExecutionResultServiceImpl implements ExecutionResultService {

  @Inject
  private ExecutionResultTranslator mavenServiceResultTranslator;

  /**
   * {@inheritDoc}
   */
  @Override
  public void setBaseBranchResult(final ExecutionResult executionResult, final MavenVerifyResult mavenVerifyResult) {
    final BaseBranchBuildResult baseBranchBuildResult = ExecutionResultFactory.INSTANCE.createBaseBranchBuildResult(mavenVerifyResult.isCompilationOk());
    baseBranchBuildResult.save();
    addResultItems(baseBranchBuildResult, mavenVerifyResult);
    executionResult.setBaseBranchResult(baseBranchBuildResult);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void addPullRequestResult(final ExecutionResult executionResult, final PullRequest pullRequest, final boolean mergeable, final MavenVerifyResult mavenVerifyResult) {
    final boolean compileable = mavenVerifyResult != null ? mavenVerifyResult.isCompilationOk() : false;
    final PullRequestBuildResult pullRequestBuildResult = ExecutionResultFactory.INSTANCE.createPullRequestBuildResult(pullRequest, mergeable, compileable);
    pullRequestBuildResult.save();
    addResultItems(pullRequestBuildResult, mavenVerifyResult);
    executionResult.addPullRequestResult(pullRequestBuildResult);
  }

  private void addResultItems(final AbstractBuildResult buildResult, final MavenVerifyResult mavenVerifyResult) {
    if (buildResult != null && mavenVerifyResult != null) {
      addTestCases(buildResult, mavenVerifyResult);
      addCheckstyleViolations(buildResult, mavenVerifyResult);
      addPmdViolations(buildResult, mavenVerifyResult);
    }
  }

  private void addTestCases(final AbstractBuildResult buildResult, final MavenVerifyResult mavenVerifyResult) {
    final MavenTestingReport mavenTestingReport = mavenVerifyResult.getTestingReport();
    if (mavenTestingReport != null) {
      for (final TestCase testCase : mavenTestingReport.getItems()) {
        buildResult.addTestCase(mavenServiceResultTranslator.create(testCase));
      }
    }
  }

  private void addCheckstyleViolations(final AbstractBuildResult buildResult, final MavenVerifyResult mavenVerifyResult) {
    final MavenCheckstyleReport mavenCheckstyleReport = (MavenCheckstyleReport) mavenVerifyResult.getVerifyPluginReport(MavenPluginType.CHECKSTYLE);
    if (mavenCheckstyleReport != null) {
      for (final CheckstyleViolation checkstyleViolation : mavenCheckstyleReport.getItems()) {
        buildResult.addCheckstyleViolation(mavenServiceResultTranslator.create(checkstyleViolation));
      }
    }
  }

  private void addPmdViolations(final AbstractBuildResult buildResult, final MavenVerifyResult mavenVerifyResult) {
    final MavenPmdReport mavenPmdReport = (MavenPmdReport) mavenVerifyResult.getVerifyPluginReport(MavenPluginType.PMD);
    if (mavenPmdReport != null) {
      for (final PmdViolation pmdViolation : mavenPmdReport.getItems()) {
        buildResult.addPmdViolation(mavenServiceResultTranslator.create(pmdViolation));
      }
    }
  }
}
