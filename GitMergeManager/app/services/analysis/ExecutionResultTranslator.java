package services.analysis;

import model.core.results.AbstractBuildResultItem;
import model.core.results.CheckstyleBuildResultItem;
import model.core.results.PmdBuildResultItem;
import model.core.results.TestBuildResultItem;
import services.maven.plugins.results.CheckstyleViolation;
import services.maven.plugins.results.MavenPluginReportItem;
import services.maven.plugins.results.PmdViolation;
import services.maven.plugins.results.TestCase;

/**
 * Provides methods to translate {@link MavenPluginReportItem}'s into {@link AbstractBuildResultItem}'s.
 */
public interface ExecutionResultTranslator {

  /**
   * Creates a {@link TestBuildResultItem} out of a {@link TestCase}.
   * 
   * @param testCase
   *          {@link TestCase}
   * @return the created {@link TestBuildResultItem}
   */
  public TestBuildResultItem create(TestCase testCase);

  /**
   * Creates a {@link CheckstyleBuildResultItem} out of a {@link CheckstyleViolation}.
   * 
   * @param checkstyleViolation
   *          {@link CheckstyleViolation}
   * @return the created {@link CheckstyleBuildResultItem}
   */
  public CheckstyleBuildResultItem create(CheckstyleViolation checkstyleViolation);

  /**
   * Creates a {@link PmdBuildResultItem} out of a {@link PmdViolation}.
   * 
   * @param pmdViolation
   *          {@link PmdViolation}
   * @return the created {@link PmdBuildResultItem}
   */
  public PmdBuildResultItem create(PmdViolation pmdViolation);

}
