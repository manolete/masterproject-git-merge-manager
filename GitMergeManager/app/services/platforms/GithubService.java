package services.platforms;

import java.util.List;

import model.dto.BranchDto;
import model.dto.PullRequestDto;
import model.dto.RepositoryDto;
import model.dto.UserDto;

/**
 * Provides DTO's of data fetched from Github.
 */
public interface GithubService {

  /**
   * Fetch data about the currently authenticated user.
   * 
   * @return {@link UserDto} containing the users data
   */
  public UserDto getUser();

  /**
   * Fetch data about the currently authenticated user.
   * 
   * @return {@link UserDto} containing the users data
   */
  public UserDto getUser(String username);

  /**
   * Search on a project by name.
   * 
   * @param searchTerm
   *          search term
   * @return list of projects that correspond to the given search term
   */
  public List<RepositoryDto> searchRepositoriesByName(final String searchTerm);

  /**
   * Fetch data about a repository.
   * 
   * @param owner
   *          the name of the owner of the repository
   * @param name
   *          the name of the repository
   * @return {@link RepositoryDto} containing the data of the repository
   */
  public RepositoryDto getRepository(String owner, String name);

  /**
   * Fetch all branches of a repository.
   * 
   * @param repositoryOwner
   *          name of the owner of the repository
   * @param repositoryName
   *          name of the repository
   * @return all branches of the repository
   */
  public List<BranchDto> getBranches(String repositoryOwner, String repositoryName);

  /**
   * Fetch all pull requests of a repository.
   * 
   * @param repositoryOwner
   *          name of the owner of the repository
   * @param repositoryName
   *          name of the repository
   * @return all pull requests of the repository
   */
  public List<PullRequestDto> getPullRequests(final String repositoryOwner, final String repositoryName);

  /**
   * Fetch a pull request.
   * 
   * @param repositoryOwner
   *          name of the owner of the repository
   * @param repositoryName
   *          name of the repository
   * @param pullRequestNumber
   *          number of the pull request
   * @return pull request with the given number
   */
  public PullRequestDto getPullRequest(final String repositoryOwner, final String repositoryName, int pullRequestNumber);

}
