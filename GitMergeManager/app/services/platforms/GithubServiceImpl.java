package services.platforms;

import infrastructure.platforms.GithubApiAdapter;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import model.dto.BranchDto;
import model.dto.BranchReferenceDto;
import model.dto.LabelDto;
import model.dto.PullRequestDto;
import model.dto.RepositoryDto;
import model.dto.UserDto;

import com.fasterxml.jackson.databind.JsonNode;
import com.google.inject.Inject;

/**
 * Implementation of {@link GithubService}.
 */
public class GithubServiceImpl implements GithubService {

  @Inject
  private GithubApiAdapter githubApiAdapter;

  @Override
  public UserDto getUser() {
    return getUser(githubApiAdapter.getUser());
  }

  @Override
  public UserDto getUser(final String username) {
    return getUser(githubApiAdapter.getUser(username));
  }

  private UserDto getUser(final JsonNode jsonUser) {
    final UserDto user = new UserDto();
    user.id = jsonUser.findValue("id").asLong();
    user.username = jsonUser.findValue("login").asText();
    user.url = jsonUser.findValue("html_url").asText();
    user.avatarUrl = jsonUser.findValue("avatar_url").asText();
    return user;
  }

  @Override
  public List<RepositoryDto> searchRepositoriesByName(final String searchTerm) {
    final List<RepositoryDto> repositories = new ArrayList<RepositoryDto>();
    final JsonNode searchResults = githubApiAdapter.searchRepositoriesByName(searchTerm);
    final Iterator<JsonNode> searchResultsIt = searchResults.findValue("items").elements();
    while (searchResultsIt.hasNext()) {
      final RepositoryDto repository = new RepositoryDto();
      final JsonNode jsonRepository = searchResultsIt.next();
      repository.id = jsonRepository.findValue("id").asLong();
      repository.name = jsonRepository.findValue("name").asText();
      repository.owner = jsonRepository.findValue("owner").findValue("login").asText();
      repositories.add(repository);
    }
    return repositories;
  }

  @Override
  public RepositoryDto getRepository(final String repositoryOwner, final String repositoryName) {
    final JsonNode repositoryData = githubApiAdapter.getRepository(repositoryOwner, repositoryName);
    final RepositoryDto repository = new RepositoryDto();
    repository.id = repositoryData.findValue("id").asLong();
    repository.name = repositoryData.findValue("name").asText();
    repository.owner = repositoryData.findValue("owner").findValue("login").asText();
    repository.cloneUrl = repositoryData.findValue("clone_url").asText();
    return repository;

  }

  @Override
  public List<BranchDto> getBranches(final String repositoryOwner, final String repositoryName) {
    final List<BranchDto> branches = new ArrayList<BranchDto>();
    final JsonNode jsonBranches = githubApiAdapter.getBranches(repositoryOwner, repositoryName);
    final Iterator<JsonNode> jsonBranchesIt = jsonBranches.elements();
    while (jsonBranchesIt.hasNext()) {
      final BranchDto branch = new BranchDto();
      final JsonNode jsonBranch = jsonBranchesIt.next();
      branch.name = jsonBranch.findValue("name").asText();
      branch.latestCommitSha = jsonBranch.findValue("sha").asText();
      branches.add(branch);
    }
    return branches;
  }

  @Override
  public List<PullRequestDto> getPullRequests(final String repositoryOwner, final String repositoryName) {
    final List<PullRequestDto> pullRequests = new ArrayList<PullRequestDto>();
    final JsonNode jsonPullRequests = githubApiAdapter.getPullRequests(repositoryOwner, repositoryName);
    final Iterator<JsonNode> jsonPullRequestsIt = jsonPullRequests.elements();
    while (jsonPullRequestsIt.hasNext()) {
      final JsonNode jsonPullRequest = jsonPullRequestsIt.next();
      final PullRequestDto pullRequest = getPullRequest(repositoryOwner, repositoryName, jsonPullRequest.findValue("number").asInt());
      if (pullRequest != null) {
        pullRequests.add(pullRequest);
      }
    }

    return pullRequests;
  }

  @Override
  public PullRequestDto getPullRequest(final String repositoryOwner, final String repositoryName, final int pullRequestNumber) {
    final JsonNode jsonPullRequest = githubApiAdapter.getPullRequest(repositoryOwner, repositoryName, pullRequestNumber);
    final PullRequestDto pullRequest = new PullRequestDto();
    pullRequest.id = jsonPullRequest.findValue("id").asLong();
    pullRequest.title = jsonPullRequest.findValue("title").asText();
    pullRequest.url = jsonPullRequest.findValue("html_url").asText();
    pullRequest.description = jsonPullRequest.findValue("body").asText();
    pullRequest.number = jsonPullRequest.findValue("number").asText();
    pullRequest.isMergeable = jsonPullRequest.findValue("mergeable").asBoolean();
    pullRequest.base = jsonPullRequest.findValue("base").findValue("ref").asText();
    final JsonNode jsonHeadBranch = jsonPullRequest.findValue("head");
    final JsonNode jsonHeadRepo = jsonHeadBranch.findValue("repo");
    if (jsonHeadRepo != null && !jsonHeadRepo.isNull()) {
      final BranchReferenceDto branchReferenceDto = new BranchReferenceDto();
      branchReferenceDto.repositoryOwnerUsername = jsonHeadRepo.findValue("owner").findValue("login").asText();
      branchReferenceDto.repositoryName = jsonHeadRepo.findValue("name").asText();
      branchReferenceDto.branchName = jsonHeadBranch.findValue("ref").asText();
      branchReferenceDto.cloneUrl = jsonHeadBranch.findValue("clone_url").asText();
      pullRequest.head = branchReferenceDto;
    }
    else {
      pullRequest.head = BranchReferenceDto.unknown();
    }
    final boolean hasIssues = jsonPullRequest.findValue("base").findValue("repo").findValue("has_issues").asBoolean();
    if (hasIssues) {
      final JsonNode labelsNode = githubApiAdapter.getIssue(repositoryOwner, repositoryName, pullRequest.number).findValue("labels");
      final Iterator<JsonNode> labelsIt = labelsNode.elements();
      while (labelsIt.hasNext()) {
        pullRequest.labels.add(createLabelDto(labelsIt.next()));
      }
      final String referencedIssueNumber = ReferencedIssueParser.create().parse(pullRequest.description, repositoryOwner, repositoryName);
      if (referencedIssueNumber != null) {
        pullRequest.referencedIssueNumber = referencedIssueNumber;
        final JsonNode referencedIssueLabelsNode = githubApiAdapter.getIssue(repositoryOwner, repositoryName, referencedIssueNumber).findValue("labels");
        final Iterator<JsonNode> referencedIssueIabelsIt = referencedIssueLabelsNode.elements();
        while (referencedIssueIabelsIt.hasNext()) {
          pullRequest.referencedIssueLabels.add(createLabelDto(referencedIssueIabelsIt.next()));
        }
      }
    }
    return pullRequest;
  }

  private LabelDto createLabelDto(final JsonNode labelNode) {
    final LabelDto labelDto = new LabelDto();
    labelDto.name = labelNode.findValue("name").asText();
    labelDto.url = labelNode.findValue("url").asText();
    labelDto.color = labelNode.findValue("color").asText();
    return labelDto;
  }
}
