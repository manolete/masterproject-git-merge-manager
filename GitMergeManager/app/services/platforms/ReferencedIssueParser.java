package services.platforms;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import model.core.PullRequest;

/**
 * The description of a {@link PullRequest} can contain a reference to an issue that is resolved. This can be done using one of the following
 * notations:
 * 
 * - close, closes, closed, fix, fixes, fixed, resolve, resolves, resolved
 * 
 * followed by the issue number prefixed with a hash tag or the http-URL to the issue (https://github.com/[owner]/[repo]/issues/[issueNr]).
 * 
 * This class provides a method to parse the description to find the referenced issues.
 */
public class ReferencedIssueParser {

  private static final String KEYWORDS = "(((C|c)lose(s|d)?)|((F|f)ix(e(s|d))?)|((R|r)esolve(s|d)?))";
  private static final String SPACE = " ";
  private static final String SLASH = "/";
  private static final String HASH = "#";
  private static final String ISSUE_NR = "issueNr";
  private static final String ISSUE = "(?<" + ISSUE_NR + ">\\d+)";

  /**
   * Searches and returns the first referenced issue that is found in the given description.
   * 
   * @param description
   *          to be parsed
   * @param repoOwner
   *          owner of the repository
   * @param repoName
   *          name of the repository
   * @return referenced issue or null, if no issue is found
   */
  public String parse(final String description, final String repoOwner, final String repoName) {
    if (description != null) {
      final Pattern pattern = Pattern.compile(regex(repoOwner, repoName));
      final Matcher matcher = pattern.matcher(description);
      if (matcher.find()) {
        return matcher.group(ISSUE_NR);
      }
    }
    return null;
  }

  private String regex(final String repoOwner, final String repoName) {
    return KEYWORDS + SPACE + prefix(repoOwner, repoName) + ISSUE;
  }

  private String prefix(final String repoOwner, final String repoName) {
    if (repoOwner != null && repoName != null) {
      return "(" + HASH + "|" + url(repoOwner, repoName) + ")";
    }
    return HASH;
  }

  private String url(final String repoOwner, final String repoName) {
    return "https://(www.)?github.com" + SLASH + repoOwner + SLASH + repoName + SLASH;
  }

  public static ReferencedIssueParser create() {
    return new ReferencedIssueParser();
  }
}
