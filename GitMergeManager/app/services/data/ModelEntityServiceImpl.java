package services.data;

import java.util.List;

import services.platforms.GithubService;
import model.core.Branch;
import model.core.BranchReference;
import model.core.PullRequest;
import model.core.Repository;
import model.core.User;
import model.dto.BranchDto;
import model.dto.BranchReferenceDto;
import model.dto.LabelDto;
import model.dto.PullRequestDto;
import model.dto.RepositoryDto;
import model.dto.UserDto;
import model.factories.BranchReferenceFactory;
import model.factories.LabelFactory;
import model.factories.RepositoryFactory;
import model.factories.UserFactory;
import model.repositories.RepositoryRepository;
import model.repositories.UserRepository;

import com.google.inject.Inject;

/**
 * Implementation of {@link ModelEntityService}.
 */
public class ModelEntityServiceImpl implements ModelEntityService {

  @Inject
  private RepositoryRepository repositoryRepository;
  @Inject
  private UserRepository userRepository;

  @Inject
  private GithubService githubService;

  /**
   * {@inheritDoc}
   */
  @Override
  public User getUser(final String username) {
    return getUser(username, false);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public User getUpdatedUser(final String username) {
    return getUser(username, true);
  }

  private User getUser(final String username, final boolean update) {
    User user = userRepository.one(username);
    if (user == null) {
      final UserDto userDto = githubService.getUser(username);
      user = UserFactory.INSTANCE.create(userDto.id, userDto.username);
      user.setUrl(userDto.url);
      user.setAvatarUrl(userDto.avatarUrl);
      user.save();
    }
    else if (update) {
      final UserDto userDto = githubService.getUser(username);
      user.setUrl(userDto.url);
      user.setAvatarUrl(userDto.avatarUrl);
      user.save();
    }
    return user;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Repository getRepository(final String owner, final String name) {
    return getRepository(owner, name, false, false);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Repository getUpdatedRepository(final String owner, final String name) {
    return getRepository(owner, name, true, false);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Repository getRepositoryStub(final String owner, final String name) {
    return getRepository(owner, name, false, true);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Repository getUpdatedRepositoryStub(final String owner, final String name) {
    return getRepository(owner, name, true, true);
  }

  private Repository getRepository(final String owner, final String name, final boolean forceUpdate, final boolean stub) {
    Repository repository = getRepositoryFromDatabase(owner, name);

    // create or update repository stub
    if (repository == null) {
      repository = createRepositoryStub(owner, name);
    }
    else if (forceUpdate) {
      repository = updateRepositoryStub(repository);
    }

    // create or update branches and pull requests
    if (!stub) {
      if (repository.isEmpty()) {
        createBranches(repository, githubService.getBranches(owner, name));
        repository = repositoryRepository.one(repository.getId());
        createPullRequests(repository, githubService.getPullRequests(owner, name));
      }
      else if (forceUpdate) {
        updateBranches(repository);
        repository.refresh();
        updatePullRequests(repository);
      }
    }

    return repository;
  }

  private Repository createRepositoryStub(final String owner, final String name) {
    final RepositoryDto repositoryDto = getRepositoryFromGithub(owner, name);
    final Repository repository = RepositoryFactory.INSTANCE.create(repositoryDto.id, getUser(repositoryDto.owner), repositoryDto.name, repositoryDto.cloneUrl);
    repositoryRepository.store(repository);
    return repositoryRepository.one(repository.getId());
  }

  private Repository updateRepositoryStub(final Repository repository) {
    final RepositoryDto repositoryDto = getRepositoryFromGithub(repository.getOwner().getUsername(), repository.getName());
    repository.getOwner().setUsername(repositoryDto.owner);
    repository.getOwner().save();
    return repository;
  }

  private void createBranches(final Repository repository, final List<BranchDto> branchDtos) {
    for (final BranchDto branchDto : branchDtos) {
      final Branch branch = repository.addBranch(branchDto.name);
      branch.setLatestCommitSha(branchDto.latestCommitSha);
      branch.save();
    }
  }

  private void updateBranches(final Repository repository) {
    final List<BranchDto> branchDtos = githubService.getBranches(repository.getOwner().getUsername(), repository.getName());
    for (final Branch branch : repository.getBranches()) {
      final BranchDto branchDto = getBranchDtoByName(branchDtos, branch.getName());
      if (branchDto == null) {
        // delete existing branch
        repository.removeBranch(branch);

      }
      else {
        // update existing branch
        branch.setLatestCommitSha(branchDto.latestCommitSha);
        branchDtos.remove(branchDto);
      }
    }
    // create new branches
    createBranches(repository, branchDtos);
  }

  private BranchDto getBranchDtoByName(final List<BranchDto> branchDtos, final String name) {
    for (final BranchDto branchDto : branchDtos) {
      if (branchDto.name.equals(name)) {
        return branchDto;
      }
    }
    return null;
  }

  private void createPullRequests(final Repository repository, final List<PullRequestDto> pullRequestDtos) {
    for (final PullRequestDto pullRequestDto : pullRequestDtos) {
      final BranchReferenceDto headDto = pullRequestDto.head;
      final BranchReference head = BranchReferenceFactory.INSTANCE.create(headDto.repositoryOwnerUsername, headDto.repositoryName, headDto.branchName, headDto.cloneUrl);
      final PullRequest pullRequest = repository.getBranch(pullRequestDto.base).addPullRequest(pullRequestDto.id, pullRequestDto.title, pullRequestDto.description, head, pullRequestDto.number);
      pullRequest.setUrl(pullRequestDto.url);
      for (final LabelDto labelDto : pullRequestDto.labels) {
        pullRequest.addLabel(LabelFactory.INSTANCE.create(labelDto.name, labelDto.url, labelDto.color));
      }
      if (pullRequestDto.referencedIssueNumber != null) {
        pullRequest.setReferencedIssueNumber(pullRequestDto.referencedIssueNumber);
        for (final LabelDto labelDto : pullRequestDto.referencedIssueLabels) {
          pullRequest.addReferencedIssueLabel(LabelFactory.INSTANCE.create(labelDto.name, labelDto.url, labelDto.color));
        }
      }
      pullRequest.save();
    }
  }

  private void updatePullRequests(final Repository repository) {
    final List<PullRequestDto> pullRequestDtos = githubService.getPullRequests(repository.getOwner().getUsername(), repository.getName());

    for (final Branch branch : repository.getBranches()) {
      for (final PullRequest pullRequest : branch.getPullRequests()) {

        final PullRequestDto pullRequestDto = getPullRequestDtoById(pullRequestDtos, pullRequest.getId());
        if (pullRequestDto == null) {
          // delete existing pull request
          branch.removePullRequest(pullRequest);
        }
        else {
          // update existing pull request
          pullRequest.setIsMergeable(pullRequestDto.isMergeable);
          pullRequest.setTitle(pullRequestDto.title);
          pullRequest.setUrl(pullRequestDto.url);
          pullRequest.setDescription(pullRequestDto.description);
          pullRequestDtos.remove(pullRequestDto);
          pullRequest.clearLabels();
          for (final LabelDto labelDto : pullRequestDto.labels) {
            pullRequest.addLabel(LabelFactory.INSTANCE.create(labelDto.name, labelDto.url, labelDto.color));
          }
          if (pullRequestDto.referencedIssueNumber != null) {
            pullRequest.setReferencedIssueNumber(pullRequestDto.referencedIssueNumber);
            for (final LabelDto labelDto : pullRequestDto.referencedIssueLabels) {
              pullRequest.addReferencedIssueLabel(LabelFactory.INSTANCE.create(labelDto.name, labelDto.url, labelDto.color));
            }
          }
        }
        pullRequest.save();
      }

    }
    // create new branches
    createPullRequests(repository, pullRequestDtos);
  }

  private PullRequestDto getPullRequestDtoById(final List<PullRequestDto> pullRequestDtos, final Long id) {
    for (final PullRequestDto pullRequestDto : pullRequestDtos) {
      if (pullRequestDto.id.equals(id)) {
        return pullRequestDto;
      }
    }
    return null;
  }

  private Repository getRepositoryFromDatabase(final String owner, final String name) {
    return repositoryRepository.one(owner, name);
  }

  private RepositoryDto getRepositoryFromGithub(final String owner, final String name) {
    return githubService.getRepository(owner, name);
  }
}
