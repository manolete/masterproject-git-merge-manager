package services.data;

import model.core.Repository;
import model.core.User;

/**
 * Provides access to model aggregates ({@link User} and {@link Repository}).
 */
public interface ModelEntityService {

  /**
   * Get a {@link User} without updating it from Github.
   * 
   * @param username
   *          name of the {@link User}
   * @return {@link User}
   */
  public User getUser(String username);

  /**
   * Get a {@link User} after it has been updated from Github.
   * 
   * @param username
   *          name of the {@link User}
   * @param forceUpdate
   * @return {@link User}
   */
  public User getUpdatedUser(String username);

  /**
   * Get a {@link Repository} without updating it from Github.
   * 
   * @param owner
   *          owner of the {@link Repository}
   * @param name
   *          name of the {@link Repository}
   * @return {@link Repository}
   */
  public Repository getRepository(String owner, String name);

  /**
   * Get a {@link Repository} after it has been updated from Github.
   * 
   * @param owner
   *          owner of the {@link Repository}
   * @param name
   *          name of the {@link Repository}
   * @return {@link Repository}
   */
  public Repository getUpdatedRepository(String owner, String name);

  /**
   * Get the stub of a {@link Repository} without updating it from Github. The
   * stub of a {@link Repository} is a {@link Repository} without its branches
   * and pull requests.
   * 
   * @param owner
   *          owner of the {@link Repository}
   * @param name
   *          name of the {@link Repository}
   * @return stub of the {@link Repository}
   */
  public Repository getRepositoryStub(final String owner, final String name);

  /**
   * Get the stub of a {@link Repository} after it has been updated from Github.
   * The stub of a {@link Repository} is a {@link Repository} without its
   * branches and pull requests.
   * 
   * @param owner
   *          owner of the {@link Repository}
   * @param name
   *          name of the {@link Repository}
   * @return stub of the {@link Repository}
   */
  public Repository getUpdatedRepositoryStub(final String owner, final String name);

}
