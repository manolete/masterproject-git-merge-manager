package services.git;

import static util.StringUtil.EMPTY;
import static util.StringUtil.string;

import org.eclipse.jgit.lib.ProgressMonitor;

import model.core.MergeAnalysis;
import services.events.Event.EventType;
import services.events.EventService;

public class GitServiceProgressMonitor implements ProgressMonitor {

  private static final String END_SUB_TASK = "endSubTask";
  private static final String STATUS_UPDATE = "statusUpdate";
  private static final String START_SUB_TASK = "startSubTask";
  private static final String COMPLETE = "100";

  private final EventService eventService;
  private final MergeAnalysis mergeAnalysis;
  private String currentTask;
  private double updatedItems;
  private double lastMsg;
  private int totalItems;

  public GitServiceProgressMonitor(final EventService eventService, final MergeAnalysis mergeAnalysis) {
    this.eventService = eventService;
    this.mergeAnalysis = mergeAnalysis;
    this.currentTask = EMPTY;
    this.updatedItems = 0;
    this.lastMsg = 0;
    this.totalItems = 0;
  }

  @Override
  public boolean isCancelled() {
    return false;
  }

  @Override
  public void beginTask(final String currentTask, final int numberOfItems) {
    this.totalItems = numberOfItems;
    this.currentTask = currentTask;
    this.updatedItems = 0;
    this.lastMsg = 0;

    // Send Event
    sendEvent(string(totalItems), START_SUB_TASK);
  }

  @Override
  public void endTask() {
    // Send Event
    sendEvent(COMPLETE, END_SUB_TASK);
  }

  @Override
  public void update(final int x) {
    updatedItems += x;
    final double percentage = Math.floor(updatedItems / totalItems * 100);

    if (percentage > lastMsg) {
      lastMsg = percentage;
      sendEvent(string(percentage), STATUS_UPDATE);
    }
  }

  @Override
  public void start(final int x) {
    // do nothing
  }

  private void sendEvent(final String msg, final String type) {
    eventService.sendEvent(mergeAnalysis, msg, currentTask, EventType.buildEvent, type);
  }
}
