package services.git;

import java.io.File;

import model.core.MergeAnalysis;
import model.core.PullRequest;

/**
 * Provides methods to execute operations on local git repositories.
 */
public interface GitService {

  /**
   * Clones the base repository of a {@link MergeAnalysis} and checks out the
   * base branch.
   * 
   * @param mergeAnalysis
   *          {@link MergeAnalysis}
   * @return true if clone was successful, false otherwise
   */
  public boolean cloneBaseRepository(MergeAnalysis mergeAnalysis);

  /**
   * Clones the head branch of a {@link PullRequest} and merges it with the base
   * branch.
   * 
   * @param mergeAnalysis
   *          {@link MergeAnalysis}
   * @param pullRequest
   *          {@link PullRequest}
   *          @return true if merge was successful, false otherwise
   */
  public boolean mergePullRequest(MergeAnalysis mergeAnalysis, PullRequest pullRequest);

  /**
   * Removes all local repositories from this {@link MergeAnalysis}.
   * 
   * @param mergeAnalysis
   *          {@link MergeAnalysis}
   */
  public void cleanupRepositoryFolder(MergeAnalysis mergeAnalysis);

  /**
   * Reset working directory to the content of the base directory for this
   * {@link MergeAnalysis}.
   * 
   * @param mergeAnalysis
   *          {@link MergeAnalysis}
   */
  public void resetWorkingFolder(MergeAnalysis mergeAnalysis);

  /**
   * Returns {@link File} of the working directory for this
   * {@link MergeAnalysis}.
   * 
   * @param mergeAnalysis
   *          {@link MergeAnalysis}
   * @return {@link File} of the working directory for this
   *         {@link MergeAnalysis}
   */
  public File getWorkingDirectory(final MergeAnalysis mergeAnalysis);
}
