package services.git;

import static util.StringUtil.SLASH;

import java.io.File;

import global.ConfigLoader;
import infrastructure.filesystem.FileSystemOperator;
import infrastructure.git.GitCommandExecutor;
import infrastructure.git.GitMergeResult;
import model.core.MergeAnalysis;
import model.core.PullRequest;
import model.core.Repository;

import org.eclipse.jgit.lib.ProgressMonitor;

import play.Logger;
import services.events.EventService;

import com.google.inject.Inject;

public class GitServiceImpl implements GitService {

  @Inject
  private ConfigLoader conf;

  @Inject
  private EventService eventService;

  @Inject
  private GitCommandExecutor gitCommandExecutor;

  private static final String BASE = "base";
  private static final String WORKING = "working";

  @Override
  public boolean cloneBaseRepository(final MergeAnalysis mergeAnalysis) {
    if (mergeAnalysis.getBaseBranch() == null) {
      Logger.info("MergeAnalysis '" + mergeAnalysis.getName() + "' has no base branch.");
      return false;
    }

    final String localRootPath = getRootPath(mergeAnalysis);
    final Repository repository = mergeAnalysis.getRepository();

    final ProgressMonitor progressMonitor = new GitServiceProgressMonitor(eventService, mergeAnalysis);
    gitCommandExecutor.clone(repository.getCloneUrl(), localRootPath + SLASH + BASE, mergeAnalysis.getBaseBranch().getName(), progressMonitor);

    final String workingDirectory = localRootPath + SLASH + WORKING;
    FileSystemOperator.create().copy(localRootPath + SLASH + BASE, workingDirectory);

    return true;
  }

  @Override
  public boolean mergePullRequest(final MergeAnalysis mergeAnalysis, final PullRequest pullRequest) {
    final String localRootPath = getRootPath(mergeAnalysis);

    final ProgressMonitor progressMonitor = new GitServiceProgressMonitor(eventService, mergeAnalysis);
    final String workingDirectory = localRootPath + SLASH + WORKING;
    final String headDirectory = localRootPath + SLASH + pullRequest.getId();
    gitCommandExecutor.cloneSingleBranch(pullRequest.getHead().getCloneUrl(), headDirectory, pullRequest.getHead().getBranchName(), progressMonitor);
    final GitMergeResult gitMergeResult = gitCommandExecutor.merge(workingDirectory, pullRequest.getBase().getName(), headDirectory, pullRequest.getHead().getBranchName(), progressMonitor);

    return gitMergeResult.mergeable();
  }

  private String getRootPath(final MergeAnalysis mergeAnalysis) {
    final Repository repo = mergeAnalysis.getRepository();
    return conf.string("jgit.rootpath") + SLASH + repo.getOwner().getUsername() + SLASH + repo.getName() + SLASH + mergeAnalysis.getId();
  }

  @Override
  public void cleanupRepositoryFolder(final MergeAnalysis mergeAnalysis) {
    final String localRootPath = getRootPath(mergeAnalysis);
    FileSystemOperator.create().cleanAndDelete(new File(localRootPath));
  }

  @Override
  public void resetWorkingFolder(final MergeAnalysis mergeAnalysis) {
    final String localRootPath = getRootPath(mergeAnalysis);
    FileSystemOperator.create().copy(localRootPath + SLASH + BASE, localRootPath + SLASH + WORKING);
  }

  @Override
  public File getWorkingDirectory(final MergeAnalysis mergeAnalysis) {
    return new File(getRootPath(mergeAnalysis) + SLASH + WORKING);
  }
}
