package services.events;

import services.events.Event.EventType;
import model.core.MergeAnalysis;
import model.core.MergeAnalysisExecution;

public interface EventService {

  public void sendEvent(final MergeAnalysis mergeAnalysis, final String msg, final String eventInfo, final EventType event, final String type);

  public void sendEvent(final MergeAnalysisExecution mergeAnalysisExecution, final String msg, final String eventInfo, final EventType event, final String type);

  public void sendEvent(final String msg, final String repository, final String owner, final String eventInfo, final EventType event, final String type);

  public void sendAnalysisUpdateEvent(final MergeAnalysis mergeAnalysis);

  public void sendAnalysisFinishedEvent(final MergeAnalysis mergeAnalysis);
}
