package services.events

import play.api._
import play.api.mvc._
import play.api.libs.EventSource
import play.api.libs.json.Json
import play.api.libs.iteratee.Enumerator
import play.api.libs.iteratee.Concurrent
import scala.io.Source
import play.api.libs.json.JsValue
import play.api.libs.iteratee.Enumeratee
import play.api.libs.concurrent.Execution.Implicits._
import play.api.libs.EventSource.EventNameExtractor
import util.JsonUtil
import play.api.libs.json.JsValue
import scala.collection.mutable.ListBuffer

object EventBroadcaster {

  val (out, channel) = Concurrent.broadcast[JsValue]

  var jsonUtil = new JsonUtil();

  var alert = """
			{ 
			  "msg": "I am a dangerous alert",
			  "type": "success",
			  "repository": "junit",
			  "owner" : "junit-team",
              "event" : "alert"
		}
		""";

  def pushEvent(event: Event) = {

    if (event.event == Event.EventType.buildEvent.toString()) {
      if (EventStorage.buildEvents == null) {
        EventStorage.buildEvents = new java.util.Hashtable[String, scala.collection.mutable.ListBuffer[services.events.Event]]
      }

      var eventList = EventStorage.buildEvents.get(event.owner + event.repository);
      if (eventList == null) {
        eventList = scala.collection.mutable.ListBuffer.empty[Event];
        EventStorage.buildEvents.put(event.owner + event.repository, eventList);
      }
      eventList += event;
    }

    // Pushing
    var jsonString = jsonUtil.createJson(event).toString();
    var jsValue = Json.parse(jsonString);
    channel.push(jsValue);
  }
}