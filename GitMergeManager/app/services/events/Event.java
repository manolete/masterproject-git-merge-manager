package services.events;

/*  
 "msg": "I am beautiful notification",
 "type": "success",
 "repository": "junit",
 "owner" : "junit-team",
 "event" : "notification"
 */

public class Event {
  public String msg;
  public String repository;
  public String owner;
  public String eventInfo;
  public String event;
  public String type;

  public enum EventType {
    alert,
    notification,
    buildEvent,
    analysisUpdate,
    analysisFinished
  };
}