package services.events;

import play.Logger;
import services.events.Event.EventType;
import model.core.MergeAnalysis;
import model.core.MergeAnalysisExecution;
import model.core.Repository;

public class EventServiceImpl implements EventService {

  @Override
  public void sendEvent(final MergeAnalysisExecution mergeAnalysisExecution, final String msg, final String eventInfo, final EventType event, final String type) {
    sendEvent(mergeAnalysisExecution.getMergeAnalysis(), msg, eventInfo, event, type);
  }

  @Override
  public void sendEvent(final MergeAnalysis mergeAnalysis, final String msg, final String eventInfo, final EventType event, final String type) {
    sendEvent(mergeAnalysis.getRepository(), msg, eventInfo, event, type);
  }

  private void sendEvent(final Repository repository, final String msg, final String eventInfo, final EventType event, final String type) {
    sendEvent(msg, repository.getName(), repository.getOwner().getUsername(), eventInfo, event, type);
  }

  @Override
  public void sendEvent(final String msg, final String repository, final String owner, final String eventInfo, final EventType event, final String type) {
    EventBroadcaster.pushEvent(createEvent(msg, repository, owner, eventInfo, event, type));
    Logger.trace("SENT " + event + " EVENT for repo " + owner + "/" + repository + ": " + msg);
  }

  @Override
  public void sendAnalysisUpdateEvent(final MergeAnalysis mergeAnalysis) {
    final Repository repository = mergeAnalysis.getRepository();
    sendEvent("", repository.getName(), repository.getOwner().getUsername(), "", EventType.analysisUpdate, ""); // update the analysis dropdown
  }

  @Override
  public void sendAnalysisFinishedEvent(final MergeAnalysis mergeAnalysis) {
    final String message = mergeAnalysis.getId().toString();
    sendEvent(mergeAnalysis, message, "", EventType.analysisFinished, "endTask");
    sendAnalysisUpdateEvent(mergeAnalysis);
  }

  private Event createEvent(final String msg, final String repository, final String owner, final String eventInfo, final EventType event, final String type) {
    final Event e = new Event();
    e.msg = msg;
    e.repository = repository;
    e.owner = owner;
    e.eventInfo = eventInfo;
    e.event = event.toString();
    e.type = type;
    return e;
  }
}
