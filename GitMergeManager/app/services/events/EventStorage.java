package services.events;

import java.util.Hashtable;

import model.core.Repository;
import scala.collection.mutable.ListBuffer;

/**
 * Stores events that can be shown on the status page.
 */
public class EventStorage {

  public static Hashtable<String, ListBuffer<Event>> buildEvents = new Hashtable<String, ListBuffer<Event>>();

  public static void clean(final Repository repository) {
    final String key = repository.getOwner().getUsername() + repository.getName();
    if (buildEvents != null && buildEvents.containsKey(key)) {
      buildEvents.remove(key);
    }
  }

}
