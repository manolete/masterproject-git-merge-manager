package services.maven;

import infrastructure.maven.MavenPluginType;

import java.util.Map;

import services.maven.plugins.results.MavenPluginReport;
import services.maven.plugins.results.MavenPluginReportItem;
import services.maven.plugins.results.MavenTestingReport;

import com.google.common.collect.ImmutableMap;

/**
 * Wraps the result of all plugins that were executed during maven verify.
 */
public class MavenVerifyResult extends MavenCompilationResult {

  private final MavenTestingReport testingReport;

  private final Map<MavenPluginType, MavenPluginReport<? extends MavenPluginReportItem>> verifyPluginReports;

  private MavenVerifyResult(final boolean compilationOk, final MavenTestingReport testingReport, final Map<MavenPluginType, MavenPluginReport<? extends MavenPluginReportItem>> verifyPluginReports) {
    super(compilationOk);
    this.testingReport = testingReport;
    this.verifyPluginReports = verifyPluginReports;
  }

  public MavenTestingReport getTestingReport() {
    return testingReport;
  }

  public Map<MavenPluginType, MavenPluginReport<? extends MavenPluginReportItem>> getVerifyPluginReports() {
    return verifyPluginReports;
  }

  public MavenPluginReport<? extends MavenPluginReportItem> getVerifyPluginReport(final MavenPluginType pluginType) {
    return verifyPluginReports.get(pluginType);
  }

  public static MavenVerifyResult createCompilationOk(final MavenTestingReport testingReport, final Map<MavenPluginType, MavenPluginReport<? extends MavenPluginReportItem>> pluginReport) {
    return create(true, testingReport, pluginReport);
  }

  public static MavenVerifyResult createCompilationError() {
    final Map<MavenPluginType, MavenPluginReport<? extends MavenPluginReportItem>> pluginReports = ImmutableMap.of();
    return create(false, null, pluginReports);
  }

  private static MavenVerifyResult create(final boolean compilationOk, final MavenTestingReport testingReport,
      final Map<MavenPluginType, MavenPluginReport<? extends MavenPluginReportItem>> pluginReport) {
    return new MavenVerifyResult(compilationOk, testingReport, pluginReport);
  }
}
