package services.maven;

import infrastructure.maven.MavenCommandExecutor;

/**
 * Implementation of {@link MavenServiceFactory}.
 */
public class MavenServiceFactoryImpl implements MavenServiceFactory {

  /**
   * {@inheritDoc}
   */
  @Override
  public MavenService create(final MavenCommandExecutor mavenCommandExecutor) {
    return new MavenServiceImpl(mavenCommandExecutor);
  }

}