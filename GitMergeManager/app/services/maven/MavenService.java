package services.maven;

import infrastructure.maven.MavenProgressMonitor;

import java.io.File;

/**
 * This class provides methods to execute maven and return the result of all registered plugins.
 */
public interface MavenService {

  /**
   * Executes maven compile lifecycle phase and returns the result as {@link MavenCompilationResult}.
   * 
   * @param projectRoot
   *          root path of the project
   * @param mavenProgressMonitor
   *          {@link MavenProgressMonitor}
   * @return result of the compile lifecycle phase
   */
  public MavenCompilationResult executeMavenCompilation(final File projectRoot, MavenProgressMonitor mavenProgressMonitor);

  /**
   * Executes maven verify lifecycle phase and returns the results of the executed plugins wrapped in a {@link MavenVerifyResult}.
   * 
   * @param projectRoot
   *          root path of the project
   * @param mavenProgressMonitor
   *          {@link MavenProgressMonitor}
   * @return result of the verify lifecycle phase plugins wrapped in a {@link MavenVerifyResult}
   */
  public MavenVerifyResult executeMavenVerify(final File projectRoot, MavenProgressMonitor mavenProgressMonitor);

}
