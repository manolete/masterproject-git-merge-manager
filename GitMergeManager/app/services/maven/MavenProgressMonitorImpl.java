package services.maven;

import infrastructure.maven.MavenPluginType;
import infrastructure.maven.MavenProgressMonitor;

import java.util.Iterator;
import java.util.List;

import services.events.EventService;
import services.events.Event.EventType;

import com.google.common.collect.Lists;

public class MavenProgressMonitorImpl implements MavenProgressMonitor {

  private static final String INFO = "[INFO]";
  private static final String BUILDING = "Building";
  private static final String START = "Start";
  private static final String SPACE = " ";

  private static final String START_SUB_TASK = "startSubTask";

  private final EventService eventService;
  private final List<MavenPluginType> pluginTypes;
  private String repoOwner;
  private String repoName;

  private boolean compilationStarted;

  private MavenProgressMonitorImpl(final EventService eventService, final String repoOwner, final String repoName) {
    this.pluginTypes = Lists.newArrayList(MavenPluginType.values());
    this.eventService = eventService;
    this.repoOwner = repoOwner;
    this.repoName = repoName;
    this.compilationStarted = false;
  }

  @Override
  public void update(final String line) {
    sendCompilationEvent(line);
    sendPluginEvents(line);
  }

  private void sendCompilationEvent(final String line) {
    if (!compilationStarted && line.startsWith(INFO + SPACE + BUILDING)) {
      final String message = "compiling sources";
      eventService.sendEvent("", repoName, repoOwner, message, EventType.buildEvent, START_SUB_TASK);
      compilationStarted = true;
    }
  }

  private void sendPluginEvents(final String line) {
    if (line.startsWith(INFO)) {
      final Iterator<MavenPluginType> iterator = pluginTypes.iterator();
      while (iterator.hasNext()) {
        final MavenPluginType pluginType = iterator.next();
        if (line.contains(pluginType.getArtifactid())) {
          final String message = START + SPACE + pluginType.getDescription();
          eventService.sendEvent("", repoName, repoOwner, message, EventType.buildEvent, START_SUB_TASK);
          iterator.remove();
        }
      }
    }
  }

  public static MavenProgressMonitor create(final EventService eventService, final String repoOwner, final String repoName) {
    return new MavenProgressMonitorImpl(eventService, repoOwner, repoName);
  }

}
