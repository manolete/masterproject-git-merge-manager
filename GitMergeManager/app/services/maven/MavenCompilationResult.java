package services.maven;

public class MavenCompilationResult {

  private final boolean compilationOk;

  protected MavenCompilationResult(final boolean compilationOk) {
    this.compilationOk = compilationOk;
  }

  public boolean isCompilationOk() {
    return compilationOk;
  }

  public static MavenCompilationResult createCompilationOk() {
    return new MavenCompilationResult(true);
  }

  public static MavenCompilationResult createCompilationError() {
    return new MavenCompilationResult(false);
  }

}
