package services.maven;

import infrastructure.maven.MavenCommandExecutor;

/**
 * Factory for {@link MavenService}.
 */
public interface MavenServiceFactory {

  /**
   * Creates a {@link MavenService}.
   * 
   * @param mavenCommandExecutor
   *          {@link MavenCommandExecutor}
   * @return created {@link MavenService}
   */
  public MavenService create(MavenCommandExecutor mavenCommandExecutor);

}
