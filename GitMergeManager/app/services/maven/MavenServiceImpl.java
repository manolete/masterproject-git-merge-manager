package services.maven;

import infrastructure.maven.MavenCommandExecutionOutput;
import infrastructure.maven.MavenCommandExecutor;
import infrastructure.maven.MavenLifecyclePhase;
import infrastructure.maven.MavenPluginType;
import infrastructure.maven.MavenProgressMonitor;
import infrastructure.maven.pom.PomAdapter;
import infrastructure.maven.pom.PomAdapterFactory;
import infrastructure.maven.pom.PomPluginConfiguration;

import java.io.File;
import java.util.Map;

import services.maven.plugins.MavenPlugin;
import services.maven.plugins.MavenPluginRegistry;
import services.maven.plugins.MavenTestingPlugin;
import services.maven.plugins.results.MavenPluginReport;
import services.maven.plugins.results.MavenPluginReportItem;
import services.maven.plugins.results.MavenTestingReport;

import com.google.common.collect.Maps;

/**
 * Implementation of {@link MavenService}.
 */
public class MavenServiceImpl implements MavenService {

  private final MavenCommandExecutor mavenCommandExecutor;

  public MavenServiceImpl(final MavenCommandExecutor mavencommandExecutor) {
    this.mavenCommandExecutor = mavencommandExecutor;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public MavenCompilationResult executeMavenCompilation(final File projectRoot, final MavenProgressMonitor mavenProgressMonitor) {
    final MavenCommandExecutionOutput executionOutput = mavenCommandExecutor.execute(projectRoot, mavenProgressMonitor, MavenLifecyclePhase.COMPILE);
    return executionOutput.compilationOk() ? MavenCompilationResult.createCompilationOk() : MavenCompilationResult.createCompilationError();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public MavenVerifyResult executeMavenVerify(final File projectRoot, final MavenProgressMonitor mavenProgressMonitor) {
    final PomAdapter pomAdapter = PomAdapterFactory.INSTANCE.create(projectRoot);
    // TODO Before the execution, the POM file should maybe be adapted to also support project that have some special configurations that are not
    // supported in this service. (example: checkstyle-output as plain text instead of xml: The service can only parse XML documents)
    final MavenCommandExecutionOutput executionOutput = mavenCommandExecutor.execute(projectRoot, mavenProgressMonitor, MavenLifecyclePhase.VERIFY);
    final Map<MavenPluginType, MavenPluginReport<? extends MavenPluginReportItem>> pluginReports = Maps.newHashMap();

    // compilation
    if (!executionOutput.compilationOk()) {
      return MavenVerifyResult.createCompilationError();
    }

    // tests
    final MavenTestingPlugin testingPlugin = MavenPluginRegistry.INSTANCE.getTestingPlugin();
    final MavenTestingReport testingResult = testingPlugin.getReport(projectRoot);

    // verify plugins
    for (final MavenPlugin<? extends MavenPluginReport<? extends MavenPluginReportItem>> plugin : MavenPluginRegistry.INSTANCE.getVerifyPlugins()) {
      final PomPluginConfiguration pluginConfiguration = pomAdapter.getDefaultLifecyclePluginConfiguration(plugin.getType());
      if (pluginConfiguration != null) {
        pluginReports.put(plugin.getType(), plugin.getReport(projectRoot));
      }
    }

    return MavenVerifyResult.createCompilationOk(testingResult, pluginReports);
  }
}
