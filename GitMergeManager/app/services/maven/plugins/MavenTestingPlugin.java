package services.maven.plugins;

import java.io.File;

import services.maven.plugins.results.MavenTestingReport;

/**
 * Extension of {@link MavenPlugin} for plugins that execute tests.
 */
public interface MavenTestingPlugin extends MavenPlugin<MavenTestingReport> {

  /**
   * {@inheritDoc}
   */
  @Override
  public MavenTestingReport getReport(final File projectRoot);

}
