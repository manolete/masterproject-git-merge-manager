package services.maven.plugins;

import infrastructure.maven.MavenPluginType;

import java.io.File;

import services.maven.plugins.results.MavenPluginReport;
import services.maven.plugins.results.MavenPluginReportItem;

/**
 * A maven plugin that can be executed during a phase of the default lifecycle.
 * 
 * @param <R>
 *          report that is generated after the execution
 */
public interface MavenPlugin<R extends MavenPluginReport<? extends MavenPluginReportItem>> {

  /**
   * Returns the {@link MavenPluginType} of the current {@link MavenPlugin}.
   * 
   * @return {@link MavenPluginType} of the current {@link MavenPlugin}
   */
  public MavenPluginType getType();

  /**
   * Parses the report-files of a maven plugins that has been executed and returns the result as {@link MavenPluginReport}.
   * 
   * @param projectRoot
   *          the root path of the maven project
   * @return report of a maven plugin that has been executed
   */
  public R getReport(final File projectRoot);

}
