package services.maven.plugins.results;

import services.maven.plugins.MavenCheckstylePlugin;

import com.google.common.collect.ImmutableList;

/**
 * Implementation of {@link MavenPluginReport} for {@link MavenCheckstylePlugin}.
 */
public class MavenPmdReport extends AbstractMavenPluginReport<PmdViolation> implements MavenPluginReport<PmdViolation> {

  public MavenPmdReport(final ImmutableList<PmdViolation> violations) {
    super(violations);
  }

}
