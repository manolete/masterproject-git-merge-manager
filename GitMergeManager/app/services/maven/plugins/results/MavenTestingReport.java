package services.maven.plugins.results;

import services.maven.plugins.MavenTestingPlugin;

/**
 * Extension of {@link MavenPluginReport} for {@link MavenTestingPlugin}'s.
 */
public interface MavenTestingReport extends MavenPluginReport<TestCase> {

}
