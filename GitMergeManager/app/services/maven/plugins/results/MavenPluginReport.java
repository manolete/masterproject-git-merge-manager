package services.maven.plugins.results;

import java.util.List;

import com.google.common.collect.ImmutableList;

/**
 * The report of a maven plugin that has bee executed.
 * 
 * @param <I>
 *          {@link MavenPluginReportItem}
 */
public interface MavenPluginReport<I extends MavenPluginReportItem> {

  /**
   * Returns a {@link List} containing all {@link MavenPluginReportItem} of the plugin execution.
   * 
   * @return a {@link List} containing all {@link MavenPluginReportItem} of the plugin execution
   */
  public ImmutableList<I> getItems();

}
