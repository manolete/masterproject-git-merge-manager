package services.maven.plugins.results;

import com.google.common.collect.ImmutableList;

/**
 * Implementation of {@link MavenPluginReportItem} for {@link MavenPluginReport}.
 */
public class MavenSurefireReport extends AbstractMavenPluginReport<TestCase> implements MavenTestingReport {

  public MavenSurefireReport(final ImmutableList<TestCase> testCases) {
    super(testCases);
  }

}
