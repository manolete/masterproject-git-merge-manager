package services.maven.plugins.results;

/**
 * Implementation of {@link MavenPluginReportItem} for {@link MavenPluginReport}.
 */
public class PmdViolation implements MavenPluginReportItem {

  private final String file;
  private final int beginLine;
  private final int endLine;
  private final int beginColumn;
  private final int endColumn;
  private final String rule;
  private final String ruleSet;
  private final String packageName;
  private final String className;
  private final String methodName;
  private final String externalInfoUrl;
  private final int priority;
  private final String content;

  public PmdViolation(final String file, final int beginLine, final int endLine, final int beginColumn, final int endColumn, final String rule, final String ruleSet, final String packageName, final String className, final String methodName, final String externalInfoUrl, final int priority,
      final String content) {
    this.file = file;
    this.beginLine = beginLine;
    this.endLine = endLine;
    this.beginColumn = beginColumn;
    this.endColumn = endColumn;
    this.rule = rule;
    this.ruleSet = ruleSet;
    this.packageName = packageName;
    this.className = className;
    this.methodName = methodName;
    this.externalInfoUrl = externalInfoUrl;
    this.priority = priority;
    this.content = content;
  }

  public String getFile() {
    return file;
  }

  public int getBeginLine() {
    return beginLine;
  }

  public int getEndLine() {
    return endLine;
  }

  public int getBeginColumn() {
    return beginColumn;
  }

  public int getEndColumn() {
    return endColumn;
  }

  public String getRule() {
    return rule;
  }

  public String getRuleSet() {
    return ruleSet;
  }

  public String getPackageName() {
    return packageName;
  }

  public String getClassName() {
    return className;
  }

  public String getMethodName() {
    return methodName;
  }

  public String getExternalInfoUrl() {
    return externalInfoUrl;
  }

  public int getPriority() {
    return priority;
  }

  public String getContent() {
    return content;
  }

  @Override
  public String toString() {
    return file + ":" + beginLine + "," + beginColumn + ":" + endLine + "," + endColumn + ":" + rule + " (" + ruleSet + "), priority: " + priority + ", in" + packageName + "." + className + "." + methodName + ":" + content + " (" + externalInfoUrl + ")";
  }
}
