package services.maven.plugins.results;

/**
 * Implementation of {@link MavenPluginReportItem} for {@link MavenTestingReport}.
 */
public class TestCase implements MavenPluginReportItem {

  private final String className;
  private final String methodName;
  private final double time;
  private final TestCaseFailure failure;

  public TestCase(final String className, final String methodName, final double time, final TestCaseFailure failure) {
    this.time = time;
    this.className = className;
    this.methodName = methodName;
    this.failure = failure;
  }

  public String getClassName() {
    return className;
  }

  public String getMethodName() {
    return methodName;
  }

  public double getTime() {
    return time;
  }

  public boolean isSuccessful() {
    return failure == null;
  }

  public TestCaseFailure getFailure() {
    return failure;
  }

  @Override
  public String toString() {
    final String result = isSuccessful() ? "Success" : "failure: " + getFailure().toString();
    return getClassName() + "." + getMethodName() + ": time: " + getTime() + "s, result: " + result;
  }

  public static class TestCaseFailure {
    private final String type;
    private final String message;

    public TestCaseFailure(final String type, final String message) {
      this.type = type;
      this.message = message;
    }

    public String getType() {
      return type;
    }

    public String getMessage() {
      return message;
    }

    @Override
    public String toString() {
      return getType() + ": " + getMessage();
    }
  }

}
