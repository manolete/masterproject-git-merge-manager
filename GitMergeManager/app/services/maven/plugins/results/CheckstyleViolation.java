package services.maven.plugins.results;

/**
 * Implementation of {@link MavenPluginReportItem} for {@link MavenCheckstyleReport}.
 */
public class CheckstyleViolation implements MavenPluginReportItem {

  private final String filename;
  private final int line;
  private final int column;
  private final Severity severity;
  private final String message;
  private final String source;

  public CheckstyleViolation(final String filename, final int line, final int column, final Severity severity, final String message, final String source) {
    this.filename = filename;
    this.line = line;
    this.column = column;
    this.severity = severity;
    this.message = message;
    this.source = source;
  }

  public String getFilename() {
    return filename;
  }

  public int getLine() {
    return line;
  }

  public int getColumn() {
    return column;
  }

  public Severity getSeverity() {
    return severity;
  }

  public String getMessage() {
    return message;
  }

  public String getSource() {
    return source;
  }

  @Override
  public String toString() {
    return getFilename() + ":" + getLine() + ":" + getColumn() + ": " + getSeverity() + ": " + getMessage() + " (" + getSource() + ")";
  }

  public static enum Severity {
    ERROR("error"),
    IGNORE("ignore"),
    INFO("info"),
    WARNING("warning"),
    UNKNOWN("<unknown>");

    private String value;

    Severity(final String value) {
      this.value = value;
    }

    public String getValue() {
      return value;
    }

    public static Severity get(final String value) {
      for (final Severity severity : Severity.values()) {
        if (value.equals(severity.getValue())) {
          return severity;
        }
      }
      return Severity.UNKNOWN;
    }

    @Override
    public String toString() {
      return getValue();
    }
  }
}
