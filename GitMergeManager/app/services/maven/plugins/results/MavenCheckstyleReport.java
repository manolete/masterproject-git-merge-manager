package services.maven.plugins.results;

import services.maven.plugins.MavenCheckstylePlugin;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMultimap;
import com.google.common.collect.Multimap;

/**
 * Implementation of {@link MavenPluginReport} for {@link MavenCheckstylePlugin}.
 */
public class MavenCheckstyleReport extends AbstractMavenPluginReport<CheckstyleViolation> implements MavenPluginReport<CheckstyleViolation> {

  private Multimap<String, CheckstyleViolation> violationsPerFilenameCache;

  public MavenCheckstyleReport(final ImmutableList<CheckstyleViolation> violations) {
    super(violations);
  }

  /**
   * Returns the checkstyle violations per file.
   * 
   * @return checkstyle violations per file
   */
  public Multimap<String, CheckstyleViolation> getViolationsPerFilename() {
    if (violationsPerFilenameCache == null) {
      final Multimap<String, CheckstyleViolation> violoationsPerFilename = ArrayListMultimap.create();
      for (final CheckstyleViolation violation : getItems()) {
        violoationsPerFilename.put(violation.getFilename(), violation);
      }
      violationsPerFilenameCache = ImmutableMultimap.copyOf(violoationsPerFilename);
    }
    return violationsPerFilenameCache;
  }
}
