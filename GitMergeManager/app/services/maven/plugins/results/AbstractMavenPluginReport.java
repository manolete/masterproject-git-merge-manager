package services.maven.plugins.results;

import com.google.common.collect.ImmutableList;

/**
 * Abstract implementation of {@link MavenPluginReport}.
 * 
 * @param <I>
 *          {@link MavenPluginReportItem}
 */
public abstract class AbstractMavenPluginReport<I extends MavenPluginReportItem> implements MavenPluginReport<I> {

  private final ImmutableList<I> items;

  public AbstractMavenPluginReport(final ImmutableList<I> items) {
    this.items = items;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public final ImmutableList<I> getItems() {
    return items;
  }

}
