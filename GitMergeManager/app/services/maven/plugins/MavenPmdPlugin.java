package services.maven.plugins;

import infrastructure.maven.MavenPluginType;
import infrastructure.maven.pom.PomAdapter;

import java.io.File;
import java.util.List;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import services.maven.plugins.results.MavenPmdReport;
import services.maven.plugins.results.PmdViolation;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;

/**
 * Implementation of {@link MavenPlugin} for PMD execution.
 */
public class MavenPmdPlugin extends AbstractMavenPlugin<MavenPmdReport> implements MavenPlugin<MavenPmdReport> {

  private static final String TARGET_DIRECTORY = "targetDirectory";
  private static final String PMD_XML = "pmd.xml";
  private static final String FILE = "file";
  private static final String NAME = "name";
  private static final String VIOLATION = "violation";
  private static final String BEGINLINE = "beginline";
  private static final String ENDLINE = "endline";
  private static final String BEGINCOLUMN = "begincolumn";
  private static final String ENDCOLUMN = "endcolumn";
  private static final String RULE = "rule";
  private static final String RULESET = "ruleset";
  private static final String PACKAGE = "package";
  private static final String CLASS = "class";
  private static final String METHOD = "method";
  private static final String EXTERNAL_INFO_URL = "externalInfoUrl";
  private static final String PRIORITY = "priority";

  @Override
  public MavenPluginType getType() {
    return MavenPluginType.PMD;
  }

  @Override
  public MavenPmdReport getReport(final File projectRoot, final PomAdapter pomAdapter, final File absoluteResultPath) {
    final List<PmdViolation> pmdViolations = Lists.newArrayList();
    final Document xmlDocument = xmlReader().getDocument(new File(absoluteResultPath, PMD_XML));
    for (final Element fileElement : xmlReader().getElementsByTagName(xmlDocument, FILE)) {
      final String file = xmlReader().getAttributeValue(fileElement, NAME);
      for (final Element violationElement : xmlReader().getElementsByTagName(fileElement, VIOLATION)) {
        final int beginLine = Integer.parseInt(xmlReader().getAttributeValue(violationElement, BEGINLINE));
        final int endLine = Integer.parseInt(xmlReader().getAttributeValue(violationElement, ENDLINE));
        final int beginColumn = Integer.parseInt(xmlReader().getAttributeValue(violationElement, BEGINCOLUMN));
        final int endColumn = Integer.parseInt(xmlReader().getAttributeValue(violationElement, ENDCOLUMN));
        final String rule = xmlReader().getAttributeValue(violationElement, RULE);
        final String ruleSet = xmlReader().getAttributeValue(violationElement, RULESET);
        final String packageName = xmlReader().getAttributeValue(violationElement, PACKAGE);
        final String className = xmlReader().getAttributeValue(violationElement, CLASS);
        final String methodName = xmlReader().getAttributeValue(violationElement, METHOD);
        final String externalInfoUrl = xmlReader().getAttributeValue(violationElement, EXTERNAL_INFO_URL);
        final int priority = Integer.parseInt(xmlReader().getAttributeValue(violationElement, PRIORITY));
        final String content = violationElement.getTextContent();
        pmdViolations.add(new PmdViolation(file, beginLine, endLine, beginColumn, endColumn, rule, ruleSet, packageName, className, methodName, externalInfoUrl, priority, content));
      }
    }
    return new MavenPmdReport(ImmutableList.copyOf(pmdViolations));
  }

  /**
   * {@inheritDoc}
   * 
   * The default path to the PMD report-folder is ${project.build.directory}/checkstyle-result.xml.
   */
  @Override
  protected String getRelativeDefaultReportPath() {
    return null;
  }

  /**
   * {@inheritDoc}
   * 
   * The path to the PMD report-folder can be configured with '<targetDirectory>[path to report]</targetDirectory>'.
   */
  @Override
  protected String getConfigKeyForReportPath() {
    return TARGET_DIRECTORY;
  }
}
