package services.maven.plugins;

import infrastructure.maven.MavenPluginType;
import infrastructure.maven.pom.PomAdapter;

import java.io.File;
import java.util.List;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import services.maven.plugins.results.CheckstyleViolation;
import services.maven.plugins.results.CheckstyleViolation.Severity;
import services.maven.plugins.results.MavenCheckstyleReport;
import util.StringUtil;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;

/**
 * Implementation of {@link MavenPlugin} for checkstyle execution.
 */
public class MavenCheckstylePlugin extends AbstractMavenPlugin<MavenCheckstyleReport> implements MavenPlugin<MavenCheckstyleReport> {

  private static final String DEFAULT_REPORT_FILENAME = "checkstyle-result.xml";
  private static final String OUTPUT_FILE = "outputFile";
  private static final String FILE = "file";
  private static final String ERROR = "error";
  private static final String NAME = "name";
  private static final String LINE = "line";
  private static final String COLUMN = "column";
  private static final String SEVERITY = "severity";
  private static final String MESSAGE = "message";
  private static final String SOURCE = "source";

  /**
   * {@inheritDoc}
   */
  @Override
  public MavenPluginType getType() {
    return MavenPluginType.CHECKSTYLE;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected MavenCheckstyleReport getReport(final File projectRoot, final PomAdapter pomAdapter, final File absoluteResultPath) {
    final List<CheckstyleViolation> checkstyleViolations = Lists.newArrayList();
    final Document xmlDocument = xmlReader().getDocument(absoluteResultPath);
    for (final Element fileElement : xmlReader().getElementsByTagName(xmlDocument, FILE)) {
      final String filename = xmlReader().getAttributeValue(fileElement, NAME);
      for (final Element errorElement : xmlReader().getElementsByTagName(fileElement, ERROR)) {
        final String lineString = xmlReader().getAttributeValue(errorElement, LINE);
        int line = 0;
        if (StringUtil.isInt(lineString)) {
          line = Integer.parseInt(lineString);
        }
        int column = 0;
        final String columnString = xmlReader().getAttributeValue(errorElement, COLUMN);
        if (StringUtil.isInt(columnString)) {
          column = Integer.parseInt(columnString);
        }
        final Severity severity = Severity.get(xmlReader().getAttributeValue(errorElement, SEVERITY));
        final String message = xmlReader().getAttributeValue(errorElement, MESSAGE);
        final String source = xmlReader().getAttributeValue(errorElement, SOURCE);
        checkstyleViolations.add(new CheckstyleViolation(filename, line, column, severity, message, source));
      }
    }
    return new MavenCheckstyleReport(ImmutableList.copyOf(checkstyleViolations));
  }

  /**
   * {@inheritDoc}
   * 
   * The default path to the checkstyle report-file is ${project.build.directory}/checkstyle-result.xml.
   */
  @Override
  protected String getRelativeDefaultReportPath() {
    return DEFAULT_REPORT_FILENAME;
  }

  /**
   * {@inheritDoc}
   * 
   * The path to the checkstyle report-file can be configured with '<outputFile>[path to report]</outputFile>'.
   */
  @Override
  protected String getConfigKeyForReportPath() {
    return OUTPUT_FILE;
  }
}
