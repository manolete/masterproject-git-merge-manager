package services.maven.plugins;

import infrastructure.maven.pom.PomAdapter;
import infrastructure.maven.pom.PomAdapterFactory;
import infrastructure.maven.pom.PomPluginConfiguration;
import infrastructure.xml.XmlReader;

import java.io.File;

import services.maven.plugins.results.MavenPluginReport;
import services.maven.plugins.results.MavenPluginReportItem;

/**
 * Abstract implementation of {@link MavenPlugin}.
 * 
 * @param <R>
 *          report that is generated after the execution
 */
public abstract class AbstractMavenPlugin<R extends MavenPluginReport<? extends MavenPluginReportItem>> implements MavenPlugin<R> {

  protected static final String DEFAULT_OUTPUT_DIR = "target";

  private XmlReader xmlReader;

  protected final XmlReader xmlReader() {
    if (xmlReader == null) {
      xmlReader = XmlReader.create();
    }
    return xmlReader;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public final R getReport(final File projectRoot) {
    final PomAdapter pomAdapter = PomAdapterFactory.INSTANCE.create(projectRoot);
    final File absoluteResultPath = getAbsoluteReportPath(projectRoot, pomAdapter);
    return getReport(projectRoot, pomAdapter, absoluteResultPath);
  }

  /**
   * Parses the report-files of a maven plugins that has been executed and returns the result as {@link MavenPluginReport}.
   * 
   * @param projectRoot
   *          the root path of the maven project
   * @param pomAdapter
   *          {@link PomAdapter} of the maven project
   * @param absoluteResultPath
   *          the absolute path to the result (can be a file or a directory)
   * @return report of a maven plugin that has been executed
   */
  protected abstract R getReport(final File projectRoot, final PomAdapter pomAdapter, final File absoluteResultPath);

  /**
   * Provides the path to the report of the executed maven plugin. This can be a file or a directory. The subclass has to know itself what it is.
   * 
   * @param projectRoot
   *          root path of the maven project
   * @param pomAdapter
   *          {@link PomAdapter} of the maven project
   * @return path to the report of the executed maven plugin
   */
  private File getAbsoluteReportPath(final File projectRoot, final PomAdapter pomAdapter) {
    final String configuredPath = getConfiguration(pomAdapter, getConfigKeyForReportPath());
    return configuredPath != null ? pomAdapter.getAbsolutePath(configuredPath) : getAbsoluteDefaultReportPath(pomAdapter);
  }

  /**
   * Provides the default path to the report of the executed plugin.
   * 
   * @param pomAdapter
   *          {@link PomAdapter} of the maven project
   * @return the default path to the report of the executed plugin
   */
  protected final File getAbsoluteDefaultReportPath(final PomAdapter pomAdapter) {
    final File buildDir = pomAdapter.getAbsoluteBuildDir();
    final String subDir = getRelativeDefaultReportPath();
    return subDir != null ? new File(buildDir, subDir) : buildDir;
  }

  /**
   * Returns the relative default path of the plugin report within the build directory (which is target per default). If the default path is the build
   * directory, null is returned.
   * 
   * @return the relative default path of the plugin report within the build directory or null, if the default path is the build directory
   */
  protected abstract String getRelativeDefaultReportPath();

  /**
   * Specifies the config key that allows to explicitly specify the path to the plugin report (i.e. overwriting the default path).
   * 
   * @return the config key that allows to explicitly specify the path to the plugin report
   */
  protected abstract String getConfigKeyForReportPath();

  /**
   * Returns the {@link String} value of a configuration for a given key or null, if no configuration for the key exists.
   * 
   * @param pomAdapter
   *          {@link PomAdapter} of the project
   * @param key
   *          configuration key (i.e. name of the XML tag of the configuration)
   * @return the {@link String} value of a configuration for the given key or null, if no configuration for the key exists
   */
  protected String getConfiguration(final PomAdapter pomAdapter, final String key) {
    final PomPluginConfiguration pluginConfiguration = getPluginConfiguration(pomAdapter);
    return pluginConfiguration != null ? pluginConfiguration.getConfiguration(key) : null;
  }

  /**
   * Returns the {@link PomPluginConfiguration} of the current plugin or null, if no configuration for the current plugin exist.
   * 
   * @param pomAdapter
   *          {@link PomAdapter} of the maven project
   * @return the {@link PomPluginConfiguration} of the current plugin or null, if no configuration for the current plugin exist
   */
  protected PomPluginConfiguration getPluginConfiguration(final PomAdapter pomAdapter) {
    return pomAdapter.getPluginConfiguration(getType());
  }

}
