package services.maven.plugins;

import infrastructure.maven.MavenPluginType;
import infrastructure.maven.pom.PomAdapter;

import java.io.File;
import java.io.FilenameFilter;
import java.util.List;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import services.maven.plugins.results.MavenSurefireReport;
import services.maven.plugins.results.MavenTestingReport;
import services.maven.plugins.results.TestCase;
import services.maven.plugins.results.TestCase.TestCaseFailure;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;

/**
 * Implementation of {@link MavenPlugin} for test execution.
 */
public class MavenSurefirePlugin extends AbstractMavenPlugin<MavenTestingReport> implements MavenTestingPlugin {

  private static final String SUREFIRE_REPORTS = "surefire-reports";
  private static final String TESTSUITE = "testsuite";
  private static final String TESTCASE = "testcase";
  private static final String NAME = "name";
  private static final String CLASSNAME = "classname";
  private static final String TIME = "time";
  private static final String FAILURE = "failure";
  private static final String ERROR = "error";
  private static final String TYPE = "type";
  private static final String REPORTS_DIRECTORY = "reportsDirectory";

  /**
   * {@inheritDoc}
   */
  @Override
  public MavenPluginType getType() {
    return MavenPluginType.SUREFIRE;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected MavenSurefireReport getReport(final File projectRoot, final PomAdapter pomAdapter, final File absoluteResultPath) {
    final List<TestCase> testCases = Lists.newArrayList();
    for (final File xmlFile : getTestReportFiles(absoluteResultPath)) {
      for (final Element testcaseElement : getTestcaseElements(xmlFile)) {
        final String className = xmlReader().getAttributeValue(testcaseElement, CLASSNAME);
        final String methodName = xmlReader().getAttributeValue(testcaseElement, NAME);
        final double time = Double.parseDouble(xmlReader().getAttributeValue(testcaseElement, TIME));
        final TestCaseFailure failure = getTestCaseFailure(testcaseElement);
        testCases.add(new TestCase(className, methodName, time, failure));
      }
    }
    return new MavenSurefireReport(ImmutableList.copyOf(testCases));
  }

  /**
   * {@inheritDoc}
   * 
   * The default path of the surefire report-folder is ${project.build.directory}/surefire-reports. This folder contains an XML-file for every test
   * suite that has been executed.
   */
  @Override
  protected String getRelativeDefaultReportPath() {
    return SUREFIRE_REPORTS;
  }

  /**
   * {@inheritDoc}
   * 
   * The path to the surefire report-folder can be configured with '<reportsDirectory>[path to report]</reportsDirectory>'.
   */
  @Override
  protected String getConfigKeyForReportPath() {
    return REPORTS_DIRECTORY;
  }

  private File[] getTestReportFiles(final File resultPath) {
    return resultPath.exists() ? resultPath.listFiles(new XmlFileFilter()) : new File[0];
  }

  private List<Element> getTestcaseElements(final File xmlFile) {
    final Document document = xmlReader().getDocument(xmlFile);
    final Element testsuiteElement = xmlReader().getElementByTagName(document, TESTSUITE);
    return xmlReader().getElementsByTagName(testsuiteElement, TESTCASE);
  }

  private TestCaseFailure getTestCaseFailure(final Element testcaseElement) {
    TestCaseFailure testCaseFailure = createTestCaseFailure(xmlReader().getElementByTagName(testcaseElement, FAILURE));
    if (testCaseFailure == null) {
      testCaseFailure = createTestCaseFailure(xmlReader().getElementByTagName(testcaseElement, ERROR));
    }
    return testCaseFailure;
  }

  private TestCaseFailure createTestCaseFailure(final Element failureElement) {
    if (failureElement != null) {
      final String failureType = xmlReader().getAttributeValue(failureElement, TYPE);
      final String failureMessage = xmlReader().getTextContent(failureElement);
      return new TestCaseFailure(failureType, failureMessage);
    }
    return null;
  }

  class XmlFileFilter implements FilenameFilter {
    @Override
    public boolean accept(final File dir, final String name) {
      return name.endsWith(".xml");
    }

  }
}
