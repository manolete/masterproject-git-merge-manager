package services.maven.plugins;

import infrastructure.maven.MavenPluginType;

import java.util.List;

import services.maven.plugins.results.MavenPluginReport;
import services.maven.plugins.results.MavenPluginReportItem;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;

/**
 * Provides methods to register {@link MavenPlugin}'s.
 */
public class MavenPluginRegistry {

  public static MavenPluginRegistry INSTANCE = new MavenPluginRegistry();

  private MavenTestingPlugin testingPlugin;
  private final List<MavenPlugin<? extends MavenPluginReport<? extends MavenPluginReportItem>>> verifyPlugins;

  private MavenPluginRegistry() {
    verifyPlugins = Lists.newArrayList();
    registerTestingPlugin();
    registerPlugins();
  }

  private final void registerTestingPlugin() {
    this.testingPlugin = new MavenSurefirePlugin();
  }

  private final void registerPlugins() {
    verifyPlugins.add(new MavenSurefirePlugin());
    verifyPlugins.add(new MavenCheckstylePlugin());
    verifyPlugins.add(new MavenPmdPlugin());
  }

  public MavenTestingPlugin getTestingPlugin() {
    return testingPlugin;
  }

  public List<MavenPlugin<? extends MavenPluginReport<? extends MavenPluginReportItem>>> getVerifyPlugins() {
    return ImmutableList.copyOf(verifyPlugins);
  }

  public List<MavenPlugin<? extends MavenPluginReport<? extends MavenPluginReportItem>>> getAll() {
    final List<MavenPlugin<? extends MavenPluginReport<? extends MavenPluginReportItem>>> allPlugins = Lists.newArrayList(verifyPlugins);
    allPlugins.add(testingPlugin);
    return ImmutableList.copyOf(allPlugins);
  }

  public MavenPlugin<? extends MavenPluginReport<? extends MavenPluginReportItem>> get(final MavenPluginType pluginType) {
    for (final MavenPlugin<? extends MavenPluginReport<? extends MavenPluginReportItem>> plugin : verifyPlugins) {
      if (plugin.getType().equals(pluginType)) {
        return plugin;
      }
    }
    return null;
  }
}
