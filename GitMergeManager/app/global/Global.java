package global;

import play.Application;
import play.GlobalSettings;
import play.Logger.ALogger;

import com.google.inject.Guice;
import com.google.inject.Injector;

/**
 * This class is instantiated by the framework when an application starts. It allows to perform specific tasks at start-up and shut-down.
 */
public class Global extends GlobalSettings {

  private Injector injector;

  /**
   * {@inheritDoc}
   */
  @Override
  public void onStart(final Application app) {
    this.injector = createInjector();
    super.onStart(app);
    displayLogLevels();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public <A> A getControllerInstance(final Class<A> controllerClass) throws Exception {
    return getInjector().getInstance(controllerClass);
  }

  private final Injector getInjector() {
    if (this.injector == null) {
      this.injector = createInjector();
    }
    return injector;
  }

  private final Injector createInjector() {
    return Guice.createInjector(new ProductionModule());
  }

  private void displayLogLevels() {
    displayLogLevel(Loggers.FS);
  }

  private void displayLogLevel(final ALogger logger) {
    logger.info("log-level=" + Loggers.getLevel(logger));
  }
}
