package global;

import play.mvc.Http;
import play.mvc.Http.Session;

import util.ApplicationConstants;

/**
 * Provides some methods to access information about the current {@link Session}
 * .
 */
public class SessionHandler {

  public static Session current() {
    return Http.Context.current().session();
  }

  public static String authenticationToken() {
    return current().get(ApplicationConstants.SESSION_ACCESS_TOKEN_KEY);
  }

  public static String username() {
    return current().get(ApplicationConstants.SESSION_USERNAME_KEY);
  }

  public static boolean isAuthenticated() {
    return username() != null;
  }
}
