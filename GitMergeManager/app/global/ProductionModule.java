package global;

import infrastructure.async.tasks.AsyncTaskRegistry;
import infrastructure.async.tasks.AsyncTaskRegistryImpl;
import infrastructure.git.GitCommandExecutor;
import infrastructure.git.GitCommandExecutorImpl;
import infrastructure.platforms.GithubApiAdapter;
import infrastructure.platforms.GithubApiAdapterImpl;
import model.dto.factory.DtoFactory;
import model.dto.factory.DtoFactoryImpl;
import model.dto.factory.ResultDtoFactory;
import model.dto.factory.ResultDtoFactoryImpl;
import model.repositories.MergeAnalysisRepository;
import model.repositories.RepositoryRepository;
import model.repositories.UserRepository;
import model.repositories.impl.MergeAnalysisRepositoryImpl;
import model.repositories.impl.RepositoryRepositoryImpl;
import model.repositories.impl.UserRepositoryImpl;
import services.analysis.ExecutionResultService;
import services.analysis.ExecutionResultServiceImpl;
import services.analysis.ExecutionResultTranslator;
import services.analysis.ExecutionResultTranslatorImpl;
import services.analysis.MergeAnalysisExecutionService;
import services.analysis.MergeAnalysisExecutionServiceImpl;
import services.analysis.MergeAnalysisService;
import services.analysis.MergeAnalysisServiceImpl;
import services.data.ModelEntityService;
import services.data.ModelEntityServiceImpl;
import services.events.EventService;
import services.events.EventServiceImpl;
import services.git.GitService;
import services.git.GitServiceImpl;
import services.maven.MavenServiceFactory;
import services.maven.MavenServiceFactoryImpl;
import services.platforms.GithubService;
import services.platforms.GithubServiceImpl;

import com.google.inject.AbstractModule;
import com.google.inject.Module;
import com.google.inject.Singleton;

/**
 * {@link Module} for production.
 */
public class ProductionModule extends AbstractModule {

  /**
   * {@inheritDoc}
   */
  @Override
  protected void configure() {
    // application
    bind(ConfigLoader.class).to(ConfigLoaderImpl.class).in(Singleton.class);

    // services
    bind(GithubApiAdapter.class).to(GithubApiAdapterImpl.class).in(Singleton.class);
    bind(GitCommandExecutor.class).to(GitCommandExecutorImpl.class).in(Singleton.class);
    bind(GitService.class).to(GitServiceImpl.class).in(Singleton.class);
    bind(MergeAnalysisService.class).to(MergeAnalysisServiceImpl.class).in(Singleton.class);
    bind(MergeAnalysisExecutionService.class).to(MergeAnalysisExecutionServiceImpl.class).in(Singleton.class);
    bind(GithubService.class).to(GithubServiceImpl.class).in(Singleton.class);
    bind(EventService.class).to(EventServiceImpl.class).in(Singleton.class);
    bind(ExecutionResultTranslator.class).to(ExecutionResultTranslatorImpl.class).in(Singleton.class);
    bind(ExecutionResultService.class).to(ExecutionResultServiceImpl.class).in(Singleton.class);

    // repositories
    bind(RepositoryRepository.class).to(RepositoryRepositoryImpl.class).in(Singleton.class);
    bind(UserRepository.class).to(UserRepositoryImpl.class).in(Singleton.class);
    bind(MergeAnalysisRepository.class).to(MergeAnalysisRepositoryImpl.class).in(Singleton.class);

    // factories
    bind(DtoFactory.class).to(DtoFactoryImpl.class).in(Singleton.class);
    bind(ResultDtoFactory.class).to(ResultDtoFactoryImpl.class).in(Singleton.class);
    bind(ModelEntityService.class).to(ModelEntityServiceImpl.class).in(Singleton.class);
    bind(MavenServiceFactory.class).to(MavenServiceFactoryImpl.class).in(Singleton.class);

    // infrastructure
    bind(AsyncTaskRegistry.class).to(AsyncTaskRegistryImpl.class).in(Singleton.class);
  }
}
