package global;

import play.Logger;
import play.Logger.ALogger;

public class Loggers {

  /**
   * A logger for file system observation.
   */
  public static final ALogger FS = Logger.of("fs");

  public static Level getLevel(final ALogger logger) {
    return logger.isTraceEnabled() ? Level.TRACE : logger.isDebugEnabled() ? Level.DEBUG : logger.isInfoEnabled() ? Level.INFO : logger.isWarnEnabled() ? Level.WARN
        : logger.isErrorEnabled() ? Level.ERROR : Level.OFF;
  }

  public static enum Level {
    OFF("off"),
    ERROR("error"),
    WARN("warning"),
    INFO("info"),
    DEBUG("debug"),
    TRACE("trace");

    private final String textual;

    Level(final String textual) {
      this.textual = textual;
    }

    @Override
    public String toString() {
      return textual;
    }
  }
}
