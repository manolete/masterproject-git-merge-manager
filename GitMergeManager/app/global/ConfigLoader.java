package global;

/**
 * Provides just some 'shortcut' methods to load configuration parameters from
 * the config-file.
 */
public interface ConfigLoader {

  public static ConfigLoader DEFAULT = new ConfigLoaderImpl();

  public String string(final String key);

  public String string(final String key, final String defaultString);

}
