package global;

import play.Play;

/**
 * Implementation of {@link ConfigLoader}.
 */
public class ConfigLoaderImpl implements ConfigLoader {

  @Override
  public String string(final String key) {
    return Play.application().configuration().getString(key);
  }

  @Override
  public String string(final String key, final String defaultString) {
    return Play.application().configuration().getString(key, defaultString);
  }

}
