package util;

/**
 * Application constants.
 */
public interface ApplicationConstants {
  public static final String SESSION_USERNAME_KEY = "username";
  public static final String SESSION_ACCESS_TOKEN_KEY = "access_token";

  public static final String FLASH_REDIRECT_AFTER_LOGIN_KEY = "redirect_after_login";

  public static final String BASE_GITHUB_WEB_URL = "https://github.com";
  public static final String BASE_GITHUB_API_URL = "https://api.github.com";
  public static final String BASE_GITHUB_SSH_URL = "git@github.com:";
}
