package util;

import java.util.List;

public class StringUtil {

  public static final String EMPTY = "";
  public static final String SLASH = "/";
  public static final String BACKSLASH = "\\";

  public static String NEWLINE = System.getProperty("line.separator");

  public static String merge(final List<String> strings, final String separator) {
    String result = EMPTY;
    for (int i = 0; i < strings.size(); i++) {
      result += strings.get(i);
      if (i + 1 < strings.size()) {
        result += separator;
      }
    }
    return result;
  }

  public static String string(final int value) {
    return new Integer(value).toString();
  }

  public static String string(final double value) {
    return new Double(value).toString();
  }

  public static boolean isInt(final String string) {
    try {
      Integer.parseInt(string);
    }
    catch (final NumberFormatException e) {
      return false;
    }
    return true;
  }
}
