package util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

import com.fasterxml.jackson.databind.JsonNode;

public class DiffParser {

  static JsonUtil jsonUtil = new JsonUtil();

  private static String SEARCHTERM_SEPARATOR = "diff --git";
  private static String NEWFILE = "new file mode";

  private static String findFileString(final String diffs, final String filename) {
    final int position = diffs.indexOf(filename);
    final String tempFileString = diffs.substring(position, diffs.length());
    final int endPosition = tempFileString.indexOf(SEARCHTERM_SEPARATOR);
    String file = "";
    if (endPosition != -1) {
      file = tempFileString.substring(0, endPosition);
    }
    else {
      file = tempFileString.substring(0, tempFileString.length());
    }
    return file;
  }

  public static JsonNode parseDiff(final String diffs, final String filename) {
    return constructJsonObject(findFileString(diffs, filename));
  }

  public static JsonNode parseDiffLines(final ArrayList<String> filenames, final String input) {
    String fileString = "";
    final ArrayList<Object> objects = new ArrayList<>();
    for (int i = 0; i < filenames.size(); ++i) {

      final String filename = filenames.get(i);
      fileString = findFileString(input, filenames.get(i));

      final ArrayList<Object> diffLines = getDiffLines(fileString);
      for (final Object diffLine : diffLines) {
        @SuppressWarnings("unused")
        final Object node = new Object() {
          public final String file = filename;
          public final int line = (int) diffLine;
        };
        objects.add(node);
      }

    }

    return jsonUtil.createJson(objects);
  }

  private static JsonNode constructJsonObject(final String file) {
    final JsonNode returnObject = buildJsonNode(file);
    return returnObject;
  }

  private static ArrayList<Object> getDiffLines(final String input) {
    int cut = 4;
    if (input.contains(NEWFILE)) {
      cut = 5;
    }
    final ArrayList<Object> changes = new ArrayList<Object>();
    HashMap<String, String> lines;
    final Scanner scanner = new Scanner(input);
    String line = "";
    String decider = "";

    // Delete the first x information lines...
    for (int ii = 0; ii < cut; ++ii) {
      if (scanner.hasNextLine()) {
        scanner.nextLine();
      }
    }

    int originalLineNumber = 0;
    while (scanner.hasNextLine()) {
      lines = new HashMap<String, String>();
      line = scanner.nextLine();
      if (line.length() >= 1) {
        decider = line.substring(0, 1);

        if (decider.contains("+")) {
          lines.put("line", originalLineNumber + "");
        }
        else if (decider.contains("-")) {
          ++originalLineNumber;
          lines.put("line", originalLineNumber + "");
        }
        else if (decider.contains("@")) {
          final String jumper = line.substring(2, line.length() - 2);
          originalLineNumber = getOriginalLineNumber(jumper);
          --originalLineNumber;
        }
        else {
          ++originalLineNumber;
        }

        changes.add(originalLineNumber);
      }
    }
    scanner.close();
    return changes;
  }

  private static JsonNode buildJsonNode(final String input) {
    int cut = 4;
    if (input.contains(NEWFILE)) {
      cut = 5;
    }

    final HashMap<String, Object> object = new HashMap<String, Object>();
    HashMap<String, String> lines;
    final Scanner scanner = new Scanner(input);
    int id = 0;
    String line = "";
    String decider = "";
    int originalLineNumber = 1;
    int changedLineNumber = 1;
    // Delete the first x information lines...
    for (int ii = 0; ii < cut; ++ii) {
      if (scanner.hasNextLine()) {
        scanner.nextLine();
      }
    }
    while (scanner.hasNextLine()) {
      lines = new HashMap<String, String>();
      line = scanner.nextLine();
      if (line.length() >= 1) {
        decider = line.substring(0, 1);
        if (decider.contains("+")) {
          lines.put("id", calculateId(id));
          lines.put("originalLineNumber", " ");
          lines.put("changedLineNumber", changedLineNumber + "");
          lines.put("color", "diff-bg-color-add");
          lines.put("content", line);
          ++changedLineNumber;
        }
        else if (decider.contains("-")) {
          lines.put("id", calculateId(id));
          lines.put("originalLineNumber", originalLineNumber + "");
          lines.put("changedLineNumber", " ");
          lines.put("color", "diff-bg-color-remove");
          lines.put("content", line);
          ++originalLineNumber;
        }
        else if (decider.contains("@")) {
          lines.put("id", calculateId(id));
          final String jumper = line.substring(2, line.length() - 2);
          lines.put("originalLineNumber", " ");
          lines.put("changedLineNumber", " ");
          lines.put("color", "diff-bg-color-jumper");
          lines.put("content", line);
          originalLineNumber = getOriginalLineNumber(jumper);
          changedLineNumber = getChangeLineNumber(jumper);
        }
        else {
          lines.put("id", calculateId(id));
          lines.put("originalLineNumber", originalLineNumber + "");
          lines.put("changedLineNumber", changedLineNumber + "");
          lines.put("color", "diff-bg-color-neutral");
          lines.put("content", line);
          ++originalLineNumber;
          ++changedLineNumber;
        }
      }
      else {}
      object.put(id + "", lines);
      ++id;
    }
    scanner.close();
    return jsonUtil.createJson(object);
  }

  private static int getOriginalLineNumber(final String line) {
    if (line.contains("-1 +1")) {
      return 1;
    }
    else {
      final int start = line.indexOf("-");
      final int end = line.indexOf(",");
      final int number = Integer.parseInt(line.substring(start + 1, end));
      return number;
    }

  }

  private static int getChangeLineNumber(String line) {
    if (line.contains("-1 +1")) {
      return 1;
    }
    else {
      final int temp = line.indexOf(",");
      line = line.substring(temp + 1, line.length());
      final int start = line.indexOf("+");
      final int end = line.indexOf(",");
      final int number = Integer.parseInt(line.substring(start, end));
      return number;
    }

  }

  private static String calculateId(final int id) {
    final String finalId = Integer.toString(id);
    switch (finalId.length()) {
      case 1:
        return "A" + finalId;
      case 2:
        return "B" + finalId;
      case 3:
        return "C" + finalId;
      case 4:
        return "D" + finalId;
      case 5:
        return "E" + finalId;
      case 6:
        return "F" + finalId;
      default:
        return "Z" + finalId;
    }
  }
}
