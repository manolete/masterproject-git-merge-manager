package util;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

/**
 * Representation of an URL.
 */
public class URL {

  private static final String DELIMITER = "?";
  private static final String PARAMETER_DELIMITER = "&";
  private static final String EQUAL_SIGN = "=";
  private static final String EMPTY_STRING = "";

  private final String basicUrl;
  private final Map<String, String> parameters;

  public URL(final String url) {
    this.basicUrl = url;
    this.parameters = new HashMap<String, String>();
  }

  public void addParameter(final String name, final String value) {
    this.parameters.put(name, value);
  }

  public void addParameters(final Map<String, String> parameters) {
    this.parameters.putAll(parameters);
  }

  @Override
  public String toString() {
    return basicUrl + parametersAsString();
  }

  private String parametersAsString() {
    return parameters.size() > 0 ? DELIMITER + concatenateParameters() : EMPTY_STRING;
  }

  private String concatenateParameters() {
    String result = "";
    int counter = 1;
    for (final Entry<String, String> parameter : parameters.entrySet()) {
      result += parameter.getKey() + EQUAL_SIGN + parameter.getValue();
      result += counter < parameters.size() ? PARAMETER_DELIMITER : EMPTY_STRING;
      counter++;
    }
    return result;
  }
}
