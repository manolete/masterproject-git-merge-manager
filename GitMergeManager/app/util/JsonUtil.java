package util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Provides some methods to generate JSON.
 */
public class JsonUtil {

  private final ObjectMapper objectMapper;

  private static final String OPENING_CURLY_BRACKET = "{";
  private static final String CLOSING_CURLY_BRACKET = "}";
  private static final String DOUBLE_QUOTE = "\"";
  private static final String COLON = ":";
  private static final String SPACE = " ";

  private static final String MESSAGE = "message";

  public JsonUtil() {
    this.objectMapper = new ObjectMapper();
  }

  public JsonUtil(final ObjectMapper objectMapper) {
    this.objectMapper = objectMapper;
  }

  /**
   * Serialises an {@link Object} to a {@link JsonNode}.
   * 
   * @param o
   *          {@link Object} to be serialised
   * @return {@link JsonNode}
   */
  public JsonNode createJson(final Object o) {
    return objectMapper.valueToTree(o);
  }

  public String createJsonString(final Object o) {
    try {
      return objectMapper.writeValueAsString(o);
    }
    catch (final JsonProcessingException e) {
      return createJsonNodeAsString(MESSAGE, e.getMessage());
    }
  }

  /**
   * Serialises an Object to a formatted JSON-string.
   * 
   * @param o
   *          {@link Object} to be serialised
   * @return formatted JSON-string
   */
  public String createFormattedJsonString(final Object o) {
    try {
      return objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(o);
    }
    catch (final JsonProcessingException e) {
      return createJsonNodeAsString(MESSAGE, e.getMessage());
    }
  }

  /**
   * Creates a JSON node.
   * 
   * @param name
   *          name
   * @param value
   *          value
   * @return created JSON node
   */
  public String createJsonNodeAsString(final String name, final String value) {
    return OPENING_CURLY_BRACKET + SPACE + DOUBLE_QUOTE + name + DOUBLE_QUOTE + SPACE + COLON + SPACE + DOUBLE_QUOTE + value + DOUBLE_QUOTE + SPACE + CLOSING_CURLY_BRACKET;
  }

}
