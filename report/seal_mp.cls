% SEAL ARTICLE DOCUMENT CLASS -- version 3.0 (16-Sep-2011)
% Beat Fluri, Matthias Hert LaTeX2e support for Special Articles
%
%%
%% \CharacterTable
%%  {Upper-case    \A\B\C\D\E\F\G\H\I\J\K\L\M\N\O\P\Q\R\S\T\U\V\W\X\Y\Z
%%   Lower-case    \a\b\c\d\e\f\g\h\i\j\k\l\m\n\o\p\q\r\s\t\u\v\w\x\y\z
%%   Digits        \0\1\2\3\4\5\6\7\8\9
%%   Exclamation   \!     Double quote  \"     Hash (number) \#
%%   Dollar        \$     Percent       \%     Ampersand     \&
%%   Acute accent  \'     Left paren    \(     Right paren   \)
%%   Asterisk      \*     Plus          \+     Comma         \,
%%   Minus         \-     Point         \.     Solidus       \/
%%   Colon         \:     Semicolon     \;     Less than     \<
%%   Equals        \=     Greater than  \>     Question mark \?
%%   Commercial at \@     Left bracket  \[     Backslash     \\
%%   Right bracket \]     Circumflex    \^     Underscore    \_
%%   Grave accent  \`     Left brace    \{     Vertical bar  \|
%%   Right brace   \}     Tilde         \~}
%%
\NeedsTeXFormat{LaTeX2e}[1995/12/01]
\ProvidesClass{seal_mp}[2011/09/16 v3.0 ^^J LaTeX document class for seal style articles]
% jhof: changed seal_article to seal_mp in order to avoid LaTeX warning "you have requested document class.."

\LoadClass[10pt,a4paper,fleqn]{article}
\RequirePackage{seal}

%% paper dimensions and lengths
\setlength{\textwidth}{15cm}
\setlength{\textheight}{21.3cm}
\setlength{\topsep}{1.5em}
\setlength{\textfloatsep}{1.5em}
\setlength{\intextsep}{1.5em}

\setlength\oddsidemargin   {11\p@}
\setlength\evensidemargin  {11\p@}
\setlength\marginparwidth  {90\p@}

\setcounter{secnumdepth}{3} % sets the depth for numbering titles (section, subsection for 2)

%% header/footer
\def\ps@sealheadings{\let\@mkboth\@gobbletwo
   \def\@oddfoot{\hfil\fontfamily{phv}\selectfont\small\bfseries\thepage}
   \def\@evenfoot{\hfil\fontfamily{phv}\selectfont\small\bfseries\thepage}
   \let\@evenhead\@empty\let\@oddhead\@empty}
\ps@sealheadings

%% section redefinitions
\renewcommand\section{\@startsection{section}{1}{\z@}%
                       {-18\p@ \@plus -4\p@ \@minus -4\p@}%
                       {12\p@ \@plus 4\p@ \@minus 4\p@}%
                       {\phvbold\Large}}
\renewcommand\subsection{\@startsection{subsection}{2}{\z@}%
                       {-18\p@ \@plus -4\p@ \@minus -4\p@}%
                       {8\p@ \@plus 4\p@ \@minus 4\p@}%
                       {\phvbold\large}}
\renewcommand\subsubsection{\@startsection{subsubsection}{3}{\z@}%
                       {-18\p@ \@plus -4\p@ \@minus -4\p@}%
                       {6\p@ \@plus 4\p@ \@minus 4\p@}%
                       {\phvbold}}
\renewcommand\paragraph{\@startsection{paragraph}{4}{\z@}%
                       {-12\p@ \@plus -4\p@ \@minus -4\p@}%
                       {-0.5em \@plus -0.22em \@minus -0.1em}%
                       {\phvbold\small}}

%% command definitions for title pages
\def\thesisType#1{\gdef\@thesisType{#1}}
\def\subtitle#1{\gdef\@subtitle{#1}}
\def\authorOne#1{\gdef\@authorOne{#1}}
\def\authorTwo#1{\gdef\@authorTwo{#1}}
\def\authorThree#1{\gdef\@authorThree{#1}}
\def\authorFour#1{\gdef\@authorFour{#1}}
\def\legiOne#1{\gdef\@legiOne{#1}}
\def\legiTwo#1{\gdef\@legiTwo{#1}}
\def\legiThree#1{\gdef\@legiThree{#1}}
\def\legiFour#1{\gdef\@legiFour{#1}}
\def\prof#1{\gdef\@prof{#1}}
\def\assistent#1{\gdef\@assistent{#1}}

% make list for articles a little tighter
\def\@listI{\leftmargin\leftmargini
            \parsep 0\p@ \@plus1\p@ \@minus\p@
            \topsep 8\p@ \@plus2\p@ \@minus4\p@
            \itemsep2\p@}
\let\@listi\@listI
\@listi
\def\@listii {\leftmargin\leftmarginii
              \labelwidth\leftmarginii
              \advance\labelwidth-\labelsep
              \topsep    0\p@ \@plus2\p@ \@minus\p@}
\def\@listiii{\leftmargin\leftmarginiii
              \labelwidth\leftmarginiii
              \advance\labelwidth-\labelsep
              \topsep    0\p@ \@plus\p@\@minus\p@
              \parsep    \z@
              \partopsep \p@ \@plus\z@ \@minus\p@}

\newif\ifsubtitle
\subtitletrue
\newif\ifemail
\emailtrue
\newif\ifurl
\urltrue

\newlength{\normaltopmargin}
\setlength\normaltopmargin{\topmargin}

%% title page
\renewcommand\maketitle{%
  \newpage
  \thispagestyle{empty}
  \begingroup
  \fontfamily{phv}\selectfont
  \begin{flushright}%
    \vspace*{50\p@}
    {\fontsize{20}{0}\selectfont \@thesisType}
    \vskip -9\p@
    \vbox{\hrule height0.25pt width\textwidth}
    \vskip -2\p@
    {\fontsize{9}{0}\selectfont \@date}
    \vskip 20\p@
    {\fontfamily{phv}\fontsize{40}{0}\selectfont\bfseries \@title}
    \ifsubtitle
      \vskip 10\p@  
  \parbox{\textwidth}{
  \begin{flushright}
    \fontfamily{phv}\fontsize{20}{30}\selectfont \@subtitle
  \end{flushright}}
    \fi
    \vfill
    {\fontsize{14}{0}\selectfont\bfseries \@authorOne}
    \vskip 2\p@
    {\fontsize{10}{0}\selectfont \ (\@legiOne)}
    \vskip 2\p@
    {\fontsize{14}{0}\selectfont\bfseries \@authorTwo}
    \vskip 2\p@
    {\fontsize{10}{0}\selectfont \ (\@legiTwo)}
    \vskip 2\p@
    {\fontsize{14}{0}\selectfont\bfseries \@authorThree}
    \vskip 2\p@
    {\fontsize{10}{0}\selectfont \ (\@legiThree)}
    \vskip 2\p@
    {\fontsize{14}{0}\selectfont\bfseries \@authorFour}
    \vskip 2\p@
    {\fontsize{10}{0}\selectfont \ (\@legiFour)}
    \vskip 35\p@
    {\fontsize{10}{0}\selectfont\bfseries supervised by}
    \vskip 4\p@
    {\fontsize{11}{0}\selectfont \@prof\\[2\p@] \@assistent}
 \end{flushright}%
 \vskip 87\p@
 \parindent \z@
 \includegraphics[width=4.5cm]{logo_uzh}\hfill\includegraphics[width=3.5cm]{seal_bw}
 \endgroup
 \clearpage\thispagestyle{empty}\cleardoublepage\null
  \thispagestyle{empty}
}

\endinput

%end of seal_article.cls
